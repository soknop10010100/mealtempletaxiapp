package com.station29.mealtempledelivery.ui.taxi;

import static com.station29.mealtempledelivery.utils.Util.isConnectedToInternet;
import static com.station29.mealtempledelivery.utils.Util.logDebug;
import static com.station29.mealtempledelivery.utils.Util.popupMessage;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.Uri;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.ebanx.swipebtn.SwipeButton;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.PolyUtil;
import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.api.Constant;
import com.station29.mealtempledelivery.api.request.DeliveryWs;
import com.station29.mealtempledelivery.data.model.Delivery;
import com.station29.mealtempledelivery.data.model.TaxiDeliveryDetail;
import com.station29.mealtempledelivery.data.model.Waypoint;
import com.station29.mealtempledelivery.service.LocationUpdatesService;
import com.station29.mealtempledelivery.ui.MainActivity;
import com.station29.mealtempledelivery.ui.home.WaitingDialog;
import com.station29.mealtempledelivery.utils.Util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class TaxiCompleteActivity extends TaxiBaseActivity {

    private int deliveryId;
    private Runnable updater;
    private String typeOrder;
    private LocationUpdateReceiver locationUpdateReceiver;
    private WaitingDialog dialog;
    private Handler timerHandler;

    @Override
    protected void onStart() {
        super.onStart();
        locationUpdateReceiver = new LocationUpdateReceiver();
        deliveryId = getIntent().getIntExtra("deliveryId", -1);
        typeOrder = getIntent().getStringExtra("typeOrder");
    }

    @Override
    protected Drawable getSwipeButtonColor() {
        return getResources().getDrawable(R.drawable.on_swipe_shape_red);
    }

    @Override
    protected String getSwipeButtonText() {
        return getString(R.string.slide_to_complete);
    }

    @Override
    protected String visibleBtnCancel() {
        return "COMPLETED";
    }

    @Override
    protected void onSlideCompleted(SwipeButton swipeButton, boolean active) {
        String status = Constant.DELIVERED;
        updateStatusTask(deliveryId, typeOrder, status, swipeButton, 0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(locationUpdateReceiver, new IntentFilter(LocationUpdatesService.ACTION_BROADCAST));
        String start = Util.getString("tracking", this);
        if (!start.isEmpty() && start.equals("start")) {
            latLngList = Util.getTrackPoints(TaxiCompleteActivity.this);
        }
        if (typeOrder != null && typeOrder.equalsIgnoreCase("service")) {
            startTimerTask(); // Countdown time & distance only for Taxi
        }
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(locationUpdateReceiver);
        super.onPause();
    }


    private void startTimerTask() {
        timerHandler = new Handler();
        updater = () -> {
            updatePriceView(latLngList, taxiDeliveryDetail);
            timerHandler.postDelayed(updater, 60000);
        };
        timerHandler.post(updater);
    }

    private void stopTimer() {
        if (timerHandler != null && updater != null) {
            timerHandler.removeCallbacks(updater);
        }
    }

    private void updateStatusTask(final int deliveryId, String typeOrder, String mStatus, final SwipeButton button, int cooperation) {
        if (Constant.getBaseUrl() != null) {
            button.setVisibility(View.VISIBLE);
            dialog = new WaitingDialog(TaxiCompleteActivity.this, onCloseButtonClickListener);
            if (!isFinishing()) dialog.show();
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("delivery_id", deliveryId);
            hashMap.put("status", mStatus);
            hashMap.put("cooperation", cooperation);
            hashMap.put("start_latitute", taxiDeliveryDetail.getPickUpLat());
            hashMap.put("start_longitute", taxiDeliveryDetail.getPickUpLong());
            hashMap.put("end_latitute", taxiDeliveryDetail.getShippingLat() != 0 ? taxiDeliveryDetail.getShippingLat() : LocationUpdatesService.mLocation.getLatitude());
            hashMap.put("end_longitute", taxiDeliveryDetail.getShippingLong() != 0 ? taxiDeliveryDetail.getShippingLong() : LocationUpdatesService.mLocation.getLongitude());
            String polyline = PolyUtil.encode(latLngList);
            hashMap.put("polyline", polyline);
            String dropOffAddress = Util.getLoc(LocationUpdatesService.mLocation.getLatitude(),LocationUpdatesService.mLocation.getLongitude(), this);
            hashMap.put("drop_off_address",  dropOffAddress);

            new DeliveryWs().acceptDelivery(TaxiCompleteActivity.this, typeOrder, hashMap, new DeliveryWs.OnAcceptDeliveryListener() {
                @Override
                public void onLoadListSuccess(List<Delivery> deliveryList) {
                }

                @Override
                public void onAcceptDeliverySuccess(Waypoint waypoint) {
                }

                @Override
                public void onAcceptSuccessTaxi(Waypoint waypoint) {
                    dialog.onLoadingCompleted();
                    Util.stopRecordLocation(TaxiCompleteActivity.this);
                    stopTimer();
                    Util.updateServiceInterval(TaxiCompleteActivity.this, LocationUpdatesService.UPDATE_INTERVAL_IN_MILLISECONDS);

                }

                @Override
                public void onAcceptFailed(String message) {
                    if (!isConnectedToInternet (TaxiCompleteActivity.this)){
                        popupMessage ( null,  getString(R.string.check_yor) ,TaxiCompleteActivity.this);
                        dialog.onDismiss();
                    } else {
                        Util.popupMessage(null, message,TaxiCompleteActivity.this);
                    }
                    button.toggleState();
                    dialog.onDismiss();
                }
            });
        }
    }

    private final WaitingDialog.OnCloseButtonClickListener onCloseButtonClickListener = new WaitingDialog.OnCloseButtonClickListener() {
        @Override
        public void onClose(WaitingDialog dialog) {
            refresh();
        }

        @Override
        public void onOk(WaitingDialog waitingDialog) {
            goDetail();
        }

        @Override
        public void onClickRate(WaitingDialog waitingDialog) {
            Intent intent = new Intent(TaxiCompleteActivity.this, HistoryTaxiActivity.class);
            intent.putExtra("typeOrder", typeOrder);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("delivery", deliveryId);
            intent.putExtra("back","back");
            intent.putExtra("open_rate","open_rate");
            startActivity(intent);
            finish();
        }
    };

    private void goDetail() {
        Intent intent = new Intent(this, HistoryTaxiActivity.class);
        intent.putExtra("typeOrder", typeOrder);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("delivery", deliveryId);
        intent.putExtra("back","back");
        startActivity(intent);
        finish();
    }

    @Override
    protected void onDrawRoute(GoogleMap googleMap, Waypoint pickupWayPoint, Waypoint waypoint) {
        drawMap(googleMap, waypoint);
    }

    private void drawMap(GoogleMap mMap, Waypoint waypoint) {
        ArrayList<LatLng> routelist = new ArrayList<>();
        double lat, lng;
        if (waypoint.getArrJsonLatLng().size() > 0) {
            for (int i = 0; waypoint.getArrJsonLatLng().size() > i; i++) {
                lat = waypoint.getArrJsonLatLng().get(i).getLatitute();
                lng = waypoint.getArrJsonLatLng().get(i).getLongitute();
                routelist.add(new LatLng(lat, lng));
            }
        }
        if (routelist.size() > 0) {
            PolylineOptions rectLine = new PolylineOptions().width(5).color(Color.parseColor("#E62138"));//set line color to app color
            for (int i = 0; i < routelist.size(); i++) {
                rectLine.add(routelist.get(i));
            }
            if (mMap != null) {
                MarkerOptions markerClientPoint = new MarkerOptions();
                MarkerOptions markerDriverPoint = new MarkerOptions();
                markerClientPoint.position(routelist.get(routelist.size() - 1));
                markerDriverPoint.position(routelist.get(0));
                markerDriverPoint.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_taxi));
                markerClientPoint.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_drop_off));
                mMap.addMarker(markerClientPoint);
                mMap.addMarker(markerDriverPoint);
                // Adding route on the map
                mMap.addPolyline(rectLine);
                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                builder.include(markerClientPoint.getPosition());
                builder.include(markerDriverPoint.getPosition());
                LatLngBounds bounds = builder.build();
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, getResources().getDisplayMetrics().widthPixels, getResources().getDisplayMetrics().heightPixels / 2, (int) (getResources().getDisplayMetrics().widthPixels * 0.18));
                mMap.animateCamera(cameraUpdate);
            } else {
                logDebug("mapIsNull", "null");
            }
        }
    }

    @Override
    protected void onPhoneCallClick(ImageView buttonCall) {
        if (taxiDeliveryDetail == null) return;
        if("service".equalsIgnoreCase(typeOrder)){
            buttonCall.setVisibility(View.GONE);
        }
        if ("send".equalsIgnoreCase(typeOrder)) {
            callToCustomer(taxiDeliveryDetail);
        } else {
            call(taxiDeliveryDetail.getPhoneNumber());
        }
    }

    private void callToCustomer(final TaxiDeliveryDetail deliveryDetails) {
        taxiDeliveryDetail = deliveryDetails;
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(TaxiCompleteActivity.this, R.style.DialogTheme);
        builder.setCancelable(false);
        View view = LayoutInflater.from(this).inflate(R.layout.layout_view_call, null);
        builder.setView(view);
        final Dialog dialog = builder.create();
        TextView textView = view.findViewById(R.id.tex_phone);
        TextView closeTv = view.findViewById(R.id.close);
        textView.setText(String.format("  %s %s", taxiDeliveryDetail.getSendPackage().getPhoneReceiver(), taxiDeliveryDetail.getSendPackage().getNameReceiver()));
        textView.setOnClickListener(v -> call(taxiDeliveryDetail.getSendPackage().getPhoneReceiver()));
        closeTv.setOnClickListener(v -> dialog.dismiss());
        dialog.show();
    }

    private void call(String phone) {
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:" + phone));
        callIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(callIntent);
    }

    public class LocationUpdateReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Location location = intent.getParcelableExtra(LocationUpdatesService.EXTRA_LOCATION);
            if (LocationUpdatesService.ACTION_BROADCAST.equalsIgnoreCase(intent.getAction()) && location != null) {
                latLngList.add(new LatLng(location.getLatitude(), location.getLongitude()));
                updatePriceView(latLngList, taxiDeliveryDetail);
                if (accuractyTv != null)
                    accuractyTv.setText(String.format("%s~%s", getString(R.string.accuracy), location.getAccuracy()));
            }
        }
    }

    public void refresh() {
        Intent intent = new Intent(TaxiCompleteActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
        startActivity(intent);
    }

    private double getTripDuration() {
        double pickupTime = Util.getDouble("pickup_time", this);
        double currentTime = Calendar.getInstance(Locale.getDefault()).getTimeInMillis();
        double duration = (currentTime - pickupTime) / 60000;
        return (int)duration;
    }

    protected void updatePriceView(List<LatLng> latLngList, TaxiDeliveryDetail taxiDeliveryDetail) {
        if (distanceCounterTv == null || durationCounterTv == null || totalPriceTv == null) return;

        double totalPrice = 0;
        double totalDuration = getTripDuration(); // millisecond
        float mTotalDistance = calculateDistanceInMetre(latLngList);
        if (taxiDeliveryDetail != null) {
            float unitPrice = taxiDeliveryDetail.getServiceCostPerDistance();
            float distanceFlagDown = taxiDeliveryDetail.getServiceFlagDownDistance() * 1000f; // convert to m
            float priceFlagDown = taxiDeliveryDetail.getServiceFlagDownCost();
            float pricePerMinute = taxiDeliveryDetail.getServiceCostPerMin();
            double durationPrice = totalDuration * pricePerMinute;

            if (mTotalDistance <= distanceFlagDown) { // compare metre to metre
                totalPrice = priceFlagDown + durationPrice;
            } else {
                totalPrice = priceFlagDown + (mTotalDistance - distanceFlagDown) / 1000 * unitPrice + durationPrice;
            }
        }
        distanceCounterTv.setText(String.format(Locale.US, "%s : %s km", getString(R.string.distance), Util.formatDistance(mTotalDistance / 1000))); // display to Km
        durationCounterTv.setText(String.format(Locale.US, "%s : %s",getString(R.string.duration), Util.formatTime((int) totalDuration)));
        if (taxiDeliveryDetail != null && taxiDeliveryDetail.getWaypoints() == null) {
            // Update total price real time only for taxi without destination
            totalPriceTv.setText(String.format(Locale.US, " %s%s", taxiDeliveryDetail.getCurrency(), Util.formatPrice(totalPrice)));
        } else {
            logDebug("taxiDeliveryDetail", "null");
        }
    }

    private float calculateDistanceInMetre(List<LatLng> latLngList) {
        float totalDistance = 0;
        for (int i = 0; i < latLngList.size() - 1; i++) {
            LatLng latLng1 = latLngList.get(i);
            LatLng latLng2 = latLngList.get(i + 1);

            Location loc1 = new Location("loc1");
            loc1.setLatitude(latLng1.latitude);
            loc1.setLongitude(latLng1.longitude);

            Location loc2 = new Location("loc2");
            loc2.setLatitude(latLng2.latitude);
            loc2.setLongitude(latLng2.longitude);

            float distance = loc1.distanceTo(loc2);
            totalDistance += distance;
        }
        return totalDistance;
    }

    @Override
    protected void onRequestSuccess(TaxiDeliveryDetail detail) {
        updatePriceView(latLngList, detail);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(dialog != null && dialog.isShowing()){
            dialog.cancel();
        }
    }
}