package com.station29.mealtempledelivery.ui.setting

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.station29.mealtempledelivery.R
import com.station29.mealtempledelivery.api.request.RewardsWs
import com.station29.mealtempledelivery.data.model.Rewards
import com.station29.mealtempledelivery.data.model.Season
import com.station29.mealtempledelivery.ui.base.BaseActivity
import com.station29.mealtempledelivery.ui.setting.adapter.RewardsListAdapter
import com.station29.mealtempledelivery.utils.Util
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class RewardsActivity : BaseActivity()  {

    private lateinit var recyclerViewAll : RecyclerView
    private lateinit var linearLayoutManager : LinearLayoutManager
    private var rewardsAdapter: RewardsListAdapter?=null
    private lateinit var totalPoint :TextView
    private lateinit var reward: Rewards
    private lateinit var loading: View
    private lateinit var  refresh : SwipeRefreshLayout
    private lateinit var txtSeasonDay : TextView

    @SuppressLint("NotifyDataSetChanged")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rewards)

        val title: TextView = findViewById(R.id.toolBarTitle)
        val btnBack : ImageView = findViewById(R.id.backButton)
        title.text = getString(R.string.rewards)
        btnBack.setOnClickListener { finish() }
        val btnBu : LinearLayout = findViewById(R.id.btnBuy)
        totalPoint = findViewById(R.id.total_reward)
        loading = findViewById(R.id.progressBar)
        recyclerViewAll = findViewById(R.id.list_view)
        refresh = findViewById(R.id.pull_refresh)
        txtSeasonDay = findViewById(R.id.txtSeasonDay)

        getAllRewards()

        btnBu.setOnClickListener {
            activityLauncher.launch(Intent(this,ExchangeRewardsActivity::class.java)) { result : ActivityResult ->
                if (result.resultCode == Activity.RESULT_OK ){
                    rewardsAdapter?.clear()
                    getAllRewards()
                    rewardsAdapter?.notifyDataSetChanged()
                }
            }
        }

        refresh.setColorSchemeResources(R.color.colorPrimary)
        refresh.setOnRefreshListener {
            rewardsAdapter?.clear()
            getAllRewards()
            rewardsAdapter?.notifyDataSetChanged()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun getAllRewards(){
        loading.visibility = View.VISIBLE
        RewardsWs().getListRewards(this,rewardsObject)
    }

    fun getDaysDifference(fromDate: Date?, toDate: Date?) : Int {
        return if (fromDate == null || toDate == null) 0 else ((toDate.time - fromDate.time) / (1000 * 60 * 60 * 24)).toInt()
    }

    private val rewardsObject = object : RewardsWs.RewardSCallBack{
        @SuppressLint("SetTextI18n", "SimpleDateFormat")
        override fun onLoadingSuccessFully(season : Season? , listRewards: ArrayList<Rewards>, total:Int) {
            totalPoint.text = total.toString()
            for (rewards: Rewards in listRewards){
                reward = rewards
            }
            if (season != null){
                val df = SimpleDateFormat("yyyy-MM-dd")
                val date = df.format(Calendar.getInstance().time)
                val date1 = SimpleDateFormat("yyyy-MM-dd").parse(date)
                val date2 = SimpleDateFormat("yyyy-MM-dd").parse(season.end_date)
                val day = getDaysDifference(date1,date2)
                if (day != 0) {
                    txtSeasonDay.text = getString(R.string.season_end_in) + " : " + getDaysDifference(date1,date2) + "d"
                } else {
                    val rightNow = Calendar.getInstance()
                    val currentHourIn24Format = rightNow[Calendar.HOUR_OF_DAY]
                    val nowTime = 24 - currentHourIn24Format
                    txtSeasonDay.text = getString(R.string.season_end_in) + " : " + nowTime + "h"
                }
            } else {
                txtSeasonDay.text = getString(R.string.season_ended)
                Util.popupMessage("",getString(R.string.sorry_reward_have_not_start_yet),this@RewardsActivity)
            }
            linearLayoutManager = LinearLayoutManager(this@RewardsActivity)
            recyclerViewAll.layoutManager = linearLayoutManager
            rewardsAdapter = RewardsListAdapter(listRewards,this@RewardsActivity)
            recyclerViewAll.adapter = rewardsAdapter
            loading.visibility = View.GONE
            refresh.isRefreshing = false
        }

        override fun onLoadingFail(message: String?) {
            refresh.isRefreshing = false
            loading.visibility = View.GONE
            Toast.makeText(this@RewardsActivity, message, Toast.LENGTH_LONG).show()
        }
    }
}