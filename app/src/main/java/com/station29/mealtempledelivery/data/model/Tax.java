package com.station29.mealtempledelivery.data.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Tax implements Serializable {

    @SerializedName("name")
    @Expose
    private String nameTax;

    @SerializedName("amount")
    @Expose
    private float amount ;

    @SerializedName("money")
    @Expose
    private float taxAmount ;

    @SerializedName("rules")
    @Expose
    private int rules ;

    public String getNameTax() {
        return nameTax;
    }

    public float getAmount() {
        return amount;
    }

    public boolean isRules() {
        return rules==1;//mean true
    }

    public float getTaxAmount() {
        return taxAmount;
    }
}
