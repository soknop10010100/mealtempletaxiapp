package com.station29.mealtempledelivery.ui.setting;

import static android.app.Activity.RESULT_OK;
import static com.station29.mealtempledelivery.utils.Util.checkVisiblePaymentService;
import static com.station29.mealtempledelivery.utils.Util.isConnectedToInternet;
import static com.station29.mealtempledelivery.utils.Util.logDebug;
import static com.station29.mealtempledelivery.utils.Util.popupMessage;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Base64;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.api.Constant;
import com.station29.mealtempledelivery.api.request.LogoutWs;
import com.station29.mealtempledelivery.api.request.ProfileWs;
import com.station29.mealtempledelivery.data.model.User;
import com.station29.mealtempledelivery.data.model.Wallets;
import com.station29.mealtempledelivery.ui.base.BaseFragment;
import com.station29.mealtempledelivery.ui.base.MusicControl;
import com.station29.mealtempledelivery.ui.login.LoginActivity;
import com.station29.mealtempledelivery.ui.setting.payment.DepositAndWithdrawActivity;
import com.station29.mealtempledelivery.ui.wallet.ScanMeFragment;
import com.station29.mealtempledelivery.ui.wallet.WalletActivity;
import com.station29.mealtempledelivery.utils.Util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class ProfileFragment extends BaseFragment {

    private static final String IMAGE_DIRECTORY = "/MealTempleDriver";
    private final int GALLERY = 1;
    private final int CAMERA = 2;
    private ImageView userProfile;
    private TextView firstName;
    private TextView lastName;
    private TextView phoneNUmber ,account_number_setting;
    private TextView serviceType;
    @SuppressLint("UseSwitchCompatOrMaterialCode")
    private Switch switchStatus;
    private ProgressBar progressBar;
    private LinearLayout showProfile;
    private TextView totalWalletTv;
    private OnRegisterServiceListener onRegisterServiceListener;
    private User mUser;
    private List<Wallets> walletsList = new ArrayList<>();
    private boolean isFinishGetProfile = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_profile, container, false);
        setHasOptionsMenu(true);

        LinearLayout wallet = root.findViewById(R.id.wallet);
        totalWalletTv = root.findViewById(R.id.wallet_price);
        firstName = root.findViewById(R.id.txt_first_name_setting);
        lastName = root.findViewById(R.id.txt_last_name_setting);
        phoneNUmber = root.findViewById(R.id.phone_number_setting);
        serviceType = root.findViewById(R.id.service_setting);
        progressBar = root.findViewById(R.id.progressBar);
        showProfile = root.findViewById(R.id.showProfile);
        LinearLayout withdraw = root.findViewById(R.id.withdrow);
        switchStatus = root.findViewById(R.id.status_switch_setting);
        userProfile = root.findViewById(R.id.user_profile_image);
        ImageView imgQRCode = root.findViewById(R.id.scanMe);
        RelativeLayout btnSetting = root.findViewById(R.id.btn_setting);
        TextView rewardTv = root.findViewById(R.id.rewardsTv);
        account_number_setting = root.findViewById(R.id.account_number_setting);

        if (Constant.getBaseUrl() != null) {
            showProfileDriver();
        }
        imgQRCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ScanMeFragment scanMeFragment = ScanMeFragment.newInstance();
                scanMeFragment.show(getChildFragmentManager(), ScanMeFragment.class.getName());
            }
        });

        switchStatus.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isFinishGetProfile) {
                offlineWarning(isChecked ? 1 : 0);
            }
        });

        wallet.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), WalletActivity.class);
            startActivity(intent);
        });

        root.findViewById(R.id.edit_photo).setOnClickListener(v -> showPictureDialog());

        root.findViewById(R.id.btn_log_out_setting).setOnClickListener(v -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(mActivity, R.style.DialogTheme);
            builder.setTitle(R.string.logout);
            builder.setMessage(R.string.logout_inform);
            builder.setPositiveButton(R.string.logout, (dialog, which) -> {
                logout();
                onRegisterServiceListener.onRegister(false);
            });
            builder.setNegativeButton(getString(R.string.cancel), null).show();
        });

        btnSetting.setOnClickListener(v -> {
            Intent intent1 = new Intent(mActivity, SettingActivity.class);
            intent1.putExtra("user", mUser);
            activityLauncher.launch(intent1, result -> {
                if (result.getResultCode() == RESULT_OK){
                    onRegisterServiceListener.onRegister(false);
                    Intent intent = new Intent(mActivity, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            });
        });

        withdraw.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), DepositAndWithdrawActivity.class);
            intent.putExtra("user", mUser);
            intent.putExtra("wallets", (Serializable) walletsList);
            intent.putExtra("size", checkVisiblePaymentService());
            startActivity(intent);
        });

        root.findViewById(R.id.linear_rewards).setOnClickListener(view -> {
            Intent intent = new Intent(mActivity, RewardsActivity.class);
            intent.putExtra("user",mUser);
            startActivity(intent);
        });

        root.findViewById(R.id.btn_logout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mActivity, R.style.DialogTheme);
                builder.setCancelable(false);
                builder.setTitle(R.string.logout);
                builder.setMessage(R.string.logout_inform);
                builder.setPositiveButton(R.string.logout, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        progressBar.setVisibility(View.VISIBLE);
                        logout();
                        //onRegisterServiceListener.onRegister(false);
                    }
                });
                builder.setNegativeButton(getString(R.string.cancel), null).show();
            }
        });

        return root;
    }

    private void logout() {
        HashMap<String, String> deviceTypes = new HashMap<>();
        deviceTypes.put("device_type", "ANDROID");
        String topic = Util.getString("topic", mActivity);
        String deviceToken = Util.getString("firebase_token", mActivity);
        new LogoutWs().logOut(mActivity, deviceTypes, deviceToken, topic, new LogoutWs.LogoutCallback() {
            @Override
            public void onLoadSuccess(String message) {
                MusicControl.countNotification -= 1;
                MusicControl.getInstance(mActivity).stopMusic();
                removeNotification();

                signOutFireStore();
                Bundle bundle = new Bundle();
                bundle.putString(Constant.USER_ID, Util.getString("user_id", mActivity) + "was Logout at " + Calendar.getInstance());
                mFirebaseAnalytics.logEvent(this.getClass().getSimpleName(), bundle);

                Intent intent = new Intent(getActivity(), LoginActivity.class);
                Util.saveString("token", "", mActivity);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }

            @Override
            public void onLoadFailed(String errorMessage) {
                Util.popupMessage(null, errorMessage, mActivity);
            }
        });
    }

    private void signOutFireStore() {
        FirebaseAuth.getInstance().signOut();
    }

    private void updateStatusTask(final int online) {
        if (Constant.getBaseUrl() != null) {
            new ProfileWs().updateStatus(mActivity, online, new ProfileWs.ReadOnOffLine() {
                @Override
                public void onLoadSuccess(String message) {
                    onRegisterServiceListener.onRegister(online != 0);
                    // Save online status, Thus we can decide if we need to bind service from Main Screen or not
                    Util.saveString("online", String.valueOf(online), mActivity);

                    // ===============================================================================================================
                    Bundle bundle = new Bundle();
                    bundle.putString(Constant.USER_ID, Util.getString("user_id", mActivity) + " was " + (online == 1 ? "Online" : "Offline") + "  at " + Calendar.getInstance());
                    mFirebaseAnalytics.logEvent(this.getClass().getSimpleName(), bundle);
                    // =================================================================================================================
                }

                @Override
                public void onLoadFail(String message) {
                    Toast toast = Toast.makeText(getContext(), message, Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            });
        }
    }

    private final ProfileWs.ProfileViewAndUpdate profileViewAndUpdate = new ProfileWs.ProfileViewAndUpdate() {
        @Override
        public void onLoadProfileSuccess(User user, String message) {

        }

        @Override
        public void onError(String errorMessage) {

        }

        @Override
        public void onUpdateProfilesSuccess(String message) {

        }
    };

    private void showProfileDriver() {
        progressBar.setVisibility(View.VISIBLE);
        if (Constant.getBaseUrl() != null) {
            new ProfileWs().viewProfiles(mActivity, new ProfileWs.ProfileViewAndUpdate() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onLoadProfileSuccess(User user, String message) {
                    mUser = user;
                    walletsList = mUser.getWalletsList();
                    if (mUser != null) {
                        if (mUser.getFirstName() != null && !mUser.getFirstName().isEmpty()) {
                            firstName.setText(mUser.getFirstName());
                        }
                        if (mUser.getLastName() != null && !mUser.getLastName().isEmpty()) {
                            lastName.setText(mUser.getLastName());
                        }
                        if (mUser.getPhoneNumber() != null && !mUser.getPhoneNumber().isEmpty()) {
                            phoneNUmber.setText(mUser.getPhoneNumber());
                        }
                        if (mUser.getServiceType() != null && !mUser.getServiceType().isEmpty()) {
                            serviceType.setText(mUser.getServiceType());
                        }
                        if (mUser.getWalletsList() != null && !mUser.getWalletsList().isEmpty()
                                && mUser.getWalletsList().get(0).getBalance() != null
                                && mUser.getWalletsList().get(0).getCurrency() != null) {
                            totalWalletTv.setText(String.format("%s %s", mUser.getWalletsList().get(0).getCurrency(), mUser.getWalletsList().get(0).getBalance()));
                        }
                        account_number_setting.setText(mUser.getAccountId());
                        switchStatus.setChecked(user.getOnline() == 1);
                        Glide.with(mActivity).load(mUser.getProfile_image()).apply(new RequestOptions().error(R.drawable.user_image).placeholder(R.drawable.user_image)).into(userProfile);
                        showProfile.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                        isFinishGetProfile = true;
                    }
                }

                @Override
                public void onError(String errorMessage) {
                    progressBar.setVisibility(View.GONE);
                    if (!isConnectedToInternet(mActivity)) {
                        popupMessage(null, getString(R.string.check_yor), mActivity);
                    }
                    //   Util.popupMessage(null, errorMessage, mActivity);
                }

                @Override
                public void onUpdateProfilesSuccess(String message) {
                }
            });
        }
    }

    private void saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream btyes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, btyes);
        File wallpaperDirectory = new File(Environment.getExternalStorageState() + IMAGE_DIRECTORY);
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }
        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance().getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(btyes.toByteArray());
            MediaScannerConnection.scanFile(getContext(),
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            logDebug("TAG", "File Saved::---&gt;" + f.getAbsolutePath());

            f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    private String encodeImage(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
//        Util.byteArrayToBinary(b);
        String encImage = Base64.encodeToString(b, Base64.DEFAULT);

        return encImage;
    }

    private void savePictureProfile(String base64) {
        HashMap<String, String> imageHashMap = new HashMap<>();
        imageHashMap.put("image", base64);
        imageHashMap.put("function", "driver");
        if (Constant.getBaseUrl() != null)
            new ProfileWs().changeProfile(mActivity, imageHashMap, profileViewAndUpdate);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(mActivity, R.string.camera_successful, Toast.LENGTH_LONG).show();
                takePhotoFromCamera();
            } else {
                Toast.makeText(mActivity, R.string.cannot_take_photo, Toast.LENGTH_LONG).show();
            }
        }
        if (requestCode == GALLERY) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(mActivity, R.string.have_gallery, Toast.LENGTH_LONG).show();
                choosePhotoFromGallery();
            } else {
                Toast.makeText(mActivity, R.string.cannot_pickgallery, Toast.LENGTH_LONG).show();
            }

        }

    }

    private void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(mActivity);
        pictureDialog.setTitle(getString(R.string.select_action));
        String[] pictureDialogItems = {getString(R.string.choose_gallery), getString(R.string.take_photo)};
        pictureDialog.setItems(pictureDialogItems, (dialog, which) -> {
                    switch (which) {
                        case 0:
                            if (ContextCompat.checkSelfPermission(mActivity, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, GALLERY);

                            } else {
                                choosePhotoFromGallery();
                            }
                            break;
                        case 1:
                            if (ContextCompat.checkSelfPermission(mActivity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                                requestPermissions(new String[]{Manifest.permission.CAMERA}, CAMERA);

                            } else {
                                takePhotoFromCamera();
                            }
                            break;
                    }
                });
        pictureDialog.show();
    }

    private void choosePhotoFromGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        activityLauncher.launch(galleryIntent, result -> {
            if (result.getResultCode() == RESULT_OK){
                Intent data = result.getData();
                if (data != null) {
                    final Uri imageUri = data.getData();
                    final InputStream imageStream;
                    try {
                        imageStream = mActivity.getContentResolver().openInputStream(imageUri);
                        final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                        String encodedImage = encodeImage(selectedImage);
                        userProfile.setImageBitmap(selectedImage);
                        savePictureProfile(encodedImage);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        activityLauncher.launch(intent, result -> {
            if (result.getResultCode() == RESULT_OK) {
                Intent data = result.getData();
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                userProfile.setImageBitmap(thumbnail);
                if (thumbnail != null) {
                    saveImage(thumbnail);
                    savePictureProfile(Util.convert(thumbnail));
                }
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onRegisterServiceListener = (OnRegisterServiceListener) context;
    }

    private void offlineWarning(final int online) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity, R.style.DialogTheme);
        builder.setMessage(online == 0 ? getString(R.string.offline_warning) : getString(R.string.online_warning));
        builder.setNegativeButton(getString(R.string.cancel), (dialog, which) -> {
            switchStatus.setChecked(!(online == 1));// coz we don't have any way to stop switch from check, so we store to previous state
        });
        builder.setPositiveButton(getString(R.string.ok), (dialog, which) -> updateStatusTask(online));
        AlertDialog alert = builder.create();
        alert.setCanceledOnTouchOutside(false);
        alert.setCancelable(false);
        alert.show();
    }

    public interface OnRegisterServiceListener {
        void onRegister(boolean register);
    }

    private void removeNotification() {
        NotificationManager manager = (NotificationManager) mActivity.getSystemService(Context.NOTIFICATION_SERVICE);
        if (manager != null) {
            manager.cancelAll();
        }
    }


}