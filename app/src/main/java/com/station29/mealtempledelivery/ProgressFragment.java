//package com.station29.mealtempledelivery;
//
//import android.content.Intent;
//import android.net.Uri;
//import android.os.Bundle;
//import android.text.method.LinkMovementMethod;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.MenuItem;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Button;
//import android.widget.ProgressBar;
//import android.widget.TextView;
//
//import androidx.annotation.NonNull;
//import androidx.annotation.Nullable;
//import androidx.appcompat.app.ActionBar;
//import androidx.appcompat.app.AppCompatActivity;
//import androidx.navigation.NavController;
//import androidx.navigation.Navigation;
//
//import com.ebanx.swipebtn.OnStateChangeListener;
//import com.ebanx.swipebtn.SwipeButton;
//import com.facebook.shimmer.ShimmerFrameLayout;
//import com.google.android.gms.maps.CameraUpdate;
//import com.google.android.gms.maps.CameraUpdateFactory;
//import com.google.android.gms.maps.GoogleMap;
//import com.google.android.gms.maps.OnMapReadyCallback;
//import com.google.android.gms.maps.SupportMapFragment;
//import com.google.android.gms.maps.model.BitmapDescriptorFactory;
//import com.google.android.gms.maps.model.LatLng;
//import com.google.android.gms.maps.model.MarkerOptions;
//import com.station29.mealtempledelivery.api.Constant;
//import com.station29.mealtempledelivery.api.request.DeliveryWs;
//import com.station29.mealtempledelivery.data.model.Delivery;
//import com.station29.mealtempledelivery.data.model.DeliveryDetails;
//import com.station29.mealtempledelivery.service.LocationUpdatesService;
//import com.station29.mealtempledelivery.ui.base.BaseFragment;
//import com.station29.mealtempledelivery.ui.home.OrderDetailActivity;
//import com.station29.mealtempledelivery.ui.home.WaitingDialog;
//import com.station29.mealtempledelivery.utils.Util;
//
//import java.util.HashMap;
//import java.util.List;
//import java.util.Locale;
//
//
//public class ProgressFragment extends BaseFragment implements OnMapReadyCallback {
//
//    private ProgressBar progressBar;
//    private GoogleMap mMap;
//    private int deliveryId;
//    private View root;
//    private DeliveryDetails mDetail;
//    private ActionBar actionBar;
//    private String mStatus;
//
//    public ProgressFragment() {
//        // Required empty public constructor
//    }
//
//    @Override
//    public void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        if (getArguments() != null && getArguments().containsKey("deliveryId") && getArguments().containsKey("updateStatus")) {
//            deliveryId = getArguments().getInt("deliveryId");
//            mStatus = getArguments().getString("updateStatus");
//        }
//        // In order to user build in back button
//        setHasOptionsMenu(true);
//        // Hide defaut action bar to save more space for map
//        actionBar = ((AppCompatActivity) mActivity).getSupportActionBar();
//        getDeliveryDetailTask();
//    }
//
//    @Override
//    public View onCreateView(@NonNull final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        root = inflater.inflate(R.layout.layout_service_arrival, container, false);
//        progressBar = root.findViewById(R.id.progress_bar);
//
//        final SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
//                .findFragmentById(R.id.map);
//        if (mapFragment != null) {
//            mapFragment.getMapAsync(this);
//        }
//
//        final SwipeButton pickUpBtn = root.findViewById(R.id.accept1);
//        final SwipeButton completeBtn = root.findViewById(R.id.accept2);
//        final ShimmerFrameLayout shimmerFrameLayout = root.findViewById(R.id.shimmer);
//        pickUpBtn.setOnStateChangeListener(new OnStateChangeListener() {
//            @Override
//            public void onStateChange(boolean active) {
//                if (!active) return;
//                shimmerFrameLayout.hideShimmer();
//                progressBar.setVisibility(View.VISIBLE);
//                if (mDetail == null) {
//                    return;
//                }
//                updateStatusTask(deliveryId , completeBtn, mDetail.getCooperation());
//                shimmerFrameLayout.showShimmer(true);
//                pickUpBtn.setVisibility(View.GONE);
//                logDebug("activeTest", active +"");
//            }
//        });
//        completeBtn.setOnStateChangeListener(new OnStateChangeListener() {
//            @Override
//            public void onStateChange(boolean active) {
//                if (!active) return;
//                shimmerFrameLayout.hideShimmer();
//                progressBar.setVisibility(View.VISIBLE);
//                if (mDetail == null) {
//                    return;
//                }
//                updateStatusTask(deliveryId , completeBtn, mDetail.getCooperation());
//            }
//        });
//        logDebug("mstatus", mStatus);
//        if (mStatus.equals(Constant.ACCEPTED)){
//            pickUpBtn.setText(getString(R.string.slide_to_pickup));
//            pickUpBtn.setButtonBackground(mActivity.getDrawable(R.drawable.on_swipe_shape_green));
//        } else if (mStatus.equals(Constant.PICKED_UP)){
//            pickUpBtn.setVisibility(View.GONE);
//            completeBtn.setVisibility(View.VISIBLE);
//        }
//        return root;
//    }
//
//    private void getDeliveryDetailTask() {
//        if (Constant.getBaseUrl() != null)
//            new DeliveryWs().readDelivery(deliveryId, mActivity, new DeliveryWs.ReadDeliveryListener() {
//                @Override
//                public void onLoadSuccess(final DeliveryDetails details) {
//                    mDetail = details;
//                    if (actionBar != null) {
//                        actionBar.setTitle("From " + mDetail.getStoreName());
//                    }
//                    if (root == null) return;
//                    final TextView deliverTime = root.findViewById(R.id.DeliveryTimePf);
//                    deliverTime.setText(String.format(Locale.US, "Delivery by %s", Util.formatTimeZoneLocal(details.getDeliveryTime())));
//                    final TextView storeName = root.findViewById(R.id.storeNamePf);
//                    storeName.setText(String.format("From %s", details.getStoreName()));
//                    final TextView totalPrice = root.findViewById(R.id.totalPricePf);
//                    totalPrice.setText(String.format("Total: %s", details.getTotalFormatted()));
//                    final TextView deliveryFee = root.findViewById(R.id.deliveryFeePf);
////                    deliveryFee.setText(String.format("Delivery fee: %s %s", details.getCurrency(), Util.formatPrice(details.getDeliveryFee())));
////                    final TextView riderTip = root.findViewById(R.id.riderTipPf);
////                    riderTip.setText(String.format("Rider tip: %s %s", details.getCurrency(), Util.formatPrice(details.getRiderTip())));
////                    if (details.getRiderTip() <= 0) {
////                        riderTip.setVisibility(View.GONE);
////                    }
//                    final TextView storePhone = root.findViewById(R.id.storePhone);
//                    storePhone.setText(String.format("Store : %s", details.getTel()));
//                    final TextView clientPhone = root.findViewById(R.id.clientPhone);
//                    clientPhone.setText(String.format("Customer : %s", details.getShippingPhone()));
//                    clientPhone.setMovementMethod(LinkMovementMethod.getInstance());
//
//                    final Button viewMenu = root.findViewById(R.id.viewMenuPick);
//                    viewMenu.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            Intent intent = new Intent(mActivity, OrderDetailActivity.class);
//                            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
//                            intent.putExtra("id", deliveryId);
//                            startActivity(intent);
//                        }
//                    });
//                    clientPhone.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            Intent callIntent = new Intent(Intent.ACTION_DIAL);
//                            callIntent.setData(Uri.parse("tel:" + details.getShippingPhone()));
//                            callIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                            startActivity(callIntent);
//
//                        }
//
//                    });
//
//                    storePhone.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            Intent callIntent = new Intent(Intent.ACTION_DIAL);
//                            callIntent.setData(Uri.parse("tel:" + details.getTel()));
//                            callIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                            startActivity(callIntent);
//                        }
//                    });
//                }
//                @Override
//                public void onLoadFail() {
//                }
//            });
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
//        if (item.getItemId() == android.R.id.home) {
//            super.mActivity.onBackPressed();
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }
//
//    @Override
//    public void onMapReady(final GoogleMap googleMap) {
//        mMap = googleMap;
//        zoomMap();
//        mMap.setMyLocationEnabled(true);
//        mMap.getUiSettings().setZoomControlsEnabled(true);
//        // Add a marker in Sydney and move the camera
//        if (mDetail!=null){
//            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mDetail.getStoreLat(), mDetail.getStoreLng()), 16));
//        }
//       //
//        // mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(serviceModel.getClientLat(), serviceModel.getClientLng()), 16));
//        mMap.setMyLocationEnabled(true);
//        mMap.getUiSettings().setZoomControlsEnabled(false);
//        CameraUpdateFactory.zoomIn();
//        CameraUpdateFactory.zoomOut();
//    }
//
//    private void zoomMap() {
//        if (LocationUpdatesService.mLocation != null) {
//            LatLng sydney = new LatLng(LocationUpdatesService.mLocation.getLatitude(), LocationUpdatesService.mLocation.getLongitude());
//            CameraUpdate location = CameraUpdateFactory.newLatLngZoom(sydney, 15);
//            mMap.moveCamera(location);
//            if (mDetail != null) {
//                //add shop marker
//                addMarker(mMap, R.drawable.circle_out, mDetail.getStoreLat(), mDetail.getStoreLng(), mDetail.getStoreName(), true);
//                // Add client Marker
//                addMarker(mMap, R.drawable.circle_in, mDetail.getStoreLat(), mDetail.getShippingLng(), mDetail.getShippingPhone(), false);
//            }
//        }
//    }
//
//    private void addMarker(GoogleMap googleMap, int icon, double lat, double lng, String title, boolean isShowWindowInfos) {
//        LatLng sydney = new LatLng(lat, lng);
//        MarkerOptions markerOptions = new MarkerOptions();
//        markerOptions.position(sydney).icon(BitmapDescriptorFactory.fromResource(icon)).title(title);
//
//        if (isShowWindowInfos) {
//            googleMap.addMarker(markerOptions).showInfoWindow();
//        } else {
//            googleMap.addMarker(markerOptions);
//        }
//    }
//
//    private WaitingDialog.OnCloseButtonClickListener onCloseButtonClickListener = new WaitingDialog.OnCloseButtonClickListener() {
//        @Override
//        public void onClose(WaitingDialog dialog) {
//            NavController navController = Navigation.findNavController(mActivity, R.id.nav_host_fragment);
//            navController.popBackStack();
//            dialog.dismiss();
//        }
//    };
//
//    private void updateStatusTask(final int deliveryId, final SwipeButton button, int cooperation) {
//
//        if (Constant.getBaseUrl() != null) {
//            button.setVisibility(View.VISIBLE);
//            if (mStatus!= null && mStatus.equalsIgnoreCase(Constant.ACCEPTED)){
//                mStatus = Constant.PICKED_UP;
//            } else if (mStatus != null && mStatus.equalsIgnoreCase(Constant.PICKED_UP)){
//                button.setVisibility(View.VISIBLE);
//                mStatus = Constant.DELIVERED;
//            }
//            logDebug("statusSend", mStatus);
//            final WaitingDialog dialog = new WaitingDialog(button.getContext(), onCloseButtonClickListener);
//            dialog.show();
//            progressBar.setVisibility(View.VISIBLE);
//            HashMap<String, Object> hashMap = new HashMap<>();
//            hashMap.put("delivery_id", deliveryId);
//            hashMap.put("status", mStatus);
//            hashMap.put("cooperation", cooperation);
//
//            new DeliveryWs().acceptDelivery(getContext(), hashMap, new DeliveryWs.OnAcceptDeliveryListener() {
//                @Override
//                public void onLoadListSuccess(List<Delivery> deliveryList) {
//                }
//
//                @Override
//                public void onAcceptSuccess() {
//                    // If picked up => go to completed
//                    if (mStatus.equals(Constant.PICKED_UP)){
//                        // Change slide button to Delivered
////                        button.setText(getString(R.string.slide_to_complete));
////                        button.setButtonBackground(mActivity.getDrawable(R.drawable.on_swipe_shape_red));
//                        dialog.onDismiss();
//                        progressBar.setVisibility(View.GONE);
//                        logDebug("mStatus1", mStatus);
//                    } else {
//                        // If completed => go back to home screen
//                        dialog.onLoadingCompleted();
//                        logDebug("mStatus2", mStatus);
//                    }
//                    progressBar.setVisibility(View.GONE);
//                }
//
//                @Override
//                public void onAcceptFailed(String message) {
//                    Util.popupMessage(null, message, button.getContext());
//                    progressBar.setVisibility(View.GONE);
//                    dialog.onloading();
//                }
//            });
//        }
//    }
//}
