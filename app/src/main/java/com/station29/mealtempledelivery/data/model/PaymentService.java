package com.station29.mealtempledelivery.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PaymentService implements Serializable {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("psp_name")
    @Expose
    private String pspName;
    @SerializedName("logo")
    @Expose
    private String logo;
    @SerializedName("country_code")
    @Expose
    private String countryCode;
    @SerializedName("is_cash")
    @Expose
    private Integer isCash;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("type")
    @Expose
    private String type;

    public String getDaytimeAdd() {
        return daytimeAdd;
    }

    @SerializedName("daytime_add")
    @Expose
    private String daytimeAdd;

    private String phone;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getStatus() {
        return status;
    }

    public Integer getIsCash() {
        return isCash;
    }

    public Integer getId() {
        return id;
    }

    public String getPspName() {
        return pspName;
    }

    public String getLogo() {
        return logo;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setPspName(String pspName) {
        this.pspName = pspName;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public void setIsCash(Integer isCash) {
        this.isCash = isCash;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
