package com.station29.mealtempledelivery.ui.setting.payment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.material.button.MaterialButton;
import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.api.request.PaymentWs;
import com.station29.mealtempledelivery.data.model.PaymentService;
import com.station29.mealtempledelivery.data.model.PaymentSuccess;
import com.station29.mealtempledelivery.ui.base.BaseActivity;
import com.station29.mealtempledelivery.utils.Util;

public class WaitingVerifyOrange extends BaseActivity {

    private String paymentId;
    private View progress ;
    private PaymentSuccess paymentSuccess;
    private String action = "e_shopping";

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_orange);

        TextView title = findViewById(R.id.toolBarTitle);
        MaterialButton btnComplete = findViewById(R.id.btnComplete);
        progress = findViewById(R.id.progress);
        ImageView imgLogo = findViewById(R.id.imgLogo);
        TextView txtTitleVerify = findViewById(R.id.txtTitleVerify);
        TextView txtVerify = findViewById(R.id.txtVerify);

        if (getIntent().hasExtra("paymentId")) {
            paymentId = getIntent().getStringExtra("paymentId");
        }
        if (getIntent().hasExtra("action")) {
            action = getIntent().getStringExtra("action");
        }
        if (getIntent().hasExtra("message")) {
            String message = getIntent().getStringExtra("message");
            txtVerify.setText(message);
        }
        if (action.equals("top_up")) {
            txtTitleVerify.setVisibility(View.GONE);
        }
        if (getIntent().hasExtra("paymentSuccess")){
            paymentSuccess = (PaymentSuccess) getIntent().getSerializableExtra("paymentSuccess");
        }
        if (getIntent().hasExtra("paymentService")) {
            PaymentService paymentService = (PaymentService) getIntent().getSerializableExtra("paymentService");
            Glide.with(WaitingVerifyOrange.this).load(paymentService.getLogo()).into(imgLogo);
        }

        title.setText(""+getString(R.string.payment) );

        btnComplete.setOnClickListener(view -> {
            progress.setVisibility(View.VISIBLE);
            new PaymentWs().getPaymentStatus(WaitingVerifyOrange.this, paymentId, new PaymentWs.OnPaymentWalletsCallBack() {
                @Override
                public void onSuccess(String status, String paymentId) {
                    progress.setVisibility(View.GONE);
                    if (status.equalsIgnoreCase("SUCCESSFULL")){
                        if (action.equals("top_up")) {
                            Intent intent = new Intent(WaitingVerifyOrange.this,PaymentSuccessActivity.class);
                            intent.putExtra("paymentSuccess",paymentSuccess);
                            intent.putExtra("action","top_up");
                            activityLauncher.launch(intent, result -> {
                               finish();
                            });
                        }
                    } else {
                        Util.popupMessage("",getString(R.string.you_not_verify_your_orange_pay_otp_yet),WaitingVerifyOrange.this);
                    }
                }

                @Override
                public void onError(String message) {
                    progress.setVisibility(View.GONE);
                    Util.popupMessage("",message,WaitingVerifyOrange.this);
                }
            });
        });

        findViewById(R.id.backButton).setOnClickListener(v ->{
            finish();
        });
    }
}
