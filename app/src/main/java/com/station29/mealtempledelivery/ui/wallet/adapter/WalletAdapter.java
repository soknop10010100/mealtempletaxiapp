package com.station29.mealtempledelivery.ui.wallet.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.data.model.User;
import com.station29.mealtempledelivery.data.model.Wallets;
import com.station29.mealtempledelivery.utils.Util;

import java.util.List;

public class WalletAdapter extends RecyclerView.Adapter<WalletAdapter.ViewHolder>{

    private List<Wallets> data;
    private User user;
    private Activity activity;

    public WalletAdapter(Activity activity, List<Wallets> data , User user) {
        this.data = data;
        this.user = user;
        this.activity = activity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.layout_wallet_card, parent, false);
        return new ViewHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Wallets wallets = data.get(position);
        if (wallets != null){
            holder.name_wallet.setText(Util.changeWalletType(activity,wallets.getDefaultType()));
           holder.wallet_amount.setText(wallets.getCurrency() + " " + Util.stringFormatPrice(wallets.getBalance()));
            if (wallets.getDefaultType().equalsIgnoreCase("DEFAULT_WALLET")){
                holder.imgCopy.setVisibility(View.VISIBLE);
                holder.wallet_number.setText(holder.itemView.getContext().getString(R.string.account_number) +" : " + user.getAccountId());
            } else {
                holder.imgCopy.setVisibility(View.GONE);
                holder.wallet_number.setText(holder.itemView.getContext().getString(R.string.user_name) + " : " + user.getFirstName()+ " "+user.getLastName());
            }

            holder.imgCopy.setOnClickListener( v -> {
                Toast.makeText(holder.itemView.getContext(), "Text Copied to clipboard",Toast.LENGTH_SHORT).show();
                ClipboardManager clipboard = (ClipboardManager) holder.imgCopy.getContext().getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText(holder.imgCopy.getContext().getString(R.string.account_number), user.getAccountId());
                clipboard.setPrimaryClip(clip);
            });
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView name_wallet, wallet_amount,wallet_number;
        private final ImageView imgCopy;

        public ViewHolder(View itemView) {
            super(itemView);
            name_wallet = itemView.findViewById(R.id.name_wallet);
            wallet_amount = itemView.findViewById(R.id.wallet_amount);
            wallet_number = itemView.findViewById(R.id.wallet_number);
            imgCopy = itemView.findViewById(R.id.imgCopy);
        }
    }
}
