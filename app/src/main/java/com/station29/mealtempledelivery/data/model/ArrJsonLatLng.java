package com.station29.mealtempledelivery.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ArrJsonLatLng implements Serializable {
    @SerializedName("latitute")
    @Expose
    private double latitute;
    @SerializedName("longitute")
    @Expose
    private double longitute;

    public double getLatitute() {
        return latitute;
    }

    public void setLatitute(double latitute) {
        this.latitute = latitute;
    }

    public double getLongitute() {
        return longitute;
    }

    public void setLongitute(double longitute) {
        this.longitute = longitute;
    }
}
