package com.station29.mealtempledelivery.ui.base;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.view.KeyEvent;
import android.view.Window;
import android.view.WindowManager;

import androidx.activity.result.ActivityResult;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.utils.BetterActivityResult;
import com.station29.mealtempledelivery.utils.NetworkChangeReceiver;
import com.station29.mealtempledelivery.utils.PickupActivity;
import com.station29.mealtempledelivery.utils.Util;

import java.util.Timer;
import java.util.TimerTask;

import static com.station29.mealtempledelivery.api.Constant.LANG_EN;
import static com.station29.mealtempledelivery.utils.Util.isConnectedToInternet;
import static com.station29.mealtempledelivery.utils.Util.popupMessage;

public class BaseActivity extends AppCompatActivity {

    public static String countryCode;
    public FirebaseAnalytics mFirebaseAnalytics;
    public static double latitude;
    public static String language;
    public static double longitude;
    private BroadcastReceiver mNetworkReceiver;
    public static String checkThisActivityName;
    protected final BetterActivityResult<Intent, ActivityResult> activityLauncher = BetterActivityResult.registerActivityForResult(this);


    @SuppressLint({"SourceLockedOrientationActivity", "MissingPermission"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // set the whole app to PORTRAIT
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        language = Util.getString("language", this);
        if (language.equals("")){
            language = LANG_EN;
        }
        Util.changeLanguage(this, language);
        checkThisActivityName = this.getClass().getName();
        mNetworkReceiver = new NetworkChangeReceiver();
        registerNetworkBroadcastForNougat();

    }

    public void unlockKeyguard() {
    //    clearNotification();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
            setTurnScreenOn(true);
            setShowWhenLocked(true);
        } else {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
            window.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
            window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
    }

    public void turnOnScreenn() {
      //  clearNotification();
        PowerManager pm = (PowerManager) this.getSystemService(Context.POWER_SERVICE);
        assert pm != null;
        boolean isScreenOn = pm.isScreenOn();
        if (!isScreenOn) {//wakeupfind the replace
            @SuppressLint("InvalidWakeLockTag") PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.ON_AFTER_RELEASE, "MyLock");
            wl.acquire(10000);
            @SuppressLint("InvalidWakeLockTag") PowerManager.WakeLock wl_cpu = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyCpuLock");
            wl_cpu.acquire(10000);

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void registerNetworkBroadcastForNougat() {
        registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    public static void dialog(boolean isConnected, Activity activity){
        if (checkThisActivityName.equalsIgnoreCase(activity.getClass().getName())){
            if (!isConnected )
                popUpRefresh(activity);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterNetworkChanges();
    }

    protected void unregisterNetworkChanges() {
        try {
            unregisterReceiver(mNetworkReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    private static void popUpRefresh(Activity activity) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(activity.getString(R.string.check_yor));
        builder.setPositiveButton(activity.getString(R.string.retry), (dialogInterface, i) -> {
            activity.recreate();
        });
        AlertDialog dialogNoInternet = builder.create();
        dialogNoInternet.setCanceledOnTouchOutside(false);
        dialogNoInternet.setCancelable(false);
        if (!activity.isDestroyed()){
            dialogNoInternet.show();
        }
    }

}
