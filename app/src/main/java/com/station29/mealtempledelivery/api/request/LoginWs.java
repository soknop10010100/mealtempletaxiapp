package com.station29.mealtempledelivery.api.request;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.api.Constant;
import com.station29.mealtempledelivery.api.CustomCallback;
import com.station29.mealtempledelivery.api.CustomResponseListener;
import com.station29.mealtempledelivery.api.RetrofitGenerator;
import com.station29.mealtempledelivery.api.ServiceAPI;
import com.station29.mealtempledelivery.data.model.User;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.station29.mealtempledelivery.utils.Util.logDebug;

public class LoginWs {

    public void loginPhone(final Context context, final HashMap<String, Object> account, final LoadLoginCallback loadingLoginCallBack) {
        ServiceAPI serviceAPI = RetrofitGenerator.createService();
        final Call<JsonElement> res = serviceAPI.loginPhone(account);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("data")) {
                    String token = "";
                    int id = 0, isDeactivated = 0;
                    String globalId = "";
                    JsonObject dataObj = resObj.getAsJsonObject("data");
                    if (dataObj.has("access_token")) {
                        token = dataObj.get("access_token").getAsString();
                    }
                    if (dataObj.has("user")) {
                        JsonObject userObj = dataObj.getAsJsonObject("user");

                        if (userObj.has("id")) {
                            id = userObj.get("id").getAsInt();
                        }
                        if (userObj.has("global_id")) {
                            globalId = userObj.get("global_id").getAsString();
                        }
                        if (userObj.has("is_deactivated")) {
                            isDeactivated = userObj.get("is_deactivated").getAsInt();
                        }
                        loadingLoginCallBack.onLoadSuccess(token, id , globalId, isDeactivated);
                    } else{
                        loadingLoginCallBack.onLoadFailed(context.getString(R.string.something_went_wrong),401);
                    }
                }
            }

            @Override
            public void onError(String error , int code) {
                loadingLoginCallBack.onLoadFailed(error,code);
            }
        }));
    }

    public interface LoadLoginCallback {
        void onLoadSuccess(String token, int userId, String globalId , int isDeactivated);
        void onLoadFailed(String errorMessage, int code);
    }
}
