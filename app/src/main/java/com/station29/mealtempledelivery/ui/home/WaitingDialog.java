package com.station29.mealtempledelivery.ui.home;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import com.station29.mealtempledelivery.R;

public class WaitingDialog extends AlertDialog {

    private View waitingView;
    private View completedView;
    private final OnCloseButtonClickListener mCancelListener;

    public WaitingDialog(@NonNull Context context, OnCloseButtonClickListener listener) {
        super(context);
        mCancelListener = listener;
        setCancelable(false);
        setCanceledOnTouchOutside(false);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_waiting);
        waitingView = findViewById(R.id.waiting);
        completedView = findViewById(R.id.completed);
        View closeButton = findViewById(R.id.close);
        View okButton = findViewById(R.id.ok);
        View rateReview = findViewById(R.id.rate_review);
        if (closeButton == null || okButton == null) return;

        closeButton.setOnClickListener(v -> {
            if (mCancelListener!= null) mCancelListener.onClose(WaitingDialog.this);
        });

        rateReview.setOnClickListener(v -> {
            if (mCancelListener!= null) mCancelListener.onClickRate(WaitingDialog.this);
        });

        okButton.setOnClickListener(v -> {
            if (mCancelListener!= null) mCancelListener.onOk(WaitingDialog.this);
        });
    }

    public void onLoadingCompleted() {
        if (waitingView != null) waitingView.setVisibility(View.GONE);
        if (completedView != null) completedView.setVisibility(View.VISIBLE);
    }

    public void onDismiss(){
        dismiss();
    }

    public interface OnCloseButtonClickListener {
        void onClose(WaitingDialog waitingDialog);
        void onOk(WaitingDialog waitingDialog);
        void onClickRate(WaitingDialog waitingDialog);
    }

}
