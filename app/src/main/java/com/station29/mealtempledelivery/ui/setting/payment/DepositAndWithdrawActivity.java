package com.station29.mealtempledelivery.ui.setting.payment;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.api.Constant;
import com.station29.mealtempledelivery.api.request.MockupData;
import com.station29.mealtempledelivery.api.request.ProfileWs;
import com.station29.mealtempledelivery.data.model.User;
import com.station29.mealtempledelivery.data.model.Wallets;
import com.station29.mealtempledelivery.ui.base.BaseActivity;
import com.station29.mealtempledelivery.utils.MenuStoreFragmentsPagerAdapter;
import com.station29.mealtempledelivery.utils.Util;

import java.util.List;
import java.util.Objects;

public class DepositAndWithdrawActivity extends BaseActivity {

    private ImageView backButton;
    private TextView title;
    public static ViewPager viewPager;
    private TabLayout tabLayout;
    private List<Wallets> walletsList;
    private User user;
    private int size;
    private View progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(getSupportActionBar()).hide();
        setContentView(R.layout.activity_deposit_withdraw);

        initView();
        getProfile();
    }

    private void initView(){
        backButton = findViewById(R.id.backButton);
        title = findViewById(R.id.toolBarTitle);
        viewPager = findViewById(R.id.view_pager_deposit_withdraw);
        tabLayout = findViewById(R.id.tab_deposit_withdraw);
        progress = findViewById(R.id.progress);
    }

    private void initAction(){

        title.setText(R.string.deposit_and_withdraw);
        backButton.setOnClickListener(view -> {
            setResult(RESULT_OK);
            finish();
        });

        MenuStoreFragmentsPagerAdapter menuStoreFragmentsPagerAdapters = new MenuStoreFragmentsPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter (menuStoreFragmentsPagerAdapters);
        tabLayout.setupWithViewPager ( viewPager );

        boolean checkScreenType = true;
        for (String s : MockupData.getListTabPaymentDepositWithDraw(DepositAndWithdrawActivity.this)) {
            DepositAndWithdrawFragment depositAndWithDrawFragment;
            if (checkScreenType){
                checkScreenType = false;
                depositAndWithDrawFragment = DepositAndWithdrawFragment.newInstance(walletsList,user,size, Constant.DEPOSIT);
            } else {
                depositAndWithDrawFragment = DepositAndWithdrawFragment.newInstance(walletsList,user,size,Constant.WITHDRAW);
            }
            menuStoreFragmentsPagerAdapters.addFragment(depositAndWithDrawFragment,s);
        }

        menuStoreFragmentsPagerAdapters.notifyDataSetChanged();
    }

    private void getProfile(){
        progress.setVisibility(View.VISIBLE);
        new ProfileWs().viewProfiles(DepositAndWithdrawActivity.this, new ProfileWs.ProfileViewAndUpdate() {
            @Override
            public void onLoadProfileSuccess(User myUser, String message) {
                progress.setVisibility(View.GONE);
                user = myUser;
                walletsList  = myUser.getWalletsList();
                size = Util.checkVisiblePaymentService();
                initAction();
            }

            @Override
            public void onError(String errorMessage) {
                progress.setVisibility(View.GONE);
                Util.popupMessage("",errorMessage,DepositAndWithdrawActivity.this);
            }

            @Override
            public void onUpdateProfilesSuccess(String message) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_OK);
        finish();
    }
}
