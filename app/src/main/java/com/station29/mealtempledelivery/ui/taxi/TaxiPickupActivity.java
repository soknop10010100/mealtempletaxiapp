package com.station29.mealtempledelivery.ui.taxi;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


import com.ebanx.swipebtn.SwipeButton;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.api.Constant;
import com.station29.mealtempledelivery.api.request.DeliveryWs;
import com.station29.mealtempledelivery.data.model.Delivery;
import com.station29.mealtempledelivery.data.model.TaxiDeliveryDetail;
import com.station29.mealtempledelivery.data.model.TaxiModel;
import com.station29.mealtempledelivery.data.model.Waypoint;
import com.station29.mealtempledelivery.service.LocationUpdatesService;
import com.station29.mealtempledelivery.ui.MainActivity;
import com.station29.mealtempledelivery.ui.home.WaitingDialog;
import com.station29.mealtempledelivery.utils.CompleteActivity;
import com.station29.mealtempledelivery.utils.PickupActivity;
import com.station29.mealtempledelivery.utils.Util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.station29.mealtempledelivery.utils.Util.isConnectedToInternet;
import static com.station29.mealtempledelivery.utils.Util.popupMessage;

public class TaxiPickupActivity extends TaxiBaseActivity {

    private WaitingDialog dialog;
    private SwipeButton btnSwipe;

    @Override
    protected Drawable getSwipeButtonColor() {
        return getResources().getDrawable(R.drawable.on_swipe_shape_yellow);
    }

    @Override
    protected String getSwipeButtonText() {
        return getString(R.string.slide_to_pickup);
    }

    @Override
    protected String visibleBtnCancel() {
        return Constant.PICKED_UP;
    }

    @Override
    protected void onSlideCompleted(SwipeButton swipeButton, boolean active) {
        String status = Constant.PICKED_UP;
        btnSwipe  = swipeButton;
        if (getIntent() != null && getIntent().hasExtra("deliveryId") && getIntent().hasExtra("typeOrder")) {
            int deliveryId = getIntent().getIntExtra("deliveryId", -1);
            String typeOrder = getIntent().getStringExtra("typeOrder");
            int orderId = getIntent().getIntExtra("orderId",0);
            updateStatusTask(deliveryId, status, typeOrder, btnSwipe, 0);
        }
    }

    @Override
    protected void onPhoneCallClick(ImageView buttonCall) {
        String typeOrder = getIntent().getStringExtra("typeOrder");
        if (taxiDeliveryDetail == null) return;
        String phoneNumber;
        if ("send".equalsIgnoreCase(typeOrder)) {
            phoneNumber = taxiDeliveryDetail.getSendPackage().getPhoneSender();
        } else {
            phoneNumber = taxiDeliveryDetail.getPhoneNumber();
        }
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:" + phoneNumber));
        callIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(callIntent);
    }

    @Override
    protected void onDrawRoute(GoogleMap map, Waypoint pickupWaypoint, Waypoint waypoint) {
        drawMap(map, pickupWaypoint);
    }

    private void drawMap(GoogleMap mMap, Waypoint waypoint) {
        ArrayList<LatLng> routelist = new ArrayList<>();
        if (waypoint != null) {
            double lat, lng;
            if (waypoint.getArrJsonLatLng().size() > 0) {
                for (int i = 0; waypoint.getArrJsonLatLng().size() > i; i++) {
                    lat = waypoint.getArrJsonLatLng().get(i).getLatitute();
                    lng = waypoint.getArrJsonLatLng().get(i).getLongitute();
                    routelist.add(new LatLng(lat, lng));
                }
            }
        }
        if (routelist.size() > 0) {
            PolylineOptions rectLine = new PolylineOptions().width(5).color(Color.parseColor("#E62138"));//set line color to app color
            for (int i = 0; i < routelist.size(); i++) {
                rectLine.add(routelist.get(i));
            }
            if (mMap != null) {
                MarkerOptions markerPickUP = new MarkerOptions();
                MarkerOptions markerDriver = new MarkerOptions();
                markerPickUP.position(routelist.get(routelist.size() - 1));
                markerDriver.position(routelist.get(0));
                markerDriver.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_taxi));
                markerPickUP.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_icon_pickup));
                mMap.addMarker(markerPickUP);
                mMap.addMarker(markerDriver);
                // Adding route on the map
                mMap.addPolyline(rectLine);
                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                builder.include(markerPickUP.getPosition());
                builder.include(markerDriver.getPosition());
                LatLngBounds bounds = builder.build();
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, getResources().getDisplayMetrics().widthPixels, getResources().getDisplayMetrics().heightPixels / 2, (int) (getResources().getDisplayMetrics().widthPixels * 0.18));
                mMap.animateCamera(cameraUpdate);
            }

        }
    }

    private final WaitingDialog.OnCloseButtonClickListener onCloseButtonClickListener = new WaitingDialog.OnCloseButtonClickListener() {
        @Override
        public void onClose(WaitingDialog dialog) {
            finish();
            refresh();
        }

        @Override
        public void onOk(WaitingDialog waitingDialog) {
        }

        @Override
        public void onClickRate(WaitingDialog waitingDialog) {

        }
    };

    private void refresh(){
        Intent intent = new Intent(TaxiPickupActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(dialog != null && dialog.isShowing()){
            dialog.cancel();
        }
    }

    private void updateStatusTask(final int deliveryId, String mStatus, final String typeOrder, final SwipeButton button , int cooperation) {

        if (Constant.getBaseUrl() != null) {
            button.setVisibility(View.VISIBLE);
            if (mStatus != null && mStatus.equalsIgnoreCase(Constant.ACCEPTED)) {
                mStatus = Constant.PICKED_UP;
            } else if (mStatus != null && mStatus.equalsIgnoreCase(Constant.PICKED_UP)) {
                button.setVisibility(View.VISIBLE);
                mStatus = Constant.PICKED_UP;
            }

            dialog = new WaitingDialog(TaxiPickupActivity.this, onCloseButtonClickListener);
            if (!isFinishing())
                dialog.show();
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("delivery_id", deliveryId);
            hashMap.put("status", mStatus);
            hashMap.put("cooperation", cooperation);
            hashMap.put("start_latitute", taxiDeliveryDetail.getPickUpLat());
            hashMap.put("start_longitute", taxiDeliveryDetail.getPickUpLong());
            hashMap.put("end_latitute", taxiDeliveryDetail.getShippingLat());
            hashMap.put("end_longitute", taxiDeliveryDetail.getShippingLong());

            new DeliveryWs().acceptDelivery(this, typeOrder, hashMap, new DeliveryWs.OnAcceptDeliveryListener() {
                @Override
                public void onLoadListSuccess(List<Delivery> deliveryList) {
                }

                @Override
                public void onAcceptDeliverySuccess(Waypoint waypoint) {
                }

                @Override
                public void onAcceptSuccessTaxi(Waypoint waypoint) {
                    Intent intent = new Intent(TaxiPickupActivity.this, TaxiCompleteActivity.class);
                    intent.putExtra("deliveryId", deliveryId);
                    intent.putExtra("updateStatus", Constant.DELIVERED);
                    intent.putExtra("typeOrder", typeOrder);
                    // =======================================================================
                    latLngList.clear();
                    Util.clearFile(TaxiPickupActivity.this);
                    Util.startRecordLocation(TaxiPickupActivity.this);
                    Util.updateServiceInterval(TaxiPickupActivity.this, LocationUpdatesService.TRACKING_INTERVAL);
                    // =======================================================================
                    startActivity(intent);
                    finish();
                }

                @Override
                public void onAcceptFailed(String message) {
                    if (!isConnectedToInternet (TaxiPickupActivity.this)){
                        popupMessage ( null, getString(R.string.check_yor) , TaxiPickupActivity.this);
                        dialog.onDismiss();
                    }else {
                        Util.popupMessageAndClose(null, message, TaxiPickupActivity.this);
                    }
                    button.toggleState();
                    dialog.onDismiss();

                }
            });
        }
    }
}