package com.station29.mealtempledelivery.ui.kyc

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.activity.result.ActivityResult
import com.bumptech.glide.Glide
import com.squareup.picasso.Picasso
import com.station29.mealtempledelivery.R
import com.station29.mealtempledelivery.api.request.KYCWs
import com.station29.mealtempledelivery.api.request.MockupData
import com.station29.mealtempledelivery.ui.base.BaseActivity
import com.station29.mealtempledelivery.ui.base.BaseCameraActivity
import com.station29.mealtempledelivery.utils.FileUtil
import java.io.File

class UploadKYCActivity : BaseActivity() {

    private var imgBackPath : String = ""
    private var imgFrontPath : String = ""
    private var imgSelfiePath : String = ""
    private lateinit var frontPhoto : ImageView
    private lateinit var backPhoto : ImageView
    private lateinit var selfiePhoto : ImageView
    private lateinit var dgIdentityType : RadioGroup
    private var identity = -1
    private lateinit var progressBar : View
    private var action = ""
    private var imageAction = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_upload_kycactivity)

        findViewById<TextView>(R.id.toolBarTitle).text =  getString(R.string.upload_kyc)
        findViewById<ImageView>(R.id.backButton).setOnClickListener { finish() }

        frontPhoto = findViewById(R.id.img_front)
        backPhoto = findViewById(R.id.img_back)
        selfiePhoto = findViewById(R.id.img_selfie)
        dgIdentityType = findViewById(R.id.dgIdentityType)
        progressBar = findViewById(R.id.progress_bar)

        findViewById<Button>(R.id.btnStartVerification).setOnClickListener {
            startVerify()
        }

        findViewById<RelativeLayout>(R.id.front_photo).setOnClickListener {
            startCameraResult("front")
        }

        findViewById<RelativeLayout>(R.id.back_photo).setOnClickListener {
            startCameraResult("back")
        }

        findViewById<RelativeLayout>(R.id.selfie_photo).setOnClickListener {
            startCameraResult("selfie")
        }

        dgIdentityType.setOnCheckedChangeListener { group, _ ->
            val radioBtnID: Int = group.checkedRadioButtonId
            val radioB: View = group.findViewById(radioBtnID)
            identity = group.indexOfChild(radioB)
        }
    }

    private fun startVerify() {
        when {
            identity == -1 -> {
                Toast.makeText(this,getString(R.string.please_select_identity_type),Toast.LENGTH_SHORT).show()
            }
            imgFrontPath == "" -> {
                Toast.makeText(this,getString(R.string.please_upload_front_side_image),Toast.LENGTH_SHORT).show()
            }
            imgBackPath == "" -> {
                Toast.makeText(this,getString(R.string.please_upload_back_side_image),Toast.LENGTH_SHORT).show()
            }
            imgSelfiePath == "" -> {
                Toast.makeText(this,getString(R.string.please_upload_selfie_image),Toast.LENGTH_SHORT).show()
            }
            else -> {
                progressBar.visibility = View.VISIBLE
                val body  = HashMap<String,Any>()
                body["front"] = imgFrontPath
                body["back"] = imgBackPath
                body["selfie"] = imgSelfiePath

                if (identity == 0) {
                    body["identity_type"] = "NID"
                } else if (identity == 1) {
                    body["identity_type"] = "POP"
                }

                if (MockupData.getUser() != null && MockupData.getUser().kyc != null && MockupData.getUser().kyc.status == "REJECT") {
                    action = "update"
                }
                KYCWs().uploadKYC(this@UploadKYCActivity ,action,body, object : KYCWs.OnCallBackUploadKYC{
                    override fun onSuccess(message: String) {
                        progressBar.visibility = View.GONE
                        val intent = Intent(this@UploadKYCActivity, IdentityCompleteActivity::class.java)
                        activityLauncher.launch(intent) { result: ActivityResult ->
                            if (result.resultCode == RESULT_OK) {
                                setResult(RESULT_OK)
                                finish()
                            }
                        }
                    }

                    override fun onFailed(message: String) {
                        progressBar.visibility = View.GONE
                        Toast.makeText(this@UploadKYCActivity, message, Toast.LENGTH_SHORT).show()
                    }
                })
            }
        }
    }

    private fun startCameraResult(action: String) {
            activityLauncher.launch(
                Intent(this@UploadKYCActivity, BaseCameraActivity::class.java)
            ) { result: ActivityResult ->
                if (result.resultCode == RESULT_OK) {
                    val data = result.data
                    val mCameraFileName: String
                    if (data != null) {
                        if (data.hasExtra("image_path")) {
                            val imagePath = data.getStringExtra("image_path")
                            val uri = Uri.parse(imagePath)
                            val actualImage =
                                FileUtil.from(this, uri)
                            when (action) {
                                "selfie" -> {
                                    imgSelfiePath = actualImage.absolutePath
                                    Picasso.get().load(actualImage).into(selfiePhoto)
                                }
                                "front" -> {
                                    imgFrontPath = actualImage.absolutePath
                                    Picasso.get().load(actualImage).into(frontPhoto)
                                }
                                "back" -> {
                                    imgBackPath = actualImage.absolutePath
                                    Picasso.get().load(actualImage).into(backPhoto)
                                }
                            }
                        }
                        if (data.hasExtra("select_image")) {
                            val imagePath = data.getStringExtra("select_image")
                            val uri = Uri.parse(imagePath)
                            mCameraFileName = uri.toString()
                            val actualFile = File(mCameraFileName)
                            val loadedBitmap = BaseCameraActivity.getImageGallery(uri, this)
                            when (action) {
                                "selfie" -> {
                                    imgSelfiePath = actualFile.absolutePath
                                    Glide.with(this).load(loadedBitmap).into(selfiePhoto)
                                }
                                "front" -> {
                                    imgFrontPath = actualFile.absolutePath
                                    Glide.with(this).load(loadedBitmap).into(frontPhoto)
                                }
                                "back" -> {
                                    imgBackPath = actualFile.absolutePath
                                    Glide.with(this).load(loadedBitmap).into(backPhoto)
                                }
                            }
                        }
                    }
                }
            }
    }

}