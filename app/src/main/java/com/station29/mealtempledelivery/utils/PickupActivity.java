package com.station29.mealtempledelivery.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.core.app.ActivityCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.ebanx.swipebtn.OnStateChangeListener;
import com.ebanx.swipebtn.SwipeButton;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.api.Constant;
import com.station29.mealtempledelivery.api.request.DeliveryWs;
import com.station29.mealtempledelivery.data.model.Delivery;
import com.station29.mealtempledelivery.data.model.DeliveryDetails;
import com.station29.mealtempledelivery.data.model.ServiceModel;
import com.station29.mealtempledelivery.data.model.TaxiDeliveryDetail;
import com.station29.mealtempledelivery.data.model.Waypoint;
import com.station29.mealtempledelivery.service.LocationUpdatesService;
import com.station29.mealtempledelivery.ui.base.BaseActivity;
import com.station29.mealtempledelivery.ui.MainActivity;
import com.station29.mealtempledelivery.ui.home.OrderDetailActivity;
import com.station29.mealtempledelivery.ui.home.WaitingDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.station29.mealtempledelivery.utils.Util.isConnectedToInternet;
import static com.station29.mealtempledelivery.utils.Util.logDebug;
import static com.station29.mealtempledelivery.utils.Util.popupMessage;

public class PickupActivity extends BaseActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private int deliveryId;
    private DeliveryDetails mDetail;
    private ActionBar actionBar;
    private String mStatus;
    private TextView storePhone, clientPhone, storeName, totalPrice, deliveryFee, riderTip;
    private SwipeButton pickUp;
    private final int REQUEST_CODE_COMPLETED = 567;
    private MyReceiver myReceiver;
    String typeOrder = "Delivery";
    private ServiceModel serviceModel;
    SupportMapFragment mapFragment;
    private Waypoint waypoint;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pickup);
        myReceiver = new MyReceiver();
        storeName = findViewById(R.id.storeNamePick);

        storePhone = findViewById(R.id.storePhonePick);
        clientPhone = findViewById(R.id.clientPhonePick);

        actionBar = this.getSupportActionBar();
        ImageButton btnBack = findViewById(R.id.btnBackPickUp);
        ImageButton btnCancelService = findViewById(R.id.btnCancelService);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refresh();
                finish();
            }
        });

        btnCancelService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Constant.getBaseUrl()!=null)     Util.popUpCancelOrder(getString(R.string.cancel),PickupActivity.this,mDetail.getOrderId());
            }
        });

        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        if (mapFragment != null) mapFragment.getMapAsync(PickupActivity.this);

        final ShimmerFrameLayout shimmerFrameLayout = findViewById(R.id.shimmer);
        shimmerFrameLayout.startShimmer();

        pickUp = findViewById(R.id.pickUp);
        pickUp.setOnStateChangeListener(new OnStateChangeListener() {
            @Override
            public void onStateChange(boolean active) {
                if (!active) return;
                shimmerFrameLayout.hideShimmer();
                if (mDetail == null) {
                    return;
                }
                updateStatusTask(deliveryId, pickUp, mDetail.getCooperation());
                shimmerFrameLayout.showShimmer(true);
            }
        });
        if (getIntent() != null && getIntent().hasExtra("updateStatus") && getIntent().hasExtra("deliveryId")) {
            //main screen(home fragment)
            deliveryId = (int) getIntent().getSerializableExtra("deliveryId");
            mStatus = getIntent().getStringExtra("updateStatus");
            if (getIntent().hasExtra("typeOrder")) {
                typeOrder = (String) getIntent().getSerializableExtra("typeOrder");
            }
        }
        if (getIntent() != null && getIntent().hasExtra("serviceModel")) {
            serviceModel = (ServiceModel) getIntent().getSerializableExtra("serviceModel");
            if (serviceModel == null) return;
            deliveryId = serviceModel.getDeliveryId();

        }
        if (getIntent() != null && getIntent().hasExtra("waypoint")) {
            waypoint = (Waypoint) getIntent().getSerializableExtra("waypoint");

        }
        if (Util.getString("waypointSave", PickupActivity.this) != null) {
            try {
                JSONObject json = new JSONObject(Util.getString("waypointSave", PickupActivity.this));
                waypoint = new Gson().fromJson(String.valueOf(json), Waypoint.class);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (mStatus != null ) getDeliveryDetailTask();

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            refresh();
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        zoomMap();
        if(waypoint!=null)
        drawMap(waypoint,mMap);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
        if (mDetail != null && mDetail.getShippingLat() != null && mDetail.getShippingLng() != null) {
            mMap.addMarker(new MarkerOptions().position(new LatLng(mDetail.getShippingLat(), mDetail.getShippingLng())).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_start)));
        }

        View locationButton = ((View) mapFragment.getView().findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
        RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
        // position on right bottom
        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
        rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        rlp.setMargins(0, 0, 30, 30);
    }


    private void zoomMap() {
        if (LocationUpdatesService.mLocation != null && mMap != null) {
            LatLng sydney = new LatLng(LocationUpdatesService.mLocation.getLatitude(), LocationUpdatesService.mLocation.getLongitude());
            CameraUpdate location = CameraUpdateFactory.newLatLngZoom(sydney, 15);
            mMap.moveCamera(location);
        }
    }
    //Get Delivery
    private void getDeliveryDetailTask() {
        if (Constant.getBaseUrl() != null)
            new DeliveryWs().readDelivery(deliveryId, typeOrder, this, new DeliveryWs.ReadDeliveryListener() {
                @SuppressLint("StringFormatInvalid")
                @Override
                public void onLoadDeliverySuccess(final DeliveryDetails details) {
                    mDetail = details;
                    if (waypoint != null) {
                        drawMap(waypoint,mMap);
                    }
                    if (mDetail != null && mMap != null && mDetail.getShippingLng() != null && mDetail.getShippingLng() != null) {
                        mMap.addMarker(new MarkerOptions().position(new LatLng(mDetail.getShippingLat(), mDetail.getShippingLng())).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_start)));
                    }
                    if (actionBar != null) {
                        actionBar.setTitle(String.format("%s %s", getString(R.string.from), mDetail.getStoreName()));
                    }
                    storePhone.setText(String.format("%s %s", getString(R.string.store), details.getTel()));
                    clientPhone.setText(String.format("%s %s",getString(R.string.customers), details.getShippingPhone()));

                    clientPhone.setMovementMethod(LinkMovementMethod.getInstance());

                    final Button viewMenu = findViewById(R.id.viewMenuPick);
                    viewMenu.setOnClickListener(v -> {
                        Intent intent = new Intent(PickupActivity.this, OrderDetailActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        intent.putExtra("id", deliveryId);
                        intent.putExtra("back","back");
                        startActivity(intent);
                    });

                    clientPhone.setOnClickListener(view -> {
                        Intent callIntent = new Intent(Intent.ACTION_DIAL);
                        callIntent.setData(Uri.parse("tel:" + details.getShippingPhone()));
                        callIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(callIntent);

                    });

                    storePhone.setOnClickListener(view -> {
                        Intent callIntent = new Intent(Intent.ACTION_DIAL);
                        callIntent.setData(Uri.parse("tel:" + details.getTel()));
                        callIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(callIntent);
                    });

                }

                @Override
                public void onLoadTaxiDetailSuccess(TaxiDeliveryDetail deliveryDetail) {

                }

                @Override
                public void onLoadFail(String message) {
                    Util.popupMessage(null,message,PickupActivity.this);
                }
            });
    }


    private final WaitingDialog.OnCloseButtonClickListener onCloseButtonClickListener = new WaitingDialog.OnCloseButtonClickListener() {
        @Override
        public void onClose(WaitingDialog dialog) {
            finish();
        }

        @Override
        public void onOk(WaitingDialog waitingDialog) {}

        @Override
        public void onClickRate(WaitingDialog waitingDialog) {

        }
    };
    //Slide PickUp
    private void updateStatusTask(final int deliveryId, final SwipeButton button, int cooperation) {

        if (Constant.getBaseUrl() != null) {
            button.setVisibility(View.VISIBLE);
            if (mStatus != null && mStatus.equalsIgnoreCase(Constant.ACCEPTED)) {
                mStatus = Constant.PICKED_UP;
            } else if (mStatus != null && mStatus.equalsIgnoreCase(Constant.PICKED_UP)) {
                button.setVisibility(View.VISIBLE);
                mStatus = Constant.DELIVERED;
            }
            logDebug("statusSend", mStatus);
            final WaitingDialog dialog = new WaitingDialog(PickupActivity.this, onCloseButtonClickListener);
            dialog.show();
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("delivery_id", deliveryId);
            hashMap.put("status", mStatus);
            hashMap.put("cooperation", cooperation);
            hashMap.put("start_latitute", mDetail.getStoreLat());
            hashMap.put("start_longitute", mDetail.getStoreLng());
            hashMap.put("end_latitute", mDetail.getShippingLat());
            hashMap.put("end_longitute", mDetail.getShippingLng());

            new DeliveryWs().acceptDelivery(this, typeOrder, hashMap, new DeliveryWs.OnAcceptDeliveryListener() {
                @Override
                public void onLoadListSuccess(List<Delivery> deliveryList) {
                }

                @Override
                public void onAcceptDeliverySuccess(Waypoint waypoint) {
                    Intent intent = new Intent(PickupActivity.this, CompleteActivity.class);
                    intent.putExtra("deliveryId", deliveryId);
                    intent.putExtra("updateStatus", Constant.DELIVERED);
                    intent.putExtra("serviceModel", serviceModel);
                    intent.putExtra("typeOrder", typeOrder);
                    Util.clearString(PickupActivity.this, "waypointSave");//clear waypoint locale
                    startActivity(intent);
                    finish();
                }

                @Override
                public void onAcceptSuccessTaxi(Waypoint waypoint) {

                }

                @Override
                public void onAcceptFailed(String message) {
                    if (!isConnectedToInternet (PickupActivity.this)){
                        popupMessage ( null, getString(R.string.check_yor) , PickupActivity.this);
                        dialog.onDismiss();

                    } else {
                        Util.popupMessage(null, message, PickupActivity.this);
                    }
                    dialog.onDismiss();
                    pickUp.toggleState();
                }
            });
        }
    }

    //draw way point
    private void drawMap(Waypoint waypoint,GoogleMap mMap) {
        ArrayList<LatLng> routelist = new ArrayList<>();
        double lat, lng;
        if (waypoint.getArrJsonLatLng().size() > 0) {
            for (int i = 0; waypoint.getArrJsonLatLng().size() > i; i++) {
                lat = waypoint.getArrJsonLatLng().get(i).getLatitute();
                lng = waypoint.getArrJsonLatLng().get(i).getLongitute();
                routelist.add(new LatLng(lat, lng));
            }
        }
        if (routelist.size() > 0) {
            PolylineOptions rectLine = new PolylineOptions().width(5).color(Color.parseColor("#E62138"));//set line color to app color
            for (int i = 0; i < routelist.size(); i++) {
                rectLine.add(routelist.get(i));
            }
            if (mMap != null) {
                MarkerOptions markerOptions = new MarkerOptions();
                MarkerOptions markerOptions1 = new MarkerOptions();
                markerOptions.position(routelist.get(routelist.size() - 1));
                markerOptions1.position(routelist.get(0));
                markerOptions1.icon(BitmapDescriptorFactory.fromResource(R.drawable.motor));
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_store_marker));
                mMap.addMarker(markerOptions);
                mMap.addMarker(markerOptions1);
                // Adding route on the map
                mMap.addPolyline(rectLine);
                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                builder.include(markerOptions.getPosition());
                builder.include(markerOptions1.getPosition());
                LatLngBounds bounds = builder.build();
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, getResources().getDisplayMetrics().widthPixels, getResources().getDisplayMetrics().heightPixels / 2, (int) (getResources().getDisplayMetrics().widthPixels * 0.18));
                mMap.animateCamera(cameraUpdate);
            }

        }
    }

    //refresh screen when have intent
    public void refresh() {
        Intent intent = new Intent(PickupActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(myReceiver,
                new IntentFilter(LocationUpdatesService.ACTION_BROADCAST));
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(myReceiver);
        super.onPause();
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            refresh();
        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * Receiver for broadcasts sent by {@link LocationUpdatesService}.
     */
    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Location location = intent.getParcelableExtra(LocationUpdatesService.EXTRA_LOCATION);
            if (location != null) {
                logDebug("location222", location.getLatitude() + " : " + location.getLongitude());
                //   driverMarker.setPosition(new LatLng(location.getLatitude(),location.getLongitude()));
            }
        }
    }
}
