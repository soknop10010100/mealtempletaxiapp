package com.station29.mealtempledelivery.api.request;

import android.app.Activity;
import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.api.Constant;
import com.station29.mealtempledelivery.api.CustomCallback;
import com.station29.mealtempledelivery.api.CustomResponseListener;
import com.station29.mealtempledelivery.api.RetrofitGenerator;
import com.station29.mealtempledelivery.api.ServiceAPI;
import com.station29.mealtempledelivery.data.model.User;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import okhttp3.FormBody;
import okhttp3.RequestBody;
import retrofit2.Call;

public class ProfileWs {

    public void viewProfiles(final Activity activity, final ProfileViewAndUpdate getUserProfile) {
        ServiceAPI serviceAPI = RetrofitGenerator.createServiceWithAuth(activity);
        final retrofit2.Call<JsonElement> res = serviceAPI.viewProfile();
        res.enqueue(new CustomCallback(activity,new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("success") && resObj.get("success").getAsBoolean()) {
                    JsonElement jsonElement = resObj.getAsJsonObject("data");
                    User user = new Gson().fromJson(jsonElement, User.class);
                    MockupData.setUser(user);
                    getUserProfile.onLoadProfileSuccess(user,"");
                } else {
                    getUserProfile.onError("Invaild Data");
                }
            }

            @Override
            public void onError(String error, int code) {
                getUserProfile.onError(error);
            }
        }));
    }

    public void viewProfiles(Context activity, final ProfileViewAndUpdate getUserProfile) {
        ServiceAPI serviceAPI = RetrofitGenerator.createServiceWithAuth(activity);
        final retrofit2.Call<JsonElement> res = serviceAPI.viewProfile();
        res.enqueue(new CustomCallback(activity,new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("success") && resObj.get("success").getAsBoolean()) {
                    JsonElement jsonElement = resObj.getAsJsonObject("data");
                    User user = new Gson().fromJson(jsonElement, User.class);
                    MockupData.setUser(user);
                    getUserProfile.onLoadProfileSuccess(user,"");
                } else {
                    getUserProfile.onError("Invaild Data");
                }
            }

            @Override
            public void onError(String error, int code) {
                getUserProfile.onError(error);
            }
        }));
    }

    public void changeProfile(Activity activity, HashMap<String, String> image, final ProfileViewAndUpdate getPictureView) {
        ServiceAPI serviceAPI = RetrofitGenerator.createServiceWithAuth(activity);
        FormBody.Builder bodyBuilder = new FormBody.Builder();
        Iterator it = image.entrySet().iterator();
        Map.Entry pair = (Map.Entry) it.next();
        bodyBuilder.add((String) pair.getKey(), (String) pair.getValue());
        bodyBuilder.add("function", "Driver");
        it.remove();
        RequestBody requestBody = bodyBuilder.build();
        retrofit2.Call<JsonElement> res = serviceAPI.changeProfile(requestBody);
        res.enqueue(new CustomCallback(activity,new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                getPictureView.onUpdateProfilesSuccess("");
            }

            @Override
            public void onError(String error , int code) {
                getPictureView.onError(error);
            }
        }));
    }

    public void updateStatus(Activity activity, final int on, final ProfileWs.ReadOnOffLine onOffLine) {
        ServiceAPI service = RetrofitGenerator.createServiceWithAuth(activity);
        final HashMap<String, Object> param = new HashMap<>();
        param.put("online", on);
        param.put("action_type","update_online");
        Call<JsonElement> res = service.updateStatus(param);
        res.enqueue(new CustomCallback(activity,new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("success") && resObj.get("success").getAsBoolean()) {
                    if (resObj.has("message")) {
                        String successMsg = resObj.get("message").getAsString();
                        if(Constant.serviceLanguage.containsKey(successMsg))
                            onOffLine.onLoadSuccess(Constant.serviceLanguage.get(successMsg));
                        else onOffLine.onLoadSuccess(successMsg);
                    }
                }
            }

            @Override
            public void onError(String error , int code) {
                onOffLine.onLoadFail(error);
            }
        }));
    }

    public void updateProfiles(final Activity activity, HashMap<String,Object> mapUpdateProfile, final ProfileViewAndUpdate profileViewAndUpdate ){
        ServiceAPI serviceAPI = RetrofitGenerator.createServiceWithAuth ( activity );
        final retrofit2.Call<JsonElement> res = serviceAPI.updateProfile ( mapUpdateProfile );
        res.enqueue(new CustomCallback(activity, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                String message = activity.getString(R.string.successfull);
                if (resObj.has("message")) {
                    if (Constant.serviceLanguage != null && Constant.serviceLanguage.containsKey(resObj.get("message").getAsString())) {
                        message = Constant.serviceLanguage.get(resObj.get("message").getAsString());
                    } else {
                        message = resObj.get("message").getAsString().replaceAll("_"," ");
                    }
                }
                profileViewAndUpdate.onUpdateProfilesSuccess (message);
            }

            @Override
            public void onError(String error, int responseCode) {
                profileViewAndUpdate.onError(error);
            }
        }));
    }

    public void deleteAccountUser (Activity activity , HashMap<String ,Object> hashMap, ReadOnOffLine deleteAccountCallBack){
        ServiceAPI serviceAPI = RetrofitGenerator.createServiceWithAuth(activity);
        Call<JsonElement> res = serviceAPI.updateProfile(hashMap);
        res.enqueue(new CustomCallback(activity, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                deleteAccountCallBack.onLoadSuccess(resObj.get("message").getAsString());
            }

            @Override
            public void onError(String error , int code) {
                deleteAccountCallBack.onLoadFail(error);
            }
        }));
    }

    public interface ProfileViewAndUpdate {
        void onLoadProfileSuccess(User user, String message);
        void onError(String errorMessage);
        void onUpdateProfilesSuccess(String message);
    }

    public interface ReadOnOffLine {
        void onLoadSuccess(String message);
        void onLoadFail(String message);
    }

}
