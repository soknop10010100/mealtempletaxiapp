package com.station29.mealtempledelivery.ui.setting;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextThemeWrapper;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.api.Constant;
import com.station29.mealtempledelivery.api.request.MockupData;
import com.station29.mealtempledelivery.ui.base.BaseActivity;
import com.station29.mealtempledelivery.ui.SplashScreenActivity;
import com.station29.mealtempledelivery.ui.setting.adapter.LanguagesAdapter;
import com.station29.mealtempledelivery.utils.Util;

import java.util.Locale;

import static com.station29.mealtempledelivery.api.Constant.LANG_AR;
import static com.station29.mealtempledelivery.api.Constant.LANG_CH;
import static com.station29.mealtempledelivery.api.Constant.LANG_EN;
import static com.station29.mealtempledelivery.api.Constant.LANG_FR;
import static com.station29.mealtempledelivery.api.Constant.LANG_GM;
import static com.station29.mealtempledelivery.api.Constant.LANG_HE;
import static com.station29.mealtempledelivery.api.Constant.LANG_KH;
import static com.station29.mealtempledelivery.api.Constant.LANG_KO;
import static com.station29.mealtempledelivery.api.Constant.LANG_LA;
import static com.station29.mealtempledelivery.api.Constant.LANG_SB;
import static com.station29.mealtempledelivery.api.Constant.LANG_SP;
import static com.station29.mealtempledelivery.api.Constant.LANG_SW;

public class ChangLanguageActivity extends BaseActivity {

   private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.language));
        setContentView(R.layout.layout_list_lang);
        recyclerView = findViewById(R.id.list_languages);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        LanguagesAdapter languageAdapter = new LanguagesAdapter(MockupData.listLanguages(ChangLanguageActivity.this),itemClickOnLange,ChangLanguageActivity.this);
        recyclerView.setAdapter(languageAdapter);

        findViewById(R.id.cancel).setOnClickListener(view -> finish());
    }

    private final LanguagesAdapter.ItemClickOnLange itemClickOnLange = (lang, pos) -> popupMessageFin(getString(R.string.this_app_will_be_restarted),ChangLanguageActivity.this,lang);

    public void popupMessageFin(String message, Activity activity, String lang) {
            AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(activity, R.style.DialogTheme));
            builder.setMessage(message);
            builder.setNegativeButton(String.format(Locale.US, "%s", activity.getString(R.string.cancel)), (dialog, which) -> activity.finish());
            builder.setPositiveButton(String.format(Locale.US, "%s", activity.getString(R.string.ok)),
                    (dialog, which) ->
                            changeNow(activity,lang)  );
            builder.setCancelable(false);
            builder.show();

    }

    private void changeNow(Context mActivity, String lang){

        if(lang.equalsIgnoreCase(getString(R.string.khmer))){
            Util.changeLanguage(ChangLanguageActivity.this, LANG_KH);
            Constant.setState(LANG_KH);
        }else if(lang.equalsIgnoreCase(getString(R.string.english))){
            Constant.setState(LANG_EN);
            Util.changeLanguage(ChangLanguageActivity.this, LANG_EN);
        }else if(lang.equalsIgnoreCase(getString(R.string.france))){
            Constant.setState(LANG_FR);
            Util.changeLanguage(ChangLanguageActivity.this, LANG_FR);
        }else if(lang.equalsIgnoreCase(getString(R.string.laos))){
            Constant.setState(LANG_LA);
            Util.changeLanguage(ChangLanguageActivity.this, LANG_LA);
        }else if(lang.equalsIgnoreCase(getString(R.string.spainish))){
            Util.changeLanguage(ChangLanguageActivity.this, LANG_SP);
            Constant.setState(LANG_SP);
        }else if(lang.equalsIgnoreCase(getString(R.string.herbrew))){
            Constant.setState(LANG_HE);
            Util.changeLanguage(ChangLanguageActivity.this, LANG_HE);
        }else if(lang.equalsIgnoreCase(getString(R.string.swedish))){
            Constant.setState(LANG_SW);
            Util.changeLanguage(ChangLanguageActivity.this, LANG_SW);
        }else if(lang.equalsIgnoreCase(getString(R.string.german))){
            Constant.setState(LANG_GM);
            Util.changeLanguage(ChangLanguageActivity.this, LANG_GM);
        }else if(lang.equalsIgnoreCase(getString(R.string.serbian))){
            Constant.setState(LANG_SB);
            Util.changeLanguage(ChangLanguageActivity.this, LANG_SB);
        }else if(lang.equalsIgnoreCase(getString(R.string.chinese))){
            Constant.setState(LANG_CH);
            Util.changeLanguage(ChangLanguageActivity.this, LANG_CH);
        }else if(lang.equalsIgnoreCase(getString(R.string.arabic))){
            Constant.setState(LANG_AR);
            Util.changeLanguage(ChangLanguageActivity.this, LANG_AR);
        }else if(lang.equalsIgnoreCase(getString(R.string.korea))){
            Constant.setState(LANG_KO);
            Util.changeLanguage(ChangLanguageActivity.this, LANG_KO);
        } else {
            Constant.setState(LANG_EN);
            Util.changeLanguage(ChangLanguageActivity.this, LANG_EN);
        }
        Intent intent = new Intent(mActivity, SplashScreenActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        mActivity.startActivity(intent);
    }
}