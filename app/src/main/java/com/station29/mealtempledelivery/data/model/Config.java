package com.station29.mealtempledelivery.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Config implements Serializable {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("currency_code")
    @Expose
    private String currencyCode;
    @SerializedName("rider_tip")
    @Expose
    private String riderTip;
    @SerializedName("currency_name")
    @Expose
    private String currencyName;
    @SerializedName("currency_sign")
    @Expose
    private String currencySign;
    @SerializedName("service_time")
    @Expose
    private List<ServiceTime> serviceTime = null;
    @SerializedName("available_server")
    @Expose
    private List<String> availableServer = null;
    @SerializedName("service_type_driver")
    @Expose
    private List<ServiceTypeDriver> serviceTypeDriver = null;
    @SerializedName("colors")
    @Expose
    private List<String> colors = null;
    @SerializedName("local_name")
    @Expose
    private String localName;
    @SerializedName("local_full_name")
    @Expose
    private Object localFullName;
    @SerializedName("local_alias")
    @Expose
    private String localAlias;
    @SerializedName("local_abbr")
    @Expose
    private String localAbbr;
    @SerializedName("local_currency_name")
    @Expose
    private String localCurrencyName;
    @SerializedName("payment_service_providers")
    @Expose
    private List<PaymentService> PaymentServiceList;
    @SerializedName("decimal_places")
    @Expose
    private int decimalPlace;

    public int getDecimalPlace() {
        return decimalPlace;
    }

    public List<PaymentService> getPaymentServiceList() {
        return PaymentServiceList;
    }
    public void setPaymentServiceList(List<PaymentService> paymentServiceList) {
        PaymentServiceList = paymentServiceList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getRiderTip() {
        return riderTip;
    }

    public void setRiderTip(String riderTip) {
        this.riderTip = riderTip;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public String getCurrencySign() {
        return currencySign;
    }

    public void setCurrencySign(String currencySign) {
        this.currencySign = currencySign;
    }

    public List<ServiceTime> getServiceTime() {
        return serviceTime;
    }

    public void setServiceTime(List<ServiceTime> serviceTime) {
        this.serviceTime = serviceTime;
    }

    public List<String> getAvailableServer() {
        return availableServer;
    }

    public void setAvailableServer(List<String> availableServer) {
        this.availableServer = availableServer;
    }

    public List<ServiceTypeDriver> getServiceTypeDriver() {
        return serviceTypeDriver;
    }

    public void setServiceTypeDriver(List<ServiceTypeDriver> serviceTypeDriver) {
        this.serviceTypeDriver = serviceTypeDriver;
    }

    public List<String> getColors() {
        return colors;
    }

    public void setColors(List<String> colors) {
        this.colors = colors;
    }

    public String getLocalName() {
        return localName;
    }

    public void setLocalName(String localName) {
        this.localName = localName;
    }

    public Object getLocalFullName() {
        return localFullName;
    }

    public void setLocalFullName(Object localFullName) {
        this.localFullName = localFullName;
    }

    public String getLocalAlias() {
        return localAlias;
    }

    public void setLocalAlias(String localAlias) {
        this.localAlias = localAlias;
    }

    public String getLocalAbbr() {
        return localAbbr;
    }

    public void setLocalAbbr(String localAbbr) {
        this.localAbbr = localAbbr;
    }

    public String getLocalCurrencyName() {
        return localCurrencyName;
    }

    public void setLocalCurrencyName(String localCurrencyName) {
        this.localCurrencyName = localCurrencyName;
    }
}
