package com.station29.mealtempledelivery.ui.history

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.*
import androidx.fragment.app.DialogFragment
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.button.MaterialButton
import com.station29.mealtempledelivery.R

class FeedBackBottomSheetFragment : BottomSheetDialogFragment() {

    private var onClickAllAction :  OnClickFeedBack?=null
    private var rating : RatingBar? = null
    private var ratingComment : EditText? = null
    private lateinit var btnRate : MaterialButton
    private lateinit var title : TextView
    private lateinit var bodyTv : TextView
    private lateinit var action : String

    fun newInstance(type : String) : FeedBackBottomSheetFragment {
        val args = Bundle()
        val fragment = FeedBackBottomSheetFragment()
        args.putString("type",type)
        fragment.arguments = args
        return fragment
    }

    @SuppressLint("UseRequireInsteadOfGet")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val root =  inflater.inflate(R.layout.layout_feedback_dialog_fragment, container, false)
        dialog!!.window!!.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        return root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.BottomSheetDialogThemeNoFloating)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        if (dialog is BottomSheetDialog) {
            dialog.behavior.skipCollapsed = true
            dialog.behavior.state = BottomSheetBehavior.STATE_EXPANDED
        }
        return dialog
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rating = view.findViewById(R.id.rating)
        ratingComment = view.findViewById(R.id.reviewEt)
        bodyTv = view.findViewById(R.id.txtView2)
        btnRate = view.findViewById(R.id.btnRate)
        view.findViewById<ImageView>(R.id.closeButton).setOnClickListener { dismiss() }
        val closeButton = view.findViewById<LinearLayout>(R.id.close_btn)
        title = view.findViewById(R.id.title)


        if (arguments != null) {
            action = requireArguments().getString("type") as String
            when (action) {
                "customer" -> {
                    title.text = getString(R.string.review_customer)
                    bodyTv.text = getString(R.string.please_rate) +" "+ getString(R.string.cust)
                }
                "shop" -> {
                    title.text = getString(R.string.review_shop)
                    bodyTv.text = getString(R.string.please_rate)+" " + getString(R.string.shop)
                }
            }
        }

        btnRate.setOnClickListener {
            onClickAllAction?.onClickSubmit(ratingComment?.text.toString(),rating?.rating.toString().toFloat(),action)
            dismiss()
        }

        closeButton.setOnClickListener {
            dismiss()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val parent = parentFragment
        onClickAllAction = if (parent != null) {
            parent as OnClickFeedBack
        } else {
            context as OnClickFeedBack
        }
    }

    interface OnClickFeedBack {
        fun onClickSubmit(txtChoose: String?, position: Float , action : String)
    }
}