package com.station29.mealtempledelivery.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class OrderItem implements Serializable {

    @SerializedName("order_id")
    @Expose
    private Integer orderId;
    @SerializedName("store_name")
    @Expose
    private String storeName;
    @SerializedName("store_lat")
    @Expose
    private Double storeLat;
    @SerializedName("store_lng")
    @Expose
    private Double storeLng;
    @SerializedName("tel")
    @Expose
    private String tel;
    @SerializedName("order_number")
    @Expose
    private String orderNumber;
    @SerializedName("shipping_lat")
    @Expose
    private Double shippingLat;
    @SerializedName("shipping_lng")
    @Expose
    private Double shippingLng;
    @SerializedName("shipping_phone")
    @Expose
    private String shippingPhone;
    @SerializedName("order_time")
    @Expose
    private String orderTime;
    @SerializedName("delivery_time")
    @Expose
    private String deliveryTime;
    @SerializedName("sub_total")
    @Expose
    private String subTotal;
    @SerializedName("rider_tip")
    @Expose
    private String riderTip;
    @SerializedName("payment_type")
    @Expose
    private String paymentType;
    @SerializedName("cooperation")
    @Expose
    private int cooperation;
    @SerializedName("subtotal_formatted")
    @Expose
    private String subtotalFormatted;
    @SerializedName("subtotal_formatted_2")
    @Expose
    private String subtotalFormatted2;
    @SerializedName("delivery_fee")
    @Expose
    private String deliveryFee;
    @SerializedName("delivery_fee_formatted")
    @Expose
    private String deliveryFeeFormatted;
    @SerializedName("delivery_fee_formatted_2")
    @Expose
    private String deliveryFeeFormatted2;
    @SerializedName("total")
    @Expose
    private String total;
    @SerializedName("total_formatted")
    @Expose
    private String totalFormatted;
    @SerializedName("total_formatted_2")
    @Expose
    private String totalFormatted2;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("items")
    @Expose
    private List<OrderItem> items = null;

    @SerializedName("comment")
    @Expose
    private String comment;
    public String getComment() {
        return comment;
    }


    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public Double getStoreLat() {
        return storeLat;
    }

    public void setStoreLat(Double storeLat) {
        this.storeLat = storeLat;
    }

    public Double getStoreLng() {
        return storeLng;
    }

    public void setStoreLng(Double storeLng) {
        this.storeLng = storeLng;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Double getShippingLat() {
        return shippingLat;
    }

    public void setShippingLat(Double shippingLat) {
        this.shippingLat = shippingLat;
    }

    public Double getShippingLng() {
        return shippingLng;
    }

    public void setShippingLng(Double shippingLng) {
        this.shippingLng = shippingLng;
    }

    public String getShippingPhone() {
        return shippingPhone;
    }

    public void setShippingPhone(String shippingPhone) {
        this.shippingPhone = shippingPhone;
    }

    public String getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }

    public String getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(String deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public String getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(String subTotal) {
        this.subTotal = subTotal;
    }

    public String getRiderTip() {
        return riderTip;
    }

    public void setRiderTip(String riderTip) {
        this.riderTip = riderTip;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public int getCooperation() {
        return cooperation;
    }

    public void setCooperation(int cooperation) {
        this.cooperation = cooperation;
    }

    public String getSubtotalFormatted() {
        return subtotalFormatted;
    }

    public void setSubtotalFormatted(String subtotalFormatted) {
        this.subtotalFormatted = subtotalFormatted;
    }

    public String getSubtotalFormatted2() {
        return subtotalFormatted2;
    }

    public void setSubtotalFormatted2(String subtotalFormatted2) {
        this.subtotalFormatted2 = subtotalFormatted2;
    }

    public String getDeliveryFee() {
        return deliveryFee;
    }

    public void setDeliveryFee(String deliveryFee) {
        this.deliveryFee = deliveryFee;
    }

    public String getDeliveryFeeFormatted() {
        return deliveryFeeFormatted;
    }

    public void setDeliveryFeeFormatted(String deliveryFeeFormatted) {
        this.deliveryFeeFormatted = deliveryFeeFormatted;
    }

    public String getDeliveryFeeFormatted2() {
        return deliveryFeeFormatted2;
    }

    public void setDeliveryFeeFormatted2(String deliveryFeeFormatted2) {
        this.deliveryFeeFormatted2 = deliveryFeeFormatted2;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getTotalFormatted() {
        return totalFormatted;
    }

    public void setTotalFormatted(String totalFormatted) {
        this.totalFormatted = totalFormatted;
    }

    public String getTotalFormatted2() {
        return totalFormatted2;
    }

    public void setTotalFormatted2(String totalFormatted2) {
        this.totalFormatted2 = totalFormatted2;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<OrderItem> getItems() {
        return items;
    }

    public void setItems(List<OrderItem> items) {
        this.items = items;
    }
    @SerializedName("order_item_id")
    @Expose
    private Integer orderItemId;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("product_image")
    @Expose
    private String productImage;
    @SerializedName("qty")
    @Expose
    private Integer qty;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("price_formatted")
    @Expose
    private String priceFormatted;
    @SerializedName("price_formatted_2")
    @Expose
    private String priceFormatted2;
    @SerializedName("options")
    @Expose
    private String options;
    @SerializedName("addons")
    @Expose
    private String addons;

    public Integer getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(Integer orderItemId) {
        this.orderItemId = orderItemId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPriceFormatted() {
        return priceFormatted;
    }

    public void setPriceFormatted(String priceFormatted) {
        this.priceFormatted = priceFormatted;
    }

    public String getPriceFormatted2() {
        return priceFormatted2;
    }

    public void setPriceFormatted2(String priceFormatted2) {
        this.priceFormatted2 = priceFormatted2;
    }

    public String getOptions() {
        return options;
    }

    public void setOptions(String options) {
        this.options = options;
    }

    public String getAddons() {
        return addons;
    }

    public void setAddons(String addons) {
        this.addons = addons;
    }

}
