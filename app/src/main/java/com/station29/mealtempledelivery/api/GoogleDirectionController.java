package com.station29.mealtempledelivery.api;

import android.graphics.Color;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.station29.mealtempledelivery.data.model.DirectionResults;
import com.station29.mealtempledelivery.utils.PolyLineUtils;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.station29.mealtempledelivery.utils.Util.logDebug;

/**
 * Created by sambath on 5/11/17.
 */

public class GoogleDirectionController implements Callback<DirectionResults> {

    private GoogleMap mMap;
    private GoogleDirectionControllerCallBack callback;
    private LatLng origin, destination, zoomLatLong;
    private String originLatLng, destinationLatLng, key;
    private int color = Color.BLUE, width = 5;
    private Integer originMarkerId, destinationMarkerId;
    private String originLocationName, destLocationName;

    private OnRouteMapResultListener mListener;

    public GoogleDirectionController() {

    }

    public GoogleDirectionController(OnRouteMapResultListener mListener) {
        this.mListener = mListener;
    }

    public GoogleDirectionController originTitle(String title) {
        this.originLocationName = title;
        return this;
    }

    public GoogleDirectionController destTitle(String title) {
        this.destLocationName = title;
        return this;
    }

    public GoogleDirectionController originMarker(Integer resource) {
        this.originMarkerId = resource;
        return this;
    }


    public GoogleDirectionController destinationMarker(Integer resource) {
        this.destinationMarkerId = resource;
        return this;
    }

    public GoogleDirectionController map(GoogleMap mMap) {
        this.mMap = mMap;
        return this;
    }

    public GoogleDirectionController key(@NonNull String key) {
        this.key = key;
        return this;
    }

    public GoogleDirectionController from(@NonNull LatLng from) {
        this.origin = from;
        this.originLatLng = this.origin.latitude + "," + this.origin.longitude;
        return this;
    }

    public GoogleDirectionController to(@NonNull LatLng to) {
        this.destination = to;
        this.destinationLatLng = this.destination.latitude + "," + this.destination.longitude;
        return this;
    }

    public GoogleDirectionController zoomTo(@NonNull LatLng zoomTo) {
        this.zoomLatLong = zoomTo;
        return this;
    }

    public GoogleDirectionController color(int color) {
        this.color = color;
        return this;
    }

    public GoogleDirectionController width(int width) {
        this.width = width;
        return this;
    }

    public GoogleDirectionController callBack(@NonNull GoogleDirectionControllerCallBack callback) {
        this.callback = callback;
        return this;
    }

    public void execute() {
        try {
            ServiceAPI api = RetrofitGenerator.createServiceMapDirection();
            Call<DirectionResults> res = api.getMapDirection(true, originLatLng, destinationLatLng, key);
            res.enqueue(this);
        } catch (Exception e) {
            Log.e(e.getClass().getName(), e.getMessage() + "");
        }
    }


    @Override
    public void onResponse(@NotNull Call<DirectionResults> call, @NotNull Response<DirectionResults> response) {
        try {
            if (response.isSuccessful()) {
                this.onDrawPolyLine(response.body());
                assert response.body() != null;
                logDebug("direction_res", response.body().getStatus());
            } else {
                if (callback != null) callback.onError(response.message());
            }
        } catch (Exception e) {
            Log.e(e.getClass().getName(), e.getMessage() + "");
        }
    }


    @Override
    public void onFailure(@NotNull Call<DirectionResults> call, @NotNull Throwable t) {
        if (callback != null) callback.onError(t.getMessage());
    }

    private void onDrawPolyLine(DirectionResults directionResults) {
        if (mListener != null) mListener.onDrawRouteResult(directionResults);
        try {
            ArrayList<LatLng> routelist = new ArrayList<>();
            long distance = 0;
            if (directionResults.getRoutes().size() > 0) {
                ArrayList<LatLng> decodelist;
                DirectionResults.Route routeA = directionResults.getRoutes().get(0);
                if (routeA.getLegs().size() > 0) {
                    distance = routeA.getLegs().get(0).getDistance().getValue();
                    List<DirectionResults.Steps> steps = routeA.getLegs().get(0).getSteps();
                    DirectionResults.Steps step;
                    DirectionResults.Location location;
                    String polyline;
                    for (int i = 0; i < steps.size(); i++) {
                        step = steps.get(i);
                        // Start Point
                        location = step.getStart_location();
                        routelist.add(new LatLng(location.getLat(), location.getLng()));
                        // Polyline
                        polyline = step.getPolyline().getPoints();
                        decodelist = PolyLineUtils.deCode(polyline);
                        routelist.addAll(decodelist);
                        // End Point
                        location = step.getEnd_location();
                        routelist.add(new LatLng(location.getLat(), location.getLng()));
                    }
                }
            }
            if (routelist.size() > 0) {
                PolylineOptions rectLine = new PolylineOptions().width(this.width).color(this.color);

                for (int i = 0; i < routelist.size(); i++) {
                    rectLine.add(routelist.get(i));
                }
                // Adding route on the map
                mMap.addPolyline(rectLine);

                if (this.originMarkerId != null) {
                    MarkerOptions fromMarker = new MarkerOptions();
                    fromMarker.position(origin);
                    fromMarker.title(TextUtils.isEmpty(this.originLocationName) ? "" : this.originLocationName);
                    fromMarker.icon(BitmapDescriptorFactory.fromResource(originMarkerId));
                    mMap.addMarker(fromMarker);
                }

                if (this.destinationMarkerId != null) {
                    MarkerOptions toMarker = new MarkerOptions();
                    toMarker.position(destination);
                    toMarker.title(TextUtils.isEmpty(this.destLocationName) ? "" : this.destLocationName);
                    toMarker.icon(BitmapDescriptorFactory.fromResource(destinationMarkerId));
                    mMap.addMarker(toMarker);
                }


                if (zoomLatLong == null) {
                    zoomLatLong = destination;
                }

                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(zoomLatLong, 16);
                mMap.animateCamera(cameraUpdate);

            }
            if (callback != null) callback.onSuccess(routelist, distance);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(e.getClass().getName(), e.getMessage() + "");
            if (callback != null) callback.onError(e.getMessage());
        }
    }

    public interface OnRouteMapResultListener {
        void onDrawRouteResult(DirectionResults results);
    }
}
