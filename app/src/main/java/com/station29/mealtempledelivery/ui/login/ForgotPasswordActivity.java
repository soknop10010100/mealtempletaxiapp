package com.station29.mealtempledelivery.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.rilixtech.widget.countrycodepicker.CountryCodePicker;
import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.api.Constant;
import com.station29.mealtempledelivery.api.request.MockupData;
import com.station29.mealtempledelivery.api.request.SignUpWs;
import com.station29.mealtempledelivery.data.model.User;
import com.station29.mealtempledelivery.ui.base.BaseActivity;

import java.util.HashMap;

public class ForgotPasswordActivity extends BaseActivity {

    private TextInputEditText txtPhone;
    private CountryCodePicker ccp;
    private final String action = null;
    private Button btnContinue;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        findViewById(R.id.btnBackLogin).setOnClickListener(v -> onBackPressed());

        txtPhone = findViewById(R.id.txtPhoneNumberF);
        ccp = findViewById(R.id.ccp);
        ccp.setDefaultCountryUsingNameCodeAndApply(BaseActivity.countryCode);

       btnContinue = findViewById(R.id.btnRest);
       progressBar = findViewById(R.id.progress_load);
        btnContinue.setOnClickListener(v -> {
            progressBar.setVisibility(View.VISIBLE);
                if (Constant.getBaseUrl() != null) {
                    if(txtPhone.getText().toString().isEmpty()){
                        btnContinue.setEnabled(true);
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(ForgotPasswordActivity.this, R.string.please_input_your_phone_email,Toast.LENGTH_LONG).show();
                    } else {
                        btnContinue.setEnabled(false);
                        forgetPass();
                    }

                }
        });

    }

    public void forgetPass() {
        MockupData.setAccount(null);
        HashMap<String, Object> userAccount = new HashMap<>();
        if (txtPhone.getText() != null && !txtPhone.getText().toString().isEmpty()) {
            userAccount.put("phone", txtPhone.getText().toString().trim());
            userAccount.put("phone_code", ccp.getFullNumber());
        }
        userAccount.put("phone_code", ccp.getFullNumber());
        userAccount.put("code", ccp.getSelectedCountryNameCode() );
        String userType = "USER_ANDROID";
        userAccount.put("user_type", userType);
        userAccount.put("function","Driver");
        MockupData.setAccount(userAccount);

        new SignUpWs().forgetAccountPassword(ForgotPasswordActivity.this,userAccount, requestServer);
    }

    private final SignUpWs.RequestServer requestServer = new SignUpWs.RequestServer() {
        @Override
        public void onRequestSuccess() {
            btnContinue.setEnabled(true);
            progressBar.setVisibility(View.GONE);
            Intent intent = new Intent(ForgotPasswordActivity.this, VerifyCodeActivity.class);
            if (action != null) {
                intent.putExtra("action", action);
            }
            intent.putExtra("newPassword","newPassword");
            startActivity(intent);
        }

        @Override
        public void onRequestToken(String token, int userId, User user) {

        }

        @Override
        public void onRequestFailed(String message) {
            btnContinue.setEnabled(true);
            progressBar.setVisibility(View.GONE);
            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
        }
    };
}
