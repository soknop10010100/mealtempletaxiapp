package com.station29.mealtempledelivery.api;


import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.station29.mealtempledelivery.utils.Util;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.station29.mealtempledelivery.utils.Util.logDebug;

public class RetrofitGenerator {

    public static ServiceAPI createService() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.readTimeout(60, TimeUnit.SECONDS).connectTimeout(60, TimeUnit.SECONDS);
        Retrofit.Builder builder = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create()).baseUrl(Constant.getBaseUrl());
        builder.client(httpClient.build());
        Retrofit retrofit = builder.build();
        return retrofit.create(ServiceAPI.class);
    }

    //create service with local token
    public static ServiceAPI createServiceWithAuth(Context activity) {
        final String authToken = "Bearer " + Util.getString("token", activity);
        logDebug("tokenUser", Util.getString("token", activity));
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            @NonNull
            public okhttp3.Response intercept(@NonNull Chain chain) throws IOException {
                Request original = chain.request();

                Request.Builder builder = original.newBuilder()
                        .header("Content-Type", "application/json")
                        .header("Authorization", authToken)
                        .method(original.method(), original.body());

                Request request = builder.build();
                return chain.proceed(request);
            }
        });

        httpClient.readTimeout(60, TimeUnit.SECONDS).connectTimeout(60, TimeUnit.SECONDS);
        Retrofit.Builder builder = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create()).baseUrl(Constant.getBaseUrl());
        builder.client(httpClient.build());
        Retrofit retrofit = builder.build();
        return retrofit.create(ServiceAPI.class);
    }

    //request to main server to get route in user user location
    public static ServiceAPI createServiceUrl() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.readTimeout(60, TimeUnit.SECONDS).connectTimeout(60, TimeUnit.SECONDS);
        Retrofit.Builder builder = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create()).baseUrl(Constant.mainUrl);
        builder.client(httpClient.build());
        Retrofit retrofit = builder.build();
        return retrofit.create(ServiceAPI.class);
    }

    public static ServiceAPI createServiceMapDirection() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.readTimeout(60, TimeUnit.SECONDS).connectTimeout(60, TimeUnit.SECONDS);
        Retrofit.Builder builder = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create()).baseUrl(Constant.MapDirectionBaseUrl);
        builder.client(httpClient.build());
        Retrofit retrofit = builder.build();
        return retrofit.create(ServiceAPI.class);
    }

    //create service download
    public static ServiceAPI  createServiceWithAuthDownload(Context activity) {
        final String authToken = "Bearer " + Util.getString("token", activity);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        httpClient.addInterceptor(chain -> {
            Request original = chain.request();
            Request.Builder builder = original.newBuilder()
                    .header("Content-Type", "application/pdf")
                    .header("Authorization", authToken)
                    .method(original.method(), original.body());
            Request request = builder.build();
            return chain.proceed(request);
        });

        httpClient.readTimeout(30, TimeUnit.SECONDS).connectTimeout(30, TimeUnit.SECONDS);
        Retrofit.Builder builder = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(Constant.getBaseUrl());
        builder.client(httpClient.build());
        Retrofit retrofit = builder.build();
        return retrofit.create(ServiceAPI.class);
    }

}
