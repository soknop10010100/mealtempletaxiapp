package com.station29.mealtempledelivery.data.model

import java.io.Serializable

data class RedeemExchange(
    val id : Int,
    val type : String,
    val user_type : String,
    val currency_code : String,
    val country_code : String,
    val redeem_amount : String,
    val active : String,
    val created_at : String,
    val updated_at : String,
    val deleted_at : String,
    val amount : String,
    val redeem_to: ArrayList<Redeem>
) : Serializable

class Redeem(
    val id : Int,
    val reward_type_id : Int,
    val reward_type : String,
    val redeem_type : String,
    val currency_code : String,
    val redeem_amount : String,
    val point : String,
    val currency_sign : String

) : Serializable