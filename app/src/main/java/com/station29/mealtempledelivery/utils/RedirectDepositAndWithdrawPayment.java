package com.station29.mealtempledelivery.utils;

import android.app.Activity;
import android.widget.Toast;

import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.api.request.PaymentWs;
import com.station29.mealtempledelivery.data.model.PaymentSuccess;

import java.util.HashMap;

public class RedirectDepositAndWithdrawPayment {

    public static void onTopUpOrWithdrawOrange(Activity mActivity , HashMap<String , Object > topUpParam , String paymentType, String paymentService , OnResponse onResponse){
        new PaymentWs().topUpOrWithdrawWallets(topUpParam, mActivity,paymentType, paymentService,new PaymentWs.OnTopUpWalletsCallBack() {
            @Override
            public void onSuccess(String message , String paymentId, PaymentSuccess paymentSuccess) {
                onResponse.onSuccess(message,paymentId,paymentSuccess,paymentType);
            }

            @Override
            public void onError(String message) {
                onResponse.onResponseMessage(message);
            }
        });
    }

    public static void checkPaymentTopUpAndWithdraw(Activity activity , Object object, String paymentType , String screenType , OnResponse onResponse){
        HashMap<String , Object> param =  new HashMap<>();
        if(object instanceof HashMap){
            param = (HashMap<String, Object>) object;
            onTopUpOrWithdrawOrange(activity, param,paymentType, screenType , onResponse);
        } else {
            Toast.makeText(activity,activity.getString(R.string.something_went_wrong),Toast.LENGTH_SHORT).show();
        }

//        if (("MOBILE_WALLET").equalsIgnoreCase(paymentType)){
//            HashMap<String , Object> param =  new HashMap<>();
//            if(object instanceof HashMap){
//                param = (HashMap<String, Object>) object;
//                onTopUpOrWithdrawOrange(activity, param, screenType , onResponse);
//            } else {
//                Toast.makeText(activity,activity.getString(R.string.something_went_wrong),Toast.LENGTH_SHORT).show();
//            }
//
//        } else {
//            Toast.makeText(activity,activity.getString(R.string.something_went_wrong),Toast.LENGTH_SHORT).show();
//        }

    }

    public interface OnResponse{
        void onSuccess(String message , String paymentId, PaymentSuccess paymentSuccess , String paymentType);
        void onResponseMessage(String message);
    }
}
