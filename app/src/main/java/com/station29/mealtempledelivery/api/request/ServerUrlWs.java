package com.station29.mealtempledelivery.api.request;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.util.Log;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.station29.mealtempledelivery.BuildConfig;
import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.api.Constant;
import com.station29.mealtempledelivery.api.CustomCallback;
import com.station29.mealtempledelivery.api.CustomResponseListener;
import com.station29.mealtempledelivery.api.RetrofitGenerator;
import com.station29.mealtempledelivery.api.ServiceAPI;
import com.station29.mealtempledelivery.utils.Util;

import retrofit2.Call;

import static com.station29.mealtempledelivery.utils.Util.isConnectedToInternet;
import static com.station29.mealtempledelivery.utils.Util.logDebug;
import static com.station29.mealtempledelivery.utils.Util.popupMessage;

public class ServerUrlWs {

    public void readServerUrl(final Activity context, String code, final loadServerUrl loadServerUrl) {
        ServiceAPI service = RetrofitGenerator.createServiceUrl();
        Call<JsonElement> res = service.getUrl(code);
        res.enqueue(new CustomCallback(context,new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                try{
                    if (resObj.has("server_url")) {
                        String base ="";
                        if (Constant.isStage){
                            base = BuildConfig.STAGE;
                            Constant.setBaseUrl(BuildConfig.STAGE);
                        } else {
                            base = resObj.get("server_url").getAsString();
                            Constant.setBaseUrl(base);
                        }
                        loadServerUrl.onLoadSuccess(true, base);
                        Util.saveString("baseUrl", base, context);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String error , int code) {
                loadServerUrl.onLoadFailed(error);
            }
        }));
    }

    public interface loadServerUrl {
        void onLoadSuccess(boolean success, String baseUrl);
        void onLoadFailed(String errorMessage);
    }
}
