package com.station29.mealtempledelivery.ui.history;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.api.Constant;
import com.station29.mealtempledelivery.api.request.DeliveryWs;
import com.station29.mealtempledelivery.data.model.DeliveryDetails;
import com.station29.mealtempledelivery.data.model.TaxiDeliveryDetail;
import com.station29.mealtempledelivery.ui.base.BaseActivity;
import com.station29.mealtempledelivery.ui.home.OrderDetailActivity;
import com.station29.mealtempledelivery.utils.Util;

import java.util.Locale;

import static com.station29.mealtempledelivery.utils.Util.isConnectedToInternet;
import static com.station29.mealtempledelivery.utils.Util.popupMessage;

public class EarningDetailActivity extends BaseActivity {

    private TextView shopNameTv;
    private TextView deliveryTv;
    private TextView riderTipTv;
    private TextView totalTv;
    private TextView paymentTv;
    private TextView customerPhoneTv;
    private TextView dateTv;
    private int deliveryDetailId;
    private String typeOrder;
    private View progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_earning);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        shopNameTv = findViewById(R.id.shop_name);
        deliveryTv = findViewById(R.id.deliveryFee);
        riderTipTv = findViewById(R.id.rider_tip);
//        TextView priceProductTv = findViewById(R.id.product_price);
        totalTv = findViewById(R.id.total_price);
        paymentTv = findViewById(R.id.paymentType);
        customerPhoneTv = findViewById(R.id.customer_phone);
        dateTv = findViewById(R.id.date);
        progress = findViewById(R.id.progress);

        if (getIntent() != null && getIntent().hasExtra("delivery") ) {
            deliveryDetailId = getIntent().getIntExtra("delivery", -1);

            if (getIntent().hasExtra("typeOrder")){
                typeOrder = (String) getIntent().getSerializableExtra("typeOrder");
            }

            if (Constant.getBaseUrl() != null){
                progress.setVisibility(View.VISIBLE);
                new DeliveryWs().readDelivery(deliveryDetailId,typeOrder ,this, new DeliveryWs.ReadDeliveryListener() {
                    @Override
                    public void onLoadDeliverySuccess(DeliveryDetails details) {
                        progress.setVisibility(View.GONE);
                        shopNameTv.setText(String.format("%s : %s", getString(R.string.from), details.getStoreName()));
                        deliveryTv.setText(String.format(Locale.US, "%s %s",getString(R.string.delivery), details.getDeliveryFeeFormatted()));
                        riderTipTv.setText(String.format(Locale.US, "%s %s %s",getString(R.string.reader_tip), details.getCurrency(), Util.stringFormatPrice(details.getRiderTip())));
                        paymentTv.setText(String.format(Locale.US, "%s", details.getPaymentType()));
                        customerPhoneTv.setText(String.format(Locale.US, "%s : %s",getString(R.string.customerphone), details.getShippingPhone()));
                        dateTv.setText(String.format(Locale.US, "%s : %s",getString(R.string.due_date), Util.formatDateUptoCurrentRegion(details.getDeliveryTime())));
                        totalTv.setText(String.format(Locale.US, "%s %s",getString(R.string.total_prices), details.getTotalFormatted()));
                    }

                    @Override
                    public void onLoadTaxiDetailSuccess(TaxiDeliveryDetail deliveryDetail) {
                        progress.setVisibility(View.GONE);
                        shopNameTv.setText(String.format("%s  %s", getString(R.string.from), deliveryDetail.getCustomerName()));
                        customerPhoneTv.setText(String.format(Locale.US, "%s : %s",getString(R.string.customerphone), deliveryDetail.getPhoneNumber()));

                    }

                    @Override
                    public void onLoadFail(String message) {
                        progress.setVisibility(View.GONE);
                        if (!isConnectedToInternet(EarningDetailActivity.this)){
                            popupMessage ( null, getString(R.string.check_yor) ,EarningDetailActivity.this);
                        }
                    }
                });
            }
        }

        findViewById(R.id.view_menu).setOnClickListener(v -> {
            Intent intent = new Intent(EarningDetailActivity.this, OrderDetailActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.putExtra("id", deliveryDetailId);
            intent.putExtra("back","notBack");
            startActivity(intent);
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            //refresh();
            this.finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)){
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }
}
