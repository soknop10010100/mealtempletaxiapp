package com.station29.mealtempledelivery.api;

import com.station29.mealtempledelivery.BuildConfig;
import com.station29.mealtempledelivery.data.model.Config;
import com.station29.mealtempledelivery.ui.SplashScreenActivity;

import java.util.HashMap;

public class Constant {

    public static HashMap<String,String> serviceLanguage = new HashMap<>();

    public final static String mainUrl = BuildConfig.DEV;
    public static boolean isStage = false;
    private static String start = "start";
    public final static String WITHDRAW = "withdraw";
    public final static String DEPOSIT = "deposit";
    public final static String USER_TYPE = "USER_ANDROID";
//  public final static String IN_PROGRESS = "IN_PROGRESS";
    public final static String ACCEPTED = "ACCEPTED";
    public final static String PICKED_UP = "PICKED_UP";
    public final static String DELIVERED = "DELIVERED";
    public static String state ;
    public final static String SHARE_PREFERANCE_NAME="MealTempleDriver";
    // ======================= Firebase Analytics Constant
    public final static String COUNTRY_ZIP_CODE = "MealTemple_CountryZipCode";
    public final static String PHONE_MODEL = "MealTemple_PhoneModel";
    public final static String LOCATION_REQUEST_PROBLEM = "MealTemple_LocationRequest";
    public final static String REQUEST_BASEURL = "MealTemple_BaseUrl";
    public final static String USER_ID = "MealTemple_UserId";
    public final static String DELIVERY_ID = "MealTemple_DeliveryId";
    public final static String ERROR_MESSAGE = "MealTemple_Error_Message";
    // ========================================================

    // ======================= Language Code
    public final static String LANG_KH = "km";
    public final static String LANG_FR = "fr";
    public final static String LANG_LA = "lo";
    public final static String LANG_EN = "en";
    public final static String LANG_SW = "sv";
    public final static String LANG_GM = "de";
    public final static String LANG_SB = "sr";
    public final static String LANG_CH = "ch";
    public final static String LANG_SP = "es";
    public final static String LANG_HE = "he";
    public final static String LANG_KO = "ko";
    public final static String LANG_AR = "ar";
    // =======================================

    public final static String KESS_SHOW = "Debit Card";
    public final static String CARD_SHOW = "Visa Master Card";
    public final static String KIWIPAY_SHOW = "KiwiPay";
    public final static String ALLOWANCE_WALLET = "Allowance Wallet";

    public final static String MapDirectionBaseUrl = "https://maps.googleapis.com/maps/";

    public final static String fileName = "movingPoints";

    private static String baseUrl;

    public static String getBaseUrl() {
        return baseUrl;
    }

    public static void setBaseUrl(String baseUrl) {
        Constant.baseUrl = baseUrl;
    }

    public static String getStart() {
        return start;
    }

    public static void setStart(String start) {
        Constant.start = start;
    }

    public final static String START_BOARDING= start;

    private static Config config;

    public static Config getConfig() {
        if (SplashScreenActivity.splashConfig!=null)
            setConfig(SplashScreenActivity.splashConfig);
        return config;
    }

    private static void setConfig(Config splashConfig) {
        Constant.config = splashConfig;
    }

    public static String getState() {
        return state;
    }

    public static void setState(String state) {
        Constant.state = state;
    }
}
