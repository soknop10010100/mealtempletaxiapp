package com.station29.mealtempledelivery.ui.kyc

import android.app.NotificationManager
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AlertDialog
import com.google.firebase.auth.FirebaseAuth
import com.station29.mealtempledelivery.R
import com.station29.mealtempledelivery.api.Constant
import com.station29.mealtempledelivery.api.request.LogoutWs
import com.station29.mealtempledelivery.api.request.ProfileWs
import com.station29.mealtempledelivery.data.model.User
import com.station29.mealtempledelivery.ui.base.BaseActivity
import com.station29.mealtempledelivery.ui.base.MusicControl
import com.station29.mealtempledelivery.ui.login.LoginActivity
import com.station29.mealtempledelivery.utils.Util
import java.util.*

class WaitingVerifyKycActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_waiting_verify_kyc)

        findViewById<ImageView>(R.id.btnBack).setOnClickListener {
            val builder = AlertDialog.Builder(this@WaitingVerifyKycActivity, R.style.DialogTheme)
            builder.setTitle(R.string.logout)
            builder.setMessage(R.string.logout_inform)
            builder.setPositiveButton(R.string.logout) { dialog, which ->
                logout()
            }
            builder.setNegativeButton(getString(R.string.cancel), null).show()
        }
    }

    private fun viewProfile () {
        findViewById<View>(R.id.progress).visibility = View.VISIBLE
        ProfileWs().viewProfiles(this@WaitingVerifyKycActivity, object : ProfileWs.ProfileViewAndUpdate{
            override fun onLoadProfileSuccess(user: User?, message: String?) {
                findViewById<View>(R.id.progress).visibility = View.GONE
                if (user?.kyc != null && user.kyc.status == "APPROVED") {
                    setResult(RESULT_OK)
                    finish()
                } else if (user?.kyc != null && user.kyc.status == "REJECT"){
                    val intent = Intent(this@WaitingVerifyKycActivity,AskToCompleteKycActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                }
            }

            override fun onError(errorMessage: String?) {
                findViewById<View>(R.id.progress).visibility = View.GONE
            }

            override fun onUpdateProfilesSuccess(message: String?) {
                findViewById<View>(R.id.progress).visibility = View.GONE
            }
        })
    }

    override fun onResume() {
        super.onResume()
        viewProfile()
    }

    private fun logout() {
        findViewById<View>(R.id.progress).visibility = View.VISIBLE
        val deviceTypes = HashMap<String, String>()
        deviceTypes["device_type"] = "ANDROID"
        val topic = Util.getString("topic", this@WaitingVerifyKycActivity)
        val deviceToken = Util.getString("firebase_token", this@WaitingVerifyKycActivity)
        LogoutWs().logOut(this@WaitingVerifyKycActivity, deviceTypes, deviceToken, topic, object :
            LogoutWs.LogoutCallback {
            override fun onLoadSuccess(message: String) {
                findViewById<View>(R.id.progress).visibility = View.GONE
                MusicControl.countNotification -= 1
                MusicControl.getInstance(this@WaitingVerifyKycActivity).stopMusic()
                removeNotification()
                FirebaseAuth.getInstance().signOut()
                val bundle = Bundle()
                bundle.putString(
                    Constant.USER_ID,
                    Util.getString("user_id", this@WaitingVerifyKycActivity) + "was Logout at " + Calendar.getInstance()
                )
                mFirebaseAnalytics.logEvent(this.javaClass.simpleName, bundle)
                val intent = Intent(this@WaitingVerifyKycActivity, LoginActivity::class.java)
                Util.saveString("token", "", this@WaitingVerifyKycActivity)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
            }

            override fun onLoadFailed(errorMessage: String) {
                findViewById<View>(R.id.progress).visibility = View.GONE
                Util.popupMessage(null, errorMessage, this@WaitingVerifyKycActivity)
            }
        })
    }

    private fun removeNotification() {
        val manager = this.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        manager.cancelAll()
    }

    override fun onBackPressed() {

    }
}