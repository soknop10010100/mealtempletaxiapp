package com.station29.mealtempledelivery.ui.setting
import android.annotation.SuppressLint
import android.app.Activity
import android.content.DialogInterface
import android.os.Bundle
import android.view.ContextThemeWrapper
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.station29.mealtempledelivery.R
import com.station29.mealtempledelivery.api.Constant
import com.station29.mealtempledelivery.api.request.ProfileWs
import com.station29.mealtempledelivery.api.request.RewardsWs
import com.station29.mealtempledelivery.data.model.Redeem
import com.station29.mealtempledelivery.data.model.RedeemExchange
import com.station29.mealtempledelivery.data.model.User
import com.station29.mealtempledelivery.ui.base.BaseActivity
import com.station29.mealtempledelivery.ui.setting.adapter.ItemExChangeAdapter
import com.station29.mealtempledelivery.utils.Util

import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class ExchangeRewardsActivity : BaseActivity() {

    private var adapterRedeem: ItemExChangeAdapter? = null
    private lateinit var  recyclerView : RecyclerView
    private lateinit var loading: View
    private lateinit var  refresh : SwipeRefreshLayout
    private lateinit var nameUserTv: TextView
    private lateinit var pointTv : TextView
    private lateinit var levelTv : TextView
    private lateinit var  redeem : ArrayList<Redeem>

    @SuppressLint("NotifyDataSetChanged")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_exchange_rewards)

        val title: TextView = findViewById(R.id.toolBarTitle)
        val btnBack : ImageView = findViewById(R.id.backButton)
        title.text = getString(R.string.exchange_now)
        btnBack.setOnClickListener {
            setResult(RESULT_OK)
            finish()
        }
        recyclerView = findViewById(R.id.list_exChang)
        refresh = findViewById(R.id.swipe_layouts)
        loading = findViewById(R.id.progressBar)
        nameUserTv = findViewById(R.id.name)
        levelTv = findViewById(R.id.level)
        pointTv = findViewById(R.id.points_text)

        viewProfile()

        refresh.setOnRefreshListener {
            adapterRedeem?.clear()
            viewProfile()
            adapterRedeem?.notifyDataSetChanged()
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun viewProfile() {
        val token = Util.getString("token", this)
        if (token != null && token.isNotEmpty() && Constant.getBaseUrl() != null) {
            ProfileWs().viewProfiles(this, object : ProfileWs.ProfileViewAndUpdate {
                @SuppressLint("SetTextI18n", "NotifyDataSetChanged")
                override fun onLoadProfileSuccess(user: User , message: String?) {
                    refresh.isRefreshing = false
                    loading.visibility = View.GONE
                    getListRedeem()
                }

                override fun onError(errorMessage: String) {
                    refresh.isRefreshing = false
                    loading.visibility = View.GONE
                }

                override fun onUpdateProfilesSuccess(message: String?) {

                }

            })
        }
    }

    private fun getListRedeem(){
        loading.visibility = View.VISIBLE
        RewardsWs().getListRedeems(this,rewardsObject)
    }

    private val rewardsObject = object : RewardsWs.RedeemsCallBack{
        override fun onLoadingSuccessFully(listRedeem: ArrayList<RedeemExchange>) {
            val type=""
            for (change : RedeemExchange in listRedeem){
                if (change.type == "CREDIT")
                redeem = change.redeem_to
            }
            loading.visibility = View.GONE
            val layoutManager = LinearLayoutManager(this@ExchangeRewardsActivity)
            recyclerView.layoutManager = layoutManager
            adapterRedeem = ItemExChangeAdapter(redeem,type,this@ExchangeRewardsActivity,itemExChangeAdapter)
            recyclerView.adapter = adapterRedeem
        }

        override fun onLoadingFail(message: String?) {
            loading.visibility = View.GONE
            Toast.makeText(this@ExchangeRewardsActivity, message, Toast.LENGTH_LONG).show()
        }
    }

    private val itemExChangeAdapter = object : ItemExChangeAdapter.OnItemClickChangeListener{
        override fun onItemClick(redeem: Redeem,type:String) {
            loading.visibility = View.GONE
            popupMessageExchange(redeem,getString(R.string.make_sure),getString(R.string.are_you_sure_to_exchange),this@ExchangeRewardsActivity)
        }
    }

    private fun exChangNow(id:Int, point:String){
        loading.visibility = View.VISIBLE
        val hashMap = HashMap<String,Any?>();
        hashMap["redeem_to_id"] = id
        hashMap["reward_type"]  = point
        RewardsWs().ecChangeRewards(this,hashMap, object : RewardsWs.RedeemsExchangeCallBack {
            override fun onLoadingSuccessFully(message: String?) {
                loading.visibility = View.GONE
               popupMessageEx(getString(R.string.success_full),message,this@ExchangeRewardsActivity)
            }

            override fun onLoadingFail(message: String?) {
                loading.visibility = View.GONE
                Toast.makeText(this@ExchangeRewardsActivity,message,Toast.LENGTH_LONG).show()
            }
        })
    }

    fun popupMessageEx(title: String?, message: String?, activity: Activity) {
        if (!activity.isFinishing) {
            val builder = AlertDialog.Builder(ContextThemeWrapper(activity, R.style.DialogTheme))
            if (title != null) builder.setTitle(title)
            builder.setMessage(message)
            builder.setPositiveButton(String.format(Locale.US, "%s", activity.getString(R.string.ok))) { _: DialogInterface?, _: Int -> viewProfile() }
            builder.setCancelable(false)
            builder.show()
        }
    }

    fun popupMessageExchange(redeem : Redeem,title: String?, message: String?, activity: Activity) {
        if (!activity.isFinishing) {
            val builder = AlertDialog.Builder(ContextThemeWrapper(activity, R.style.DialogTheme))
            if (title != null) builder.setTitle(title)
            builder.setMessage(message)
            builder.setPositiveButton(String.format(Locale.US, "%s", activity.getString(R.string.exchange_now))) { _: DialogInterface?, _: Int ->
                exChangNow(redeem.id,redeem.reward_type) }
            builder.setNegativeButton(resources.getString(R.string.cancel), null)
            builder.show()
        }
    }

    override fun onBackPressed() {
        setResult(RESULT_OK)
        finish()
    }

}