package com.station29.mealtempledelivery.ui.login;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.fonfon.geohash.GeoHash;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.GeoPoint;
import com.rilixtech.widget.countrycodepicker.CountryCodePicker;
import com.station29.mealtempledelivery.BuildConfig;
import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.api.Constant;
import com.station29.mealtempledelivery.api.request.LoginWs;
import com.station29.mealtempledelivery.api.request.MockupData;
import com.station29.mealtempledelivery.api.request.ProfileWs;
import com.station29.mealtempledelivery.cloudfirestore.Driver;
import com.station29.mealtempledelivery.fcm.MyFirebaseMessagingService;
import com.station29.mealtempledelivery.ui.base.AlertPasswordActivity;
import com.station29.mealtempledelivery.ui.base.BaseActivity;
import com.station29.mealtempledelivery.ui.MainActivity;
import com.station29.mealtempledelivery.ui.setting.ChangLanguageActivity;
import com.station29.mealtempledelivery.ui.signup.SignUpActivity;
import com.station29.mealtempledelivery.utils.GPSTracker;
import com.station29.mealtempledelivery.utils.Util;

import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import static com.station29.mealtempledelivery.utils.Util.logDebug;
import static com.station29.mealtempledelivery.utils.Util.popupMessage;

public class LoginActivity extends BaseActivity {

    private EditText phoneEditText;
    private EditText passwordEditText;
    private CountryCodePicker ccp;
    private final HashMap<String, Object> map = new HashMap<>();
    private Button loginButton;
    private View loading;

    @SuppressLint("UseCompatLoadingForDrawables")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Util.clearString(LoginActivity.this, "token");

        phoneEditText = findViewById(R.id.txtPhoneNumber);
        passwordEditText = findViewById(R.id.txtpassword);
        TextView textView = findViewById(R.id.textURL);
        loginButton = findViewById(R.id.btnLogin);
        ccp = findViewById(R.id.ccp);
        ImageButton btnChangeLanguage = findViewById(R.id.btn_change_language);
        loading = findViewById(R.id.loading);
        TextView appVersionTv = findViewById(R.id.version_app);

        appVersionTv.setText(String.format(Locale.US, "%s (%s)", BuildConfig.VERSION_NAME, BuildConfig.VERSION_CODE));

        if (countryCode != null) {
            ccp.setCountryForNameCode(countryCode);
        }

        Util.start(LoginActivity.this,"stop");

        String lang = Util.getString("language", LoginActivity.this);

        switch (lang) {
            case Constant.LANG_EN:
            case "":
                btnChangeLanguage.setBackground(getDrawable(R.drawable.flag_united_kingdom));
                break;
            case Constant.LANG_FR:
                btnChangeLanguage.setBackground(getDrawable(R.drawable.flag_france));
                break;
            case Constant.LANG_KH:
                btnChangeLanguage.setBackground(getDrawable(R.drawable.flag_cambodia));
                break;
            default:
                btnChangeLanguage.setBackground(getDrawable(R.drawable.flag_laos));
                break;
        }
        Util.changeLanguage(LoginActivity.this, lang);

        btnChangeLanguage.setOnClickListener(v -> {
            Intent intent = new Intent(LoginActivity.this, ChangLanguageActivity.class);
            startActivity(intent);
        });

        loginButton.setOnClickListener(v -> {
            logDebug("loginButton", "loginButtonClick" + "," + Constant.getBaseUrl());
            if (phoneEditText.getText().toString().equals("") && passwordEditText.getText().toString().equals("") || phoneEditText.getText().toString().length() < 7) {
                Toast toast = Toast.makeText(getApplicationContext(), R.string.in_correct, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            } else {
                if (Constant.getBaseUrl() == null) {
                    String baseUrl = Util.getString("baseUrl", LoginActivity.this);
                    if (baseUrl != null) {
                        loginPhone();
                    } else {
                        popupMessage(null, getString(R.string.cannot_recognise_server), LoginActivity.this);
                    }
                } else {
                    loginPhone();
                }
            }
        });

        findViewById(R.id.btnRegister).setOnClickListener(v -> {
            Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        });

        findViewById(R.id.btnForgetPass).setOnClickListener(v -> startActivity(new Intent(LoginActivity.this, ForgotPasswordActivity.class)));

        if (Constant.mainUrl.equals(BuildConfig.DEV)) {
            textView.setVisibility(View.VISIBLE);
        } else {
            textView.setVisibility(View.GONE);
        }
        loginFireStore();
    }

    public void loginPhone() {
        MockupData.setAccount(null);
        if (!phoneEditText.getText().toString().isEmpty()) {
            map.put("phone", phoneEditText.getText().toString().trim());
            MockupData.PHONE_NUMBER = "+" + ccp.getFullNumber() + phoneEditText.getText().toString().trim();
        }
        map.put("code", BaseActivity.countryCode);
        map.put("password", passwordEditText.getText().toString().trim());
        map.put("phone_code", ccp.getFullNumber());
        map.put("function", "Driver");

        loginButton.setEnabled(false);
        loading.setVisibility(View.VISIBLE);
        new LoginWs().loginPhone(LoginActivity.this, map, loadLoginCallback);
    }

    private final LoginWs.LoadLoginCallback loadLoginCallback = new LoginWs.LoadLoginCallback() {
        @Override
        public void onLoadSuccess(String token, int userId, String globalId, int isDeactivated) {
            if (isDeactivated == 1) {
                loading.setVisibility(View.GONE);
                Util.saveString("token", token, LoginActivity.this);
                activeDeleteAccount(token,userId,globalId);
            } else {
                loading.setVisibility(View.GONE);
                writeUserLatLng(globalId);
                loginButton.setEnabled(true);
                Util.saveString("token", token, LoginActivity.this);
                Util.saveString("user_id", String.valueOf(userId), LoginActivity.this);
                Util.saveString("global_id", globalId, LoginActivity.this);
                MyFirebaseMessagingService.sendRegistrationToServer(Util.getString("firebase_token", LoginActivity.this), LoginActivity.this);
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        }

        @Override
        public void onLoadFailed(String errorMessage, int code) {
            loading.setVisibility(View.GONE);
            loginButton.setEnabled(true);
            if (code == 403) {
                Intent intent = new Intent(LoginActivity.this, VerifyCodeActivity.class);
                intent.putExtra("phone", map);
                intent.putExtra("code",code);
                startActivity(intent);
            } else {
                Util.popupMessage("",errorMessage,LoginActivity.this);
            }
        }
    };

    @Override
    public void onBackPressed() {
        finishAffinity();
    }

    private void writeUserLatLng(String globalId) {
        GPSTracker gpsTracker = new GPSTracker(this);
        if (!globalId.isEmpty()) {
            Driver driver = new Driver();
            driver.setTimestamp(new Date());
            driver.setGeoPoint(new GeoPoint(gpsTracker.getLatitude(), gpsTracker.getLongitude()));
            GeoHash geoHashString = GeoHash.fromCoordinates(gpsTracker.getLatitude(), gpsTracker.getLongitude());
            driver.setGeoHash(geoHashString.toString());

            FirebaseFirestore db = FirebaseFirestore.getInstance();
            if (db == null) {
                Toast.makeText(LoginActivity.this, "Sync Location failed.", Toast.LENGTH_LONG).show();
                return;
            }
            // [START set_firestore_settings]
            FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                    .setPersistenceEnabled(true)
                    .build();
            db.setFirestoreSettings(settings);

            DocumentReference driverDocumentRef = db.collection("drivers").document(globalId);
            if (driverDocumentRef != null) {
                driverDocumentRef
                        .set(driver).addOnSuccessListener(aVoid -> Toast.makeText(LoginActivity.this, getString(R.string.sync_location_success), Toast.LENGTH_LONG)
                        .show()).addOnFailureListener(e -> Toast.makeText(LoginActivity.this, getString(R.string.sync_location_failed), Toast.LENGTH_LONG).show());
            }
        }
    }

    private void loginFireStore() {
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser == null) {
            mAuth.signInWithEmailAndPassword(BuildConfig.FIRESTORE_USERNAME, BuildConfig.FIRESTORE_PASSWORD).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    // Sign in success, update UI with the signed-in user's information
                    logDebug("writeUserLatLng", "signInWithEmail:success");
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w("writeUserLatLng", "signInWithEmail:failure", task.getException());
                }
            });
        }
    }

    private void activeDeleteAccount (String token, int userId, String globalId) {

        Intent intent = new Intent(LoginActivity.this, AlertPasswordActivity.class);
        intent.putExtra("action","account_deactive");
        activityLauncher.launch(intent, result -> {
            if (result.getResultCode() == RESULT_OK) {
                if (result.getData() != null && result.getData().hasExtra("pin")) {
                    String pinCode = result.getData().getStringExtra("pin");
                    loading.setVisibility(View.VISIBLE);
                    HashMap<String, Object> objectHashMap = new HashMap<>();
                    objectHashMap.put("is_deactivated", 0);
                    objectHashMap.put("password",Util.encodeByPublicKey(pinCode));
                    objectHashMap.put("action_type","activate");
                    new ProfileWs().deleteAccountUser(LoginActivity.this, objectHashMap, new ProfileWs.ReadOnOffLine() {
                        @Override
                        public void onLoadSuccess(String message) {
                            loading.setVisibility(View.GONE);
                            writeUserLatLng(globalId);
                            loginButton.setEnabled(true);
                            Util.saveString("token", token, LoginActivity.this);
                            Util.saveString("user_id", String.valueOf(userId), LoginActivity.this);
                            Util.saveString("global_id", globalId, LoginActivity.this);
                            MyFirebaseMessagingService.sendRegistrationToServer(Util.getString("firebase_token", LoginActivity.this), LoginActivity.this);
                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        }

                        @Override
                        public void onLoadFail(String message) {
                            loading.setVisibility(View.GONE);
                            loginButton.setEnabled(true);
                            Util.popupMessage(getString(R.string.error), message, LoginActivity.this);
                        }
                    });
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        MockupData.setAccount(null);
    }
}
