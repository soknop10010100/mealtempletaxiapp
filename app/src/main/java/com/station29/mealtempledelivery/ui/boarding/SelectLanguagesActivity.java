package com.station29.mealtempledelivery.ui.boarding;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.api.Constant;
import com.station29.mealtempledelivery.api.request.ConfigWs;
import com.station29.mealtempledelivery.api.request.MockupData;
import com.station29.mealtempledelivery.data.model.Config;
import com.station29.mealtempledelivery.data.model.ServiceTypeDriver;
import com.station29.mealtempledelivery.ui.base.BaseActivity;
import com.station29.mealtempledelivery.utils.Util;

import java.io.Serializable;
import java.util.List;

public class SelectLanguagesActivity extends BaseActivity {

    private View progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_languages);

        RecyclerView recyclerView = findViewById(R.id.list_language);
        progressBar = findViewById(R.id.progressBar);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        AllLanguageAdapter languageAdapter = new AllLanguageAdapter(MockupData.listLanguages(SelectLanguagesActivity.this), lang -> {
            progressBar.setVisibility(View.VISIBLE);
            getConfigLang(lang);
        });
        recyclerView.setAdapter(languageAdapter);
    }

    public static class AllLanguageAdapter extends RecyclerView.Adapter<AllLanguageAdapter.ViewHolder>{

        private final List<Language> languageList;
        private final OnClickLang onClickLang;

        public AllLanguageAdapter(List<Language> languageList, OnClickLang onClickLang) {
            this.languageList = languageList;
            this.onClickLang = onClickLang;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_select_language, parent, false);
            return new ViewHolder(row);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            final Language language = languageList.get(position);
            if (language != null ) {
                holder.flagName.setText(language.getLangName());
                holder.flag.setImageResource(language.getLangImage());
                holder.view.setOnClickListener(v -> onClickLang.onClick(language.langCode));
            }
        }

        @Override
        public int getItemCount() {
            return languageList.size();
        }

        static class ViewHolder extends RecyclerView.ViewHolder {

            private final ImageView flag;
            private final ImageView check;
            private final View view;
            private final TextView flagName;

            public ViewHolder(@NonNull View itemView) {
                super(itemView);
                view = itemView;
                flag = itemView.findViewById(R.id.flag);
                flagName = itemView.findViewById(R.id.flagName);
                check = itemView.findViewById(R.id.check);
            }
        }

        public interface OnClickLang{
            void onClick(String lang);
        }
    }

    public static class Language implements Serializable {

        private final String langName;
        private final String langCode;
        private final int langImage;
        private final boolean isCheck;

        public Language(String langName, int langImage, boolean isCheck,String langCode) {
            this.langName = langName;
            this.langImage = langImage;
            this.isCheck = isCheck;
            this.langCode = langCode;
        }

        public String getLangCode() {
            return langCode;
        }

        public String getLangName() {
            return langName;
        }

        public int getLangImage() {
            return langImage;
        }

        public boolean isCheck() {
            return isCheck;
        }
    }

    private void getConfigLang(final String language) {
        new ConfigWs().readConfig(SelectLanguagesActivity.this,language, "notification","getConfigLang", new ConfigWs.ReadConfigCallback() {
            @Override
            public void onSuccess(Config config, List<ServiceTypeDriver> serviceTypeDriver) {
                progressBar.setVisibility(View.GONE);
                Constant.setState(language);
                Util.changeLanguage(SelectLanguagesActivity.this,language);
                Intent intent = new Intent(SelectLanguagesActivity.this, BoardingActivity.class);
                intent.putExtra("lang", language);
                startActivity(intent);
            }

            @Override
            public void onError(String message) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(SelectLanguagesActivity.this, message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)){
            finishAffinity();
        }
        return super.onKeyDown(keyCode, event);
    }
}