package com.station29.mealtempledelivery.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.view.KeyEvent;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.api.Constant;
import com.station29.mealtempledelivery.api.request.ProfileWs;
import com.station29.mealtempledelivery.data.model.ServiceModel;
import com.station29.mealtempledelivery.data.model.TaxiModel;
import com.station29.mealtempledelivery.data.model.User;
import com.station29.mealtempledelivery.data.model.Waypoint;
import com.station29.mealtempledelivery.fcm.MyFirebaseMessagingService;
import com.station29.mealtempledelivery.service.LocationUpdatesService;
import com.station29.mealtempledelivery.ui.base.BaseActivity;
import com.station29.mealtempledelivery.ui.base.MusicControl;
import com.station29.mealtempledelivery.ui.kyc.AskToCompleteKycActivity;
import com.station29.mealtempledelivery.ui.kyc.IdentityVerificationActivity;
import com.station29.mealtempledelivery.ui.kyc.WaitingVerifyKycActivity;
import com.station29.mealtempledelivery.ui.setting.ProfileFragment;
import com.station29.mealtempledelivery.ui.taxi.TaxiArrivalActivity;
import com.station29.mealtempledelivery.ui.taxi.TaxiPickupActivity;
import com.station29.mealtempledelivery.utils.BetterActivityResult;
import com.station29.mealtempledelivery.utils.PickupActivity;
import com.station29.mealtempledelivery.utils.Util;

import java.util.ArrayList;
import java.util.List;

import static com.station29.mealtempledelivery.fcm.MyFirebaseMessagingService.EXTRA_LOCATION;
import static com.station29.mealtempledelivery.fcm.MyFirebaseMessagingService.EXTRA_LOCATION_TAXI;
import static com.station29.mealtempledelivery.fcm.MyFirebaseMessagingService.NOTIFICATION;
import static com.station29.mealtempledelivery.fcm.MyFirebaseMessagingService.NOTIFICATION_ID;
import static com.station29.mealtempledelivery.fcm.MyFirebaseMessagingService.n;
import static com.station29.mealtempledelivery.utils.Util.logDebug;


public class MainActivity extends BaseActivity implements ProfileFragment.OnRegisterServiceListener {

    private final int REQUEST_CODE_ENABLE_LOCATION = 999;
    private final int REQUEST_LOCATION_PERMISSION = 345;
    // Monitors the state of the connection to the service.
    private LocationUpdatesService mService;
    private boolean mBound = false;
    public static List<ServiceModel> serviceModels = new ArrayList<>();
    public static List<TaxiModel> taxiModels = new ArrayList<>() ;


    private final ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            LocationUpdatesService.LocalBinder binder = (LocationUpdatesService.LocalBinder) service;
            mService = binder.getService();
            if (mService != null) {
                mService.requestLocationUpdates();
            }
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
            mBound = false;
        }
    };

    @Override
    protected void onDestroy() {
        MusicControl.countNotification = 0;
        MusicControl.getInstance(this).stopMusic();
        taxiModels.clear();
        super.onDestroy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();
        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_dashboard,R.id.navigation_inbox, R.id.navigation_notifications)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
       // NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);
        bindService();
        checkBackgroundLocationPermission();

        if (Constant.getBaseUrl() == null) { // To prevent unexpected problem
            String baseUrl = Util.getString("baseUrl", this);
            logDebug("MainActivity", baseUrl);
            Constant.setBaseUrl(baseUrl);
        }

        int i =0;
        if (getIntent().hasExtra(NOTIFICATION)){
            i = getIntent().getIntExtra(NOTIFICATION,-1);
            if(i==0){
                i = n;
            }
        } else if (getIntent().hasExtra("not_tax")) {
            i = getIntent().getIntExtra("not_tax",-1);
            if (i == 0){
                i = MusicControl.countNotification;
            }
        }

        if (getIntent().hasExtra(EXTRA_LOCATION)) { /*1*/
            ServiceModel service = (ServiceModel) getIntent().getSerializableExtra(EXTRA_LOCATION);
            if (service != null){
                alertCall(service, getIntent(),i);
            }
        } else if (getIntent().hasExtra(EXTRA_LOCATION_TAXI)){
            TaxiModel taxiModel = (TaxiModel) getIntent().getSerializableExtra(EXTRA_LOCATION_TAXI);

            if(taxiModel != null){
                alertTaxi(taxiModel, getIntent(),i);
            }
        }

        String token = Util.getString("token",MainActivity.this);
        if (token!= null && !token.equals("")) {
            viewProfile();
        }
    }

    /*1 || 2 won't call together*/
    @Override
    protected void onNewIntent(Intent intent) { /*2*/
        super.onNewIntent(intent);

        int i = 0;
        if(getIntent().hasExtra(NOTIFICATION)){
            i = getIntent().getIntExtra(NOTIFICATION,-1);
            if(i==0){
                i = n;
            }
        } else if(getIntent().hasExtra("not_tax")){
            i = getIntent().getIntExtra("not_tax",-1);
            if(i==0){
                i = MusicControl.countNotification;
            }
        }
        if (intent.hasExtra(EXTRA_LOCATION)) {

            ServiceModel service = (ServiceModel) intent.getSerializableExtra(EXTRA_LOCATION);

            if (service != null) {
                alertCall(service, intent,i);
            }
        } else if(intent.hasExtra(EXTRA_LOCATION_TAXI)){
            TaxiModel taxiModel = (TaxiModel) intent.getSerializableExtra(EXTRA_LOCATION_TAXI);

            if(taxiModel!= null){
                alertTaxi(taxiModel,intent,i);
            }
        }
    }

    public  void alertCall(ServiceModel serviceModel, Intent dataIntent ,int i) {
        Intent intent = new Intent(this, ServiceArrivalActivity.class);
        serviceModels.add(serviceModel);
        intent.putExtra("serviceModel", serviceModel);
        intent.putExtra(NOTIFICATION,i);
        getActivityResultAlertCall(intent);
        dismissNotification(dataIntent);
    }

    private void getActivityResultAlertCall(Intent intent) {
        activityLauncher.launch(intent, result -> {
            if (result.getResultCode() == RESULT_OK) {
                boolean isSuccess = false;
                Intent data = result.getData();
                if (data != null) {
                    if (data.hasExtra("success")) {
                        isSuccess = data.getBooleanExtra("success", false);
                    }
                    ServiceModel serviceModel1 = (ServiceModel) data.getSerializableExtra("serviceModel");
                    if (isSuccess) {
                        Intent intent1 = new Intent(MainActivity.this, PickupActivity.class);
                        intent1.putExtra("deliveryId", serviceModel1.getDeliveryId());
                        intent1.putExtra("updateStatus",Constant.ACCEPTED);
                        intent1.putExtra("serviceModel", serviceModel1);
                        intent1.putExtra("typeOrder","Delivery");
                        startActivity(intent1);
                    } else if(serviceModel1 != null){
                        Intent intent1 = new Intent(MainActivity.this, ServiceArrivalActivity.class);
                        intent1.putExtra("serviceModel", serviceModel1);
                        getActivityResultAlertCall(intent1);
                    }
                }
            }
        });
    }

    public void alertTaxi(TaxiModel taxiModel,Intent taxi,int i){
        Intent intent = new Intent(this, TaxiArrivalActivity.class);
        taxiModels.add(taxiModel);
        intent.putExtra("taxiModel", taxiModel);
        intent.putExtra(NOTIFICATION,i);
        getActivityResultAlertTaxi(intent);
        dismissNotification(taxi);
    }

    private void getActivityResultAlertTaxi(Intent intent) {
        activityLauncher.launch(intent, result -> {
            Intent data = result.getData();
            if (data != null) {
                boolean issSuccess = data.getBooleanExtra("success",false);
                TaxiModel taxiModel =(TaxiModel) data.getSerializableExtra("taxiModel");
                if (issSuccess) {
                    Intent intent1 = new Intent(MainActivity.this, TaxiPickupActivity.class);
                    intent1.putExtra("deliveryId", taxiModel.getDeliverId());
                    intent1.putExtra("typeOrder", taxiModel.getTypeOrder());
                    intent1.putExtra("updateStatus", Constant.ACCEPTED);
                    intent1.putExtra("taximodel", taxiModel);
                    Waypoint waypoint = (Waypoint) data.getSerializableExtra("waypoint");
                    intent1.putExtra("waypoint", waypoint);
                    startActivity(intent1);

                } else if (taxiModel!=null) {
                    int i = 0;
                    if(getIntent().hasExtra(NOTIFICATION)){
                        i = getIntent().getIntExtra(NOTIFICATION,-1);
                    }
                    Intent intent1 = new Intent(MainActivity.this, TaxiArrivalActivity.class);
                    intent1.putExtra("taxiModel", taxiModel);
                    intent1.putExtra(NOTIFICATION,i);
                    getActivityResultAlertTaxi(intent1);
                }
            }
        });

    }

    private void checkBackgroundLocationPermission() {
        boolean permissionAccessCoarseLocationApproved =
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED;

        if (permissionAccessCoarseLocationApproved) {
            checkGps();
        } else {
            // App doesn't have access to the device's location at all. Make full request
            // for permission.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                ActivityCompat.requestPermissions(this, new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION
                }, REQUEST_LOCATION_PERMISSION);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION
                }, REQUEST_LOCATION_PERMISSION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_LOCATION_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                checkGps();
            } else {
                Toast.makeText(this, R.string.permission_denice, Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void checkGps() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        @SuppressLint("MissingPermission")
        Location location = manager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
        } else {
            if (mService != null) {
                mService.requestLocationUpdates();
            }
        }
    }

    private void bindService() {
        bindService(new Intent(this, LocationUpdatesService.class), mServiceConnection,Context.BIND_AUTO_CREATE);
    }

    private void buildAlertMessageNoGps() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());

        task.addOnSuccessListener(locationSettingsResponse -> {

        });

        task.addOnFailureListener(e -> {
            if (e instanceof ResolvableApiException) {
                // Location settings are not satisfied, but this can be fixed
                // by showing the user a dialog.
                try {
                    // Show the dialog by calling startResolutionForResult(),
                    // and check the result in onActivityResult().
                    ResolvableApiException resolvable = (ResolvableApiException) e;
                    resolvable.startResolutionForResult(MainActivity.this,
                            REQUEST_CODE_ENABLE_LOCATION);
                } catch (IntentSender.SendIntentException sendEx) {
                    // Ignore the error.
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_ENABLE_LOCATION && resultCode == RESULT_OK) {
            if (mService != null) {
                mService.requestLocationUpdates();
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
//        MusicControl.countNotification = 0;
//        MusicControl.getInstance(this).stopMusic();
        if (mBound) {
            // Unbind from the service. This signals to the service that this activity is no longer
            // in the foreground, and the service can respond by promoting itself to a foreground
            // service.
            unbindService(mServiceConnection);
            mBound = false;
        }
    }

    //  Here to dismiss popup Notification when user click on Accept Button =================================
    //  This Notification come from MyFirebaseMessagingService
    public void dismissNotification(Intent data) {
        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (manager != null) {
            int notificationId = data.getIntExtra(NOTIFICATION_ID, -1);
            logDebug("notiIddddAccept", notificationId + "");
            manager.cancel(notificationId);
        }
    }

    @Override
    public void onRegister(boolean register) {
        if (register) {
            bindService();
            logDebug("onRegister", "bindService" + register);
        } else {
            mService.removeLocationUpdates();
            if (mBound) { // to avoid app crash when service not yet bind
                unbindService(mServiceConnection);
//                stopService()
                mBound = false;
            }
            logDebug("onRegister", "unBindService" + register);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)){
            finishAffinity();
            MyFirebaseMessagingService.orderId =-1;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void buildAlertMessageNoNotification(final Activity activity) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.DialogTheme);
        builder.setMessage(R.string.your_notification)
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        Intent intent = new Intent();
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            intent.setAction(Settings.ACTION_APP_NOTIFICATION_SETTINGS);
                            intent.putExtra(Settings.EXTRA_APP_PACKAGE, activity.getPackageName());
                        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            intent.setAction("android.settings.APP_NOTIFICATION_SETTINGS");
                            intent.putExtra("app_package", activity.getPackageName());
                            intent.putExtra("app_uid", activity.getApplicationInfo().uid);
                        } else {
                            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            intent.addCategory(Intent.CATEGORY_DEFAULT);
                            intent.setData(Uri.parse("package:" + activity.getPackageName()));
                        }
                        activityLauncher.launch(intent, result -> {

                        });
                        dialog.dismiss();
                    }
                });
        final AlertDialog alert = builder.create();
        if (!activity.isFinishing()) alert.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!NotificationManagerCompat.from(MainActivity.this).areNotificationsEnabled())
            buildAlertMessageNoNotification(MainActivity.this);
    }

    private void viewProfile () {
        new ProfileWs().viewProfiles(MainActivity.this, new ProfileWs.ProfileViewAndUpdate() {
            @Override
            public void onLoadProfileSuccess(User user, String message) {
                if (user.getKyc() == null || user.getKyc() != null && user.getKyc().getStatus().equals("REJECT")) {
                    Intent intent = new Intent(MainActivity.this, AskToCompleteKycActivity.class);
                    startActivity(intent);
                } else if (user.getKyc() != null && user.getKyc().getStatus().equals("REVIEW")) {
                    Intent intent = new Intent(MainActivity.this, WaitingVerifyKycActivity.class);
                    startActivity(intent);
                }
            }

            @Override
            public void onError(String errorMessage) {

            }

            @Override
            public void onUpdateProfilesSuccess(String message) {

            }
        });
    }

}
