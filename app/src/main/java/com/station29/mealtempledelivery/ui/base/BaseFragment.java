package com.station29.mealtempledelivery.ui.base;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import androidx.activity.result.ActivityResult;
import androidx.fragment.app.Fragment;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.station29.mealtempledelivery.utils.BetterActivityResult;
import com.station29.mealtempledelivery.utils.Util;

import static com.station29.mealtempledelivery.api.Constant.LANG_EN;

/**
 * A simple {@link Fragment} subclass.
 */
public class BaseFragment extends Fragment {

    public Activity mActivity;
    public FirebaseAnalytics mFirebaseAnalytics;
    protected final BetterActivityResult<Intent, ActivityResult> activityLauncher = BetterActivityResult.registerActivityForResult(this);


    public BaseFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (mActivity == null && context instanceof BaseActivity) {
            mActivity = (BaseActivity) context;
            // Obtain the FirebaseAnalytics instance.
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
        }
        String lang = Util.getString("language", mActivity);
        if (lang.equals("")){
            lang = LANG_EN;
        }
        Util.changeLanguage(mActivity, lang);
    }
}
