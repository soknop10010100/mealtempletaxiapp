package com.station29.mealtempledelivery.api.request;

import android.app.Activity;


import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.station29.mealtempledelivery.api.CustomCallback;
import com.station29.mealtempledelivery.api.CustomResponseListener;
import com.station29.mealtempledelivery.api.RetrofitGenerator;
import com.station29.mealtempledelivery.api.ServiceAPI;
import com.station29.mealtempledelivery.data.model.Inbox;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

public class InboxWs {

    public void getInbox(Activity activity, int page, int size, String topic, String userType, final InboxWs.InboxCallBack inboxCallBack){
        ServiceAPI serviceAPI = RetrofitGenerator.createServiceWithAuth(activity);
        Call<JsonElement> res = serviceAPI.getInbox(page,size, topic, userType);
        res.enqueue(new CustomCallback(activity, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if(resObj.has("data")){
                    JsonObject jsonObject = resObj.getAsJsonObject("data");
                    JsonArray jsonArray = jsonObject.get("data").getAsJsonArray();
                    List<Inbox> inboxList = new ArrayList<>();
                    for (int i = 0; i < jsonArray.size(); i++) {
                        JsonElement ele = jsonArray.get(i);
                        Inbox inbox = new Gson().fromJson(ele, Inbox.class);
                        inboxList.add(inbox);
                    }
                    inboxCallBack.onLoadingSuccessFully(inboxList);
                }
            }

            @Override
            public void onError(String error, int code) {
                inboxCallBack.onLoadingFail(error);
            }


        }));
    }


    public interface InboxCallBack {
        void onLoadingSuccessFully(List<Inbox> getOrderListeners);
        void onLoadingFail(String message);
    }
}
