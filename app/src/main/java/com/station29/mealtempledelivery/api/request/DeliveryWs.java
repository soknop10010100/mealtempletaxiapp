package com.station29.mealtempledelivery.api.request;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.api.Constant;
import com.station29.mealtempledelivery.api.CustomCallback;
import com.station29.mealtempledelivery.api.CustomResponseListener;
import com.station29.mealtempledelivery.api.RetrofitGenerator;
import com.station29.mealtempledelivery.api.ServiceAPI;
import com.station29.mealtempledelivery.data.model.Delivery;
import com.station29.mealtempledelivery.data.model.DeliveryDetails;
import com.station29.mealtempledelivery.data.model.TaxiDeliveryDetail;
import com.station29.mealtempledelivery.data.model.Waypoint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;

import static com.station29.mealtempledelivery.utils.Util.isConnectedToInternet;
import static com.station29.mealtempledelivery.utils.Util.logDebug;
import static com.station29.mealtempledelivery.utils.Util.popupMessage;

public class DeliveryWs {

    public void acceptDelivery(final Context context, final String typeOrder, HashMap<String, Object> params, final OnAcceptDeliveryListener listener) {
        ServiceAPI serviceAPI = RetrofitGenerator.createServiceWithAuth(context);
        final Call<JsonElement> res = serviceAPI.acceptDelivery(params);
        res.enqueue(new CustomCallback(context,new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("success")) {
                    boolean isSuccess = resObj.get("success").getAsBoolean();
                    if (isSuccess) {
                        Waypoint waypoint = new Waypoint();
                        if (resObj.has("data")&& resObj.get("data")!=null){
                            if (resObj.get("data").getAsJsonObject().has("waypoint")&& !resObj.get("data").getAsJsonObject().get("waypoint").isJsonNull()){
                                JsonElement polyline = resObj.get("data").getAsJsonObject().get("waypoint");
                                if (polyline != null && !polyline.isJsonNull() && polyline.isJsonObject()){
                                    JsonObject jsonObject = polyline.getAsJsonObject();
                                    waypoint = new Gson().fromJson(jsonObject,Waypoint.class);
                                }
                            }
                        }
                        if (typeOrder.equalsIgnoreCase("service")){
                            listener.onAcceptSuccessTaxi(waypoint);
                        }else if(typeOrder.equalsIgnoreCase("send")){
                            listener.onAcceptSuccessTaxi(waypoint);
                        }else {
                            listener.onAcceptDeliverySuccess(waypoint);
                        }
                    } else {
                        JsonObject dataObj = resObj.getAsJsonObject();
                        String message = context.getString(R.string.uk);
                        if (dataObj.has("message")) {
                            message = dataObj.get("message").getAsString();
                        }
                        if(Constant.serviceLanguage.containsKey(message))
                        listener.onAcceptFailed(Constant.serviceLanguage.get(message));
                        else listener.onAcceptFailed(message);
                    }
                }
            }

            @Override
            public void onError(String error , int code) {
                 listener.onAcceptFailed(error);
            }
        }));
    }

    public void getDelivery(final Activity activity, String driverId, ArrayList<String> status, int limit, int page, final OnAcceptDeliveryListener accep) {
        ServiceAPI serviceAPI = RetrofitGenerator.createServiceWithAuth(activity);
        String statusString = status.toString().replace("[", "").replace("]", "").replace(" ","");
        logDebug("status", statusString);
        Call<JsonElement> res = serviceAPI.getDelivery(driverId, statusString, limit, page);
        res.enqueue(new CustomCallback(activity,new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("success") && resObj.get("success").getAsBoolean()) {
                    if (resObj.has("data")) {
                        List<Delivery> deliveryList = new ArrayList<>();
                        JsonObject dataObj = resObj.getAsJsonObject("data");
                        JsonArray dataArray = dataObj.getAsJsonArray("data");
                        for (int i = 0; i < dataArray.size(); i++) {
                            JsonObject object = (JsonObject) dataArray.get(i);
                            Delivery delivery = new Gson().fromJson(object, Delivery.class);
                            deliveryList.add(delivery);
                        }
                        if (accep != null) {
                            accep.onLoadListSuccess(deliveryList);
                        }
                    }
                }
            }

            @Override
            public void onError(String error , int code) {
                accep.onAcceptFailed(error);
            }
        }));
//
    }

    public void readDelivery(int deliveryId, final String typeOrder, final Activity activity, final ReadDeliveryListener listeners) {
        ServiceAPI service = RetrofitGenerator.createServiceWithAuth(activity);
        Call<JsonElement> res = service.readDeliveryV2(deliveryId);
        res.enqueue(new CustomCallback(activity,new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("success")) {
                    boolean isSuccess = resObj.get("success").getAsBoolean();
                    if (isSuccess) {
                            JsonElement data = resObj.getAsJsonObject("data");
                            if (typeOrder.equalsIgnoreCase("service")||typeOrder.equalsIgnoreCase("send")){//taxi
                                TaxiDeliveryDetail taxiDeliveryDetail = new Gson().fromJson(data,TaxiDeliveryDetail.class);
                                listeners.onLoadTaxiDetailSuccess(taxiDeliveryDetail);
                            }else {//food
                                DeliveryDetails details = new Gson().fromJson(data, DeliveryDetails.class);
                                listeners.onLoadDeliverySuccess(details);
                            }
                    } else {
                        JsonObject dataObj = resObj.getAsJsonObject("data");
                        String message = "Unknown problem";
                        if (dataObj.has("message")) {
                            message = dataObj.get("message").getAsString();
                        }
                        if(Constant.serviceLanguage.containsKey(message))
                        listeners.onLoadFail(Constant.serviceLanguage.get(message));
                        else listeners.onLoadFail(message);
                    }
                }
            }

            @Override
            public void onError(String error , int code) {
                listeners.onLoadFail(error);
            }
        }));

    }

    public void cancelTaxi(Activity activity ,int orderId, final DeliveryWs.OnAcceptDeliveryListener driverCallback) {
        ServiceAPI serviceAPI = RetrofitGenerator.createServiceWithAuth(activity);
        final Call<JsonElement> res = serviceAPI.cancelTaxi(orderId);
        res.enqueue(new CustomCallback(activity, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if(resObj.has("success") &&resObj.get("success").getAsBoolean()){
                    JsonObject jsonObject = resObj.getAsJsonObject("data");
                    if(jsonObject.has("message")) {
                        String message = jsonObject.get("message").getAsString();
                        if(Constant.serviceLanguage.containsKey(message))
                        driverCallback.onAcceptFailed(Constant.serviceLanguage.get(message));
                        else driverCallback.onAcceptFailed(message);
                    }
                }
            }

            @Override
            public void onError(String error,int code) {
                driverCallback.onAcceptFailed(error);
            }
        }));
    }


    public interface OnAcceptDeliveryListener {
        void onLoadListSuccess(List<Delivery> deliveryList);
        void onAcceptDeliverySuccess(Waypoint waypoint);
        void onAcceptSuccessTaxi(Waypoint waypoint);
        void onAcceptFailed(String message);
    }

    public interface ReadDeliveryListener {
        void onLoadDeliverySuccess(DeliveryDetails details);
        void onLoadTaxiDetailSuccess(TaxiDeliveryDetail deliveryDetail);
        void onLoadFail(String message);
    }


}
