package com.station29.mealtempledelivery.api;

import com.google.gson.JsonObject;

public interface CustomResponseListener {
    void onSuccess(JsonObject resObj);
    void onError(String error, int responseCode);
}