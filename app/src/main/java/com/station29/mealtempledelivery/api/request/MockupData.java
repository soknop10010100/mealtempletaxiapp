package com.station29.mealtempledelivery.api.request;

import android.content.Context;

import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.data.model.OnBoardingScreen;
import com.station29.mealtempledelivery.data.model.ServiceModel;
import com.station29.mealtempledelivery.data.model.User;
import com.station29.mealtempledelivery.ui.boarding.SelectLanguagesActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.station29.mealtempledelivery.api.Constant.LANG_EN;
import static com.station29.mealtempledelivery.api.Constant.LANG_FR;
import static com.station29.mealtempledelivery.api.Constant.LANG_KH;

public class MockupData {

    public static String PHONE_NUMBER = "";
    private static HashMap<String, Object> account = new HashMap<>();
    public static List<ServiceModel> saveSM = new ArrayList<>();
    private static User user;
    public static HashMap<String, Object> getAccount() {
        return MockupData.account;
    }
    public static void setAccount(HashMap<String, Object> account) { MockupData.account = account; }

    public static User getUser() {
        return user;
    }

    public static void setUser(User user) {
        MockupData.user = user;
    }

    public static List<OnBoardingScreen> getOnBoardingScreen(Context context){
        List<OnBoardingScreen> list = new ArrayList<>();
        list.add(new OnBoardingScreen(R.drawable.ic_driver_boarding_1,context.getString(R.string.congrats),context.getString(R.string.your_are_just_one),0));
        list.add(new OnBoardingScreen(R.drawable.ic_driver_2,context.getString(R.string.registration),context.getString(R.string.fill_in_your_imformation),1));
        list.add(new OnBoardingScreen(R.drawable.ic_driver_boarding_3,context.getString(R.string.flexibility),context.getString(R.string.you_are_ready_now),2));
        list.add(new OnBoardingScreen(R.drawable.ic_driver_boarding_4,context.getString(R.string.instant_income),context.getString(R.string.locate_your_first_client),3));
        return list;
    }

    public static List<String> getListTabPaymentDepositWithDraw(Context context){
        List<String> list = new ArrayList<>();
        list.add(context.getString(R.string.deposit));
        list.add(context.getString(R.string.withdraw));
        return list;
    }
    public static ArrayList<String> listLange (Context context){
        ArrayList<String> list = new ArrayList<>();
        list.add(context.getString(R.string.khmer));
        list.add(context.getString(R.string.english));
        list.add(context.getString(R.string.france));
       // list.add(context.getString(R.string.laos));
//        list.add(context.getString(R.string.spainish));
//        list.add(context.getString(R.string.herbrew));
//        list.add(context.getString(R.string.swedish));
//        list.add(context.getString(R.string.german));
//        list.add(context.getString(R.string.serbian));
//        list.add(context.getString(R.string.chinese));
//        list.add(context.getString(R.string.arabic));
//        list.add(context.getString(R.string.korea));
        return list;
    }
    public static ArrayList<SelectLanguagesActivity.Language> listLanguages(Context context) {
        ArrayList<SelectLanguagesActivity.Language> languageList = new ArrayList<>();
        languageList.add(new SelectLanguagesActivity.Language(context.getString(R.string.khmer), R.drawable.flag_cambodia, false, LANG_KH));
        languageList.add(new SelectLanguagesActivity.Language(context.getString(R.string.france), R.drawable.flag_france, false, LANG_FR));
        languageList.add(new SelectLanguagesActivity.Language(context.getString(R.string.english), R.drawable.flag_united_kingdom, false, LANG_EN));
//        languageList.add(new SelectLanguagesActivity.Language(context.getString(R.string.laos), R.drawable.flag_of_laos, false, LANG_LA));
//        languageList.add(new SelectLanguagesActivity.Language(context.getString(R.string.spainish), R.drawable.flag_of_spain, false, LANG_SP));
//        languageList.add(new SelectLanguagesActivity.Language(context.getString(R.string.herbrew), R.drawable.flag_of_herbrew, false, LANG_HE));
//        languageList.add(new SelectLanguagesActivity.Language(context.getString(R.string.swedish), R.drawable.flag_of_sweden, false, LANG_SW));
//        languageList.add(new SelectLanguagesActivity.Language(context.getString(R.string.german), R.drawable.flag_of_german, false, LANG_GM));
//        languageList.add(new SelectLanguagesActivity.Language(context.getString(R.string.serbian), R.drawable.flag_of_serbain, false, LANG_SB));
//        languageList.add(new SelectLanguagesActivity.Language(context.getString(R.string.chinese), R.drawable.flag_of_china, false, LANG_CH));
//        languageList.add(new SelectLanguagesActivity.Language(context.getString(R.string.arabic), R.drawable.ic_flag_arabic, false, LANG_AR));
//        languageList.add(new SelectLanguagesActivity.Language(context.getString(R.string.korea), R.drawable.flag_korean, false, LANG_KO));
        return languageList;
    }

    public static ArrayList<String> getStringReason(Context context){
        ArrayList<String> arrayList = new ArrayList<>();

        arrayList.add(context.getString(R.string.the_kiwigo_app_itself_sucks));
        arrayList.add(context.getString(R.string.the_kiwigo_app_is_not_secure));
        arrayList.add(context.getString(R.string.you_don_use_kiwigo_anymore));
        arrayList.add(context.getString(R.string.the_customer_service_is_not_good));
        arrayList.add(context.getString(R.string.dont_want_kiwigo_to_tracking_data));
        arrayList.add(context.getString(R.string.not_trust_kiwigo));
        return arrayList;
    }



}

