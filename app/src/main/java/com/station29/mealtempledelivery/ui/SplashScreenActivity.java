package com.station29.mealtempledelivery.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.api.Constant;
import com.station29.mealtempledelivery.api.request.ConfigWs;
import com.station29.mealtempledelivery.api.request.ServerUrlWs;
import com.station29.mealtempledelivery.data.model.Config;
import com.station29.mealtempledelivery.data.model.PaymentService;
import com.station29.mealtempledelivery.data.model.ServiceTypeDriver;
import com.station29.mealtempledelivery.ui.base.BaseActivity;
import com.station29.mealtempledelivery.ui.boarding.SelectLanguagesActivity;
import com.station29.mealtempledelivery.ui.login.LoginActivity;
import com.station29.mealtempledelivery.utils.GPSTracker;
import com.station29.mealtempledelivery.utils.Util;

import java.io.IOException;
import java.util.List;

import static com.station29.mealtempledelivery.api.Constant.LOCATION_REQUEST_PROBLEM;
import static com.station29.mealtempledelivery.utils.Util.logDebug;

@SuppressLint("CustomSplashScreen")
public class SplashScreenActivity extends BaseActivity {

    private final static int REQUEST_LOCATION_PERMISSION = 546;
    private final static int REQUEST_CODE_ENABLE_LOCATION = 246;
    private LocationCallback mLocationCallback;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationRequest locationRequest;
    public static Config splashConfig;
    private String lang = "", start;
    private boolean isAlreadyGetConfig = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        // To remember language ==================
        lang = Util.getString("language", this);
        start = Util.getString("start",this);
        if(lang.equals("")){
            lang =  Constant.LANG_EN;
        }
        Util.changeLanguage(this, lang);
        // ==================================

        // check version code
        AppUpdateManager appUpdateManager = AppUpdateManagerFactory.create(SplashScreenActivity.this);
        Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();
        appUpdateInfoTask.addOnFailureListener(e -> {
            // Add Analytics to track what pb in this screen -=========================
            Bundle bundle = new Bundle();
            bundle.putString(Constant.PHONE_MODEL, android.os.Build.MODEL);
            // =========================================================================
            checkBackgroundLocationPermission();
            initLocationRequest();
            // ======================================================================================
            bundle.putString(Constant.COUNTRY_ZIP_CODE, "No Sim Card. Find Code by GPS/Location instead");
            // ===================================================================
            mFirebaseAnalytics.logEvent(this.getClass().getSimpleName(), bundle);
            //=======================================================================
        });
        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)) {
                Util.checkUpdatePopUp(SplashScreenActivity.this);
            } else {
                // Add Analytics to track what pb in this screen -=========================
                Bundle bundle = new Bundle();
                bundle.putString(Constant.PHONE_MODEL, android.os.Build.MODEL);
                // =========================================================================
                checkBackgroundLocationPermission();
                initLocationRequest();
                // ======================================================================================
                bundle.putString(Constant.COUNTRY_ZIP_CODE, "No Sim Card. Find Code by GPS/Location instead");
                // ===================================================================
                mFirebaseAnalytics.logEvent(this.getClass().getSimpleName(), bundle);
                //=======================================================================
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_ENABLE_LOCATION && resultCode == RESULT_OK) {
            requestLocationUpdate();
        }
    }

    private void checkBackgroundLocationPermission() {
        boolean permissionAccessCoarseLocationApproved =
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED;

        buildAlertMessageNoGps();

        if (permissionAccessCoarseLocationApproved) {
            checkGps();
        } else {
            // App doesn't have access to the device's location at all. Make full request
            // for permission.
//            alertPermissionDeclarationForm();
            alertRequestPermission();
        }
    }

    private void alertRequestPermission(){
        ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION
        }, REQUEST_LOCATION_PERMISSION);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
//            ActivityCompat.requestPermissions(this, new String[]{
//                    Manifest.permission.ACCESS_FINE_LOCATION
//            }, REQUEST_LOCATION_PERMISSION);
//        } else {
//            ActivityCompat.requestPermissions(this, new String[]{
//                    Manifest.permission.ACCESS_FINE_LOCATION
//            }, REQUEST_LOCATION_PERMISSION);
//        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_LOCATION_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // permission was granted, yay! Do the
                // contacts-related task you need to do.
                checkGps();
            } else if (grantResults.length > 0 && grantResults[0] != PackageManager.PERMISSION_GRANTED){
                Util.popupMessageAndClose(getString(R.string.location_permissions_declaration), getString(R.string.location_permission_not_allow), SplashScreenActivity.this);
            }
        }
    }

    private void checkGps() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        assert manager != null;
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
        } else {
            getLastKnowLocation();
        }
    }

    private void stopLocationUpdates() {
        if (mLocationCallback == null) return;
        @SuppressLint("VisibleForTests") FusedLocationProviderClient fusedLocationProviderClient = new FusedLocationProviderClient(this);
        fusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
    }

    private void startLocationUpdates() {
        if (mLocationCallback == null) return;
        mFusedLocationClient = new FusedLocationProviderClient(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mFusedLocationClient.requestLocationUpdates(locationRequest,
                mLocationCallback,
                Looper.myLooper());
    }

    private void initLocationRequest() {
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    logDebug("locationLoginUpdate", location.getLatitude() + " : " + location.getLongitude());
                    mFusedLocationClient.removeLocationUpdates(mLocationCallback);
                    findZipCode(location.getLatitude(), location.getLongitude());
                }
            }
        };
    }

    private void buildAlertMessageNoGps() {
        if (locationRequest == null) initLocationRequest();
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());

        task.addOnSuccessListener(new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                logDebug("locationLoginGPSRequest", "locationSettingsResponse");
            }
        });

        task.addOnFailureListener(e -> {
            if (e instanceof ResolvableApiException) {
                // Location settings are not satisfied, but this can be fixed
                // by showing the user a dialog.
                try {
                    // Show the dialog by calling startResolutionForResult(),
                    // and check the result in onActivityResult().
                    ResolvableApiException resolvable = (ResolvableApiException) e;
                    resolvable.startResolutionForResult(SplashScreenActivity.this,
                            REQUEST_CODE_ENABLE_LOCATION);
                } catch (IntentSender.SendIntentException sendEx) {
                    // Ignore the error.
                }
            }
        });
    }

    private void requestLocationUpdate() {
        if (mFusedLocationClient == null || mLocationCallback == null) {
            // If code enter this block, there is no way to solve.
            logDebug("requestLocationUpdate", "This block happens only when user clear Location while app already granted those permissions.");
            logDebug("locationLoginRequest", "unexpected problem" + mFusedLocationClient + "," + locationRequest + ", " + mLocationCallback);

            // ===================================================================
            Bundle bundle = new Bundle();
            bundle.putString(LOCATION_REQUEST_PROBLEM, "unexpected problem" + mFusedLocationClient + "," + locationRequest + ", " + mLocationCallback);
            mFirebaseAnalytics.logEvent(this.getClass().getSimpleName(), bundle);
            //=======================================================================

            return;
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mFusedLocationClient.requestLocationUpdates(locationRequest,
                mLocationCallback, Looper.getMainLooper());
    }

    private void getLastKnowLocation() {
        GPSTracker gpsTracker = new GPSTracker(this);
        if (gpsTracker.canGetLocation() && gpsTracker.getLatitude() != 0 && gpsTracker.getLongitude() != 0) {
            double lat = gpsTracker.getLatitude();
            double lng = gpsTracker.getLongitude();
            logDebug("locationLoginLastKnown", lat + " : " + lng);
            findZipCode(lat, lng);
        } else {
            requestLocationUpdate();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopLocationUpdates();
    }

    @Override
    protected void onResume() {
        super.onResume();
        startLocationUpdates();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        removeLocationUpdate();
    }

    private void removeLocationUpdate() {
        if (mFusedLocationClient != null && mLocationCallback != null) {
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        }
    }

    @SuppressLint("StaticFieldLeak")
    private void findZipCode(final double lat, final double lng) {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... voids) {
                Geocoder geocoder = new Geocoder(SplashScreenActivity.this);
                List<Address> addresses;
                try {
                    addresses = geocoder.getFromLocation(lat, lng, 1);
                    if (addresses.size() > 0) {
                        countryCode = addresses.get(0).getCountryCode();
                        latitude = lat;
                        longitude = lng;
                    }
                } catch (IOException e) {
                    Log.e(e.getClass().getName(), e.getMessage() + "");
                    BaseActivity.countryCode = Util.getCountryCodeFromSimCard(SplashScreenActivity.this);
                    // In case sim card pb such as Orange Money Sim card
                    GPSTracker gps = new GPSTracker(SplashScreenActivity.this);
                    if (gps.canGetLocation()){
                        try {
                            // Lat, Lng get from LocationManager is longer than from Google Play Service Location request
                            // Geocoder has bug sometime when Lat, Lng is short
                            addresses = geocoder.getFromLocation(gps.getLatitude(), gps.getLongitude(), 1);
                            if (addresses.size() > 0) {
                                countryCode = addresses.get(0).getCountryCode();
                            }
                        } catch (IOException ioException) {
                            //Util.popupMessageAndClose(null, getString(R.string.cannot_recognise_server), SplashScreenActivity.this);
                        }
                    }
                }
                return countryCode;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
//                if (s != null && !s.isEmpty() && !isGetUrl) { isGetUrl = true; requestBaseUrlCodeTask(s);}
                if (s != null && !s.isEmpty()) {
                    if (!isAlreadyGetConfig) {
                        requestBaseUrlCodeTask(s);
                    }
                }
            }
        }.execute();
    }

    private void requestBaseUrlCodeTask(final String code) {
        new ServerUrlWs().readServerUrl(SplashScreenActivity.this, code, new ServerUrlWs.loadServerUrl() {
            @Override
            public void onLoadSuccess(boolean success, String baseUrl) {
                logDebug("getconfig", "config: " );
                isAlreadyGetConfig = true;
                if (success){
                    removeLocationUpdate(); //To stop Get Location Update when we got Base Url (Khmer or Loas)
                }
                getConfigLang();
                // ================================================================================
                Bundle bundle = new Bundle();
                bundle.putString(Constant.REQUEST_BASEURL, "Code " + code + "BaseUrl " + baseUrl);
                mFirebaseAnalytics.logEvent(this.getClass().getSimpleName(), bundle);
                //===================================================================================
            }

            @Override
            public void onLoadFailed(String errorMessage) {
                Toast.makeText(SplashScreenActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                // ================================================================================
                Bundle bundle = new Bundle();
                bundle.putString(Constant.REQUEST_BASEURL, "Code " + code + "Failed Message " + errorMessage);
                mFirebaseAnalytics.logEvent(this.getClass().getSimpleName(), bundle);
                //===================================================================================
            }
        });
    }

    private void config() {
        new ConfigWs().readConfig(SplashScreenActivity.this,BaseActivity.countryCode, "notification" , "getConfig", new ConfigWs.ReadConfigCallback() {
            @Override
            public void onSuccess(Config config, List<ServiceTypeDriver> serviceTypeDrivers) {
                splashConfig = config;

                for (PaymentService paymentService : Constant.getConfig().getPaymentServiceList()) {
                    if (paymentService.getIsCash() == 1) {
                        Constant.getConfig().getPaymentServiceList().remove(paymentService);
                        Constant.getConfig().getPaymentServiceList().add(Constant.getConfig().getPaymentServiceList().size(), paymentService);
                        break;
                    }
                }
                for (PaymentService paymentService : Constant.getConfig().getPaymentServiceList()) {
                    if ( paymentService.getType() != null && paymentService.getType().equals("KESS_PAY")) {
                        Constant.getConfig().getPaymentServiceList().remove(paymentService);
                        Constant.getConfig().getPaymentServiceList().add(Constant.getConfig().getPaymentServiceList().size(), paymentService);
                        break;
                    }
                }

                String token = Util.getString("token", SplashScreenActivity.this);
                if (token.isEmpty()) {
                    if (Constant.getStart().equals("start") && start.equals("") || start.equals("start")) {
                        Intent intent = new Intent(SplashScreenActivity.this, SelectLanguagesActivity.class);
                        intent.putExtra("lang", lang);
                        startActivity(intent);
                    } else {
                        Util.saveString("token","",SplashScreenActivity.this);
                        Intent intent = new Intent(SplashScreenActivity.this, LoginActivity.class);
                        intent.putExtra("lang", lang);
                        startActivity(intent);
                    }
                } else {
                    startActivity(new Intent(SplashScreenActivity.this, MainActivity.class));
                }
            }

            @Override
            public void onError(String message) {
                Toast.makeText(SplashScreenActivity.this, message,Toast.LENGTH_SHORT ).show();
            }
        });
    }

    private void getConfigLang() {
        new ConfigWs().readConfig(SplashScreenActivity.this,lang, "notification","getConfigLang", new ConfigWs.ReadConfigCallback() {
            @Override
            public void onSuccess(Config config, List<ServiceTypeDriver> serviceTypeDriver) {
                config();
            }

            @Override
            public void onError(String message) {
                Toast.makeText(SplashScreenActivity.this, message, Toast.LENGTH_SHORT).show();
            }
        });
    }

//    private void alertPermissionDeclarationForm() {
//        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setTitle(R.string.location_permissions_declaration);
//        builder.setMessage(R.string.location_permission_form);
//        builder.setNegativeButton(getString(R.string.decline), new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                Util.popupMessageAndClose(getString(R.string.location_permissions_declaration), getString(R.string.location_permission_not_allow), SplashScreenActivity.this);
//            }
//        });
//        builder.setPositiveButton(getString(R.string.next), new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                alertRequestPermission();
//            }
//        });
//        builder.setCancelable(false);
//        builder.show();
//    }
}
