package com.station29.mealtempledelivery.ui.setting;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;
import com.rilixtech.widget.countrycodepicker.CountryCodePicker;
import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.api.request.MockupData;
import com.station29.mealtempledelivery.api.request.PaymentWs;
import com.station29.mealtempledelivery.api.request.SignUpWs;
import com.station29.mealtempledelivery.data.model.User;
import com.station29.mealtempledelivery.ui.base.BaseActivity;
import com.station29.mealtempledelivery.ui.login.VerifyCodeActivity;
import com.station29.mealtempledelivery.utils.Util;

import java.util.HashMap;
import java.util.StringTokenizer;

public class SetChangePinActivity extends BaseActivity {

    private EditText pinEt1, pinEt2 , oldPinEt;
    private MaterialButton btnConfirm;
    private User user;
    private ImageView backButton,imgChangePin;
    private TextView title,btnForgotPin,txtForgotPin;
    private CountryCodePicker countryCodePicker;
    private boolean isForgotPin = false;
    private View progress_loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_pin);

        initView();
        initData();
        initAction();
    }

    private void initView(){

        pinEt1 = findViewById(R.id.pinEt1);
        pinEt2 = findViewById(R.id.pinEt2);
        btnConfirm = findViewById(R.id.btnConfirm);
        oldPinEt = findViewById(R.id.oldPinEt);
        backButton = findViewById(R.id.backButton);
        title = findViewById(R.id.toolBarTitle);
        btnForgotPin = findViewById(R.id.btnForgotPin);
        progress_loading = findViewById(R.id.progress_loading);
        txtForgotPin = findViewById(R.id.txtForgotPin);
        imgChangePin = findViewById(R.id.imgChangePin);

    }

    private void initData(){

        if(getIntent() != null && getIntent().hasExtra("user")) {
            user = (User) getIntent().getSerializableExtra("user");
            if (user != null && user.isConfirmPin()){
                title.setText(R.string.change_pin);
            } else {
                oldPinEt.setVisibility(View.GONE);
                title.setText(R.string.set_pin);
                btnForgotPin.setVisibility(View.GONE);
            }
            isForgotPin = false;
        } else if (getIntent() != null && getIntent().hasExtra("forgotUser")) {
            user = (User) getIntent().getSerializableExtra("forgotUser");
            imgChangePin.setVisibility(View.VISIBLE);
            txtForgotPin.setText(R.string.please_input_uour_new_pin);
            oldPinEt.setVisibility(View.GONE);
            isForgotPin = true;
            title.setText(R.string.reset_pin);
            pinEt1.setHint(R.string.input_new_pin);
            pinEt2.setHint(R.string.input_confirm_new_pin);
            btnForgotPin.setVisibility(View.GONE);
        }
    }

    private void initAction(){

        countryCodePicker = new CountryCodePicker(this);

        btnConfirm.setOnClickListener(v -> {
            HashMap<String, Object> hashMap = new HashMap<>();
            if(!isForgotPin){
                progress_loading.setVisibility(View.VISIBLE);
                if(user != null && user.isConfirmPin() && !isForgotPin){
                    hashMap.put("old_pin_password",oldPinEt.getText().toString());
                }
                hashMap.put("pin_password",pinEt1.getText().toString());
                hashMap.put("pin_password_confirm",pinEt2.getText().toString());
                progress_loading.setVisibility(View.GONE);
                new PaymentWs().setUserPin(SetChangePinActivity.this,hashMap,onTopUpWalletsCallBack);
            } else {
                if (pinEt1.getText().toString().length() == 4 && pinEt2.getText().toString().length() == 4){
                    popMakeSureSendSMS();
                    progress_loading.setVisibility(View.GONE);
                } else {
                    progress_loading.setVisibility(View.GONE);
                    Toast.makeText(SetChangePinActivity.this, R.string.pin_must_have_4,Toast.LENGTH_SHORT).show();
                }
            }
        });

        backButton.setOnClickListener(v -> {
            Intent intent = new Intent();
            intent.putExtra("status",false);
            setResult(RESULT_OK);
            finish();
        });


        btnForgotPin.setOnClickListener(v -> {
            if(user.getPhoneNumber() != null && !user.getPhoneNumber().equals("")){
                Intent intent = new Intent(SetChangePinActivity.this,SetChangePinActivity.class);
                intent.putExtra("forgotUser",user);
                startResult(intent);
                progress_loading.setVisibility(View.GONE);
            }
        });

    }

    private void forgotPinParam(){
        HashMap<String, Object> userAccount = new HashMap<>(  ) ;
        String phone = user.getPhoneNumber() ;
        String code;
        StringTokenizer defaultTokenizer = new StringTokenizer(phone);
        code = defaultTokenizer.nextToken();
        countryCodePicker.setCountryForPhoneCode(Integer.parseInt(code));
        phone = phone.replace(code, "".trim());
        phone = phone.replaceAll("\\s+", "");
        userAccount.put("phone",phone);
        userAccount.put("phone_code",countryCodePicker.getFullNumberWithPlus());
        userAccount.put ( "code",countryCodePicker.getSelectedCountryNameCode()  );
        userAccount.put ( "user_type","USER_ANDROID");
        userAccount.put("function","Driver");
        MockupData.setAccount(userAccount);
        progress_loading.setVisibility(View.VISIBLE);
        new SignUpWs().forgetAccountPassword(SetChangePinActivity.this,userAccount ,requestServer);
    }

    private final SignUpWs.RequestServer requestServer = new SignUpWs.RequestServer () {
        @Override
        public void onRequestSuccess() {
            progress_loading.setVisibility(View.GONE);
            HashMap<String, String> forgotPINMap = new HashMap<>();
            forgotPINMap.put("pin_password",pinEt1.getText().toString());
            forgotPINMap.put("pin_password_confirm",pinEt2.getText().toString());
            Intent intent = new Intent(SetChangePinActivity.this, VerifyCodeActivity.class);
            intent.putExtra("forgotPINParam",forgotPINMap);
            activityLauncher.launch(intent, result -> {
                if (result.getResultCode() == RESULT_OK){
                    finish();
                }
            });
        }

        @Override
        public void onRequestToken(String token, int id, User user) {

        }

        @Override
        public void onRequestFailed(String message) {
            progress_loading.setVisibility(View.GONE);
            Toast.makeText(SetChangePinActivity.this,message, Toast.LENGTH_SHORT).show();
        }
    };

    private final PaymentWs.OnPaymentWalletsCallBack onTopUpWalletsCallBack = new PaymentWs.OnPaymentWalletsCallBack() {
        @Override
        public void onSuccess(String message, String paymentId) {
            progress_loading.setVisibility(View.GONE);
            Toast.makeText(SetChangePinActivity.this,message, Toast.LENGTH_SHORT).show();
            Intent intent = new Intent();
            intent.putExtra("status",true);
            setResult(RESULT_OK,intent);
            finish();
        }

        @Override
        public void onError(String message) {
            progress_loading.setVisibility(View.GONE);
            Util.popupMessage(getString(R.string.error),message, SetChangePinActivity.this);
        }
    };

    private void popMakeSureSendSMS() {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(SetChangePinActivity.this,R.style.DialogTheme);
        builder.setMessage(String.format("%s : %s",getString(R.string.we_will_send_sms_to_verify_with_this_phone),user.getPhoneNumber()));
        builder.setNegativeButton(getResources().getString(R.string.cancel), null);
        builder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(pinEt1.getText().toString().equals(pinEt2.getText().toString()) && !pinEt1.getText().toString().equals("") && !pinEt2.getText().toString().equals("")){
                    forgotPinParam();
                } else {
                    progress_loading.setVisibility(View.GONE);
                    Util.popupMessage(getString(R.string.error),getString(R.string.pin_code_are_incorrect), SetChangePinActivity.this);
                }
            }
        });
        builder.show();
    }

    private void startResult(Intent intent){
        activityLauncher.launch(intent,result -> {
            if (result.getResultCode() == RESULT_OK) {
                setResult(RESULT_OK);
                progress_loading.setVisibility(View.GONE);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("status",false);
        setResult(RESULT_OK,intent);
        finish();
    }
}