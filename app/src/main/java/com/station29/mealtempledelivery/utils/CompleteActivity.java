package com.station29.mealtempledelivery.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.core.app.ActivityCompat;

import com.ebanx.swipebtn.OnStateChangeListener;
import com.ebanx.swipebtn.SwipeButton;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.api.Constant;
import com.station29.mealtempledelivery.api.GoogleDirectionControllerCallBack;
import com.station29.mealtempledelivery.api.request.DeliveryWs;
import com.station29.mealtempledelivery.data.model.Delivery;
import com.station29.mealtempledelivery.data.model.DeliveryDetails;
import com.station29.mealtempledelivery.data.model.ServiceModel;
import com.station29.mealtempledelivery.data.model.TaxiDeliveryDetail;
import com.station29.mealtempledelivery.data.model.Waypoint;
import com.station29.mealtempledelivery.service.LocationUpdatesService;
import com.station29.mealtempledelivery.ui.base.BaseActivity;
import com.station29.mealtempledelivery.ui.MainActivity;
import com.station29.mealtempledelivery.ui.home.OrderDetailActivity;
import com.station29.mealtempledelivery.ui.home.WaitingDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.station29.mealtempledelivery.utils.Util.isConnectedToInternet;
import static com.station29.mealtempledelivery.utils.Util.logDebug;
import static com.station29.mealtempledelivery.utils.Util.popupMessage;

public class CompleteActivity extends BaseActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private int deliveryId;
    private DeliveryDetails mDetail;
    private ActionBar actionBar;
    private String mStatus;
    private ServiceModel serviceModel;
    private TextView storePhone, clientPhone, storeName, deliverTime, totalPrice, deliveryFee, riderTip;
    private SwipeButton completeBtn;
    private boolean isNotyetDrawMap;
    private Marker driverMarker;
    // private MyReceiver myReceiver;
    private Waypoint waypoint;
    String typeOrder = "Delivery";
    SupportMapFragment mapFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete);

        deliverTime = findViewById(R.id.DeliveryTimeComp);
        storeName = findViewById(R.id.storeNameComp);
        storePhone = findViewById(R.id.storePhoneComp);
        clientPhone = findViewById(R.id.clientPhoneComp);
        ImageView btnCancel = findViewById(R.id.btnBackCom);
        actionBar = this.getSupportActionBar();

        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        if (mapFragment != null) mapFragment.getMapAsync(this);

        final ShimmerFrameLayout shimmerFrameLayout = findViewById(R.id.shimmer);
        shimmerFrameLayout.startShimmer();

        completeBtn = findViewById(R.id.completed);
        completeBtn.setOnStateChangeListener(active -> {
            if (!active) return;
            shimmerFrameLayout.hideShimmer();
            if (mDetail == null) {
                return;
            }
            updateStatusTask(deliveryId, completeBtn, mDetail.getCooperation());
            shimmerFrameLayout.showShimmer(true);
            // syncDriverMovingPoints();
        });
        if (getIntent() != null && getIntent().hasExtra("updateStatus") && getIntent().hasExtra("deliveryId")) {
            //main screen(home fragment)
            deliveryId = getIntent().getIntExtra("deliveryId", -1);
            mStatus = getIntent().getStringExtra("updateStatus");
            if (getIntent().hasExtra("typeOrder")) {
                typeOrder = (String) getIntent().getSerializableExtra("typeOrder");
            }
        }
        if (getIntent() != null && getIntent().hasExtra("serviceModel") && getIntent().getSerializableExtra("serviceModel") != null) {
            serviceModel = (ServiceModel) getIntent().getSerializableExtra("serviceModel");
            deliveryId = getIntent().getIntExtra("deliveryId", -1);
            waypoint = (Waypoint) getIntent().getSerializableExtra("waypoint");
            if (serviceModel == null) return;
            if (waypoint != null) {
                waypoint = mDetail.getWaypoints();
            }

        }
        btnCancel.setOnClickListener(v -> {
            refresh();
            onBackPressed();
        });

        if (mStatus != null) getDeliveryDetailTask();

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    private void getDeliveryDetailTask() {
        if (Constant.getBaseUrl() != null)
            new DeliveryWs().readDelivery(deliveryId, "delivery", this, new DeliveryWs.ReadDeliveryListener() {
                @SuppressLint("StringFormatInvalid")
                @Override
                public void onLoadDeliverySuccess(final DeliveryDetails details) {
                    mDetail = details;
                    waypoint = details.getWaypoints();
                    zoomMap();
                    if (waypoint != null) {
                        drawMap(waypoint);
                    }
                    if (actionBar != null) {
                        actionBar.setTitle(getString(R.string.from) + mDetail.getStoreName());
                    }

                    storePhone.setText(String.format("%s %s", getString(R.string.store), details.getTel()));
                    clientPhone.setText(String.format("%s %s",getString(R.string.customers), details.getShippingPhone()));
                    clientPhone.setMovementMethod(LinkMovementMethod.getInstance());

                    final Button viewMenu = findViewById(R.id.viewMenuComp);
                    viewMenu.setOnClickListener(v -> {
                        Intent intent = new Intent(CompleteActivity.this, OrderDetailActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        intent.putExtra("id", deliveryId);
                        intent.putExtra("back","back");
                        startActivity(intent);
                    });
                    clientPhone.setOnClickListener(view -> {
                        Intent callIntent = new Intent(Intent.ACTION_DIAL);
                        callIntent.setData(Uri.parse("tel:" + details.getShippingPhone()));
                        callIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(callIntent);
                    });

                    storePhone.setOnClickListener(view -> {
                        Intent callIntent = new Intent(Intent.ACTION_DIAL);
                        callIntent.setData(Uri.parse("tel:" + details.getTel()));
                        callIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(callIntent);
                    });

                }

                @Override
                public void onLoadTaxiDetailSuccess(TaxiDeliveryDetail deliveryDetail) {

                }

                @Override
                public void onLoadFail(String message) {
                }
            });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            refresh();
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void refresh() {
        Intent intent = new Intent(CompleteActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
        zoomMap();
//        View locationButton = ((View) mapFragment.getView().findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
//        RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
//        // position on right bottom
//        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
//        rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
//        rlp.setMargins(0, 0, 30, 30);
    }

    private void addMarker(GoogleMap googleMap, int icon, double lat, double lng, String title, boolean isShowWindowInfos) {
        LatLng sydney = new LatLng(lat, lng);
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(sydney).icon(BitmapDescriptorFactory.fromResource(icon)).title(title);

        if (isShowWindowInfos) {
            googleMap.addMarker(markerOptions).showInfoWindow();
        } else {
            googleMap.addMarker(markerOptions);
        }
    }

    private final WaitingDialog.OnCloseButtonClickListener onCloseButtonClickListener = new WaitingDialog.OnCloseButtonClickListener() {
        @Override
        public void onClose(WaitingDialog dialog) {
            refresh();

        }

        @Override
        public void onOk(WaitingDialog waitingDialog) {
            goDetail();
        }

        @Override
        public void onClickRate(WaitingDialog waitingDialog) {
            Intent intent = new Intent(CompleteActivity.this, OrderDetailActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.putExtra("id", deliveryId);
            intent.putExtra("back","notBack");
            intent.putExtra("open_rate","open_rate");
            startActivity(intent);
            finish();
        }
    };

    private void goDetail(){
        Intent intent = new Intent(this, OrderDetailActivity.class);
        intent.putExtra("typeOrder","delivery");
        intent.putExtra("back","notBack");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("id", deliveryId);
        startActivity(intent);
        finish();
    }

    private void updateStatusTask(final int deliveryId, final SwipeButton button, int cooperation) {

        if (Constant.getBaseUrl() != null) {
            button.setVisibility(View.VISIBLE);
            if (mStatus != null && mStatus.equalsIgnoreCase(Constant.ACCEPTED)) {
                mStatus = Constant.PICKED_UP;
            } else if (mStatus != null && mStatus.equalsIgnoreCase(Constant.PICKED_UP)) {
                button.setVisibility(View.VISIBLE);
                mStatus = Constant.DELIVERED;
            }
            logDebug("statusSend", mStatus);
            final WaitingDialog dialog = new WaitingDialog(CompleteActivity.this, onCloseButtonClickListener);
            dialog.show();
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("delivery_id", deliveryId);
            hashMap.put("status", mStatus);
            hashMap.put("cooperation", cooperation);
            hashMap.put("driver_lat", LocationUpdatesService.mLocation.getLatitude());
            hashMap.put("driver_lng", LocationUpdatesService.mLocation.getLongitude());
            hashMap.put("start_latitute", mDetail.getStoreLat());
            hashMap.put("start_longitute", mDetail.getStoreLng());
            hashMap.put("end_latitute", mDetail.getShippingLat());
            hashMap.put("end_longitute", mDetail.getShippingLng());

            new DeliveryWs().acceptDelivery(this, "Delivery", hashMap, new DeliveryWs.OnAcceptDeliveryListener() {
                @Override
                public void onLoadListSuccess(List<Delivery> deliveryList) {
                }

                @Override
                public void onAcceptDeliverySuccess(Waypoint waypoint) {
//                    Util.stopRecordLocation(CompleteActivity.this);
                    dialog.onLoadingCompleted();
                    logDebug("mStatus2", mStatus);
                }

                @Override
                public void onAcceptSuccessTaxi(Waypoint waypoint) {

                }

                @Override
                public void onAcceptFailed(String message) {
                    if (!isConnectedToInternet (CompleteActivity.this)){
                        popupMessage ( null, getString(R.string.check_yor) , CompleteActivity.this);
                        dialog.onDismiss();
                    } else {
                       popupMessage(null, message,CompleteActivity.this);
                    }

                    dialog.onDismiss();
                    button.toggleState();
                }
            });
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
//
//    private void syncDriverMovingPoints(ArrayList<LatLng> results, long distance) {
//        ServiceAPI serviceAPI = RetrofitGenerator.createServiceWithAuth(this);
//        MapDirectionRequestModel model = new MapDirectionRequestModel();
//        List<LatLngCustom> latLngs = new ArrayList<>();
//        for (LatLng l : results) {
//            LatLngCustom impl = new LatLngCustom();
//            impl.setLatitude(l.latitude);
//            impl.setLatitude(l.longitude);
//            latLngs.add(impl);
//        }
//        model.setArrJsonLatLng(latLngs);
//        model.setDistance(distance);
//
//        Call<JsonElement> res = serviceAPI.syncLocation(model);
//        res.enqueue(new CustomCallback(this, new CustomResponseListener() {
//            @Override
//            public void onSuccess(JsonObject resObj) {
//                logDebug("syncDriverMovingPoints", resObj + "");
//            }
//
//            @Override
//            public void onError(String error) {
//                logDebug("syncDriverMovingPoints", error + "");
//            }
//        }));
//    }


    private final GoogleDirectionControllerCallBack googleDirectionControllerCallBack = new GoogleDirectionControllerCallBack() {
        @Override
        public void onSuccess(ArrayList<LatLng> result, long distance) {
            isNotyetDrawMap = true;
            //  syncDriverMovingPoints(result, distance);
        }

        @Override
        public void onError(String error) {
        }
    };


    private void zoomMap() {
        if (LocationUpdatesService.mLocation != null && mMap != null) {
            LatLng sydney = new LatLng(LocationUpdatesService.mLocation.getLatitude(), LocationUpdatesService.mLocation.getLongitude());
            CameraUpdate location = CameraUpdateFactory.newLatLngZoom(sydney, 15);
            mMap.moveCamera(location);
        }
    }

    ///draw way point
    private void drawMap(Waypoint waypoint) {
        ArrayList<LatLng> routelist = new ArrayList<>();
        double lat, lng;
        if (waypoint.getArrJsonLatLng().size() > 0) {
            for (int i = 0; waypoint.getArrJsonLatLng().size() > i; i++) {
                lat = waypoint.getArrJsonLatLng().get(i).getLatitute();
                lng = waypoint.getArrJsonLatLng().get(i).getLongitute();
                routelist.add(new LatLng(lat, lng));
            }
        }
        if (routelist.size() > 0) {
            PolylineOptions rectLine = new PolylineOptions().width(5).color(Color.parseColor("#E62138"));//set line color to app color
            for (int i = 0; i < routelist.size(); i++) {
                rectLine.add(routelist.get(i));
            }
            if (mMap != null) {
                MarkerOptions markerOptions = new MarkerOptions();
                MarkerOptions markerOptions1 = new MarkerOptions();
                markerOptions.position(routelist.get(routelist.size() - 1));
                markerOptions1.position(routelist.get(0));
                markerOptions1.icon(BitmapDescriptorFactory.fromResource(R.drawable.motor));
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_start));
                mMap.addMarker(markerOptions);
                mMap.addMarker(markerOptions1);
                // Adding route on the map
                mMap.addPolyline(rectLine);
                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                builder.include(markerOptions.getPosition());
                builder.include(markerOptions1.getPosition());
                LatLngBounds bounds = builder.build();
//              CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds,(int) (getResources().getDisplayMetrics().widthPixels *0.2));
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, getResources().getDisplayMetrics().widthPixels, getResources().getDisplayMetrics().heightPixels / 2, (int) (getResources().getDisplayMetrics().widthPixels * 0.18));
                mMap.animateCamera(cameraUpdate);
            }

        }
    }


    // Move driver position every 1mn
    @Override
    protected void onResume() {
        super.onResume();
        //    LocalBroadcastManager.getInstance(this).registerReceiver(myReceiver,
        //            new IntentFilter(LocationUpdatesService.ACTION_BROADCAST));
    }

    @Override
    protected void onPause() {
        //     LocalBroadcastManager.getInstance(this).unregisterReceiver(myReceiver);
        super.onPause();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            refresh();
        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * Receiver for broadcasts sent by {@link LocationUpdatesService}.
     */
//    private class MyReceiver extends BroadcastReceiver {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            Location location = intent.getParcelableExtra(LocationUpdatesService.EXTRA_LOCATION);
//            if (location != null) {
//                logDebug("location222", location.getLatitude() + " : " + location.getLongitude());
//                driverMarker.setPosition(new LatLng(location.getLatitude(),location.getLongitude()));
//            }
//        }
//    }
}
