package com.station29.mealtempledelivery.ui.setting.payment;

import static android.app.Activity.RESULT_OK;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.api.Constant;
import com.station29.mealtempledelivery.api.request.PaymentWs;
import com.station29.mealtempledelivery.api.request.ProfileWs;
import com.station29.mealtempledelivery.data.model.PaymentHistory;
import com.station29.mealtempledelivery.data.model.PaymentService;
import com.station29.mealtempledelivery.data.model.PaymentSuccess;
import com.station29.mealtempledelivery.data.model.User;
import com.station29.mealtempledelivery.data.model.Wallets;
import com.station29.mealtempledelivery.ui.base.BaseFragment;
import com.station29.mealtempledelivery.ui.setting.PasswordConfirmActivity;
import com.station29.mealtempledelivery.ui.setting.SetChangePinActivity;
import com.station29.mealtempledelivery.ui.setting.adapter.PaymentHistoryAdapter;
import com.station29.mealtempledelivery.ui.setting.adapter.PaymentListAdapter;
import com.station29.mealtempledelivery.utils.Redirect;
import com.station29.mealtempledelivery.utils.RedirectDepositAndWithdrawPayment;
import com.station29.mealtempledelivery.utils.Util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DepositAndWithdrawFragment extends BaseFragment implements OrangePayAlertDialog.OnPaymentCallback ,
                                                                        VisaMasterCardDialogFragment.OnClickButton{

    private TextView txtChoose ,txtChooseHistory;
    private RecyclerView recyclerView , recyclerViewPaymentHistory;
    private List<Wallets> walletsList = new ArrayList<>();
    private View progressBar;
    private User user;
    private int size;
    private HashMap<String , Object> topUpParam;
    private String pspType , screenType;
    private ImageView txtNoResultPaymentService , txtNoResultPaymentHistory;
    private int removePosition;
    private PaymentHistoryAdapter paymentHistoryAdapter;
    private PaymentService paymentService;
    private LinearLayout linear_deposit_history;

    public static DepositAndWithdrawFragment newInstance(List<Wallets> walletsList, User user , int size , String screenType) {
        Bundle args = new Bundle();
        DepositAndWithdrawFragment fragment = new DepositAndWithdrawFragment();
        args.putSerializable("wallets", (Serializable) walletsList);
        args.putSerializable("user",user);
        args.putInt("size",size);
        args.putString("screenType",screenType);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_deposit_withdraw, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initView(view);
        initData();
        initAction();
    }

    private void initData(){
        if (getArguments() != null){
            walletsList = (List<Wallets>) getArguments().getSerializable("wallets");
            user = (User) getArguments().getSerializable("user");
            size = getArguments().getInt("size");
            screenType =getArguments().getString("screenType");
        }
    }

    private void initView(View view){
        recyclerView = view.findViewById(R.id.list_service_payment);
        progressBar = view.findViewById(R.id.progressBar);
        recyclerViewPaymentHistory = view.findViewById(R.id.list_payment_history);
        txtNoResultPaymentHistory = view.findViewById(R.id.txtNoResultPaymentHistory);
        txtNoResultPaymentService = view.findViewById(R.id.txtNoResultPaymentService);
        txtChoose = view.findViewById(R.id.txtChoose);
        txtChooseHistory = view.findViewById(R.id.txtChooseHistory);
        linear_deposit_history = view.findViewById(R.id.linear_deposit_history);
    }

    private void initAction(){

        if (screenType.equalsIgnoreCase("deposit")){
            txtChooseHistory.setText(getText(R.string.choose_a_payment_history_to_deposit));
            txtChoose.setText(getString(R.string.choose_a_payment_method_to_deposit));
        } else {
            txtChooseHistory.setText(getText(R.string.choose_a_payment_history_to_withdraw));
            txtChoose.setText(getString(R.string.choose_a_payment_method_to_withdraw));
        }

        if (size == 0)
            txtNoResultPaymentService.setVisibility(View.VISIBLE);
        PaymentListAdapter paymentListAdapter = new PaymentListAdapter(paymentClick, "profile", size, Constant.getConfig().getPaymentServiceList());
        recyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        recyclerView.setAdapter(paymentListAdapter);

        if(user.getPaymentHistories().isEmpty())
            linear_deposit_history.setVisibility(View.GONE);
        else linear_deposit_history.setVisibility(View.VISIBLE);
        recyclerViewPaymentHistory.setLayoutManager(new LinearLayoutManager(mActivity));
        paymentHistoryAdapter = new PaymentHistoryAdapter(user.getPaymentHistories(),"payment",paymentHistory);
        recyclerViewPaymentHistory.setAdapter(paymentHistoryAdapter);
    }

    private final PaymentHistoryAdapter.OnClickOnPaymentHistory paymentHistory = new PaymentHistoryAdapter.OnClickOnPaymentHistory() {
        @Override
        public void onClickOnPayment(PaymentHistory paymentHistory , View view) {
            if (user != null && !user.isConfirmPin()) {
                Intent intent = new Intent(mActivity, SetChangePinActivity.class);
                intent.putExtra("user",user);
                activityLauncher.launch(intent,result -> {
                    if (result.getResultCode() == RESULT_OK){
                        getProfile();
                    }
                });
            } else {
                pspType = paymentHistory.getPspType();
                PaymentService paymentService = new PaymentService();
                paymentService.setPspName(paymentHistory.getPspName());
                paymentService.setCountryCode(paymentHistory.getCountryCode());
                paymentService.setId(paymentHistory.getPivot().getPspId());
                paymentService.setLogo(paymentHistory.getLogo());
                paymentService.setPhone(paymentHistory.getPaymentToken());
                paymentService.setType(paymentHistory.getPspType() );
                checkDepositOrWithdraw(paymentService,view);
            }
        }

        @Override
        public void onClickOnRemove(int paymentId, int position) {
            removePosition = position;
            popupDeleteMessage(getString(R.string.are_you_sure_to_delete_your_paymnet_history), paymentId, mActivity);
        }
    };

    private void popupDeleteMessage(String message, int paymentId, Activity activity) {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(activity, R.style.DialogTheme);
        builder.setMessage(message);
        builder.setPositiveButton(activity.getString(R.string.ok), (dialog, which) -> {
            progressBar.setVisibility(View.VISIBLE);
            new PaymentWs().deletePaymentHistory(mActivity, paymentId, new PaymentWs.OnPaymentWalletsCallBack() {
                @Override
                public void onSuccess(String message1, String paymentId1) {
                    progressBar.setVisibility(View.GONE);
                    user.getPaymentHistories().remove(removePosition);
                    paymentHistoryAdapter.notifyDataSetChanged();
                    if (user.getPaymentHistories().isEmpty())
                        linear_deposit_history.setVisibility(View.GONE);
                    else  linear_deposit_history.setVisibility(View.VISIBLE);
                    Toast.makeText(mActivity, message1, Toast.LENGTH_SHORT).show();
                    DepositAndWithdrawActivity.viewPager.getAdapter().notifyDataSetChanged();
                }

                @Override
                public void onError(String message1) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(mActivity, message1, Toast.LENGTH_SHORT).show();
                }
            });
        });
        builder.setNegativeButton(getString(R.string.cancel), null);
        builder.show();
    }

    private void checkDepositOrWithdraw(PaymentService paymentService , View view){
        this.paymentService = paymentService;
        view.setEnabled(false);
        view.postDelayed(() -> view.setEnabled(true), 100);
        Redirect.openPaymentServiceScreen(this,mActivity, paymentService, walletsList , screenType, new  Redirect.OnCallback() {
            @Override
            public void onStart(HashMap<String, Object> param) {

            }

            @Override
            public void onFinish() {

            }
        });
    }

    private final PaymentListAdapter.SetPaymentClick paymentClick = new PaymentListAdapter.SetPaymentClick() {
        @Override
        public void onClickPaymentClick(PaymentService paymentService ,View view) {
            if (user != null && !user.isConfirmPin()) {
                Intent intent = new Intent(mActivity, SetChangePinActivity.class);
                intent.putExtra("user",user);
                activityLauncher.launch(intent,result -> {
                    if (result.getResultCode() == RESULT_OK){
                        getProfile();
                    }
                });
            } else {
                pspType = paymentService.getType();
                checkDepositOrWithdraw(paymentService ,view);
            }
        }
    };

    private void getProfile(){
        new ProfileWs().viewProfiles(mActivity, new ProfileWs.ProfileViewAndUpdate() {
            @Override
            public void onLoadProfileSuccess(User myUser,String message) {
                user = myUser;
            }

            @Override
            public void onError(String errorMessage) {

            }
            @Override
            public void onUpdateProfilesSuccess(String message) {

            }
        });
    }

    @Override
    public void onPaymentStart(HashMap<String, Object> param) {
        progressBar.setVisibility(View.VISIBLE);
        topUpParam = param;
        startPasswordConfirmActivity();
    }

    @Override
    public void onPaymentFinish() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onClickDone(@NonNull HashMap<String, Object> param) {
        progressBar.setVisibility(View.VISIBLE);
        topUpParam = param;
        startPasswordConfirmActivity();
    }

    private void startPasswordConfirmActivity() {
        activityLauncher.launch(new Intent(mActivity, PasswordConfirmActivity.class), result -> {
            if (result.getResultCode() == RESULT_OK){
                Intent data = result.getData();
                if (data != null) {
                    progressBar.setVisibility(View.VISIBLE);
                    topUpParam.put("pin_code", Util.encodeByPublicKey(data.getStringExtra("pin")));
                    RedirectDepositAndWithdrawPayment.checkPaymentTopUpAndWithdraw(mActivity, topUpParam, pspType , screenType , new RedirectDepositAndWithdrawPayment.OnResponse() {
                        @Override
                        public void onSuccess(String message, String paymentId, PaymentSuccess paymentSuccess, String paymentType) {
                            if (paymentId.equals(Constant.CARD_SHOW)) { // if top up with card have 3df
                                Intent intent = new Intent(mActivity , WebViewDialogActivity.class);
                                intent.putExtra("linkUrl",message);
                                intent.putExtra("toolBarTitle", "card");
                                intent.putExtra("action","top_up");
                                intent.putExtra("paymentSuccess",paymentSuccess);
                                activityLauncher.launch(intent, result -> {
                                    progressBar.setVisibility(View.GONE);
                                });
                            } else {
                                progressBar.setVisibility(View.GONE);
                                if (screenType.equalsIgnoreCase("deposit")){
                                    if (paymentType.equals("CARD")) { // if top up by card no 3df
                                        Intent intent = new Intent(mActivity,PaymentSuccessActivity.class);
                                        intent.putExtra("paymentSuccess",paymentSuccess);
                                        intent.putExtra("action","top_up");
                                        activityLauncher.launch(intent, result -> {
                                            progressBar.setVisibility(View.GONE);
                                        });
                                    } else if (paymentType.equalsIgnoreCase("MOBILE_WALLET")
                                            || paymentType.equalsIgnoreCase("ORANGE_PAY")
                                            || paymentType.equalsIgnoreCase("AIRTEL")) { // if top up by orange & airtel
                                        Intent intent = new Intent(mActivity,WaitingVerifyOrange.class);
                                        intent.putExtra("paymentId",paymentId);
                                        intent.putExtra("message",message);
                                        intent.putExtra("paymentSuccess",paymentSuccess);
                                        intent.putExtra("paymentService",paymentService);
                                        intent.putExtra("action","top_up");
                                        activityLauncher.launch(intent, result -> {
                                            progressBar.setVisibility(View.GONE);
                                        });
                                    } else {
                                        Util.popupMessage(getString(R.string.deposit),message, mActivity);
                                    }
                                } else {
                                    Util.popupMessage(getString(R.string.withdraw),message, mActivity); // if withdraw
                                }
                            }
                        }

                        @Override
                        public void onResponseMessage(String message) {
                            progressBar.setVisibility(View.GONE);
                            Util.popupMessage(null,message, mActivity);
                        }
                    });
                } else {
                    progressBar.setVisibility(View.GONE);
                }
            }
        });
    }
}