package com.station29.mealtempledelivery.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AddOn implements Serializable {

    @SerializedName("order_items_id")
    @Expose
    private final int id;

    @SerializedName("name")
    @Expose
    private final String label;

    @SerializedName("price")
    @Expose
    private final float price;

    public AddOn(int id, String label, float price) {
        this.id = id;
        this.label = label;
        this.price = price;
    }

    public String getLabel() {
        return label;
    }

    public float getPrice() {
        return price;
    }

    public int getId() {
        return id;
    }
}
