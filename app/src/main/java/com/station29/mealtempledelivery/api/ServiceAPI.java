package com.station29.mealtempledelivery.api;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.station29.mealtempledelivery.data.model.DirectionResults;
import com.station29.mealtempledelivery.data.model.MapDirectionRequestModel;

import java.util.HashMap;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ServiceAPI {

    @POST("api/v2/singup")
    Call<JsonElement> signUp(@Body RequestBody signup);

    @GET("api/v1/config")
    Call<JsonElement> readConfig(@Query("country") String countryCode);

    @GET("api/v1/config/lang/app")
    Call<JsonElement> readConfigLanguage(@Query("langcode") String countryCode , @Query("app_type") String appType);

    @POST("api/v1/login-driver")
    Call<JsonElement> loginPhone(@Body HashMap account);

    @POST("api/v1/verify")
    Call<JsonElement> verifyAccount(@Body HashMap accountVerify);

    @POST("api/v2/device_token")
    Call<JsonElement> deviceToken(@Body HashMap params);

    @GET("api/v1/view-profile-driver")
    Call<JsonElement> viewProfile();

    @POST("api/v2/forget-password")
    Call<JsonElement> forgetPassword(@Body HashMap forgetAccount);

    @PUT("api/v1/change-profile")
    Call<JsonElement> changeProfile(@Body RequestBody requestBody);

    @POST("api/v2/delivery/accept")
    Call<JsonElement> acceptDelivery(@Body HashMap<String, Object> params);

    @GET("api/v2/delivery/{deliverId}")
    Call<JsonElement> loadOrder(@Path("deliverId") int deliveryId);

    @GET("api/v1/delivery")
    Call<JsonElement> listOrder(@Query("page") int page, @Query("size") int size);

    @GET("api/v2/delivery")
    Call<JsonElement> getDelivery(@Query("driver_id") String driverId, @Query("status") String status, @Query("size") int limit, @Query("page") int page);

    @GET("api/v1/delivery/{deliveryId}")
    Call<JsonElement> readDelivery(@Path("deliveryId") int deliveryId);

    @GET("api/v2/delivery/{deliveryId}")
    Call<JsonElement> readDeliveryV2(@Path("deliveryId") int deliveryId);

    @PUT("api/v2/update-profile")
    Call<JsonElement> updateStatus(@Body HashMap<String, Object> param);

    //no need /v1/ for this because of main server doesnt need that .
    @GET("api/server")
    Call<JsonElement> getUrl(@Query("code") String codeCountry);

    @POST("api/v1/distance")
    Call<JsonElement> syncLocation(@Body MapDirectionRequestModel model);

    @GET("api/directions/json")
    Call<DirectionResults> getMapDirection(@Query("sensor") boolean sensor, @Query("origin") String origin, @Query("destination") String destination, @Query("key") String key);

    @PUT("api/v2/update-profile")
    Call<JsonElement> updateProfile(@Body HashMap<String,Object> updateProfile);

    @POST("api/v2/logout")
    Call<JsonElement> logout(@Body HashMap <String,String> deviceType, @Query("device_token") String deviceToken , @Query("topic") String topic);

    @PUT("api/v2/service-driver/{orderId}/cancel")
    Call<JsonElement> cancelTaxi(@Path("orderId") int orderId);

    @GET("api/v1/payment/user/transaction")
    Call<JsonElement> getUserPaymentTransaction(@Query("page")int page , @Query("wallet_id") int walletId);

    @GET("api/v1/payment/user/scan_me")
    Call<JsonElement> getMyQRCode();

    @GET("api/v1/inbox")
    Call<JsonElement> getInbox (@Query("page") int page,@Query("size") int size,@Query("topic")String topic,@Query("user_type")String userType);

    @POST("api/v1/wallets/cash_out")
    Call<JsonElement> withdrawPayment ( @Body HashMap<String , Object> param);

    @POST("api/v1/wallets/top_up")
    Call<JsonElement> topUpWallets(@Body HashMap<String ,Object> topUpHasMap);

    @PUT("api/v1/verify/pin_password")
    Call<JsonElement> setPin(@Body HashMap<String, Object> params);

    @POST("api/v1/rate-review-multi")
    Call<JsonElement> rateReview (@Body HashMap reviewData);

    //DeleteHistory
    @DELETE("api/v1/payment/user/history/{paymentHistoryId}")
    Call<JsonElement> deletePaymentHistory(@Path("paymentHistoryId") int paymentHistoryId);

    // rewards
    @GET("api/v1/driver-rewards")
    Call<JsonElement> getRewards();

    @GET("api/v1/driver-redeems")
    Call<JsonElement> getRedeems();

    @POST("api/v1/driver-redeems")
    Call<JsonElement> exChangeRewards(@Body HashMap<String,Object> hashMap);

    @GET("api/v1/payment/{id}")
    Call<JsonElement> getPaymentStatus(@Path("id") String paymentId);

    //export transaction
    @GET("api/v1/payment/user/transaction")
    Call<ResponseBody> exportTransaction(@Query("wallet_id") int walletId, @Query("transaction_type") String transactionType ,
                                         @Query("start_date") String startDate ,
                                         @Query("end_date") String endDate );

    //Upload KYC
    @POST("api/v1/upload-kyc")
    Call<JsonElement> uploadKYC (@Body RequestBody signup);
    @POST("api/v1/update-kyc")
    Call<JsonElement> updateKYC (@Body RequestBody signup);
}
