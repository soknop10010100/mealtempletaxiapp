package com.station29.mealtempledelivery.fcm;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.view.View;
import android.widget.RemoteViews;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.api.Constant;
import com.station29.mealtempledelivery.api.RetrofitGenerator;
import com.station29.mealtempledelivery.api.ServiceAPI;
import com.station29.mealtempledelivery.api.request.ProfileWs;
import com.station29.mealtempledelivery.broadcastreceiver.ButtonReceiver;
import com.station29.mealtempledelivery.data.model.ServiceModel;
import com.station29.mealtempledelivery.data.model.TaxiModel;
import com.station29.mealtempledelivery.data.model.User;
import com.station29.mealtempledelivery.ui.base.BaseActivity;
import com.station29.mealtempledelivery.ui.MainActivity;
import com.station29.mealtempledelivery.ui.base.MusicControl;
import com.station29.mealtempledelivery.ui.SplashScreenActivity;
import com.station29.mealtempledelivery.ui.kyc.AskToCompleteKycActivity;
import com.station29.mealtempledelivery.ui.kyc.WaitingVerifyKycActivity;
import com.station29.mealtempledelivery.utils.Util;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.station29.mealtempledelivery.utils.Util.getBitmapFromURL;
import static com.station29.mealtempledelivery.utils.Util.logDebug;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    public static final String NOTIFICATION_ID = "NOTIFICATION_ID";
    public static final String DELIVERY_ID = "DELIVERY_ID";
    private static final String TAG = "MyFirebaseMsgService";
    private static final String PACKAGE_NAME = "com.station29.mealtempledelivery.fcm.myfirebasemessagingservice";
    public static final String EXTRA_LOCATION = PACKAGE_NAME + ".service";
    public static final String EXTRA_LOCATION_TAXI = PACKAGE_NAME + ".taxi";
    public static final String ACTION_CANCEL = "exit_screen";
    public static final String ORDER_ID = "orderId";
    private final String channelId = "com.kiwigo.app.driver";
    public static int orderId = 0;
    private final int notification = MusicControl.countNotification;
    public FirebaseAnalytics mFirebaseAnalytics;
    private MediaPlayer mMediaPlayer,player;
    public static final String NOTIFICATION= "NOTIFICATION";
    public static int n = 0;

    public MyFirebaseMessagingService() {
        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
    }

    /**
     * Persist token to third-party servers.
     * <p>
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    public static void sendRegistrationToServer(String token, final Context context) {
        if (Constant.getBaseUrl() != null) {
            ServiceAPI serviceAPI = RetrofitGenerator.createServiceWithAuth(context);
            String userId = Util.getString("user_id", context);
            if (userId.isEmpty()) {
                return;
            }
            HashMap<String, Object> param = new HashMap<>();
            param.put("device_token", token);
            param.put("user_id", userId);
            param.put("device_type", "ANDROID");
            param.put("longitute", BaseActivity.longitude);
            param.put("latitute", BaseActivity.latitude);
            param.put("country_code", BaseActivity.countryCode);
            param.put("user_type", "DRIVER");
            final retrofit2.Call<JsonElement> res = serviceAPI.deviceToken(param);
            res.enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(@NotNull Call<JsonElement> call, @NotNull Response<JsonElement> response) {
                    JsonElement jsonElement = response.body();
                    logDebug("uploadToken", "Success" + jsonElement);
                    assert jsonElement != null;
                    JsonObject resObj = jsonElement.getAsJsonObject();
                    if (resObj.has("data")) {
                        JsonObject jsonObject = resObj.getAsJsonObject("data");
                        String topic = jsonObject.get("topic").getAsString();
                        Util.saveString("topic", topic, context);
                    }
                }

                @Override
                public void onFailure(@NotNull Call<JsonElement> call, @NotNull Throwable t) {

                }
            });
        }

        // TODO: Implement this method to send token to your app server.
    }
    // [END receive_message]
    // [START on_new_token]

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages
        // are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data
        // messages are the type
        // traditionally used with GCM. Notification messages are only received here in
        // onMessageReceived when the app
        // is in the foreground. WhenMessage data payload the app is in the background an automatically generated
        // notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages
        // containing both notification
        // and data payloads are treated as notification messages. The Firebase console always
        // sends notifica                                                                                                                                                                                                                                                                                                                                                                                                                                        tion
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        logDebug(TAG, "From: " + remoteMessage.getFrom());
        Constant.serviceLanguage = Util.getStringBody(this,"list_language");
        // Check if message contains a data payload.:470,"delivery_id":351,"delivery_time":"2020-06-16 07:56:00","account_name":"finni","account_phone":"+855 96 325 8741","account_contact":null,"account_lat":"11.583087","account_lng":"104.956055","client_phone":"+855 16 333 698","client_lat":"11.574384","client_lng":"104.875364","product_price_after_discount_vat":"40.70","rider_tip":"0.00","delivery_fee":"2.00","total":"42.70","currency":"$","payment_type":"Pay by Cash","message":null,"cooperation":false}
        if (remoteMessage.getData().size() > 0) {

            logDebug(TAG, "Message data payload: " + remoteMessage.getData());
            Map<String, String> data = remoteMessage.getData();
           n = notification;
            if (data.containsKey("service")) {
                ServiceModel serviceModel = new Gson().fromJson(data.get("service"), ServiceModel.class);
                logDebug("serviceId", serviceModel.getAccountName() + " " + serviceModel.getDeliveryId() + "");
                handleNow(serviceModel, this);
                // ===========================================================================================
                Bundle bundle = new Bundle();
                bundle.putString(Constant.USER_ID, Util.getString("user_id", this));
                bundle.putInt(Constant.DELIVERY_ID, serviceModel.getDeliveryId());
                mFirebaseAnalytics.logEvent(this.getClass().getSimpleName(), bundle);

                // ===============================================================================================
            } else if (data.containsKey("taxi")) {
                TaxiModel taxiModel = new Gson().fromJson(data.get("taxi"), TaxiModel.class);
                handleTaxi(taxiModel, this);
            }
             if (data.containsKey("display")) {
                String display = data.get("display");
                if ("hide".equalsIgnoreCase(display)) {
                    //cancel notification
                    NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    int notificationId = 0;
                    if (manager != null) {
                        notificationId = Integer.parseInt(data.get("order_id"));
                        logDebug("notification_onReceive", notificationId + "");
                        manager.cancel(notificationId);
                    }
                    closedScreen(notificationId);
                }
            }

            if (remoteMessage.getData().containsKey("refresh_kyc")) { // refresh profile user to get kyc status
                viewProfile();
            }

            if(remoteMessage.getData().containsKey("translate")){
                String translateDate = remoteMessage.getData().get("translate");
                if (translateDate != null && !translateDate.equals("[]")){
                    try {
                        JSONObject obj = new JSONObject(translateDate);
                        showNotificationTranslate(obj,remoteMessage.getData());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    String message1 = remoteMessage.getData().get("title");
                    String body = remoteMessage.getData().get("body");
                    showNotification(body, message1,"");
                }
            } else if (data.containsKey("image")) {
                String message = data.get("title");
                String body = data.get("body");
                String image = data.get("image");
                showNotification(body, message, image);
            }
        }
        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            logDebug(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }
    }

    // [END on_new_token]

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    @Override
    public void onNewToken(@NotNull String token) {
        logDebug(TAG, "c" + token);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        Util.saveString("firebase_token", token, this);
        sendRegistrationToServer(token, MyFirebaseMessagingService.this);
    }

    /**
     * Handle time allotted to BroadcastReceivers.
     */
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void handleNow(ServiceModel serviceModel, Context context) {
         n+=1;
        MusicControl.countNotification = n;
        if (orderId != serviceModel.getOrderId()) {
            alertServiceCall(serviceModel,n);
           setVibrate();
          MusicControl.getInstance(context).playMusic();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void handleTaxi(TaxiModel taxiModel, Context context) {
        n+=1;
        MusicControl.countNotification = n;
        if (orderId != taxiModel.getOrderId()) {
            taxiService(taxiModel,n);
            setVibrate();
           MusicControl.getInstance(context).playMusic();
        }
    }

    private void setVibrate() {
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        // Vibrate for 30 seconds
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            assert v != null;
            v.vibrate(VibrationEffect.createOneShot(1800, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            //deprecated in API 26
            assert v != null;
            v.vibrate(1800);
        }
    }

    private int randomNotificationId() {
        Random rand = new Random();
        return rand.nextInt(100);
    }

    // Show short time alert notification
    private void showNotification(String message, String title, String image) {

        Intent intent = new Intent(this, SplashScreenActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        PendingIntent fullScreenPendingIntent = PendingIntent.getActivity(this, 0,
                intent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(MyFirebaseMessagingService.this,channelId)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL)
                .setContentIntent(fullScreenPendingIntent);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setSmallIcon(R.drawable.ic_notification);
            notificationBuilder.setColor(getResources().getColor(R.color.colorPrimary));
        } else {
            notificationBuilder.setSmallIcon(R.mipmap.ic_launcher);
        }

        if(image != null && !image.equals("")){
            notificationBuilder.setLargeIcon(getBitmapFromURL(image));
        }

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.app_name);
            String description = getString(R.string.app_name);
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel(channelId, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            notificationManager.createNotificationChannel(channel);
        }

        //  Notification incomingCallNotification = builder.build();
//        startForeground(randomNotificationId(), incomingCallNotification);
        notificationManager.notify(randomNotificationId(), notificationBuilder.build());
    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void showNotificationTranslate(JSONObject jsonObject , Map<String, String> stringMap) {
        String mess = "" , tit = "";
        try {
            if (jsonObject.has("title_translate")){
                String titleNotTranslate = Constant.serviceLanguage.get(jsonObject.getString("title_translate").replaceAll(" ",""));
                tit = Util.translateMessageFromSever(titleNotTranslate,jsonObject);
            } else {
                if (stringMap.containsKey("title"))
                    tit = stringMap.get("title");
            }
            if (jsonObject.has("notification_translate")){
                String messageNotTranslate = Constant.serviceLanguage.get(jsonObject.getString("notification_translate"));
                mess = Util.translateMessageFromSever(messageNotTranslate,jsonObject);
            } else {
                if (stringMap.containsKey("body"))
                    mess = stringMap.get("body");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Intent intent = new Intent(this, SplashScreenActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent fullScreenPendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(MyFirebaseMessagingService.this,channelId)
                .setContentTitle(tit)
                .setContentText(mess)
                .setAutoCancel(true)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(mess))
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL)
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setContentIntent(fullScreenPendingIntent);
        notificationBuilder.setSmallIcon(R.drawable.ic_notification);
        notificationBuilder.setColor(getResources().getColor(R.color.colorPrimary));

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.app_name);
            String description = getString(R.string.app_name);
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel(channelId, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            notificationManager.createNotificationChannel(channel);
        }

        //  Notification incomingCallNotification = builder.build();
//        startForeground(randomNotificationId(), incomingCallNotification);
        notificationManager.notify(randomNotificationId(), notificationBuilder.build());
    }

    public void closedScreen(int notificationId) {
        //closed screen pickup when get notification cancel
       if (n >0){
           MusicControl.countNotification -= 1;
        } else {
           MusicControl.countNotification = 0;
       }
        MusicControl.getInstance(MyFirebaseMessagingService.this).stopMusic();
        orderId = 0;
        int i=1;
        logDebug("closescasdasd", "closedScreen: "+i++);
        Intent intent = new Intent(ACTION_CANCEL);
        intent.putExtra(ORDER_ID, notificationId);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
    }

    // Show long time Notification until Driver pick up call
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void alertServiceCall(ServiceModel serviceModel, int  not) {
        orderId = serviceModel.getOrderId();
        Intent fullScreenIntent = new Intent(this, MainActivity.class);
        fullScreenIntent.putExtra(EXTRA_LOCATION, serviceModel);
        fullScreenIntent.putExtra(NOTIFICATION_ID, serviceModel.getOrderId());
        fullScreenIntent.putExtra(NOTIFICATION,not);
        PendingIntent fullScreenPendingIntent = PendingIntent.getActivity(this, serviceModel.getOrderId(),
                fullScreenIntent, PendingIntent.FLAG_ONE_SHOT | PendingIntent.FLAG_IMMUTABLE);

        Intent buttonIntent = new Intent(this, ButtonReceiver.class);
        buttonIntent.putExtra(NOTIFICATION_ID, serviceModel.getOrderId());
        buttonIntent.putExtra(DELIVERY_ID, serviceModel.getDeliveryId());
        buttonIntent.putExtra(NOTIFICATION,not);
        PendingIntent dismissIntent = PendingIntent.getBroadcast(this, serviceModel.getOrderId(), buttonIntent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);
        //custom notification
        showHighPriorityNotification(serviceModel.getOrderId(), serviceModel.getAccountName(), fullScreenPendingIntent, dismissIntent);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void taxiService(TaxiModel taxiModel, int notifica) {
        orderId = taxiModel.getOrderId();
        Intent fullScreenIntent = new Intent(this, MainActivity.class);
        fullScreenIntent.putExtra(EXTRA_LOCATION_TAXI, taxiModel);
        fullScreenIntent.putExtra(NOTIFICATION_ID, taxiModel.getOrderId());
        fullScreenIntent.putExtra("not_tax",notifica);
        fullScreenIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent fullScreenPendingIntent = PendingIntent.getActivity(this, taxiModel.getOrderId(),
                fullScreenIntent,   PendingIntent.FLAG_CANCEL_CURRENT | PendingIntent.FLAG_ONE_SHOT | PendingIntent.FLAG_IMMUTABLE);

        Intent buttonIntent = new Intent(this, ButtonReceiver.class);
        buttonIntent.putExtra(NOTIFICATION_ID, taxiModel.getOrderId());
        buttonIntent.putExtra("not_tax",notifica);
        PendingIntent dismissIntent = PendingIntent.getBroadcast(this, taxiModel.getOrderId(), buttonIntent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);
        //custom notification
        showHighPriorityNotification(taxiModel.getOrderId(), null, fullScreenPendingIntent, dismissIntent);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void showHighPriorityNotification(int orderId, String shopName, PendingIntent fullScreenPendingIntent, PendingIntent dismissIntent) {
        //custom notification

        RemoteViews bigNotification = new RemoteViews(channelId, R.layout.notification_expanded);
        int nightModeFlags = getBaseContext().getResources().getConfiguration().uiMode & Configuration.UI_MODE_NIGHT_MASK;
        switch (nightModeFlags) {
            case Configuration.UI_MODE_NIGHT_YES:
                bigNotification.setTextColor(R.id.txtKiwiGo,getColor(R.color.white));
                bigNotification.setTextColor(R.id.incoming_call,getColor(R.color.white));
                bigNotification.setTextColor(R.id.shop_nameTv,getColor(R.color.white));
                break;
        }
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
           bigNotification.setViewVisibility(R.id.view_icon, View.GONE);
        }

        bigNotification.setTextViewText(R.id.shop_nameTv, shopName);
        bigNotification.setTextViewText(R.id.incoming_call, getText(R.string.incoming_call) + " " + orderId);
        bigNotification.setImageViewResource(R.id.app_image, R.drawable.ic_launcher_notification);
        bigNotification.setOnClickPendingIntent(R.id.cancel_btn_delivery, dismissIntent);
        bigNotification.setOnClickPendingIntent(R.id.view_btn_delivery, fullScreenPendingIntent);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, channelId);
        builder.setSmallIcon(R.drawable.ic_launcher_notification);
        builder.setWhen(System.currentTimeMillis());
        builder.setCustomContentView(bigNotification);
        builder.setContent(bigNotification);
        builder.setCustomBigContentView(bigNotification);
        builder.setCustomHeadsUpContentView(bigNotification);
        builder.setAutoCancel(true);
        builder.setOngoing(true);
        builder.setPriority(NotificationCompat.PRIORITY_MAX);
        builder.setCategory(NotificationCompat.CATEGORY_CALL);
        builder.setFullScreenIntent(fullScreenPendingIntent, true);

        Notification callNotification = builder.build();
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.app_name);
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel(channelId, name, importance);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            notificationManager.createNotificationChannel(channel);
        }
        notificationManager.notify(orderId, callNotification);
    }

    private void viewProfile () {
        new ProfileWs().viewProfiles(getBaseContext(), new ProfileWs.ProfileViewAndUpdate() {
            @Override
            public void onLoadProfileSuccess(User user, String message) {
                if (user.getKyc() != null && user.getKyc().getStatus().equals("APPROVED")) {
                    Intent intent = new Intent(getBaseContext(), MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } else if (user.getKyc() != null && user.getKyc().getStatus().equals("REJECT")){
                    Intent intent = new Intent(getBaseContext(), AskToCompleteKycActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            }
            @Override
            public void onError(String errorMessage) {}
            @Override
            public void onUpdateProfilesSuccess(String message) {}
        });
    }
}
