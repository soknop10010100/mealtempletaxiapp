package com.station29.mealtempledelivery.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ServiceTime implements Serializable {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("day")
    @Expose
    private int day;
    @SerializedName("open_time")
    @Expose
    private int openTime;
    @SerializedName("close_time")
    @Expose
    private int closeTime;
    @SerializedName("close")
    @Expose
    private int close;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getOpenTime() {
        return openTime;
    }

    public void setOpenTime(int openTime) {
        this.openTime = openTime;
    }

    public int getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(int closeTime) {
        this.closeTime = closeTime;
    }

    public int getClose() {
        return close;
    }

    public void setClose(int close) {
        this.close = close;
    }
}
