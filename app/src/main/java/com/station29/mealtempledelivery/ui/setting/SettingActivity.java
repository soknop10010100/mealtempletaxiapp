package com.station29.mealtempledelivery.ui.setting;

import androidx.annotation.NonNull;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.button.MaterialButton;
import com.google.firebase.auth.FirebaseAuth;
import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.api.request.ProfileWs;
import com.station29.mealtempledelivery.data.model.User;
import com.station29.mealtempledelivery.ui.base.BaseActivity;
import com.station29.mealtempledelivery.utils.BetterActivityResult;

public class SettingActivity extends BaseActivity {

    private User user;
    private View progress_bar;
    private  MaterialButton btnSetPin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(R.string.setting);
        }

        btnSetPin = findViewById(R.id.setPin);
        progress_bar = findViewById(R.id.progress_bar);

        if(getIntent().hasExtra("user")){
            user = (User) getIntent().getSerializableExtra("user");
        }

        if(user!=null&&!user.isConfirmPin()){
            btnSetPin.setText(R.string.set_pin);
        } else {
            btnSetPin.setText(R.string.change_pin);
        }

        findViewById(R.id.setPin).setOnClickListener(v -> {
            Intent intent = new Intent(SettingActivity.this,SetChangePinActivity.class);
            intent.putExtra("user",user);
            activityLauncher.launch(intent, result -> {
                if (result.getResultCode() == RESULT_OK) {
                    progress_bar.setVisibility(View.VISIBLE);
                    viewProfile();
                }
            });
        });

        findViewById(R.id.btn_change_language).setOnClickListener(v -> {
            Intent intent = new Intent(SettingActivity.this,ChangLanguageActivity.class);
            startActivity(intent);
        });

        findViewById(R.id.contactUs).setOnClickListener(view -> {
            Intent intent = new Intent(SettingActivity.this,ContactUsActivity.class);
            intent.putExtra("user",user);
            startActivity(intent);
        });

        findViewById(R.id.policy).setOnClickListener(v -> startActivity(new Intent(SettingActivity.this, PolicyActivity.class)));

        findViewById(R.id.deleteAccount).setOnClickListener(view -> {
            startActivity(new Intent(SettingActivity.this,DeleteAccountActivity.class));
        });

    }

    private void viewProfile () {
        new ProfileWs().viewProfiles(SettingActivity.this, new ProfileWs.ProfileViewAndUpdate() {
            @Override
            public void onLoadProfileSuccess(User user1 ,String message) {
                progress_bar.setVisibility(View.GONE);
                user = user1;
                if(user!=null&&!user.isConfirmPin()){
                    btnSetPin.setText(R.string.set_pin);
                } else {
                    btnSetPin.setText(R.string.change_pin);
                }

            }

            @Override
            public void onError(String errorMessage) {
                progress_bar.setVisibility(View.GONE);
            }

            @Override
            public void onUpdateProfilesSuccess(String message) {
                progress_bar.setVisibility(View.GONE);
            }
        });
    }

    /*private void logout() {
        HashMap<String, String> deviceTypes = new HashMap<>();
        deviceTypes.put("device_type","ANDROID");
        String topic = Util.getString("topic",SettingActivity.this);
        String deviceToken = Util.getString("firebase_token",SettingActivity.this);
        new LogoutWs().logOut(SettingActivity.this,deviceTypes,deviceToken,topic, new LogoutWs.LogoutCallback() {
            @Override
            public void onLoadSuccess(String message) {
                progress_bar.setVisibility(View.GONE);

                MusicControl.countNotification -=1;
                MusicControl.getInstance(SettingActivity.this).stopMusic();
                removeNotification();

                signOutFireStore();
                Bundle bundle = new Bundle();
                bundle.putString(Constant.USER_ID, Util.getString("user_id", SettingActivity.this) + "was Logout at " + Calendar.getInstance());
                mFirebaseAnalytics.logEvent(this.getClass().getSimpleName(), bundle);

                Util.saveString("token", "", SettingActivity.this);
                setResult(RESULT_OK);
                finish();

            }
            @Override
            public void onLoadFailed(String errorMessage) {
                progress_bar.setVisibility(View.GONE);
                Util.popupMessage(null, errorMessage, SettingActivity.this);
            }
        });
    }*/

    //on back
    private void signOutFireStore() {
        FirebaseAuth.getInstance().signOut();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void removeNotification(){
        NotificationManager manager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        if (manager != null) {
            manager.cancelAll();
        }
    }


}