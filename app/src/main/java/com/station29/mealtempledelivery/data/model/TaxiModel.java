package com.station29.mealtempledelivery.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TaxiModel implements Serializable {

    @SerializedName("pickup_latitude")
    @Expose
    private String pickupLatitude;
    @SerializedName("order_id")
    @Expose
    private int orderId;
    @SerializedName("pickup_longitude")
    @Expose
    private String pickupLongitude;
    @SerializedName("dropoff_latitude")
    @Expose
    private String dropoffLatitude;
    @SerializedName("dropoff_longitude")
    @Expose
    private String dropoffLongitude;
    @SerializedName("waypoints")
    @Expose
    private Waypoint waypoints;

    public String getTypeOrder() {
        return typeOrder;
    }

    public void setTypeOrder(String typeOrder) {
        this.typeOrder = typeOrder;
    }

    @SerializedName("type_order")
    @Expose
    private String typeOrder;
    @SerializedName("trip_duration")
    @Expose
    private double tripDuration;
    @SerializedName("total_distance")
    @Expose
    private double totalDistance;
    @SerializedName("total_price")
    @Expose
    private double totalPrice;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("payment_type")
    @Expose
    private Object paymentType;
    @SerializedName("customer_phone")
    @Expose
    private String customerPhone;
    @SerializedName("customer_name")
    @Expose
    private String customerName;
    @SerializedName("delivery_id")
    @Expose
    private int deliverId;

    @SerializedName("coupon_discount")
    @Expose
    private String couponDiscount;
    @SerializedName("coupon_discount_price")
    @Expose
    private String couponDiscountPrice;
    @SerializedName("coupon_discount_type")
    @Expose
    private String couponDiscountType;
    @SerializedName("price_after_coupon_discount")
    @Expose
    private String  priceAfterCouponDiscount;

//    @SerializedName("send_package")
//    @Expose
//    private SendPackage sendPackage;
//    public SendPackage getSendPackage() {
//        return sendPackage;
//    }
//
//    public void setSendPackage(SendPackage sendPackage) {
//        this.sendPackage = sendPackage;
//    }
    public String getCouponDiscount() {
        return couponDiscount;
    }

    public void setCouponDiscount(String couponDiscount) {
        this.couponDiscount = couponDiscount;
    }

    public String getCouponDiscountPrice() {
        return couponDiscountPrice;
    }

    public void setCouponDiscountPrice(String couponDiscountPrice) {
        this.couponDiscountPrice = couponDiscountPrice;
    }

    public String getCouponDiscountType() {
        return couponDiscountType;
    }

    public void setCouponDiscountType(String couponDiscountType) {
        this.couponDiscountType = couponDiscountType;
    }

    public String getPriceAfterCouponDiscount() {
        return priceAfterCouponDiscount;
    }

    public void setPriceAfterCouponDiscount(String priceAfterCouponDiscount) {
        this.priceAfterCouponDiscount = priceAfterCouponDiscount;
    }


    public Double getPickupLatitude() {
        return Double.parseDouble(pickupLatitude);
    }

    public void setPickupLatitude(String pickupLatitude) {
        this.pickupLatitude = pickupLatitude;
    }

    public Double getPickupLongitude() {
        return Double.parseDouble(pickupLongitude);
    }

    public void setPickupLongitude(String pickupLongitude) {
        this.pickupLongitude = pickupLongitude;
    }

    public Double getDropoffLatitude() {
        if (dropoffLatitude != null) return Double.parseDouble(dropoffLatitude);
        else return null;
    }

    public void setDropoffLatitude(String dropoffLatitude) {
        this.dropoffLatitude = dropoffLatitude;
    }

    public Double getDropoffLongitude() {
        if (dropoffLongitude != null) return Double.parseDouble(dropoffLongitude);
        else return null;
    }

    public void setDropoffLongitude(String dropoffLongitude) {
        this.dropoffLongitude = dropoffLongitude;
    }

    public Waypoint getWaypoints() {
        return waypoints;
    }

    public void setWaypoints(Waypoint waypoints) {
        this.waypoints = waypoints;
    }

    public double getTripDuration() {
        return tripDuration;
    }

    public void setTripDuration(int tripDuration) {
        this.tripDuration = tripDuration;
    }

    public double getTotalDistance() {
        return totalDistance;
    }

    public void setTotalDistance(double totalDistance) {
        this.totalDistance = totalDistance;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Object getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(Object paymentType) {
        this.paymentType = paymentType;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public int getDeliverId() {
        return deliverId;
    }

    public void setDeliverId(int deliverId) {
        this.deliverId = deliverId;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }
}

