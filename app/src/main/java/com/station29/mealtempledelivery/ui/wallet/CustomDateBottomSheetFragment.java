package com.station29.mealtempledelivery.ui.wallet;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.station29.mealtempledelivery.R;

public class CustomDateBottomSheetFragment extends BottomSheetDialogFragment {

    private View view;
    private Button btn_done;
    private ImageView img_cancel;
    private DatePicker datePickerFrom,datePickerTo;
    private GetDate getDate;

    public static CustomDateBottomSheetFragment newInstance(){
        CustomDateBottomSheetFragment customDateBottomSheetFragment = new CustomDateBottomSheetFragment();
        customDateBottomSheetFragment.setArguments(new Bundle());
        return customDateBottomSheetFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.layout_dialog_custom_date,container,false);

        initView();
        initAction();

        return view;
    }

    public void initView(){
        btn_done = view.findViewById(R.id.btn_done);
        img_cancel = view.findViewById(R.id.img_cancel_customDate);
        datePickerFrom = view.findViewById(R.id.date_picker_from);
        datePickerTo = view.findViewById(R.id.date_picker_to);
    }

    public void initAction(){
        img_cancel.setOnClickListener(view -> {
            getDate.getDateFromAndTo("","");
            dismiss();
        });

        btn_done.setOnClickListener(view -> {

            String stringMonthFrom = "",stringMonthTo="";

            int monthFrom = (datePickerFrom.getMonth()+1);
            if (monthFrom != 12 && monthFrom != 10 && monthFrom != 11){
                stringMonthFrom =  "0" + monthFrom ;
            }

            int monthTo = (datePickerTo.getMonth()+1);
            if (monthTo != 12 && monthTo != 10 && monthTo != 11){
                stringMonthTo =  "0" + monthTo ;
            }

            String stringDayFrom ="", stringDayTo="";
            int dayFrom = datePickerFrom.getDayOfMonth();
            if (dayFrom == 1 || dayFrom == 2 || dayFrom == 3 || dayFrom == 4 || dayFrom == 5 ||
                    dayFrom == 6 || dayFrom == 7 || dayFrom == 8 || dayFrom == 9) {
                stringDayFrom = "0" + dayFrom;
            }else{
                stringDayFrom = Integer.toString(dayFrom);
            }

            int dayTo = datePickerTo.getDayOfMonth();
            if (dayTo == 1 || dayTo == 2 || dayTo == 3 || dayTo == 4 || dayTo == 5 ||
                    dayTo == 6 || dayTo == 7 || dayTo == 8 || dayTo == 9) {
                stringDayTo = "0" + dayFrom;
            }else{
                stringDayTo = Integer.toString(dayTo);
            }

            String dateFrom = datePickerFrom.getYear()+"-"+stringMonthFrom+"-"+stringDayFrom;
            String dateTo = datePickerTo.getYear()+"-"+stringMonthTo+"-"+stringDayTo;
            getDate.getDateFromAndTo(dateFrom,dateTo);
            dismiss();
        });
    }

    public interface GetDate{
        void getDateFromAndTo(String from, String to);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Fragment fragment = getParentFragment();
        if(fragment!=null){
            getDate = (GetDate) fragment;
        }else{
            getDate = (GetDate) context;
        }

    }
}
