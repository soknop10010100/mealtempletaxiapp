package com.station29.mealtempledelivery.ui.home;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.data.model.DeliveryDetails;
import com.station29.mealtempledelivery.data.model.OrderItem;

import java.util.List;
import java.util.Locale;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ItemHolder> {

    private final List<OrderItem> itemArrayList;
    private final DeliveryDetails deliveryDetails;
    public OrderAdapter(List<OrderItem> itemArrayList,DeliveryDetails deliveryDetails) {
        this.itemArrayList = itemArrayList;
        this.deliveryDetails = deliveryDetails;
    }

    @NonNull
    @Override
    public ItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_item, parent, false);
        return new ItemHolder(row);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemHolder holder, int position) {
        OrderItem item = itemArrayList.get(position);
        if (item != null) {
            // when product doesn't have image we set default image.
            Glide.with ( holder.itemView.getContext () ).load ( item.getProductImage () ).into(holder.img);
            holder.nameTv.setText(item.getProductName());
            String op = item.getOptions();
            String ad = item.getAddons();
            if (item.getOptions()!= null && !item.getOptions().isEmpty()) {
                if(item.getOptions().length()>1){
                    holder.optionTv.setText("/");
                } else {
                    holder.optionTv.setText("");
                    holder.optionTv.setVisibility(View.GONE);
                }
                holder.optionTv.setVisibility(View.VISIBLE);
                holder.optionTv.setText(op);
            }
            if (item.getAddons()!= null && !item.getAddons().isEmpty()) {
                holder.addOnTv.setVisibility(View.VISIBLE);
                holder.addOnTv.setText(ad);
            } else {
                holder.addOnTv.setVisibility(View.GONE);
            }

            if(item.getCooperation() == 0 && item.getComment()!=null){
                holder.commentTv.setVisibility(View.VISIBLE);
                holder.commentTv.setText(String.format(Locale.US,"%s",item.getComment()));
            }

            holder.qtyTv.setText(String.format(Locale.US, "%d", item.getQty()));
            holder.priceTv.setText(String.format("%s %s %s", holder.priceTv.getContext().getString(R.string.price),deliveryDetails.getCurrency() , item.getPrice()));
          //  holder.priceTv.setText(String.format(Locale.US, "Price: $ %s", Util.formatPrice(item.getProductAmount() - item.getProductDiscount())));
        }
    }

    @Override
    public int getItemCount() {
        return itemArrayList.size();
    }

    static class ItemHolder extends RecyclerView.ViewHolder {

        ImageView img;
        TextView nameTv, qtyTv, priceTv, optionTv, addOnTv, commentTv;

        ItemHolder(@NonNull View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.img_product);
            nameTv = itemView.findViewById(R.id.productName);
            qtyTv = itemView.findViewById(R.id.productQty);
//            amountTv = itemView.findViewById(R.id.amount);
            priceTv = itemView.findViewById(R.id.productPrice);
            optionTv = itemView.findViewById(R.id.productOption);
            addOnTv = itemView.findViewById(R.id.productAddOn);
            commentTv = itemView.findViewById(R.id.productComment);
        }
    }
}
