package com.station29.mealtempledelivery.ui.setting.payment;

import static com.station29.mealtempledelivery.ui.base.BaseActivity.countryCode;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.button.MaterialButton;
import com.rilixtech.widget.countrycodepicker.CountryCodePicker;
import com.station29.mealtempledelivery.BuildConfig;
import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.data.model.PaymentService;
import com.station29.mealtempledelivery.data.model.Wallets;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class OrangePayAlertDialog extends BottomSheetDialogFragment {

    private PaymentService paymentService;
    private EditText phoneNumber,amount;
    private MaterialButton submitBtn;
    private ImageView notNowBtn;
    private Spinner spinner;
    private List<Wallets> walletsList;
    private final Context mContext;
    private final Activity mActivity;
    private OnPaymentCallback onPaymentCallback;
    private CheckBox saveWallet;
    private RelativeLayout checkBox , viewAmount;
    private TextView currencyTv , paymentTitle;
    private CountryCodePicker ccp;
    private String action = "";

    // for top up
    public static OrangePayAlertDialog newInstance(@NonNull Context context , Activity activity , PaymentService paymentService , List<Wallets> walletsList) {
        Bundle args = new Bundle();
        OrangePayAlertDialog fragment = new OrangePayAlertDialog(context,activity,paymentService,walletsList);
        fragment.setArguments(args);
        return fragment;
    }

    // for order
    public static OrangePayAlertDialog newInstance(@NonNull Context context , Activity activity , String action) {
        Bundle args = new Bundle();
        OrangePayAlertDialog fragment = new OrangePayAlertDialog(context,activity,action);
        fragment.setArguments(args);
        return fragment;
    }

    public OrangePayAlertDialog(@NonNull Context context , Activity activity , PaymentService paymentService , List<Wallets> walletsList) {
        this.paymentService = paymentService;
        this.mContext = context;
        this.mActivity = activity;
        this.walletsList = walletsList;
    }

    public OrangePayAlertDialog(@NonNull Context context , Activity activity , String action) {
        this.mContext = context;
        this.mActivity = activity;
        this.action = action;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.BottomSheetDialogThemeNoFloating);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        dialog.setOnShowListener(dialog1 -> {
            BottomSheetDialog d = (BottomSheetDialog) dialog1;
            FrameLayout bottomSheet = d.findViewById(R.id.design_bottom_sheet);
            assert bottomSheet != null;
            BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
            BottomSheetBehavior.from(bottomSheet).setSkipCollapsed(true);
        });
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.layout_dialog_topup_orange, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initView(view);
        initAction();
    }

    private void initView(View view){
        spinner = view.findViewById(R.id.listWallet);
        phoneNumber = view.findViewById(R.id.phone_number);
        amount = view.findViewById(R.id.amount);
        submitBtn = view.findViewById(R.id.submitNowBtn);
        notNowBtn= view.findViewById(R.id.notNowBtn);
        saveWallet = view.findViewById(R.id.saveWallet);
        checkBox = view.findViewById(R.id.checkBox);
        currencyTv = view.findViewById(R.id.currencyTv);
        paymentTitle = view.findViewById(R.id.paymentTitle);
        ccp = view.findViewById(R.id.ccp);
        viewAmount = view.findViewById(R.id.viewAmount);
    }

    private void initAction(){

        ccp.setDefaultCountryUsingNameCodeAndApply(countryCode);

        if (!action.equals("order")) {
            paymentTitle.setText(paymentService.getPspName());

            if(paymentService.getPhone() != null && !paymentService.getPhone().equals("")){
                String phoneText = paymentService.getPhone();
                ccp.setDefaultCountryUsingNameCodeAndApply(paymentService.getCountryCode());
                phoneText = phoneText.replace(ccp.getDefaultCountryCodeWithPlus(), "".trim());
                phoneText = phoneText.replaceAll("\\s+", "");
                phoneNumber.setText(phoneText);
                checkBox.setVisibility(View.GONE);
            }

            List<String> serverName = new ArrayList<>();
            serverName.add(mActivity.getString(R.string.please_select_wallets));
            currencyTv.setText( walletsList.get(0).getCurrency());

            for (Wallets wallets : walletsList) {
                serverName.add(wallets.getCurrency());
            }
            ArrayAdapter<String> serviceAdapter = new ArrayAdapter<>(mActivity, android.R.layout.simple_spinner_item, serverName);
            serviceAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(serviceAdapter);
        } else {
            viewAmount.setVisibility(View.GONE);
            checkBox.setVisibility(View.GONE);
        }

        notNowBtn.setOnClickListener(v -> {
            onPaymentCallback.onPaymentFinish();
            dismiss();
        });

        submitBtn.setOnClickListener(v -> {
            if (!action.equals("order")){
                if(amount.getText().toString().equals(""))
                    Toast.makeText(mActivity, mActivity.getString(R.string.please_enter_amount),Toast.LENGTH_SHORT).show();
                else if (phoneNumber.getText().toString().equals(""))
                    Toast.makeText(mActivity,mActivity.getString(R.string.please_input_your_phone_email),Toast.LENGTH_SHORT).show();
                else {
                    dismiss();
                    onPaymentCallback.onPaymentStart(getAllParam());
                }
            }  else {
                HashMap<String,Object> param = new HashMap<>();
                param.put("phone",phoneNumber.getText().toString());
                onPaymentCallback.onPaymentStart(param);
                dismiss();
            }
        });
    }

    private HashMap<String ,Object> getAllParam(){
        HashMap<String, Object> topUpParam = new HashMap<>();
        topUpParam.put("psp_id", paymentService.getId());
        topUpParam.put("phone", phoneNumber.getText().toString());
        topUpParam.put("amount", amount.getText().toString());
        if(saveWallet.isChecked())
            topUpParam.put("is_save",1);
        else
            topUpParam.put("is_save",0);
        if(paymentService.getPhone() != null && !paymentService.getPhone().equals("")){
            topUpParam.put("is_save",0);
        }
        topUpParam.put("wallet_id",walletsList.get(0).getWalletId());
        if(BuildConfig.DEBUG)
            topUpParam.put("code","cm");
        else topUpParam.put("code", ccp.getSelectedCountryNameCode());
        return topUpParam;
    }

    public interface OnPaymentCallback{
         void onPaymentStart(HashMap<String ,Object> param);
         void onPaymentFinish();
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        final Fragment parent = getParentFragment();
        if (parent != null) {
            onPaymentCallback = (OnPaymentCallback) parent;
        } else {
            onPaymentCallback = (OnPaymentCallback) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onPaymentCallback = null;
    }
}
