package com.station29.mealtempledelivery.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Item implements Serializable {
    @SerializedName("order_item_id")
    @Expose
    private Integer orderItemId;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("product_image")
    @Expose
    private String productImage;
    @SerializedName("qty")
    @Expose
    private Integer qty;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("price_formatted")
    @Expose
    private String priceFormatted;
    @SerializedName("price_formatted_2")
    @Expose
    private String priceFormatted2;
    @SerializedName("options")
    @Expose
    private String options;
    @SerializedName("addons")
    @Expose
    private String addons;

    @SerializedName("comment")
    @Expose
    private String comment;

    public String getComment() {
        return comment;
    }


    public Integer getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(Integer orderItemId) {
        this.orderItemId = orderItemId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPriceFormatted() {
        return priceFormatted;
    }

    public void setPriceFormatted(String priceFormatted) {
        this.priceFormatted = priceFormatted;
    }

    public String getPriceFormatted2() {
        return priceFormatted2;
    }

    public void setPriceFormatted2(String priceFormatted2) {
        this.priceFormatted2 = priceFormatted2;
    }

    public String getOptions() {
        return options;
    }

    public void setOptions(String options) {
        this.options = options;
    }

    public String getAddons() {
        return addons;
    }

    public void setAddons(String addons) {
        this.addons = addons;
    }
//
//    @SerializedName("product_id")
//    @Expose
//    private Integer productId;
//    @SerializedName("qty")
//    @Expose
//    private Integer qty;
//    @SerializedName("comment")
//    @Expose
//    private String comment;
//    @SerializedName("arr_option_ids")
//    @Expose
//    private List<Integer> arrOptionIds = null;
//    @SerializedName("arr_add_on_ids")
//    @Expose
//    private List<Object> arrAddOnIds = null;
//
//    public Integer getProductId() {
//        return productId;
//    }
//
//    public void setProductId(Integer productId) {
//        this.productId = productId;
//    }
//
//    public Integer getQty() {
//        return qty;
//    }
//
//    public void setQty(Integer qty) {
//        this.qty = qty;
//    }
//
//    public String getComment() {
//        return comment;
//    }
//
//    public void setComment(String comment) {
//        this.comment = comment;
//    }
//
//    public List<Integer> getArrOptionIds() {
//        return arrOptionIds;
//    }
//
//    public void setArrOptionIds(List<Integer> arrOptionIds) {
//        this.arrOptionIds = arrOptionIds;
//    }
//
//    public List<Object> getArrAddOnIds() {
//        return arrAddOnIds;
//    }
//
//    public void setArrAddOnIds(List<Object> arrAddOnIds) {
//        this.arrAddOnIds = arrAddOnIds;
//    }

}
