package com.station29.mealtempledelivery.ui.setting.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.station29.mealtempledelivery.R
import com.station29.mealtempledelivery.api.Constant

import com.station29.mealtempledelivery.data.model.Rewards
import java.util.*


class RewardsListAdapter(private val listRewards: ArrayList<Rewards>, private var context:Context): RecyclerView.Adapter<RewardsListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.layout_adpater_list_reward, parent, false)
        return ViewHolder(v)
    }

    @SuppressLint("SetTextI18n", "CheckResult")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val rewards = listRewards[position]

        if (Constant.serviceLanguage.containsKey(rewards.title)) {
            holder.nameTv.text = Constant.serviceLanguage[rewards.title]
        } else {
            holder.nameTv.text = rewards.title
        }

        val itemAdapter = ItemRewardsAdapter(rewards.user_rules,rewards.id,context)
      //  holder.recyclerView.layoutManager = LinearLayoutManager(context,RecyclerView.VERTICAL,false)
        holder.recyclerView.layoutManager = LinearLayoutManager(context)
        holder.recyclerView.adapter = itemAdapter
        itemAdapter.notifyDataSetChanged()


    }

    override fun getItemCount(): Int {
        return listRewards.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var nameTv : TextView = view.findViewById(R.id.text_title)
        var recyclerView : RecyclerView = view.findViewById(R.id.list_all)

    }

    fun clear() {
        val size: Int = listRewards.size
        if (size > 0) {
            for (i in 0 until size) {
                listRewards.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }
}