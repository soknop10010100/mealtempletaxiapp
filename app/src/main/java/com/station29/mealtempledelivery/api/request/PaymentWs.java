package com.station29.mealtempledelivery.api.request;

import android.app.Activity;
import android.os.Environment;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.api.Constant;
import com.station29.mealtempledelivery.api.CustomCallback;
import com.station29.mealtempledelivery.api.CustomResponseListener;
import com.station29.mealtempledelivery.api.RetrofitGenerator;
import com.station29.mealtempledelivery.api.ServiceAPI;
import com.station29.mealtempledelivery.data.model.PaymentSuccess;
import com.station29.mealtempledelivery.data.model.PaymentTransaction;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

import static com.station29.mealtempledelivery.utils.Util.isConnectedToInternet;
import static com.station29.mealtempledelivery.utils.Util.logDebug;
import static com.station29.mealtempledelivery.utils.Util.popupMessage;

public class PaymentWs {

    public void getPaymentStatus(Activity activity, String paymentId, OnPaymentWalletsCallBack onTopUpWalletsCallBack){
        ServiceAPI serviceAPI = RetrofitGenerator.createServiceWithAuth(activity);
        Call<JsonElement> res = serviceAPI.getPaymentStatus(paymentId);
        res.enqueue(new CustomCallback(activity, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if(resObj.has("data") && !resObj.get("data").isJsonNull() ){
                    JsonObject jsonObject = resObj.get("data").getAsJsonObject();
                    if(jsonObject.has("status")){
                        String status = jsonObject.get("status").getAsString();
                        String id = jsonObject.get("id").getAsString();
                        onTopUpWalletsCallBack.onSuccess(status,id);
                    }
                } else {
                    onTopUpWalletsCallBack.onError(activity.getString(R.string.something_went_wrong));
                }

            }

            @Override
            public void onError(String error , int code) {
                onTopUpWalletsCallBack.onError(error);
            }
        }));
    }

    public void setUserPin(Activity activity, HashMap<String , Object> body , final OnPaymentWalletsCallBack onTopUpWalletsCallBack){
        ServiceAPI serviceAPI = RetrofitGenerator.createServiceWithAuth(activity);
        Call<JsonElement> res = serviceAPI.setPin(body);
        res.enqueue(new CustomCallback(activity, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if(resObj != null && resObj.has("message")){
                    if (Constant.serviceLanguage.containsKey(resObj.get("message").getAsString()))
                        onTopUpWalletsCallBack.onSuccess(Constant.serviceLanguage.get(resObj.get("message").getAsString()),"0");
                    else onTopUpWalletsCallBack.onSuccess(resObj.get("message").getAsString(),"0");
                }
            }

            @Override
            public void onError(String error , int code) {
                onTopUpWalletsCallBack.onError(error);
            }

        }));
    }

    public void topUpOrWithdrawWallets(HashMap<String, Object> body , Activity activity , String paymentType , String paymentService, final PaymentWs.OnTopUpWalletsCallBack onTopUpWalletsCallBack) {
        String map = new Gson().toJson(body);
        logDebug("map", map);
        ServiceAPI service = RetrofitGenerator.createServiceWithAuth(activity);
        Call<JsonElement> resDeposit ;
        Call<JsonElement> resWithDraw ;
        if(paymentService.equalsIgnoreCase(Constant.DEPOSIT)){
            resDeposit = service.topUpWallets(body);
            resDeposit.enqueue(new CustomCallback(activity, new CustomResponseListener() {
                @Override
                public void onSuccess(JsonObject object) {
                    if (paymentType.equals("CARD")){
                        if( object.has("data") && !object.get("data").isJsonNull()){
                            String linkUrl= "";
                            JsonObject obj = object.get("data").getAsJsonObject();
                            PaymentSuccess paymentSuccess = new Gson().fromJson(obj, PaymentSuccess.class);
                            JsonObject jsonObject = obj.get("payment_data").getAsJsonObject();
                            if (jsonObject.has("required_3ds") && jsonObject.get("required_3ds").getAsBoolean()) { // if have 3ds open web view
                                if (jsonObject.has("html_confirm_payment")){
                                    linkUrl = jsonObject.get("html_confirm_payment").getAsString();
                                }
                                onTopUpWalletsCallBack.onSuccess(linkUrl,Constant.CARD_SHOW,paymentSuccess); // link and action
                            } else { // if 2df order success close screen
                                onTopUpWalletsCallBack.onSuccess(obj.get("description").getAsString(),"", paymentSuccess); // if not 3ds
                            }
                        }
                    } else {
                        if( object.has("data") && !object.get("data").isJsonNull()){
                            JsonObject jsonObject = object.getAsJsonObject("data").getAsJsonObject();
                            if( jsonObject.has("txn_id") && jsonObject.get("txn_id").isJsonNull()){
                                if (jsonObject.has("description") && !jsonObject.get("description").isJsonNull()){
                                    onTopUpWalletsCallBack.onError(jsonObject.get("description").getAsString());
                                } else {
                                    onTopUpWalletsCallBack.onError(activity.getString(R.string.something_went_wrong));
                                }
                            } else {
                                JsonObject json = object.getAsJsonObject("data").getAsJsonObject();
                                String paymentId = "";
                                if (json.has("payment_id") && !json.get("payment_id").isJsonNull()){
                                    paymentId = json.get("payment_id").getAsString();
                                }
                                PaymentSuccess paymentSuccess = new Gson().fromJson(json, PaymentSuccess.class);
                                if (json.has("description") && !json.get("description").isJsonNull()){
                                    onTopUpWalletsCallBack.onSuccess(json.get("description").getAsString(),paymentId,paymentSuccess);
                                } else {
                                    onTopUpWalletsCallBack.onError(activity.getString(R.string.something_went_wrong));
                                }
                            }
                        } else {
                            onTopUpWalletsCallBack.onError(activity.getString(R.string.something_went_wrong));
                        }
                    }

                }

                @Override
                public void onError(String error , int code) {
                    onTopUpWalletsCallBack.onError(error);
                }
            }));
        } else  {
            resWithDraw = service.withdrawPayment(body);
            resWithDraw.enqueue(new CustomCallback(activity, new CustomResponseListener() {
                @Override
                public void onSuccess(JsonObject resObj) {
                    if ( resObj.has("data") && !resObj.get("data").isJsonNull()) {
                        JsonObject jsonObject = resObj.get("data").getAsJsonObject() ;
                        PaymentSuccess paymentSuccess = new Gson().fromJson(jsonObject, PaymentSuccess.class);
                        String paymentId = "";
                        if (jsonObject.get("transaction").getAsJsonObject().has("payment_id") && !jsonObject.get("transaction").getAsJsonObject().get("payment_id").isJsonNull()){
                            paymentId = jsonObject.get("transaction").getAsJsonObject().get("payment_id").getAsString();
                        }
                        String message = activity.getString(R.string.something_went_wrong);
                        if (jsonObject.get("transaction").getAsJsonObject().has("description")) {
                            message = jsonObject.get("transaction").getAsJsonObject().get("description").getAsString();
                        }
                        if (Constant.serviceLanguage.containsKey(message)){
                            onTopUpWalletsCallBack.onSuccess(Constant.serviceLanguage.get(message),paymentId,paymentSuccess);
                        } else {
                            onTopUpWalletsCallBack.onSuccess(message,paymentId,paymentSuccess);
                        }
                    } else {
                        onTopUpWalletsCallBack.onError(activity.getString(R.string.something_went_wrong));
                    }
                }

                @Override
                public void onError(String error , int code) {
                    onTopUpWalletsCallBack.onError(error);
                }
            }));
        }

    }

    public void getUserPaymentTransaction(final Activity activity ,int walletId, int page, final onGetPaymentTransaction onTopUpWalletsCallBack){
        ServiceAPI serviceAPI = RetrofitGenerator.createServiceWithAuth(activity);
        Call<JsonElement> res = serviceAPI.getUserPaymentTransaction(page,walletId);
        onTopUpWalletsCallBack.onCancelService(res);
        res.enqueue(new CustomCallback(activity, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if(resObj.has("data")) {
                    JsonObject jsonObject = resObj.get("data").getAsJsonObject();
                    JsonArray jsonArray = jsonObject.get("data").getAsJsonArray();
                    List<PaymentTransaction> paymentTransactionArrayList = new ArrayList<>();
                    for (int i = 0; i < jsonArray.size(); i++) {
                        JsonElement ele = jsonArray.get(i);
                        PaymentTransaction paymentTransaction = new Gson().fromJson(ele, PaymentTransaction.class);
                        paymentTransactionArrayList.add(paymentTransaction);
                    }
                    onTopUpWalletsCallBack.onSuccess(paymentTransactionArrayList);
                }
            }

            @Override
            public void onError(String error , int code) {
                if (!res.isCanceled())
                    onTopUpWalletsCallBack.onError(error);
            }
        }));
    }

    public void deletePaymentHistory(Activity activity, int paymentId,PaymentWs.OnPaymentWalletsCallBack onTopUpWalletsCallBack){
        ServiceAPI service = RetrofitGenerator.createServiceWithAuth(activity);
        Call<JsonElement> res = service.deletePaymentHistory(paymentId);
        res.enqueue(new CustomCallback(activity, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if(resObj.has("message") && !resObj.get("message").isJsonNull()){
                    String message = resObj.get("message").getAsString();
                    if (Constant.serviceLanguage.containsKey(message))
                        onTopUpWalletsCallBack.onSuccess(Constant.serviceLanguage.get(message),"");
                    else onTopUpWalletsCallBack.onSuccess(message,"0");
                }
            }

            @Override
            public void onError(String error , int code) {
                onTopUpWalletsCallBack.onError(error);
            }
        }));
    }

    public void exportTransaction(final Activity activity,int walletId, String startDate, String endDate, final OnCallBackExport onCallBackExport){
        ServiceAPI serviceAPI = RetrofitGenerator.createServiceWithAuthDownload(activity);
        Call<ResponseBody> res = serviceAPI.exportTransaction(walletId,"EXPORT",startDate,endDate);
        res.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                if(response.body() != null){
                    try{
                        File futureStudioIconFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "Delivery App Transaction");
                        futureStudioIconFile.mkdirs();
                        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
                        File mediaFile = new File(futureStudioIconFile.getPath() +  File.separator +  "Transaction"+ timeStamp + ".pdf");

                        InputStream inputStream = null;
                        OutputStream outputStream = null;
                        try {
                            byte[] fileReader = new byte[ (int) response.body().contentLength()];
                            inputStream = response.body().byteStream();
                            outputStream = new FileOutputStream(mediaFile);
                            while (true) {
                                int read = inputStream.read(fileReader);
                                if (read == -1) {
                                    break;
                                }
                                outputStream.write(fileReader, 0, read);
                            }
                            outputStream.flush();
                            onCallBackExport.onSuccess(fileReader, mediaFile);
                        } catch (IOException e) {
                            onCallBackExport.onSuccess(response.body().bytes(), mediaFile);
                        } finally {
                            if (inputStream != null) {
                                inputStream.close();
                            }
                            if (outputStream != null) {
                                outputStream.close();
                            }
                        }
                    } catch (IOException e) {
                        try {
                            onCallBackExport.onSuccess(response.body().bytes(), new File(""));
                        } catch (IOException ioException) {
                            ioException.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                onCallBackExport.onFailed(t.getMessage());
            }
        });
    }

    public interface OnCallBackExport{
        void onSuccess(byte[] file, File filePdf);
        void onFailed (String message);
    }

    public interface onGetPaymentTransaction {
        void onCancelService (Call<JsonElement> res);
        void onSuccess(List<PaymentTransaction> paymentTransaction);
        void onError(String message);
    }

    public interface OnPaymentWalletsCallBack {
        void onSuccess(String message , String paymentId);
        void onError(String message);
    }

    public interface OnTopUpWalletsCallBack {
        void onSuccess(String message , String paymentId, PaymentSuccess paymentSuccess);
        void onError(String message);
    }
}
