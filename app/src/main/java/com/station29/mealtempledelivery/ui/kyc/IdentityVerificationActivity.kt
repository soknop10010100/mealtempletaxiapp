package com.station29.mealtempledelivery.ui.kyc

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.activity.result.ActivityResult
import com.station29.mealtempledelivery.R
import com.station29.mealtempledelivery.ui.base.BaseActivity

class IdentityVerificationActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_identity_verification)

        findViewById<TextView>(R.id.toolBarTitle).text =  getString(R.string.identity_verification)
        findViewById<ImageView>(R.id.backButton).setOnClickListener { finish() }

        findViewById<Button>(R.id.btnStartVerification).setOnClickListener {
                val intent = Intent(this@IdentityVerificationActivity, UploadKYCActivity::class.java)
                activityLauncher.launch(intent) { result: ActivityResult ->
                    if (result.resultCode == RESULT_OK) {
                        setResult(RESULT_OK)
                        finish()
                    }
                }
        }

    }

}