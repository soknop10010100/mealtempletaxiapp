package com.station29.mealtempledelivery.ui.home;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.api.Constant;
import com.station29.mealtempledelivery.api.request.DeliveryWs;
import com.station29.mealtempledelivery.data.model.Config;
import com.station29.mealtempledelivery.data.model.Delivery;
import com.station29.mealtempledelivery.data.model.Waypoint;
import com.station29.mealtempledelivery.ui.base.BaseFragment;
import com.station29.mealtempledelivery.ui.history.DeliveryItemAdapter;
import com.station29.mealtempledelivery.ui.taxi.TaxiCompleteActivity;
import com.station29.mealtempledelivery.ui.taxi.TaxiPickupActivity;
import com.station29.mealtempledelivery.utils.CompleteActivity;
import com.station29.mealtempledelivery.utils.PickupActivity;
import com.station29.mealtempledelivery.utils.Util;

import java.util.ArrayList;
import java.util.List;

import static com.station29.mealtempledelivery.utils.Util.logDebug;

public class HomeFragment extends BaseFragment {

    private final ArrayList<String> status = new ArrayList<>();
    private List<Delivery> list = new ArrayList<>();
    private int currentItem;
    private int total;
    private int scrollDown;
    private int currentPage = 1;
    private int size = 10;
    private boolean  isScrolling = false;
    private ProgressBar progress;
    private String driverId = "";
    private RelativeLayout relativeLayout;
    private DeliveryItemAdapter deliveryItemAdapter;
    private SwipeRefreshLayout refreshLayout;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        status.add(Constant.ACCEPTED);
        status.add(Constant.PICKED_UP);

        View root = inflater.inflate(R.layout.fragment_home, container, false);

        RecyclerView recyclerView = root.findViewById(R.id.list_home);
        TextView tex = root.findViewById(R.id.empty_text);
        relativeLayout = root.findViewById(R.id.relate);
        progress = root.findViewById(R.id.progressBar);
        refreshLayout = root.findViewById(R.id.swipe_layout);

        tex.setTextColor(Color.BLACK);
        list = new ArrayList<>();

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mActivity);
        recyclerView.setLayoutManager(linearLayoutManager);
        deliveryItemAdapter = new DeliveryItemAdapter(list, 0, itemClickListener,mActivity);
        recyclerView.setAdapter(deliveryItemAdapter);

        driverId = Util.getString("user_id", container.getContext());
        if (!driverId.isEmpty()) {
            loadDelivery(driverId, status, currentPage);
        } else {
            //tex.setVisibility(View.VISIBLE);
            relativeLayout.setVisibility(View.VISIBLE);
        }

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL){
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItem = linearLayoutManager.getChildCount();
                total = linearLayoutManager.getItemCount();
                scrollDown = linearLayoutManager.findFirstVisibleItemPosition();
                if(dy > 0) {
                    if (isScrolling && (currentItem + scrollDown == total)) {
                        if(total == size) {
                            isScrolling = false;
                            currentPage++;
                            progress.setVisibility(View.VISIBLE);
                            recyclerView.scrollToPosition(list.size() - 1);
                            loadDelivery(driverId, status, currentPage);
                            size+=10;
                        }
                    }
                }

            }
        });

        refreshLayout.setColorSchemeResources(R.color.colorPrimary);

        refreshLayout.setOnRefreshListener(() -> {
            currentPage = 1;
            size = 10;
            deliveryItemAdapter.clear();
            loadDelivery(driverId, status, currentPage);
        });

        return root;
    }

    private void loadDelivery(final String driverId, final ArrayList<String> status, final int currentPage) {
        progress.setVisibility(View.VISIBLE);
        if (Constant.getBaseUrl() != null) {
            int sizePage = 10;
            new DeliveryWs().getDelivery(mActivity, driverId, status, sizePage, currentPage, new DeliveryWs.OnAcceptDeliveryListener() {
                @SuppressLint("NotifyDataSetChanged")
                @Override
                public void onLoadListSuccess(List<Delivery> deliveryList) {
                    list.addAll(deliveryList);
                    if (list.isEmpty()) {
                        relativeLayout.setVisibility(View.VISIBLE);
                    }
                    progress.setVisibility(View.GONE);
                    deliveryItemAdapter.notifyDataSetChanged();
                    refreshLayout.setRefreshing(false);
                    logDebug("listSize", list.size() + "");

                }

                @Override
                public void onAcceptDeliverySuccess(Waypoint waypoint) {

                }

                @Override
                public void onAcceptSuccessTaxi(Waypoint waypoint) {
                }

                @Override
                public void onAcceptFailed(String message) {
                    progress.setVisibility(View.GONE);
                    relativeLayout.setVisibility(View.VISIBLE);
                  //  tex.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    private final DeliveryItemAdapter.DeliveryItemClickListener itemClickListener = deliveryDetail -> {
        Intent intent = null;

        if (deliveryDetail.getDeliveryStatus().equalsIgnoreCase(Constant.PICKED_UP)) {
            if (deliveryDetail.getTypeOrder().equalsIgnoreCase("service")){
                intent = new Intent(mActivity, TaxiCompleteActivity.class);
                intent.putExtra("typeOrder","service");

            } else if((deliveryDetail.getTypeOrder().equalsIgnoreCase("send"))){
                intent = new Intent(mActivity, TaxiCompleteActivity.class);
                intent.putExtra("typeOrder","send");
            } else {
                //food
                intent = new Intent(mActivity, CompleteActivity.class);
                intent.putExtra("typeOrder","delivery");
            }

        } else if (deliveryDetail.getDeliveryStatus().equalsIgnoreCase(Constant.ACCEPTED)) {
            if (deliveryDetail.getTypeOrder().equalsIgnoreCase("service")){
                intent = new Intent(mActivity, TaxiPickupActivity.class);
                intent.putExtra("typeOrder","service");
                intent.putExtra("waypoint",deliveryDetail.getWaypoints());

            }else if(deliveryDetail.getTypeOrder().equalsIgnoreCase("send")) {
                intent = new Intent(mActivity, TaxiPickupActivity.class);
                intent.putExtra("typeOrder","send");
                intent.putExtra("waypoint",deliveryDetail.getWaypoints());

            } else {
                intent = new Intent(mActivity, PickupActivity.class);
                intent.putExtra("typeOrder","delivery");
            }
        }
        if (intent != null) {
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.putExtra("deliveryId", deliveryDetail.getDeliveryId());
            intent.putExtra("updateStatus", deliveryDetail.getDeliveryStatus());
            startActivity(intent);
        }
    };

}