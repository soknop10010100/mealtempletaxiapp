package com.station29.mealtempledelivery.ui.taxi;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import com.ebanx.swipebtn.OnStateChangeListener;
import com.ebanx.swipebtn.SwipeButton;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.button.MaterialButton;
import com.station29.mealtempledelivery.BuildConfig;
import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.api.Constant;
import com.station29.mealtempledelivery.api.request.DeliveryWs;
import com.station29.mealtempledelivery.data.model.DeliveryDetails;
import com.station29.mealtempledelivery.data.model.TaxiDeliveryDetail;
import com.station29.mealtempledelivery.data.model.Waypoint;
import com.station29.mealtempledelivery.service.LocationUpdatesService;
import com.station29.mealtempledelivery.ui.base.BaseActivity;
import com.station29.mealtempledelivery.ui.MainActivity;
import com.station29.mealtempledelivery.utils.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.station29.mealtempledelivery.utils.Util.isConnectedToInternet;
import static com.station29.mealtempledelivery.utils.Util.popupMessage;

public abstract class TaxiBaseActivity extends BaseActivity implements OnMapReadyCallback, View.OnClickListener {

    private String mStatus;
    private GoogleMap mMap;
    private int deliveryId ;
    protected TaxiDeliveryDetail taxiDeliveryDetail;
    private String typeOrder;
    private Waypoint waypoint;
    private SupportMapFragment mapFragment;
    private TextView pricePerKilo, pricePerMinuteTv;
    protected TextView durationCounterTv, distanceCounterTv, totalPriceTv, accuractyTv,paymentType;
    private TextView estimatedTimeTv, estimatedDistanceTv, estimatedPriceTv;
    private LinearLayout linearLayout;
    private ImageButton btnCancelOrder;
    private MaterialButton btnView;
    private SwipeButton taxiComplete;

    public static List<LatLng> latLngList = new ArrayList<>();

    protected abstract void onSlideCompleted(SwipeButton swipeButton, boolean active);

    protected abstract void onPhoneCallClick(ImageView buttonCall);

    protected abstract Drawable getSwipeButtonColor();

    protected abstract String getSwipeButtonText();

    protected abstract String visibleBtnCancel();

    protected abstract void onDrawRoute(GoogleMap googleMap, Waypoint pickupWayPoint, Waypoint waypoint);
    protected void onRequestSuccess(TaxiDeliveryDetail detail){}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_taxi_complete);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        linearLayout = findViewById(R.id.linear_distance);
        ImageView callToCustomerTv = findViewById(R.id.callImage);
        final ShimmerFrameLayout shimmerFrameLayout = findViewById(R.id.shimmer);
        taxiComplete = findViewById(R.id.taxiComplete);
        totalPriceTv = findViewById(R.id.totalPrice);
        distanceCounterTv = findViewById(R.id.distanceComplete);
        durationCounterTv = findViewById(R.id.durationComplete);
        accuractyTv = findViewById(R.id.location_accuracy);
        btnCancelOrder = findViewById(R.id.btnCancelService);
        estimatedDistanceTv = findViewById(R.id.distanceEstimate);
        estimatedTimeTv = findViewById(R.id.durationEstimate);
        estimatedPriceTv = findViewById(R.id.priceEstimate);
        btnView = findViewById(R.id.viewIte);
        paymentType = findViewById(R.id.paymentType);

        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        if (mapFragment != null) mapFragment.getMapAsync(this);

        if (getIntent() != null && getIntent().hasExtra("updateStatus") && getIntent().hasExtra("deliveryId")) {
            //main screen(home fragment)
            deliveryId = getIntent().getIntExtra("deliveryId", -1);
            mStatus = getIntent().getStringExtra("updateStatus");
            if (getIntent().hasExtra("typeOrder")) {
                typeOrder = (String) getIntent().getSerializableExtra("typeOrder");
            }
        }

        findViewById(R.id.btnBackCom).setOnClickListener(this);
        callToCustomerTv.setOnClickListener(this);
        totalPriceTv.setOnClickListener(this);

        shimmerFrameLayout.startShimmer();
        if (getSwipeButtonColor() != null) {
            taxiComplete.setButtonBackground(getSwipeButtonColor());
        }
        if (getSwipeButtonText() != null) {
            taxiComplete.setText(getSwipeButtonText());
        }

        if (visibleBtnCancel().equals(Constant.PICKED_UP)){
            btnCancelOrder.setVisibility(View.VISIBLE);
        } else {
            btnCancelOrder.setVisibility(View.GONE);
        }

        taxiComplete.setOnStateChangeListener(active -> {
            if (!active) return;
            shimmerFrameLayout.hideShimmer();
            if (taxiDeliveryDetail != null) {
                onSlideCompleted(taxiComplete, active);
                shimmerFrameLayout.showShimmer(true);
            } else {
                getDeliveryDetailTask();
            }
        });

        btnCancelOrder.setOnClickListener(v -> {
            if(Constant.getBaseUrl()!=null)   Util.popUpCancelOrder(getString(R.string.cancel), TaxiBaseActivity.this,taxiDeliveryDetail.getOrderId());
        });

        if(typeOrder.equals("send")){
            linearLayout.setVisibility(View.GONE);
            btnView.setVisibility(View.VISIBLE);

            btnView.setOnClickListener(v -> {
                Intent intent = new Intent(TaxiBaseActivity.this,HistoryTaxiActivity.class);
                intent.putExtra("delivery",deliveryId);
                intent.putExtra("typeOrder",typeOrder);
                startActivity(intent);
            });
        }

        if(typeOrder.equals("service") && mStatus.equals(Constant.PICKED_UP)){
            btnView.setVisibility(View.GONE);
        }

        if (mStatus != null) getDeliveryDetailTask();


    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnBackCom) {
            refresh();
            finish();
        } else if (v.getId() == totalPriceTv.getId() && BuildConfig.DEBUG) {
            Util.popupMessage("LatLngTrack", Util.readTrackPointsLocal(TaxiBaseActivity.this), TaxiBaseActivity.this);
        } else if (v.getId() == R.id.callImage) {
            onPhoneCallClick((ImageView) v);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            refresh();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);
        zoomMap();
        if (taxiDeliveryDetail != null) {
            if (taxiDeliveryDetail.getWaypoints() != null) {
                onDrawRoute(mMap, taxiDeliveryDetail.getPickupWayPoint(), taxiDeliveryDetail.getWaypoints());
            } else {
                drawMarker(taxiDeliveryDetail);
            }
        }
//        if (typeOrder != null && typeOrder.equalsIgnoreCase("send")) {
//            View locationButton = ((View) Objects.requireNonNull(mapFragment.getView()).findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
//            RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
//            // position on right bottom
//            rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
//            rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
//            rlp.setMargins(0, 0, 30, 350);
//        }
    }

    private void refresh(){
        Intent intent = new Intent(TaxiBaseActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void zoomMap() {
        if (LocationUpdatesService.mLocation != null && mMap != null) {
            LatLng sydney = new LatLng(LocationUpdatesService.mLocation.getLatitude(), LocationUpdatesService.mLocation.getLongitude());
            CameraUpdate location = CameraUpdateFactory.newLatLngZoom(sydney, 18);
            mMap.moveCamera(location);
        }
    }

    private void getDeliveryDetailTask() {
        if (Constant.getBaseUrl() != null)
            new DeliveryWs().readDelivery(deliveryId, typeOrder, TaxiBaseActivity.this, new DeliveryWs.ReadDeliveryListener() {
                @Override
                public void onLoadDeliverySuccess(final DeliveryDetails details) {
                }

                @Override
                public void onLoadTaxiDetailSuccess(TaxiDeliveryDetail deliveryDetail) {
                    onRequestSuccess(deliveryDetail);
                    taxiDeliveryDetail = deliveryDetail;
                    typeOrder = taxiDeliveryDetail.getTypeOrder();
                    waypoint = taxiDeliveryDetail.getWaypoints();

                    if (waypoint != null) {
                        onDrawRoute(mMap, taxiDeliveryDetail.getPickupWayPoint(), waypoint);
                    } else {
                        drawMarker(deliveryDetail);
                    }
                    if(waypoint == null){
                        if(taxiDeliveryDetail.getStatus().equalsIgnoreCase(Constant.ACCEPTED))
                            totalPriceTv.setText(String.format(Locale.US, " %s%s", taxiDeliveryDetail.getCurrency(), Util.stringFormatPrice(taxiDeliveryDetail.getTotalPrice())));
                    } else {
                        linearLayout.setVisibility(View.GONE);
                        totalPriceTv.setText(String.format(Locale.US, " %s%s", taxiDeliveryDetail.getCurrency(),Util.stringFormatPrice( taxiDeliveryDetail.getTotalPrice())));
                    }

                    //pricePerKilo.setText(String.format(Locale.US, " %s%s/km", taxiDeliveryDetail.getCurrency(), taxiDeliveryDetail.getCostPerDistance()));

                    if (typeOrder.equals("service")){
                        if (taxiDeliveryDetail.getPaymentType() != null){
                            paymentType.setText(String.format(Locale.US,"%s: %s ",getString(R.string.payment_type),taxiDeliveryDetail.getPaymentType()));
                        }
                    } else if (typeOrder.equals("send")){
                        if (taxiDeliveryDetail.getSendPackage().getPaymentType() != null){
                            paymentType.setText(String.format("%s: %s",getString(R.string.payment_type),taxiDeliveryDetail.getSendPackage().getPaymentType()));
                        }
                    } else {
                        paymentType.setText(String.format(Locale.US,"%s: %s" ,getString(R.string.payment_type) ,getString(R.string.pay_by_cash )));
                    }

                    if (taxiDeliveryDetail.getTotalPrice() != null && !taxiDeliveryDetail.getTotalPrice().isEmpty() && Float.parseFloat(taxiDeliveryDetail.getTotalPrice()) > 0) {
                        estimatedPriceTv.setVisibility(View.VISIBLE);
                        estimatedPriceTv.setText(String.format("%s: %s %s", getString(R.string.total_price) ,taxiDeliveryDetail.getCurrency(), Util.stringFormatPrice(taxiDeliveryDetail.getTotalPrice())));
                    }

                    String estDistance = taxiDeliveryDetail.getTotalDistance();
                    if (estDistance != null && !estDistance.isEmpty() && Float.parseFloat(estDistance) > 0) {
                        estimatedDistanceTv.setVisibility(View.VISIBLE);
                        estimatedDistanceTv.setText(String.format(Locale.US, "%s : %s km",getString(R.string.distance) ,taxiDeliveryDetail.getTotalDistance()));
                    }

                    if (waypoint != null && waypoint.getDuration() > 0) {
                        double duration = waypoint.getDuration()/60 ; // convert to ms
                        double durations = Math.ceil(duration);
                        estimatedTimeTv.setVisibility(View.VISIBLE);
                        estimatedTimeTv.setText(String.format("%s: %s", getString(R.string.duration) ,Util.formatTime((int)durations)));
                    }

                    if (deliveryDetail.getStatus().equals(Constant.PICKED_UP)) {
                        linearLayout.setVisibility(View.VISIBLE);
                    } else {
                        linearLayout.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onLoadFail(String message) {
                    if (!isConnectedToInternet (TaxiBaseActivity.this)){
                        popupMessage ( null, getString(R.string.check_yor) , TaxiBaseActivity.this);
                    } else {
                        Util.popupMessage(null,message,TaxiBaseActivity.this);
                    }

                    taxiComplete.toggleState();
                }
            });
    }

    private void drawMarker(TaxiDeliveryDetail deliveryDetail) {
        if (mMap == null) return;
        mMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_taxi)).position(new LatLng(deliveryDetail.getPickUpLat(), deliveryDetail.getPickUpLong())));
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)){
            refresh();
        }
        return super.onKeyDown(keyCode, event);
    }

//    private void moveDriverMarker() {
//        if (markerDriverPoint != null && gpsTracker != null) {
//            markerDriverPoint.position(new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude()));
//        }
//    }
}
