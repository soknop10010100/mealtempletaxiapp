package com.station29.mealtempledelivery.ui.login;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.poovam.pinedittextfield.SquarePinField;
import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.api.request.MockupData;
import com.station29.mealtempledelivery.api.request.PaymentWs;
import com.station29.mealtempledelivery.api.request.SignUpWs;
import com.station29.mealtempledelivery.data.model.User;
import com.station29.mealtempledelivery.fcm.MyFirebaseMessagingService;
import com.station29.mealtempledelivery.ui.base.BaseActivity;
import com.station29.mealtempledelivery.ui.MainActivity;
import com.station29.mealtempledelivery.utils.Util;

import java.util.HashMap;
import java.util.Locale;

public class VerifyCodeActivity extends BaseActivity {

    private SquarePinField pin;
    private String action = null;
    private Button btnSubmit;
    private HashMap<String,Object> postVerifyCode = new HashMap<>();
    private boolean isChangePIN = false;
    private TextView btnReset;
    private int code;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_code);

        btnReset = findViewById(R.id.countTime);
        btnSubmit = findViewById(R.id.btn_sub_mit);

        findViewById(R.id.btnBackConfirm).setOnClickListener(v -> popupMessage(getString(R.string.are_you_sure_don_not_ver),VerifyCodeActivity.this));

        if(getIntent() != null && getIntent().hasExtra("phone")){
            HashMap<String,Object>account = (HashMap<String, Object>) getIntent().getSerializableExtra("phone");
            MockupData.setAccount(account);
            code = getIntent().getIntExtra("code",-1);
        }

        if(getIntent() != null && getIntent().hasExtra("forgotPINParam")){
            isChangePIN = true;
            postVerifyCode = (HashMap<String, Object>) getIntent().getSerializableExtra("forgotPINParam");
        }

        pin = findViewById(R.id.squareField);
        pin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 6) {
                    if(!isChangePIN)
                        verifyCode();
                    else {
                        postVerifyCode.put("reset_pin_code",s.toString());
                        changePIN();
                    }
                }
            }
        });

        btnSubmit.setOnClickListener(v -> {
            if (pin.getText() != null && !pin.getText().toString().isEmpty() && pin.length() == 6) {
                verifyCode();
            } else {
                Toast.makeText(getApplicationContext(), R.string.invalid_code, Toast.LENGTH_SHORT).show();
            }
        });

        if (getIntent() != null && getIntent().hasExtra("action")) {
            action = getIntent().getStringExtra("action");
        }

        countTime();
    }

    private void changePIN(){
        new PaymentWs().setUserPin(VerifyCodeActivity.this, postVerifyCode, new PaymentWs.OnPaymentWalletsCallBack() {
            @Override
            public void onSuccess(String message , String paymentId) {
               // progressBar.setVisibility(View.GONE);
                setResult(RESULT_OK);
                finish();
                Toast.makeText(VerifyCodeActivity.this,"success",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(String message) {
                //progressBar.setVisibility(View.GONE);
                Toast.makeText(VerifyCodeActivity.this,message,Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void verifyCode() {
        HashMap<String,Object> userAcc = MockupData.getAccount();
        if (userAcc.containsKey("phone")) {
            postVerifyCode.put("phone", userAcc.get("phone"));
        }
        postVerifyCode.put("pin_code", pin.getText().toString());
        postVerifyCode.put("user_type", "USER_ANDROID");
        postVerifyCode.put("phone_code", userAcc.get("phone_code"));
        postVerifyCode.put("code",userAcc.get("code"));
        new SignUpWs().verifyAccountCode(VerifyCodeActivity.this,postVerifyCode, requestServer);
    }

    private final SignUpWs.RequestServer requestServer = new SignUpWs.RequestServer() {
        @Override
        public void onRequestSuccess() {

        }

        @Override
        public void onRequestToken(String token, int userId, User user) {
            Toast.makeText(getApplicationContext(), getString(R.string.successfull), Toast.LENGTH_SHORT).show();
            Util.saveString("token", token, VerifyCodeActivity.this);
            Util.saveString("user_id", String.valueOf(userId), VerifyCodeActivity.this);
            if(getIntent() != null && getIntent().hasExtra("newPassword") && getIntent().getStringExtra("newPassword").equals("newPassword")) {
                Intent intent = new Intent(VerifyCodeActivity.this, NewPasswordActivity.class);
                setResult(RESULT_OK);
                startActivity(intent);
            } else if (code == 403){
                MyFirebaseMessagingService.sendRegistrationToServer(Util.getString("firebase_token", VerifyCodeActivity.this), VerifyCodeActivity.this);
                Intent intent = new Intent(VerifyCodeActivity.this, MainActivity.class);
                setResult(RESULT_OK);
                startActivity(intent);
            } else if(isChangePIN) {
                setResult(RESULT_OK);
                finish();
                Toast.makeText(VerifyCodeActivity.this,getString(R.string.successfull),Toast.LENGTH_SHORT).show();
            } else {
                Util.saveString("token","",VerifyCodeActivity.this);
                popupMessageRegisterSuccess("",getString(R.string.our_compliance_team_is_review),VerifyCodeActivity.this);
            }
        }

        @Override
        public void onRequestFailed(String message) {
            final Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
            Handler handler = new Handler();
            handler.postDelayed(() -> toast.cancel(), 500);
        }
    };

    private void countTime() {
        new CountDownTimer(30000, 1000) {
            @SuppressLint("ResourceAsColor")
            public void onTick(long millisUntilFinished) {
                btnReset.setEnabled(false);
                btnReset.setTextColor(R.color.colorGrey);
                btnReset.setText(String.format(Locale.US,"%s: %d", getString(R.string.resend_in), millisUntilFinished / 1000));
            }

            public void onFinish() {
                btnReset.setEnabled(true);
                btnReset.setTextColor(getResources().getColor(R.color.zui_color_red_600));
                btnReset.setText(R.string.resend);
                btnReset.setOnClickListener(v -> {
                    countTime();
                    new SignUpWs().forgetAccountPassword(VerifyCodeActivity.this, MockupData.getAccount(), requestServer);
                });
            }
        }.start();
    }
    
    private void popupMessage( String message, Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(message);
        builder.setNegativeButton ( activity.getString ( R.string.cancel ), (dialog, which) -> dialog.dismiss());
        builder.setPositiveButton ( activity.getString ( R.string.ok ) , (dialog, which) -> onBackPressed());
        AlertDialog alert = builder.create();
        alert.setCanceledOnTouchOutside(false);
        alert.show();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)){
            popupMessage(getString(R.string.are_you_sure_don_not_ver),VerifyCodeActivity.this);
        }
        return super.onKeyDown(keyCode, event);
    }

    private void popupMessageRegisterSuccess(String title, String message, Activity activity) {
        if(!activity.isFinishing()) {
            androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(activity, R.style.DialogTheme);
            if (title != null) builder.setTitle(title);
            builder.setMessage(message);
            builder.setPositiveButton(String.format(Locale.US, "%s", activity.getString(R.string.ok)), (dialogInterface, i) -> {
                Intent intent = new Intent(VerifyCodeActivity.this, LoginActivity.class);
                setResult(RESULT_OK);
                startActivity(intent);
            });
            builder.show();
        }
    }

}
