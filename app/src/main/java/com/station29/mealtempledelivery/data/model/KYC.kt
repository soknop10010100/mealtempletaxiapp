package com.station29.mealtempledelivery.data.model

import java.io.Serializable

data class KYC(
    val id : Int,
    val user_id : Int,
    val type : String,
    val error_message : String,
    val status : String,
) : Serializable