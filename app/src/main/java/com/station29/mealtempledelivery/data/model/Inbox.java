package com.station29.mealtempledelivery.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Inbox implements Serializable {

    @SerializedName("order_id")
    @Expose
    private String orderId;
    @SerializedName("type_order")
    @Expose
    private String typeOrder;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("send_time")
    @Expose
    private String sendTime;
    @SerializedName("notification_type")
    @Expose
    private String notificationType;
    @SerializedName("notification_image")
    @Expose
    private Object notificationImage;

    @SerializedName("notification_translate")
    @Expose
    private String notificationTranslate;

    public String getNotificationTranslate() {
        return notificationTranslate;
    }

    public void setNotificationTranslate(String notificationTranslate) {
        this.notificationTranslate = notificationTranslate;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getTypeOrder() {
        return typeOrder;
    }

    public void setTypeOrder(String typeOrder) {
        this.typeOrder = typeOrder;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSendTime() {
        return sendTime;
    }

    public void setSendTime(String sendTime) {
        this.sendTime = sendTime;
    }

    public String getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }

    public Object getNotificationImage() {
        return notificationImage;
    }

    public void setNotificationImage(Object notificationImage) {
        this.notificationImage = notificationImage;
    }
}
