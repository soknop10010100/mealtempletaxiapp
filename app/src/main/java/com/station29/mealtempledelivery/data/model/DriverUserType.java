package com.station29.mealtempledelivery.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DriverUserType implements Serializable {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("service_type")
    @Expose
    private String serviceType;
    @SerializedName("icon")
    @Expose
    private String icon;
    @SerializedName("set")
    @Expose
    private int set;
    @SerializedName("is_active")
    @Expose
    private int isActive;
    @SerializedName("rank")
    @Expose
    private int rank;
    @SerializedName("is_default")
    @Expose
    private int isDefault;
    @SerializedName("created_by")
    @Expose
    private Object createdBy;
    @SerializedName("updated_by")
    @Expose
    private int updatedBy;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("deleted_at")
    @Expose
    private Object deletedAt;
    @SerializedName("pivot")
    @Expose
    private Pivots pivot;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public int getSet() {
        return set;
    }

    public void setSet(int set) {
        this.set = set;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public int getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(int isDefault) {
        this.isDefault = isDefault;
    }

    public Object getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Object createdBy) {
        this.createdBy = createdBy;
    }

    public int getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(int updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Object getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Object deletedAt) {
        this.deletedAt = deletedAt;
    }

    public Pivots getPivot() {
        return pivot;
    }

    public void setPivot(Pivots pivot) {
        this.pivot = pivot;
    }
    public static class Pivots implements Serializable {
        @SerializedName("user_id")
        @Expose
        private Integer userId;
        @SerializedName("driver_type_id")
        @Expose
        private Integer driver_type_id;

        public Integer getUserId() {
            return userId;
        }

        public Integer getPspId() {
            return driver_type_id;
        }
    }
}
