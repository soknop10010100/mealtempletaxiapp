package com.station29.mealtempledelivery.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ServiceTypeDriver implements Serializable {
    @SerializedName("service_id")
    @Expose
    private int serviceId;
    @SerializedName("service")
    @Expose
    private String service;
    @SerializedName("is_default")
    @Expose
    private int isDefault;

    public ServiceTypeDriver(int serviceId, String service,int isDefault) {
        this.serviceId = serviceId;
        this.service = service;
        this.isDefault = isDefault;
    }

    public int getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(int isDefault) {
        this.isDefault = isDefault;
    }

    public int getServiceId() {
        return serviceId;
    }

    public void setServiceId(int serviceId) {
        this.serviceId = serviceId;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }
}
