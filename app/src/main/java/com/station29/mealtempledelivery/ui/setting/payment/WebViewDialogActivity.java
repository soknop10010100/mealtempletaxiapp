package com.station29.mealtempledelivery.ui.setting.payment;

import static com.station29.mealtempledelivery.utils.Util.logDebug;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Base64;
import android.util.JsonReader;
import android.util.JsonToken;
import android.view.View;
import android.webkit.SafeBrowsingResponse;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.api.Constant;
import com.station29.mealtempledelivery.data.model.PaymentSuccess;
import com.station29.mealtempledelivery.ui.base.BaseActivity;
import com.station29.mealtempledelivery.utils.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.StringReader;

public class WebViewDialogActivity extends BaseActivity {

    private String getLinkUrl ;
    private String paymentId;
    private View progressBar;
    private WebView webView;
    private TextView txtToolbar;
    private boolean isFirstOpen = false;
    private boolean isOpenDeepLink = false;
    private String action ;
    private PaymentSuccess paymentSuccess;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview_dialog);

        webView = findViewById(R.id.webViewUrl);
        progressBar = findViewById(R.id.progressBar);
        txtToolbar = findViewById(R.id.toolBarTitle);

        initData();

        findViewById(R.id.backButton).setOnClickListener(view -> {
            setResult(RESULT_OK);
           if (webView.canGoBack()) {
                webView.goBack();
            }  else {
                finish();
            }
        });

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.setWebViewClient(new MyWebViewClient());
        webView.setNetworkAvailable(true);
        if (getIntent().getStringExtra("toolBarTitle").equals("card")){
            String data = Base64.encodeToString(getLinkUrl.getBytes(),Base64.NO_PADDING);
            webView.loadData(data, "text/html", "base64");
        } else {
            webView.loadUrl(getLinkUrl);
        }

    }

    private void initData(){

        if (getIntent() != null && getIntent().hasExtra("linkUrl")) {
            getLinkUrl = getIntent().getStringExtra("linkUrl");
        }

        if (getIntent() != null && getIntent().hasExtra("toolBarTitle")){
            txtToolbar.setText(getIntent().getStringExtra("toolBarTitle"));
        }

        if(getIntent() != null && getIntent().hasExtra("paymentId")){
            paymentId = getIntent().getStringExtra("paymentId");
        }

        if (getIntent() != null && getIntent().hasExtra("action")) {
            action = getIntent().getStringExtra("action");
        }

        if (getIntent() != null && getIntent().hasExtra("paymentSuccess")) {
            paymentSuccess = (PaymentSuccess) getIntent().getSerializableExtra("paymentSuccess");
        }
    }

    private void getJsonResponseFromWebView(){
        webView.evaluateJavascript("(function() {return document.getElementsByTagName('html')[0].innerHTML;})();", value -> {
            JsonReader reader = new JsonReader(new StringReader(value));
            reader.setLenient(true);
            try {
                if (reader.peek() == JsonToken.STRING) {
                    String domStr = reader.nextString();
                    if (domStr != null) {
                        String jsonE = Html.fromHtml(domStr).toString();
                        JSONObject jsonObject = new JSONObject(jsonE);

                        // check payment status from json
                        if (jsonObject.has("data")){
                            JSONObject json = jsonObject.getJSONObject("data");
                            String status = json.getString("status");
                            if ("SUCCESSFULL".equalsIgnoreCase(status)){
                                if (action != null && action.equals("top_up")) {
                                    Intent intent = new Intent(WebViewDialogActivity.this,PaymentSuccessActivity.class);
                                    intent.putExtra("paymentSuccess",paymentSuccess);
                                    intent.putExtra("action","top_up");
                                    startActivity(intent);
                                    finish();
                                }
                            } else {
                                Util.popupMessageAndFinish(null,getString(R.string.something_went_wrong),WebViewDialogActivity.this);
                            }
                        } else {
                            Util.popupMessageAndFinish(null,getString(R.string.something_went_wrong),WebViewDialogActivity.this);
                        }

                    }
                }
            } catch (IOException | JSONException e) {
                setResult(RESULT_OK);
                Util.popupMessageAndFinish(null,getString(R.string.something_went_wrong),WebViewDialogActivity.this);
            }
        });
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_OK);
        if (webView.canGoBack()) {
            webView.goBack();
        } else {
            finish();
        }
    }


    public class MyWebViewClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            logDebug("webViewUrl" , "url start :       "+ url);
            progressBar.setVisibility(View.VISIBLE);
            view.setVisibility(View.GONE);

            if(!isOpenDeepLink){
                super.onPageStarted(view, url, favicon);
            } else {
                view.setVisibility(View.GONE);
                isOpenDeepLink = false;
                webView.goBack();
            }

            if (url.contains(Constant.getBaseUrl())){
                view.setVisibility(View.GONE);
            }

            if (view.getVisibility() == View.GONE){
                progressBar.setVisibility(View.VISIBLE);
            } else {
                progressBar.setVisibility(View.GONE);
            }
        }

        @Override
        public void onSafeBrowsingHit(WebView view, WebResourceRequest request, int threatType, SafeBrowsingResponse callback) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
                callback.backToSafety(false);
            }
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            logDebug("webViewUrl" , "url :       "+ url);
            progressBar.setVisibility(View.VISIBLE);
            if(url.contains("https://kesspay.io/") || !url.contains("https://") || url.contains(".apk")) {
                view.setVisibility(View.GONE);
                try {
                    isOpenDeepLink = true;
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(intent);
                } catch (Exception e) {
                    isOpenDeepLink = true;
                    progressBar.setVisibility(View.GONE);
                }
            }

            if (url.contains(Constant.getBaseUrl())){
                view.setVisibility(View.GONE);
            }

            if (view.getVisibility() == View.GONE){
                progressBar.setVisibility(View.VISIBLE);
            } else {
                progressBar.setVisibility(View.GONE);
            }

            return false;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            logDebug("webViewUrl" , "url finish :       "+ url);
            super.onPageFinished(view, url);
            if (url.contains("https://kesspay.io/") || !url.contains("https://") || url.contains(".apk")) {
                view.setVisibility(View.GONE);
            } else if (url.contains(Constant.getBaseUrl())){
                getJsonResponseFromWebView();
            } else {
                view.setVisibility(View.VISIBLE);
            }

            if (view.getVisibility() == View.GONE){
                progressBar.setVisibility(View.VISIBLE);
            } else {
                progressBar.setVisibility(View.GONE);
            }

        }
    }

}
