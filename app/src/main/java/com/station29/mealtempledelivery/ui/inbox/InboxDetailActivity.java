package com.station29.mealtempledelivery.ui.inbox;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.data.model.Inbox;
import com.station29.mealtempledelivery.ui.base.BaseActivity;

import java.util.Locale;

public class InboxDetailActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inbox_detail);

        TextView titleTv = findViewById(R.id.title_detail);
        TextView dateTimeTv = findViewById(R.id.date_time);
        TextView desTv = findViewById(R.id.body_detail);
        ImageView imageView = findViewById(R.id.profile_image);
        TextView titleToolBar = findViewById(R.id.toolBarTitle);
        ImageView btnBack = findViewById(R.id.backButton);
        titleToolBar.setText(R.string.offer_from_kiwigo);

        btnBack.setOnClickListener(v -> onBackPressed());

        if(getIntent().hasExtra("inbox")){
            Inbox inbox = (Inbox) getIntent().getSerializableExtra("inbox");
            titleTv.setText(String.format(Locale.US,"%s", inbox.getTitle()));
            dateTimeTv.setText( inbox.getSendTime());
            desTv.setText(String.format(Locale.US,"%s", inbox.getDescription()));
            if(inbox.getNotificationImage()!=null){
                Glide.with(InboxDetailActivity.this).load(inbox.getNotificationImage()).into(imageView);
            }else {
                findViewById(R.id.view_image).setVisibility(View.GONE);
            }
        }

    }
}