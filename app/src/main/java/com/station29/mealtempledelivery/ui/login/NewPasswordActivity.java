package com.station29.mealtempledelivery.ui.login;

import android.content.Intent;
import android.os.Bundle;

import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.api.request.MockupData;
import com.station29.mealtempledelivery.api.request.ProfileWs;
import com.station29.mealtempledelivery.data.model.User;
import com.station29.mealtempledelivery.ui.base.BaseActivity;
import com.station29.mealtempledelivery.utils.Util;

import java.util.HashMap;

public class NewPasswordActivity extends BaseActivity {

    private EditText txtpassword;
    private final HashMap<String, Object> userData= new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_password);

        txtpassword = findViewById(R.id.txtpassword);
        Button btnRest = findViewById(R.id.btnRest);

        findViewById(R.id.btnBackLogin).setOnClickListener(view  -> {
            finish();
        });

        btnRest.setOnClickListener(v -> {
            if (txtpassword.getText().length()>=6) {
                updatePassword();
            }else{
                Util.popupMessage(getString(R.string.error),getString(R.string.password_min_length),NewPasswordActivity.this);
            }
        });
    }

    private void updatePassword(){
        userData.put("new_password",txtpassword.getText().toString());
        userData.put("action_type","update_password");
        MockupData.setAccount(userData);
        new ProfileWs().updateProfiles(this, userData, new ProfileWs.ProfileViewAndUpdate() {
            @Override
            public void onLoadProfileSuccess(User user ,String message) {
            }

            @Override
            public void onError(String errorMessage) {
                Util.popupMessage(getString(R.string.error),errorMessage,NewPasswordActivity.this);
            }

            @Override
            public void onUpdateProfilesSuccess(String message) {
                Toast.makeText(NewPasswordActivity.this,message,Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(NewPasswordActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Util.saveString("token", "", NewPasswordActivity.this);
    }
}
