package com.station29.mealtempledelivery.ui.setting.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.api.Constant;
import com.station29.mealtempledelivery.ui.boarding.SelectLanguagesActivity;
import com.station29.mealtempledelivery.utils.Util;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class LanguagesAdapter extends RecyclerView.Adapter<LanguagesAdapter.ViewHolder> {

   private final ArrayList<SelectLanguagesActivity.Language> stringArrayList;
   private final ItemClickOnLange itemClickOnLange;
   private final Context context;
   public static int positionNoted = -1;

    public LanguagesAdapter(ArrayList<SelectLanguagesActivity.Language> stringArrayList, ItemClickOnLange itemClickOnLange,Context context) {
        this.stringArrayList = stringArrayList;
        this.itemClickOnLange = itemClickOnLange;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_chang_langauge, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, int position) {
        SelectLanguagesActivity.Language list = stringArrayList.get(position);
        String lang = Util.getString("language", context);
        holder.radioBtn.setChecked(list.getLangCode().equalsIgnoreCase(Constant.getState()) || list.getLangCode().equalsIgnoreCase(lang));
        holder.btnLang.setText(list.getLangName());
        holder.radioBtn.setOnClickListener(view -> {
            holder.radioBtn.setChecked(false);
            itemClickOnLange.onClickItem(list.getLangName(),position);
        });
        holder.linearLayout.setOnClickListener(view -> {
            holder.radioBtn.setChecked(false);
            itemClickOnLange.onClickItem(list.getLangName(),position);

        });

    }

    @Override
    public int getItemCount() {
        return stringArrayList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        RadioButton radioBtn ;
        TextView btnLang;
        LinearLayout linearLayout;

        public ViewHolder(View view) {
            super(view);
            btnLang  = view.findViewById(R.id.khmer);
            radioBtn = view.findViewById(R.id.radioBtn);
            linearLayout = view.findViewById(R.id.linear_click);
        }

    }
    public interface ItemClickOnLange{
        void onClickItem(String lang,int pos);
    }
    public void clear() {
        int size = stringArrayList.size ();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                stringArrayList.remove ( 0 );
            }
            notifyItemRangeRemoved ( 0, size );
        }
    }
}
