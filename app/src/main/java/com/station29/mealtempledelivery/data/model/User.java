package com.station29.mealtempledelivery.data.model;

import android.app.Activity;
import android.content.Intent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.station29.mealtempledelivery.ui.setting.ExchangeRewardsActivity;

import java.io.Serializable;
import java.util.List;

public class User implements Serializable {

    @SerializedName("firstname")
    @Expose
    private String firstName;

    @SerializedName("lastname")
    @Expose
    private String lastName;

    @SerializedName("phone")
    @Expose
    private String phoneNumber;

    @SerializedName("url_image")
    @Expose
    private String profile_image;

    @SerializedName("service")
    @Expose
    private String serviceType;

    @SerializedName("online")
    @Expose
    private int online;

    @SerializedName("wallets")
    @Expose
    private List<Wallets> walletsList;

    @SerializedName("payment_type")
    @Expose
    private String paymentType;

    @SerializedName("pay_tokens")
    @Expose
    private List<PaymentHistory> paymentHistories;

    @SerializedName("confirm_pin")
    @Expose
    private boolean isConfirmPin;

    @SerializedName("account_number")
    @Expose
    private String accountId;

    @SerializedName("driver_user_type")
    @Expose
    private List<DriverUserType> driverUserTypes;

    @SerializedName("total_reward")
    @Expose
    private String totalReward;

    @SerializedName("reward_type")
    @Expose
    private String rewardType;

    @SerializedName("rank")
    @Expose
    private String rank;
    @SerializedName("kyc_doc")
    @Expose
    private KYC kyc;

    public KYC getKyc() {
        return kyc;
    }

    public String getTotalReward() {
        return totalReward;
    }

    public String getRewardType() {
        return rewardType;
    }

    public String getRank() {
        return rank;
    }

    public List<PaymentHistory> getPaymentHistories() {
        return paymentHistories;
    }

    public boolean isConfirmPin() {
        return isConfirmPin;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setConfirmPin(boolean confirmPin) {
        isConfirmPin = confirmPin;
    }

    public List<DriverUserType> getDriverUserTypes() {
        return driverUserTypes;
    }

    public void setDriverUserTypes(List<DriverUserType> driverUserTypes) {
        this.driverUserTypes = driverUserTypes;
    }

    public List<Wallets> getWalletsList() {
        return walletsList;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public String getServiceType() {
        return serviceType;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public int getOnline() {
        return online;
    }
}
