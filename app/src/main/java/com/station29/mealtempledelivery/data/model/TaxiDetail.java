package com.station29.mealtempledelivery.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TaxiDetail implements Serializable {
    @SerializedName("delivery_id")
    @Expose
    private int deliveryId;
    @SerializedName("order")
    @Expose
    private OrderTaxi order;
    @SerializedName("drivers")
    @Expose
    private int drivers;

    public int getDeliveryId() {
        return deliveryId;
    }

    public void setDeliveryId(int deliveryId) {
        this.deliveryId = deliveryId;
    }

    public OrderTaxi getOrder() {
        return order;
    }

    public void setOrder(OrderTaxi order) {
        this.order = order;
    }

    public long getDrivers() {
        return drivers;
    }

    public void setDrivers(int drivers) {
        this.drivers = drivers;
    }
}
