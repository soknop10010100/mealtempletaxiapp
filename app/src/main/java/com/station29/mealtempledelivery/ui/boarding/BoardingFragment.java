package com.station29.mealtempledelivery.ui.boarding;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.api.Constant;
import com.station29.mealtempledelivery.ui.base.BaseFragment;
import com.station29.mealtempledelivery.ui.login.LoginActivity;
import com.station29.mealtempledelivery.utils.Util;



/**
 * A simple {@link Fragment} subclass.
 * Use the {@link BoardingFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BoardingFragment extends BaseFragment {

    private static final String MY_NUM_KEY = "num";
    private static final String MY_IMAGE_KEY = "IMAGE";
    private static final String MY_TITLE_KEY = "TITLE";
    private static final String MY_TEX_KEY = "TEX";
    private static final String MY_LANG_KEY = "Lang";
    private int mNum = 0,index;
    private int image;
    private String title;
    private String tex,lang;

    public static BoardingFragment newInstance(int image,String title, String text,int n,String lang) {
        BoardingFragment fragment = new BoardingFragment();
        Bundle args = new Bundle();
        args.putInt(MY_NUM_KEY,n);
        args.putInt(MY_IMAGE_KEY,image);
        args.putString(MY_TITLE_KEY,title);
        args.putString(MY_TEX_KEY,text);
        args.putString(MY_LANG_KEY,lang);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mNum = getArguments() != null ? getArguments().getInt(MY_NUM_KEY) : -1;
        index = getArguments() != null ? getArguments().getInt(MY_NUM_KEY) : -1;
        image = getArguments() != null ? getArguments().getInt(MY_IMAGE_KEY) : R.drawable.ic_driver_boarding_1;
        title = getArguments() !=null ? getArguments().getString(MY_TITLE_KEY) : "null";
        tex = getArguments() !=null ? getArguments().getString(MY_TEX_KEY):"null";
        lang = getArguments() !=null ? getArguments().getString(MY_LANG_KEY):"";
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_boarding, container, false);
        ImageView imageView = view.findViewById(R.id.imageVie);
        TextView titleTv = view.findViewById(R.id.titles);
        TextView textTv = view.findViewById(R.id.des_Tv);
        TextView skipTv = view.findViewById(R.id.skip);

        if(index==3){
            skipTv.setVisibility(View.GONE);
        } else {
            skipTv.setVisibility(View.VISIBLE);
        }

        skipTv.setOnClickListener(v -> {
            Intent intent = new Intent(mActivity, LoginActivity.class);
            intent.putExtra("lang",lang);
            startActivity(intent);
            Constant.setStart("stop");
            Util.start(mActivity,"stop");
            Util.changeLanguage(mActivity,lang);
        });

        imageView.setImageResource(image);
        titleTv.setText(title);
        textTv.setText(tex);
        return view;
    }
}