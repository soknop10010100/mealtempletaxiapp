package com.station29.mealtempledelivery.data.model

import java.io.Serializable

data class Rewards(
        val id : Int,
        val title : String,
        val start_point : String,
        val end_point : String,
        val user_rules: ArrayList<UserRoles>

) : Serializable

class UserRoles(
        val id : Int,
        val tier_id : Int,
        val activity : String,
        val activity_type : String,
        val reward_type : String,
        val currency_code : String,
        val money_spent : String,
        val activity_credit : String,
        val credit_amount : String,
        val currency_sign : String
) : Serializable

class Season(
        val id : Int,
        val name : String,
        val start_date : String,
        val end_date : String,

) : Serializable


