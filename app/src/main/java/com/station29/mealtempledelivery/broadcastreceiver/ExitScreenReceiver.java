package com.station29.mealtempledelivery.broadcastreceiver;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.api.Constant;
import com.station29.mealtempledelivery.fcm.MyFirebaseMessagingService;

public class ExitScreenReceiver  extends BroadcastReceiver {

    private final int mOrderId;
    private final Activity mActivity;

    public ExitScreenReceiver(int mOrderId, Activity activity) {
        this.mOrderId = mOrderId;
        this.mActivity = activity;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (MyFirebaseMessagingService.ACTION_CANCEL.equals(intent.getAction()) && intent.hasExtra(MyFirebaseMessagingService.ORDER_ID)){
            int orderId = intent.getIntExtra(MyFirebaseMessagingService.ORDER_ID, -1);
            if (mOrderId == orderId) {
                mActivity.finish(); // Kill Alert Call Screen
            }
        }
    }
}
