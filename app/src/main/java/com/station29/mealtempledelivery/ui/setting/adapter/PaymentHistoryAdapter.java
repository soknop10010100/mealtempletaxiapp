package com.station29.mealtempledelivery.ui.setting.adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.data.model.PaymentHistory;


import org.jetbrains.annotations.NotNull;

import java.util.List;

public class PaymentHistoryAdapter extends RecyclerView.Adapter<PaymentHistoryAdapter.ViewHolder> {

    private final List<PaymentHistory> paymentHistories;
    private final OnClickOnPaymentHistory onClickOnPaymentHistory;
    public static int noteHistoryPostion = -1;
    private final String paymentScreen;

    public PaymentHistoryAdapter(List<PaymentHistory> paymentHistories , String paymentScreen, OnClickOnPaymentHistory onClickOnPaymentHistory) {
        this.paymentHistories = paymentHistories;
        this.onClickOnPaymentHistory = onClickOnPaymentHistory;
        this.paymentScreen = paymentScreen;
    }

    @NotNull
    @Override
    public PaymentHistoryAdapter.ViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate( R.layout.fragment_list_payment_history, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint({"SetTextI18n", "ResourceAsColor", "UseCompatLoadingForColorStateLists"})
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        final PaymentHistory paymentHistory = paymentHistories.get(position);
        if(paymentHistory != null){
            Glide.with(holder.pspLogo.getContext())
                    .load(paymentHistory.getLogo())
                    .into( holder.pspLogo );
            holder.paymentToken.setText(paymentHistory.getPspName() +" : "+paymentHistory.getPaymentToken());

            holder.radioButton.setChecked(position == noteHistoryPostion);

//            if ("payment".equals(paymentScreen)) {
//                holder.radioButton.setVisibility(View.GONE);
//                holder.btnDeletePayment.setVisibility(View.VISIBLE);
//                holder.view.setBackgroundTintList(holder.view.getContext().getResources().getColorStateList(R.color.colorGrey));
//            } else if ("deposit".equals(paymentScreen)){
//                holder.radioButton.setVisibility(View.GONE);
//                holder.btnDeletePayment.setVisibility(View.GONE);
//            } else {
//                holder.view.setBackgroundTintList(holder.view.getContext().getResources().getColorStateList(R.color.colorGrey));
//                holder.btnDeletePayment.setVisibility(View.GONE);
//            }
            holder.view.setOnClickListener(v -> {
                noteHistoryPostion = position;
                onClickOnPaymentHistory.onClickOnPayment(paymentHistory , holder.view);
            });

            holder.radioButton.setOnClickListener(v -> {
                noteHistoryPostion = position;
                onClickOnPaymentHistory.onClickOnPayment(paymentHistory, holder.radioButton);
            });

            holder.btnDeletePayment.setOnClickListener(v -> onClickOnPaymentHistory.onClickOnRemove(paymentHistory.getId(),position));

            if (paymentHistories.size() - 1 == position){
                holder.viewLine.setVisibility(View.GONE);
            } else {
                holder.viewLine.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return paymentHistories.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView paymentToken;
        private final RadioButton radioButton;
        private final ImageView pspLogo;
        private final View view , viewLine;
        private final ImageButton btnDeletePayment;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            view = itemView;
            pspLogo = itemView.findViewById(R.id.pspLogo);
            paymentToken = itemView.findViewById(R.id.text);
            radioButton = itemView.findViewById(R.id.radioBtn);
            btnDeletePayment = itemView.findViewById(R.id.deletePaymentBtn);
            viewLine = itemView.findViewById(R.id.viewLine);
        }
    }

    public interface OnClickOnPaymentHistory{
        void onClickOnPayment(PaymentHistory paymentHistory , View view);
        void onClickOnRemove(int paymentId, int position);
    }
}
