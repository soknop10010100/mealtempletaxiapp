package com.station29.mealtempledelivery.ui.setting;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import com.poovam.pinedittextfield.CirclePinField;
import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.ui.base.BaseActivity;

import java.util.Objects;

public class PasswordConfirmActivity extends BaseActivity implements View.OnClickListener {

    private MaterialButton btn0 ,btn1 ,btn2 ,btn3 ,btn4 ,btn5 , btn6 ,btn7 ,btn8 ,btn9;
    private MaterialButton btnClear,btnClearAll;
    private TextView title,textInput;
    private ImageView backButton;
    private CirclePinField pin;
    private String userName;
    private String orderType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_confirm_password);

        btn0 = findViewById(R.id.button0);
        btn1 = findViewById(R.id.button1);
        btn2 = findViewById(R.id.button2);
        btn3 = findViewById(R.id.button3);
        btn4 = findViewById(R.id.button4);
        btn5 = findViewById(R.id.button5);
        btn6 = findViewById(R.id.button6);
        btn7 = findViewById(R.id.button7);
        btn8 = findViewById(R.id.button8);
        btn9 = findViewById(R.id.button9);
        LinearLayout allNumber = findViewById(R.id.allNumber);
        pin = findViewById(R.id.squareField);
        btnClear = findViewById(R.id.buttonClear);
        btnClearAll = findViewById(R.id.buttonC);
        title = findViewById(R.id.toolBarTitle);
        backButton = findViewById(R.id.backButton);
        textInput = findViewById(R.id.textInput);

        if (getIntent() != null && getIntent().hasExtra("userName")) {
            userName = getIntent().getStringExtra("userName");
            textInput.setText(String.format("%s : %s",getString(R.string.send_to),userName));
        }

        if(getIntent() != null && getIntent().hasExtra("orderType")){
            orderType = getIntent().getStringExtra("orderType");
        }
        title.setText(R.string.confirm_pin);
        btn0.setOnClickListener(this);
        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn3.setOnClickListener(this);
        btn4.setOnClickListener(this);
        btn5.setOnClickListener(this);
        btn6.setOnClickListener(this);
        btn7.setOnClickListener(this);
        btn8.setOnClickListener(this);
        btn9.setOnClickListener(this);
        btnClear.setOnClickListener(this);
        btnClearAll.setOnClickListener(this);
        backButton.setOnClickListener(this);

        pin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length() == 4){
                    Intent intent = new Intent();
                    intent.putExtra("pin",s.toString());
                    intent.putExtra("orderType",orderType);
                    setResult(RESULT_OK,intent);
                    finish();
                }
            }
        });
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button0:
                pin.setText(pin.getText().toString()+"0");
                break;
            case R.id.button1:
                pin.setText(pin.getText().toString()+"1");
                break;
            case R.id.button2:
                pin.setText(pin.getText().toString()+"2");
                break;
            case R.id.button3:
                pin.setText(pin.getText().toString()+"3");
                break;
            case R.id.button4:
                pin.setText(pin.getText().toString()+"4");
                break;
            case R.id.button5:
                pin.setText(pin.getText().toString()+"5");
                break;
            case R.id.button6:
                pin.setText(pin.getText().toString()+"6");
                break;
            case R.id.button7:
                pin.setText(pin.getText().toString()+"7");
                break;
            case R.id.button8:
                pin.setText(pin.getText().toString()+"8");
                break;
            case R.id.button9:
                pin.setText(pin.getText().toString()+"9");
                break;
            case R.id.buttonClear:
                int stringLength = Objects.requireNonNull(pin.getText()).length();
                if(stringLength != 0)
                     pin.setText(pin.getText().replace(stringLength-1,stringLength,""));
                break;
            case R.id.buttonC:
                int strength = Objects.requireNonNull(pin.getText()).length();
                if(strength != 0)
                    pin.setText("");
                break;
            case R.id.backButton:
                setResult(RESULT_OK);
                finish();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_OK);
        finish();
    }
}
