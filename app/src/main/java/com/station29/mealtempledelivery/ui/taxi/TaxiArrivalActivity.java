package com.station29.mealtempledelivery.ui.taxi;

import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.ebanx.swipebtn.OnStateChangeListener;
import com.ebanx.swipebtn.SwipeButton;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.button.MaterialButton;
import com.google.gson.Gson;
import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.api.Constant;
import com.station29.mealtempledelivery.api.request.DeliveryWs;
import com.station29.mealtempledelivery.broadcastreceiver.ExitScreenReceiver;
import com.station29.mealtempledelivery.data.model.Delivery;
import com.station29.mealtempledelivery.data.model.TaxiModel;
import com.station29.mealtempledelivery.data.model.Waypoint;
import com.station29.mealtempledelivery.fcm.MyFirebaseMessagingService;
import com.station29.mealtempledelivery.service.LocationUpdatesService;
import com.station29.mealtempledelivery.ui.ServiceArrivalActivity;
import com.station29.mealtempledelivery.ui.base.BaseActivity;
import com.station29.mealtempledelivery.ui.MainActivity;
import com.station29.mealtempledelivery.ui.base.MusicControl;
import com.station29.mealtempledelivery.ui.home.WaitingDialog;
import com.station29.mealtempledelivery.utils.Util;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import static com.station29.mealtempledelivery.fcm.MyFirebaseMessagingService.NOTIFICATION;

import static com.station29.mealtempledelivery.utils.Util.isConnectedToInternet;
import static com.station29.mealtempledelivery.utils.Util.popupMessage;

public class TaxiArrivalActivity extends BaseActivity {

   private TaxiModel taxiModel;
   private WaitingDialog dialog;
   private SwipeButton taxiAccept;
   private ExitScreenReceiver exitScreenReceiver;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_taxi_arrival);

        turnOnScreenn();
        unlockKeyguard();

        if (getIntent() != null && getIntent().hasExtra("taxiModel")) {
            taxiModel = (TaxiModel) getIntent().getSerializableExtra("taxiModel");
        }
        if(getIntent().hasExtra(NOTIFICATION)){
           int i = getIntent().getIntExtra(NOTIFICATION,-1);
            MusicControl.countNotification = i;
        }
        if (taxiModel == null)
            return;
        exitScreenReceiver = new ExitScreenReceiver(taxiModel.getOrderId(), this);
        registerReceiver();

        TextView addPickup = findViewById(R.id.customerAddBs);
        TextView addDropOff = findViewById(R.id.addDropOff);
        TextView distanceTv = findViewById(R.id.distance);
        TextView durantTv = findViewById(R.id.duration);
        TextView payment = findViewById(R.id.payment);
        TextView customNameTv = findViewById(R.id.nameCus);
        TextView totalPriceTv = findViewById(R.id.totalPrices);
        TextView phoneNumber = findViewById(R.id.phonenumber);
        MaterialButton btnView  = findViewById(R.id.viewItem);
        TextView serviceType = findViewById(R.id.serviceType);
        TextView pay = findViewById(R.id.pay);
        LinearLayout linearLayout = findViewById(R.id.pay_1);
        RelativeLayout relativeLayout = findViewById(R.id.pay_2);
        TextView  pays = findViewById(R.id.pays);
        TextView  pays_type = findViewById(R.id.payments);

        if(taxiModel.getTypeOrder().equals("send")){

            btnView.setVisibility(View.VISIBLE);
            serviceType.setText(String.format("%s",getString(R.string.send_box)));
            linearLayout.setVisibility(View.VISIBLE);
            relativeLayout.setVisibility(View.GONE);
            pays.setText(getString(R.string.payment_methods)+" : ");
            if(taxiModel.getPaymentType()!=null){
                pays_type.setText(String.format(Locale.US," %s",taxiModel.getPaymentType()));
            } else {
                pays_type.setText(String.format(Locale.US,"%s",getString(R.string.pay_by_cash)));
            }
            addPickup.setText(Util.getLoc(taxiModel.getPickupLatitude(),taxiModel.getPickupLongitude(),this));
            addDropOff.setText(Util.getLoc(taxiModel.getDropoffLatitude(),taxiModel.getDropoffLongitude(),this));
        } else {
            btnView.setVisibility(View.GONE);
            serviceType.setText(String.format("%s",getString(R.string.taxi)));
            linearLayout.setVisibility(View.GONE);
            relativeLayout.setVisibility(View.VISIBLE);
            pay.setText(getString(R.string.payment_methods));
            if(taxiModel.getPaymentType()!=null){
                payment.setText(String.format(Locale.US," %s",taxiModel.getPaymentType()));
            } else {
                payment.setText(String.format(Locale.US,"%s",getString(R.string.pay_by_cash)));
            }
            addPickup.setText(Util.getLoc(taxiModel.getPickupLatitude(),taxiModel.getPickupLongitude(),this));
            if (taxiModel.getDropoffLatitude() != null && taxiModel.getDropoffLongitude() != null) {
                addDropOff.setText(Util.getLoc(taxiModel.getDropoffLatitude(),taxiModel.getDropoffLongitude(),this));
            } else {
                addDropOff.setText(R.string.no_destination);
            }
        }

        final ShimmerFrameLayout shimmerFrameLayout = findViewById(R.id.shimmer);
        shimmerFrameLayout.startShimmer();
        taxiAccept = findViewById(R.id.acceptTaxi);
        final Button taxiCancel = findViewById(R.id.taxiCancel);

        taxiAccept.setOnStateChangeListener(active -> {
            taxiCancel.setEnabled(false);
            shimmerFrameLayout.hideShimmer();
            taxiCancel.setEnabled(false);
            if(!isFinishing())
                acceptDelivery(taxiModel.getDeliverId(),false);
            removeNotification();//case for when notification is still coming before accept order
            MusicControl.countNotification = 0;
            MusicControl.getInstance(TaxiArrivalActivity.this).stopMusic();
            MainActivity.taxiModels.clear();

        });

        taxiCancel.setOnClickListener(v -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(TaxiArrivalActivity.this, R.style.DialogTheme);
            builder.setTitle(R.string.decline_order);
            builder.setMessage(R.string.are_you_sure_to_decline_this_order);
            builder.setPositiveButton(String.format(Locale.US, "%s", getString(R.string.yes)), (dialogInterface, a) -> {
                if(MusicControl.countNotification > 0 && MainActivity.taxiModels.size()!=0 ) {
                    MyFirebaseMessagingService.orderId = 0;
                    MusicControl.countNotification -=1;
                    MusicControl.getInstance(TaxiArrivalActivity.this).stopMusic();
                    Intent intent = new Intent();
                    if (!MainActivity.taxiModels.isEmpty()) {
                        for (int i = 0; i < MainActivity.taxiModels.size(); i++) {
                            if (taxiModel.getOrderId() == MainActivity.taxiModels.get(i).getOrderId()) {
                                MainActivity.taxiModels.remove( MainActivity.taxiModels.get(i));
                                if (!MainActivity.taxiModels.isEmpty()) {
                                    intent.putExtra("taxiModel", MainActivity.taxiModels.get(0));
                                    intent.putExtra(NOTIFICATION,MusicControl.countNotification);
                                    MainActivity.taxiModels.clear();

                                    break;
                                }else {
                                    MusicControl.countNotification = 0;
                                    MusicControl.getInstance(TaxiArrivalActivity.this).stopMusic();
                                    finish();
                                }
                            }
                        }
                    }
                    setResult(RESULT_OK,intent);
                }else{
                    MusicControl.countNotification = 0;
                    MusicControl.getInstance(TaxiArrivalActivity.this).stopMusic();
                }
                finish();
            });
            builder.setNegativeButton(getString(R.string.no),null);
            builder.show();
        });

        customNameTv.setText(String.format(Locale.US,"%s",taxiModel.getCustomerName()));
        phoneNumber.setText(String.format("%s" ,taxiModel.getCustomerPhone()));
        distanceTv.setText(String.format("%s km" ,taxiModel.getTotalDistance()));
        if(taxiModel.getTripDuration()/60>=60){
            durantTv.setText(String.format(Locale.US," %s",(Util.formatDuration((int) (taxiModel.getTripDuration()/60)/60))+Util.formatTime(((int) Math.ceil(taxiModel.getTripDuration()/60)%60))));
        } else {
            double duration = taxiModel.getTripDuration()/60;
            double du = Math.ceil(duration);
            durantTv.setText(String.format(" %s " ,Util.formatTime((int) du)));
        }
        totalPriceTv.setText(String.format("%s %s",taxiModel.getCurrency() ,Util.formatPrice(taxiModel.getTotalPrice())));


       // payment.setText(R.string.pay_by_cas);
        btnView.setOnClickListener(v -> {
            Intent intent = new Intent(TaxiArrivalActivity.this,HistoryTaxiActivity.class);
            intent.putExtra("delivery",taxiModel.getDeliverId());
            intent.putExtra("typeOrder",taxiModel.getTypeOrder());
            intent.putExtra("back","notBack");
            startActivity(intent);
        });

    }
    private final WaitingDialog.OnCloseButtonClickListener onCloseButtonClickListener = new WaitingDialog.OnCloseButtonClickListener() {
        @Override
        public void onOk(WaitingDialog waitingDialog) {}

        @Override
        public void onClickRate(WaitingDialog waitingDialog) {

        }

        public void onClose(WaitingDialog waitingDialog) {
            waitingDialog.onDismiss();
        }

    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(dialog != null && dialog.isShowing()){
            dialog.cancel();
        }
        unRegisterReceiver();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unRegisterReceiver();
    }

    private void acceptDelivery(int deliveryId, boolean cooperation) {
        final WaitingDialog dialogs = new WaitingDialog(this, onCloseButtonClickListener);
        dialog = dialogs;
        if (!isFinishing())
            dialog.show();
        HashMap<String, Object> params = new HashMap<>();
        params.put("delivery_id", deliveryId);
        params.put("status", Constant.ACCEPTED);
        params.put("cooperation", cooperation);
        params.put("start_latitute", LocationUpdatesService.mLocation.getLatitude());
        params.put("start_longitute", LocationUpdatesService.mLocation.getLongitude());
        params.put("end_latitute", taxiModel.getPickupLatitude());
        params.put("end_longitute", taxiModel.getPickupLongitude());

        new DeliveryWs().acceptDelivery(this,taxiModel.getTypeOrder(), params, new DeliveryWs.OnAcceptDeliveryListener() {
            @Override
            public void onLoadListSuccess(List<Delivery> deliveryList) {

            }

            @Override
            public void onAcceptDeliverySuccess(Waypoint waypoint) {

            }

            @Override
            public void onAcceptSuccessTaxi(Waypoint waypoint) {
                Intent data = new Intent();
                data.putExtra("success", true);
                data.putExtra("taxiModel", taxiModel);
                data.putExtra("waypoint",waypoint);
                data.putExtra("orderId",taxiModel.getOrderId());
                String waypointSave = new Gson().toJson(waypoint);
                Util.saveString("waypointSave", waypointSave, TaxiArrivalActivity.this);// save waypoint to use in homefragment click
                setResult(RESULT_OK, data);
                dialog.dismiss();
                finish();
            }

            @Override
            public void onAcceptFailed(String message) {
                if (!isConnectedToInternet (TaxiArrivalActivity.this)){
                    popupMessage ( null, getString(R.string.check_yor) , TaxiArrivalActivity.this);
                    dialog.onDismiss();
                }else {
                    unRegisterReceiver();
                    Toast.makeText(TaxiArrivalActivity.this,message,Toast.LENGTH_SHORT).show();
                }
                dialog.onDismiss();
                taxiAccept.toggleState();
                finish();
            }
        });
    }

    private void removeNotification() {
        NotificationManager manager = (NotificationManager) TaxiArrivalActivity.this.getSystemService(Context.NOTIFICATION_SERVICE);
        if (manager != null) {
            manager.cancelAll();
        }
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)){
            MyFirebaseMessagingService.orderId = 0;
            MusicControl.getInstance(this).stopMusic();
            MusicControl.countNotification -=1;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void registerReceiver(){
        LocalBroadcastManager.getInstance(this).registerReceiver(exitScreenReceiver, new IntentFilter(MyFirebaseMessagingService.ACTION_CANCEL));
    }

    private void unRegisterReceiver(){
        LocalBroadcastManager.getInstance(this).unregisterReceiver(exitScreenReceiver);
    }


}