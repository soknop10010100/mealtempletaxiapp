package com.station29.mealtempledelivery.ui.history;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.api.Constant;
import com.station29.mealtempledelivery.data.model.Delivery;
import com.station29.mealtempledelivery.utils.Util;

import java.util.List;

import static com.station29.mealtempledelivery.utils.Util.formatDateUptoCurrentRegion;

public class DeliveryItemAdapter extends RecyclerView.Adapter<DeliveryItemAdapter.ViewHolder> {
    //    public static final SimpleDateFormat ymdFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
    private final List<Delivery> deliveryList;
    private final DeliveryItemClickListener itemClickListener;
    private final int tabIndex;
    private final Context context;


    public DeliveryItemAdapter(List<Delivery> deliveryList, int tabIndex, DeliveryItemClickListener itemClickListener,Context context) {
        this.deliveryList = deliveryList;
        this.itemClickListener = itemClickListener;
        this.tabIndex = tabIndex;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_delivery_item, parent, false);
        return new ViewHolder(row);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Delivery delivery = deliveryList.get(position);
        if (delivery != null) {
            holder.deliverIdTvDi.setText(String.format("%s", delivery.getAccountName()));

            String status = "";
            if(delivery.getDeliveryStatus().equalsIgnoreCase("NEW")){
                status = context.getString(R.string.new_sta);
            } else if(delivery.getDeliveryStatus().equalsIgnoreCase("ACCEPTED")){
                status = context.getString(R.string.accept_s);
            } else if(delivery.getDeliveryStatus().equalsIgnoreCase("PICKED_UP")){
                status = context.getString(R.string.pick_ups);
            } else if(delivery.getDeliveryStatus().equalsIgnoreCase("DELIVERED")){
                status = context.getString(R.string.delevered);
            } else {
                status = delivery.getDeliveryStatus();
            }

            if (status!= null && !status.isEmpty()&& status.contains("_")){
                status = status.replace("_", " ");
            }
            holder.statusTvDi.setText(status);

            if(tabIndex == 0){
                assert status != null;
                holder.statusTvDi.setTextColor(status.equalsIgnoreCase("ACCEPTED") ? ContextCompat.getColor(holder.statusTvDi.getContext(),R.color.colorAccept):
                        ContextCompat.getColor(holder.statusTvDi.getContext(),R.color.colorPickup));
            } else {
               holder.statusTvDi.setTextColor(ContextCompat.getColor(holder.statusTvDi.getContext(),R.color.colorAccept));
            }
            if (delivery.getDeliveryTime() != null){
                holder.dateTvDi.setText(formatDateUptoCurrentRegion(delivery.getDeliveryTime()));
                holder.dateTvDi.setTextColor(ContextCompat.getColor((holder.dateTvDi.getContext()),R.color.colorPrimaryDark));
            }
            holder.totalPriceTvDi.setText(String.format("%s %s", Constant.getConfig().getCurrencySign() , Util.stringFormatPrice(delivery.getTotalPrice())));

            holder.totalPriceTvDi.setTextColor(ContextCompat.getColor((holder.totalPriceTvDi.getContext()),R.color.colorPrimaryDark));
            holder.row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemClickListener.onDeliveryItemClick(delivery);
                }
            });

            holder.deliveryType.setTextColor(ContextCompat.getColor((holder.dateTvDi.getContext()),R.color.colorPrimaryDark));
            if("send".equals(delivery.getTypeOrder())){
                holder.deliveryType.setText(String.format("%s : %s",holder.deliveryType.getContext().getString(R.string.service),holder.deliveryType.getContext().getString(R.string.send_box)));
            } else if ("service".equals(delivery.getTypeOrder())){
                holder.deliveryType.setText(String.format("%s : %s",holder.deliveryType.getContext().getString(R.string.service),holder.deliveryType.getContext().getString(R.string.taxi)));
            } else{
                holder.deliveryType.setText(String.format("%s : %s",holder.deliveryType.getContext().getString(R.string.service), holder.deliveryType.getContext().getString(R.string.delivery)));
            }

            if (deliveryList.size() - 1 == position){
                holder.viewLine.setVisibility(View.GONE);
            } else {
                holder.viewLine.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return deliveryList.size();
    }

    public interface DeliveryItemClickListener {
        void onDeliveryItemClick(Delivery deliveryDetail);
//        void onDeliveryTaxi(TaxiDeliveryDetail deliveryDetail);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout delivery, isNotDelivery;
        TextView totalPriceTvDi, dateTvDi, deliverIdTvDi, statusTvDi,deliveryType;
        TextView cusAddTv, storeAddTv;
        Button viewMenuTv;
        View row , viewLine;

        private ViewHolder(@NonNull View itemView) {
            super(itemView);
            deliverIdTvDi = itemView.findViewById(R.id.deliveryIdDi);
            statusTvDi = itemView.findViewById(R.id.statusDi);
            totalPriceTvDi = itemView.findViewById(R.id.priceDi);
            dateTvDi = itemView.findViewById(R.id.dateIdDi);

            cusAddTv = itemView.findViewById(R.id.customerName);
            deliveryType = itemView.findViewById(R.id.deliveryType);
            viewLine = itemView.findViewById(R.id.viewLine);
//            storeAddTv = itemView.findViewById(R.id.storeAddPi);
//            delivery = itemView.findViewById(R.id.isDelivery);
//            isNotDelivery = itemView.findViewById(R.id.isDeliveryInProgress);
//            viewMenuTv = itemView.findViewById(R.id.viewMenuPi);

            row = itemView;
        }
//
    }
    public void clear() {
        int size = deliveryList.size ();
        if (size > 0) {
            deliveryList.subList(0, size).clear();
            notifyItemRangeRemoved ( 0, size );
        }
    }


}
