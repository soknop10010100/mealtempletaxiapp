package com.station29.mealtempledelivery.data.model;

public class OnBoardingScreen {
    int image;
    String title;
    String text;


    int index;
    public OnBoardingScreen(int image, String title, String text,int index) {
        this.image = image;
        this.title = title;
        this.text = text;
        this.index = index;
    }

    public int getIndex() {
        return index;
    }
    public int getImage() {
        return image;
    }

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }
}
