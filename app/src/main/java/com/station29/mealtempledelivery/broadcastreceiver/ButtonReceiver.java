package com.station29.mealtempledelivery.broadcastreceiver;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.station29.mealtempledelivery.api.Constant;
import com.station29.mealtempledelivery.fcm.MyFirebaseMessagingService;
import com.station29.mealtempledelivery.ui.base.MusicControl;
import com.station29.mealtempledelivery.utils.Util;

import static android.content.Context.NOTIFICATION_SERVICE;
import static com.station29.mealtempledelivery.fcm.MyFirebaseMessagingService.DELIVERY_ID;
import static com.station29.mealtempledelivery.fcm.MyFirebaseMessagingService.NOTIFICATION;
import static com.station29.mealtempledelivery.fcm.MyFirebaseMessagingService.NOTIFICATION_ID;
import static com.station29.mealtempledelivery.utils.Util.logDebug;

public class ButtonReceiver  extends BroadcastReceiver {

    public FirebaseAnalytics mFirebaseAnalytics;

    @Override
    public void onReceive(final Context context, Intent intent) {
        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
        NotificationManager manager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        if (manager != null) {
            int notificationId = intent.getIntExtra(NOTIFICATION_ID, -1);
            logDebug("notification_onReceive", notificationId + "");
            manager.cancel(intent.getIntExtra(NOTIFICATION_ID, -1));
            //    MusicControl.getInstance(context).reverse();

            // ============================================
            Bundle bundle = new Bundle();
            int serviceId = intent.getIntExtra(DELIVERY_ID, -1);
            bundle.putString(Constant.USER_ID, Util.getString("user_id", context) + " cancel Delivery Id" + serviceId);
            mFirebaseAnalytics.logEvent(this.getClass().getSimpleName(), bundle);
            //=============================================

           int notifications = intent.getIntExtra(NOTIFICATION,-1);
           int i  = intent.getIntExtra("not_tax",-1);
           MusicControl.countNotification = notifications;
            if( MusicControl.countNotification > 0 && i > 0){
                MyFirebaseMessagingService.orderId = 0;
                MusicControl.countNotification -= 1;
           } else {
                MusicControl.countNotification = 0;
            }
            MusicControl.getInstance(context).stopMusic();
        }

    }


}