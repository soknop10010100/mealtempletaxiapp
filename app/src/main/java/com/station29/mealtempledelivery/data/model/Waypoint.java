package com.station29.mealtempledelivery.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Waypoint implements Serializable {
    @SerializedName("distance")
    @Expose
    private int distance;
    @SerializedName("duration")
    @Expose
    private double duration;
    @SerializedName("polyline")
    @Expose
    private String polyline;
    @SerializedName("end_address")
    @Expose
    private String endAddress;
    @SerializedName("start_address")
    @Expose
    private String startAddress;
    @SerializedName("arr_json_lat_lng")
    @Expose
    private List<ArrJsonLatLng> arrJsonLatLng = null;

    public long getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public double getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getPolyline() {
        return polyline;
    }

    public void setPolyline(String polyline) {
        this.polyline = polyline;
    }

    public String getEndAddress() {
        return endAddress;
    }

    public void setEndAddress(String endAddress) {
        this.endAddress = endAddress;
    }

    public String getStartAddress() {
        return startAddress;
    }

    public void setStartAddress(String startAddress) {
        this.startAddress = startAddress;
    }

    public List<ArrJsonLatLng> getArrJsonLatLng() {
        return arrJsonLatLng;
    }

    public void setArrJsonLatLng(List<ArrJsonLatLng> arrJsonLatLng) {
        this.arrJsonLatLng = arrJsonLatLng;
    }

}
