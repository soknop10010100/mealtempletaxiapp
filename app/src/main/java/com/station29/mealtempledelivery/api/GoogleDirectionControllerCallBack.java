package com.station29.mealtempledelivery.api;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

/**
 * Created by sambath on 5/29/17.
 */

public interface GoogleDirectionControllerCallBack {
    void onSuccess(ArrayList<LatLng> result, long distance);
    void onError(String error);
}