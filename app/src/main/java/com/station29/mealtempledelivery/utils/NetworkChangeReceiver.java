package com.station29.mealtempledelivery.utils;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.station29.mealtempledelivery.ui.base.BaseActivity;


public class NetworkChangeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            BaseActivity.dialog(Util.isConnectedToInternet((Activity) context),(Activity) context);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }
}
