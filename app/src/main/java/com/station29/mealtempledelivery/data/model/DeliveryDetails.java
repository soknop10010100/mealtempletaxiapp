package com.station29.mealtempledelivery.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class DeliveryDetails implements Serializable {
    @SerializedName("order_id")
    @Expose
    private Integer orderId;
    @SerializedName("store_name")
    @Expose
    private String storeName;
    @SerializedName("store_lat")
    @Expose
    private Double storeLat;
    @SerializedName("store_lng")
    @Expose
    private Double storeLng;
    @SerializedName("tel")
    @Expose
    private String tel;
    @SerializedName("order_number")
    @Expose
    private String orderNumber;
    @SerializedName("shipping_lat")
    @Expose
    private Double shippingLat;
    @SerializedName("shipping_lng")
    @Expose
    private Double shippingLng;
    @SerializedName("shipping_phone")
    @Expose
    private String shippingPhone;
    @SerializedName("order_time")
    @Expose
    private String orderTime;
    @SerializedName("delivery_time")
    @Expose
    private String deliveryTime;
    @SerializedName("product_price_after_discount_vat")
    @Expose
    private String productPriceAfterDiscountVat;
    @SerializedName("rider_tip")
    @Expose
    private String riderTip;
    @SerializedName("payment_type")
    @Expose
    private String paymentType;
    @SerializedName("cooperation")
    @Expose
    private int cooperation;
    @SerializedName("product_price_after_discount_vat_formatted")
    @Expose
    private String productPriceAfterDiscountVatFormatted;
    @SerializedName("product_price_after_discount_vat_formatted_2")
    @Expose
    private String productPriceAfterDiscountVatFormatted2;
    @SerializedName("delivery_fee")
    @Expose
    private String deliveryFee;
    @SerializedName("delivery_fee_formatted")
    @Expose
    private String deliveryFeeFormatted;
    @SerializedName("delivery_fee_formatted_2")
    @Expose
    private String deliveryFeeFormatted2;
    @SerializedName("total")
    @Expose
    private String total;
    @SerializedName("total_formatted")
    @Expose
    private String totalFormatted;
    @SerializedName("total_formatted_2")
    @Expose
    private String totalFormatted2;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("items")
    @Expose
    private List<Item> items = null;
    @SerializedName("waypoints")
    @Expose
    private Waypoint waypoints;

    @SerializedName("cooperation_order_id")
    @Expose
    private String cooperationOrderId;

    @SerializedName("delivery_commission_rate")
    @Expose
    private String deliveryCommissionRate;
    @SerializedName("delivery_commission_price")
    @Expose
    private String deliveryCommissionPrice;
    @SerializedName("delivery_fee_after_commission_rate")
    @Expose
    private String deliveryFeeAfterCommissionRate;
    @SerializedName("price_discount")
    @Expose
    private String priceDiscount;
    @SerializedName("coupon_discount")
    @Expose
    private String couponDiscount;

    @SerializedName("total_product_price")
    @Expose
    private String totalProductPrice;
    @SerializedName("free_delivery_fee")
    @Expose
    private String freeDeliveryFee;
    @SerializedName("taxs")
    @Expose
    private List<Tax> taxList;
    @SerializedName("rate")
    @Expose
    private List<String> rate;

    public List<String> getRate() {
        return rate;
    }

    public List<Tax> getTaxList() {
        return taxList;
    }

    public String getFreeDeliveryFee() {
        return freeDeliveryFee;
    }

    public String getTotalProductPrice() {
        return totalProductPrice;
    }

    public String getDeliveryCommissionRate() {
        return deliveryCommissionRate;
    }

    public String getDeliveryCommissionPrice() {
        return deliveryCommissionPrice;
    }

    public String getDeliveryFeeAfterCommissionRate() {
        return deliveryFeeAfterCommissionRate;
    }

    public String getPriceDiscount() {
        return priceDiscount;
    }

    public String getCouponDiscount() {
        return couponDiscount;
    }


    public String getCooperationOrderId() {
        return cooperationOrderId;
    }


    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public Double getStoreLat() {
        return storeLat;
    }

    public void setStoreLat(Double storeLat) {
        this.storeLat = storeLat;
    }

    public Double getStoreLng() {
        return storeLng;
    }

    public void setStoreLng(Double storeLng) {
        this.storeLng = storeLng;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Double getShippingLat() {
        return shippingLat;
    }

    public void setShippingLat(Double shippingLat) {
        this.shippingLat = shippingLat;
    }

    public Double getShippingLng() {
        return shippingLng;
    }

    public void setShippingLng(Double shippingLng) {
        this.shippingLng = shippingLng;
    }

    public String getShippingPhone() {
        return shippingPhone;
    }

    public void setShippingPhone(String shippingPhone) {
        this.shippingPhone = shippingPhone;
    }

    public String getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }

    public String getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(String deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public String getProductPriceAfterDiscountVat() {
        return productPriceAfterDiscountVat;
    }

    public void setProductPriceAfterDiscountVat(String productPriceAfterDiscountVat) {
        this.productPriceAfterDiscountVat = productPriceAfterDiscountVat;
    }

    public String getRiderTip() {
        return riderTip;
    }

    public void setRiderTip(String riderTip) {
        this.riderTip = riderTip;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public int getCooperation() {
        return cooperation;
    }

    public void setCooperation(int cooperation) {
        this.cooperation = cooperation;
    }

    public String getProductPriceAfterDiscountVatFormatted() {
        return productPriceAfterDiscountVatFormatted;
    }

    public void setProductPriceAfterDiscountVatFormatted(String productPriceAfterDiscountVatFormatted) {
        this.productPriceAfterDiscountVatFormatted = productPriceAfterDiscountVatFormatted;
    }

    public String getProductPriceAfterDiscountVatFormatted2() {
        return productPriceAfterDiscountVatFormatted2;
    }

    public void setProductPriceAfterDiscountVatFormatted2(String productPriceAfterDiscountVatFormatted2) {
        this.productPriceAfterDiscountVatFormatted2 = productPriceAfterDiscountVatFormatted2;
    }

    public String getDeliveryFee() {
        return deliveryFee;
    }

    public void setDeliveryFee(String deliveryFee) {
        this.deliveryFee = deliveryFee;
    }

    public String getDeliveryFeeFormatted() {
        return deliveryFeeFormatted;
    }

    public void setDeliveryFeeFormatted(String deliveryFeeFormatted) {
        this.deliveryFeeFormatted = deliveryFeeFormatted;
    }

    public String getDeliveryFeeFormatted2() {
        return deliveryFeeFormatted2;
    }

    public void setDeliveryFeeFormatted2(String deliveryFeeFormatted2) {
        this.deliveryFeeFormatted2 = deliveryFeeFormatted2;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getTotalFormatted() {
        return totalFormatted;
    }

    public void setTotalFormatted(String totalFormatted) {
        this.totalFormatted = totalFormatted;
    }

    public String getTotalFormatted2() {
        return totalFormatted2;
    }

    public void setTotalFormatted2(String totalFormatted2) {
        this.totalFormatted2 = totalFormatted2;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public Waypoint getWaypoints() {
        return waypoints;
    }

    public void setWaypoints(Waypoint waypoints) {
        this.waypoints = waypoints;
    }
}