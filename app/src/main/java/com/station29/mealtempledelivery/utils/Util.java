package com.station29.mealtempledelivery.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.station29.mealtempledelivery.BuildConfig;
import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.api.Constant;
import com.station29.mealtempledelivery.api.request.DeliveryWs;
import com.station29.mealtempledelivery.data.model.Delivery;
import com.station29.mealtempledelivery.data.model.Waypoint;
import com.station29.mealtempledelivery.service.LocationUpdatesService;
import com.station29.mealtempledelivery.ui.base.BaseActivity;
import com.station29.mealtempledelivery.ui.MainActivity;
import com.station29.mealtempledelivery.ui.SplashScreenActivity;
import com.station29.mealtempledelivery.ui.boarding.BoardingActivity;
import com.station29.mealtempledelivery.ui.setting.adapter.ListAdapter;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import id.zelory.compressor.Compressor;

import static com.station29.mealtempledelivery.ui.taxi.TaxiCompleteActivity.latLngList;

import org.json.JSONObject;

public class Util {

    static final String KEY_REQUESTING_LOCATION_UPDATES = "requesting_locaction_updates";

    public static String getLocationTitle(Context context) {
        return context.getString(R.string.location_updated) +" "+ DateFormat.getDateTimeInstance().format(new Date());
    }

    public static String formatPrice(double number) {
        String price = String.valueOf(number);
        if (Constant.getConfig() != null){
            if (Constant.getConfig().getDecimalPlace() == 0) {
                price = (int) number + "";
            } else if (Constant.getConfig().getDecimalPlace() == 1) {
                price = String.format(Locale.US, "%.1f", number);
            } else if (Constant.getConfig().getDecimalPlace() == 2) {
                price = String.format(Locale.US, "%.2f", number);
            } else if (Constant.getConfig().getDecimalPlace() == 3) {
                price = String.format(Locale.US, "%.3f", number);
            }
        } else {
            price = String.format(Locale.US, "%.2f", number);
        }

        if (number < 0 && Constant.getConfig().getDecimalPlace() != 0){
            if (BaseActivity.language.equals("en") && Constant.getConfig().getDecimalPlace() == 2) {
                double myNumber = Double.parseDouble(price);
                NumberFormat formatter = new DecimalFormat("#,##0.00");
                return formatter.format(myNumber);
            } else {
                return NumberFormat.getNumberInstance(Locale.getDefault()).format(Double.parseDouble("0.00"));
            }
        } else {
            if (Constant.getConfig().getDecimalPlace() == 2) {
                double myNumber = Double.parseDouble(price);
                NumberFormat formatter = new DecimalFormat("#,##0.00");
                return formatter.format(myNumber);
            } else {
                return NumberFormat.getNumberInstance(Locale.getDefault()).format(Double.parseDouble(price));
            }
        }
    }

    public static String stringFormatPrice(String price) {
        String num = price;
        if (price != null && !price.equals("")) {
            if (Constant.getConfig() != null) {
                String[] arrOfStr = num.split("\\.");
                if (Constant.getConfig().getDecimalPlace() == 0) {
                    num = arrOfStr[0];
                } else if (Constant.getConfig().getDecimalPlace() == 1) {
                    num = arrOfStr[0]  + "." + arrOfStr[1].substring(1);
                } else if (Constant.getConfig().getDecimalPlace() == 2) {
                    num = price;
                }
            }
        }
        if (Constant.getConfig().getDecimalPlace() == 2) {
            double myNumber = Double.parseDouble(num);
            NumberFormat formatter = new DecimalFormat("#,##0.00");
            return formatter.format(myNumber);
        } else {
            return NumberFormat.getNumberInstance(Locale.getDefault()).format(Double.parseDouble(num));
        }
    }

    public static String stringFormatPrice(String price, int decimal) {
        String num = price;
        if (Constant.getConfig() != null) {
            String[] arrOfStr = num.split("\\.");
            if (decimal == 0) {
                num = arrOfStr[0];
            } else if (decimal == 1) {
                num = arrOfStr[0]  + "." + arrOfStr[1].substring(1);
            } else if (decimal == 2) {
                num = price;
            }
        }
        if (decimal == 2) {
            double myNumber = Double.parseDouble(num);
            NumberFormat formatter = new DecimalFormat("#,##0.00");
            return formatter.format(myNumber);
        } else {
            return NumberFormat.getNumberInstance(Locale.getDefault()).format(Double.parseDouble(num));
        }
    }

    public static String formatDistance(double number) {
        return String.format(Locale.US, "%.2f", number);
    }

    public static String format2Digits(double number) {
        NumberFormat f = new DecimalFormat("00");
        return f.format(number);
    }
    public static int checkVisiblePaymentService() {
        int size = 0;
        if (Constant.getConfig().getPaymentServiceList() != null && !Constant.getConfig().getPaymentServiceList().isEmpty()){
            for (int i = 0; i < Constant.getConfig().getPaymentServiceList().size(); i++) {
                if (Constant.getConfig().getPaymentServiceList().get(i).getStatus() != 0
                        && Constant.getConfig().getPaymentServiceList().get(i).getIsCash() != 1 &&
                        Constant.getConfig().getPaymentServiceList().get(i).getType() != null && !Constant.getConfig().getPaymentServiceList().get(i).getType().equals("KESS_PAY"))
                    size++;
            }
            return size;
        } else {
            return 0;
        }
    }


    public static String formatTime(int millisecond) {
        int minute = millisecond;
        return minute+" mn";
    }

    public static String formatDuration(int millisecond) {
        int minute = millisecond;
        return minute+"h";
    }
    public static String formatTimeToMinute(int millisecond){
        int mn = millisecond/1000/60;
        return format2Digits(mn)+"mn";
    }

    /**
     * Returns true if requesting location updates, otherwise returns false.
     *
     * @param context The {@link Context}.
     */
    public static boolean requestingLocationUpdates(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(KEY_REQUESTING_LOCATION_UPDATES, false);
    }

    /**
     * Stores the location updates state in SharedPreferences.
     * <p>
     * //     * @param requestingLocationUpdates The location updates state.
     */
    public static void setRequestingLocationUpdates(Context context, boolean requestingLocationUpdates) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putBoolean(KEY_REQUESTING_LOCATION_UPDATES, requestingLocationUpdates)
                .apply();
    }

    public static void hideSoftKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) view.getContext().getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static String getLocationText(Location location) {
        return location == null ? "Unknown location" :
                "(" + location.getLatitude() + ", " + location.getLongitude() + ")";
    }

    public static void saveString(String key, String value, Context activity) {
        SharedPreferences aSharedPreferences = activity.getSharedPreferences(
                "DeliveryApp", Context.MODE_PRIVATE);
        SharedPreferences.Editor aSharedPreferencesEdit = aSharedPreferences
                .edit();

        aSharedPreferencesEdit.putString(key, value);
        aSharedPreferencesEdit.apply();
    }

    public static void saveDouble(String key, long value, Context activity) {
        SharedPreferences aSharedPreferences = activity.getSharedPreferences(
                "DeliveryApp", Context.MODE_PRIVATE);
        SharedPreferences.Editor aSharedPreferencesEdit = aSharedPreferences
                .edit();

        aSharedPreferencesEdit.putLong(key, value);
        aSharedPreferencesEdit.apply();
    }

    public static void popupMessage(String title, String message, Activity activity) {
        if(!activity.isFinishing() && !message.equals("2131951665")&& !message.equals("2131951667")&&!message.equals("2131951691")) {
            AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.DialogTheme);
            if (title != null) builder.setTitle(title);
            builder.setMessage(message);
            builder.setPositiveButton(String.format(Locale.US, "%s", activity.getString(R.string.ok)), null);
            builder.show();
        }
    }

    public static void popupMessageAndClose(String title, String message, final Activity activity) {
        if(!activity.isFinishing()&& !message.equals("2131951665")) {
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            if (title != null) builder.setTitle(title);
            builder.setMessage(message);
            builder.setPositiveButton(String.format(Locale.US, "%s", activity.getString(R.string.close)), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    activity.finish();
                }
            });
            builder.setCancelable(false);
            builder.show();
        }
    }

    public static void clearString(Activity activity, String keys) {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(keys, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }

    public static String getString(String key, Context activity) {
        SharedPreferences aSharedPreferences = activity.getSharedPreferences(
                "DeliveryApp", Context.MODE_PRIVATE);
        return aSharedPreferences.getString(key, "");
    }

    public static long getDouble(String key, Context activity) {
        SharedPreferences aSharedPreferences = activity.getSharedPreferences(
                "DeliveryApp", Context.MODE_PRIVATE);
        return aSharedPreferences.getLong(key, 0);
    }

    public static String getBase64FromPath(String path, Context context) {
        File imagefile = new File(path);
        FileInputStream fis = null;
        try {
            File fNew = new Compressor(context).compressToFile(imagefile);
            if (!fNew.exists()) {
                fNew.createNewFile();
            }
            fis = new FileInputStream(fNew);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Bitmap bm = BitmapFactory.decodeStream(fis);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        return Base64.encodeToString(b, Base64.DEFAULT);
    }

    public static String convert(Bitmap bitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);

        return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);

    }

    public static String getLoc(double lat, double lng, Context context) {

        Geocoder geocoder = new Geocoder(context);
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(lat, lng, 1);
        } catch (IOException | IllegalArgumentException e) {
            e.printStackTrace();
            Log.e(e.getClass().getName(), e.getMessage() + "");
            return "Can't find location";
        }

        if (addresses != null && addresses.size() > 0) {
            return addresses.get(0).getAddressLine(0);
        }
        return context.getString(R.string.cannot_find_location);
    }

    public static Date formatDateTime(String dateTime) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        Date newDate = null;
        try {
            newDate = format.parse(dateTime);
            logDebug("newDate", String.valueOf(format));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        assert newDate != null;
        return newDate;
    }

    public static String formatTimeZoneLocal(String dateTime) {
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy hh:mm a", Locale.US);
        Date date = formatDateTime(dateTime);
        return format.format(date.getTime() + new GregorianCalendar().getTimeZone().getRawOffset());
    }

    public static boolean isConnectedToInternet(Activity activity) {
        ConnectivityManager connectivity = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }

        }
        return false;
    }

    public static byte[] readBytesFromPath(String filePath) {
        FileInputStream fileInputStream = null;
        byte[] bytesArray = null;
        try {
            File file = new File(filePath);
            bytesArray = new byte[(int) file.length()];

            //read file into bytes[]
            fileInputStream = new FileInputStream(file);
            fileInputStream.read(bytesArray);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return bytesArray;
    }

    public static String getFileName(String path) {
        return path.substring(path.lastIndexOf("/") + 1);
    }

    // =====================================================================================
    public static void saveTrackPointsLocal(Location loc, Context ctx) {
        try {
            if (latLngList.isEmpty()) { // if latLngList has items => app not clear while processing service => No need to read from File
                List<LatLng> oldRecords = Util.getTrackPoints(ctx);
                if (!oldRecords.isEmpty()) {
                    latLngList = oldRecords;
                }
            }
            latLngList.add(new LatLng(loc.getLatitude(), loc.getLongitude()));
            logDebug("latLngSize", latLngList.size() + "");
            logDebug("latLngData", latLngList.toString());
            JsonArray jsonArray = new JsonArray();
            for (LatLng latLng : latLngList) {
                JsonArray j = new JsonArray();
                j.add(latLng.latitude);
                j.add(latLng.longitude);
                jsonArray.add(j);
            }
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(ctx.openFileOutput(Constant.fileName, Context.MODE_PRIVATE));
            outputStreamWriter.write(jsonArray.toString());
            outputStreamWriter.close();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    public static List<LatLng> getTrackPoints(Context ctx) {
        List<LatLng> list = new ArrayList<>();
        String points = readTrackPointsLocal(ctx); // string json array
        if (points != null && !points.isEmpty()) {
            JsonArray jsonArray = new Gson().fromJson(points, JsonArray.class);
            for (int i = 0; i < jsonArray.size(); i++) {
                JsonArray ele = jsonArray.get(i).getAsJsonArray();
                list.add(new LatLng(ele.get(0).getAsDouble(), ele.get(1).getAsDouble()));
            }
        }
        return list;
    }

    public static String readTrackPointsLocal(Context ctx) {
        String ret = "";
        try {
            InputStream inputStream = ctx.openFileInput(Constant.fileName);
            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ((receiveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(receiveString);
                }
                inputStream.close();
                ret = stringBuilder.toString();
                logDebug(Constant.fileName, ret);
            }
        } catch (FileNotFoundException e) {
            Log.e(e.getClass().getName(), "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.e(e.getClass().getName(), "Can not read file: " + e.getMessage());
        }
        return ret;
    }

    public static void clearFile(Context context) {
        OutputStreamWriter outputStreamWriter;
        try {
            outputStreamWriter = new OutputStreamWriter(context.openFileOutput(Constant.fileName, Context.MODE_PRIVATE));
            outputStreamWriter.write("");
            outputStreamWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void startRecordLocation(Context context) {
        saveString("tracking", "start", context);
        saveDouble("pickup_time", Calendar.getInstance(Locale.getDefault()).getTimeInMillis(), context);
        Location loc = LocationUpdatesService.mLocation;
        GPSTracker gpsTracker = new GPSTracker(context);
        if (gpsTracker.canGetLocation() && gpsTracker.getLatitude() != 0 && gpsTracker.getLongitude() != 0) {
            double lat = gpsTracker.getLatitude();
            double lng = gpsTracker.getLongitude();
            loc.setLatitude(lat);
            loc.setLongitude(lng);
        }
        if (loc != null) Util.saveTrackPointsLocal(loc, context);
    }

    public static void stopRecordLocation(Context context) {
        saveString("tracking", "end", context);
        saveDouble("pickup_time", 0, context);
        latLngList.clear();
        clearFile(context);
    }

    public static void changeLanguage(Activity context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Util.saveString("language", language, context);

        Resources res = context.getResources();
        Configuration config = new Configuration(res.getConfiguration());
        config.locale = locale;
        res.updateConfiguration(config, res.getDisplayMetrics());
    }
    public static void start(Activity context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Util.saveString("start", language, context);

        Resources res = context.getResources();
        Configuration config = new Configuration(res.getConfiguration());
        config.locale = locale;
        res.updateConfiguration(config, res.getDisplayMetrics());
    }

    public static int dpToPx(Context context, int dp) {
        Resources r = context.getResources();
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics());
        return (int) px;
    }

    public static String getRealPathFromURI(Uri contentURI, Activity context) {
        String[] projection = {MediaStore.Images.Media.DATA};
        @SuppressWarnings("deprecation")
        Cursor cursor = context.managedQuery(contentURI, projection, null,
                null, null);
        if (cursor == null)
            return null;
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        if (cursor.moveToFirst()) {
            String s = cursor.getString(column_index);
            // cursor.close();
            return s;
        }
        // cursor.close();
        return null;
    }

    public static String formatDateUptoCurrentRegion(String dateTime){
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy hh:mm a", Locale.US);
        Date date = formatDateTime(dateTime);

        // Get Raw Offset
        Calendar mCalendar = new GregorianCalendar();
        TimeZone mTimeZone = mCalendar.getTimeZone();
        int mGMTOffset = mTimeZone.getRawOffset();

        return format.format(date.getTime() + mGMTOffset);
    }

    public static void changeLangPopup(final Activity mActivity , final String type) {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(mActivity);
        builder.setTitle(mActivity.getString(R.string.language));
        List<String> data = new ArrayList<>();
        data.add(mActivity.getString(R.string.khmer));
        data.add(mActivity.getString(R.string.english));
        data.add(mActivity.getString(R.string.france));
        data.add(mActivity.getString(R.string.laos));

        int oldCheckPos;
        final String[] lang = {Util.getString("language", mActivity)};
        if (lang[0].equals("") || lang[0].equals( Constant.LANG_KH)) {
            oldCheckPos = 0;
        } else if (lang[0].equals( Constant.LANG_EN)) {
            oldCheckPos = 1;
        } else if (lang[0].equals( Constant.LANG_FR)){
            oldCheckPos = 2;
        } else {
            oldCheckPos = 3;
        }
        ListAdapter listAdapter = new ListAdapter(mActivity, android.R.layout.simple_list_item_1, data, oldCheckPos);
        builder.setSingleChoiceItems(listAdapter, oldCheckPos, null);
        builder.setNegativeButton(mActivity.getString(R.string.cancel), null);

        androidx.appcompat.app.AlertDialog dialog = builder.create();
        final ListView listView = dialog.getListView();
        if (listView != null) {
            listView.setDivider(new ColorDrawable(Color.LTGRAY)); // set color
            listView.setDividerHeight(1);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    // To clear all focus
                    for (int k = 0 ; k< listView.getChildCount(); k++) {
                        View v = listView.getChildAt(k);
                        if (v instanceof TextView) {
                            ((TextView) v).setCompoundDrawablesRelativeWithIntrinsicBounds(0,0,0,0);
                        }
                    }
                    // To select user choice
                    if (view instanceof TextView) {
                        TextView textView = (TextView) view;
                        textView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_baseline_check_18, 0);
                    }

                    if (i == 0) {
                        lang[0] =  Constant.LANG_KH;
                        Util.changeLanguage(mActivity, lang[0]);
                    } else if (i == 1) {
                        lang[0] =  Constant.LANG_EN;
                        Util.changeLanguage(mActivity, lang[0]);
                    } else if ( i == 2){
                        lang[0] =  Constant.LANG_FR;
                        Util.changeLanguage(mActivity, lang[0]);
                    } else {
                        lang[0] =  Constant.LANG_LA;
                        Util.changeLanguage(mActivity, lang[0]);
                    }
                    Intent intent;
                    if (type.equals("start")){
                        intent = new Intent(mActivity, BoardingActivity.class);
                        intent.putExtra("lang", lang[0]);
                    } else {
                        intent = new Intent(mActivity, SplashScreenActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    }
                    mActivity.startActivity(intent);

                }
            });
        }
        dialog.show();
    }

    public static void startSplash(Context mActivity){
        Intent intent = new Intent(mActivity, SplashScreenActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        mActivity.startActivity(intent);
    }

    public static String getCountryCodeFromSimCard (Context context){
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);  //gets the current TelephonyManager
        if (tm != null && tm.getSimState() != TelephonyManager.SIM_STATE_ABSENT) {
            return tm.getNetworkCountryIso();
        }
        return null;
    }

    /** Start Service that already running, service will recall in StartCommand and execute sth send with extra
     * Once service started, it cannot create new one.
     **/
    public static void updateServiceInterval(Activity activity, long interval){
        Intent intentService = new Intent(activity, LocationUpdatesService.class);
        intentService.putExtra("interval", interval);
        activity.startService(intentService);
    }

    public static void popUpCancelOrder(String title, final Activity activity , final int deliveryId) {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(activity,R.style.DialogTheme);
        if (title != null) builder.setTitle(title);
        builder.setMessage(R.string.are_you_sure_to_cancel_order);
        builder.setNegativeButton ( activity.getString ( R.string.cancel ) ,null);
        builder.setPositiveButton(activity.getString(R.string.ok ), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog, int which) {

                    new DeliveryWs().cancelTaxi(activity, deliveryId , new DeliveryWs.OnAcceptDeliveryListener() {
                        @Override
                        public void onLoadListSuccess(List<Delivery> deliveryList) {

                        }

                        @Override
                        public void onAcceptDeliverySuccess(Waypoint waypoint) {

                        }

                        @Override
                        public void onAcceptSuccessTaxi(Waypoint waypoint) {

                        }

                        @Override
                        public void onAcceptFailed(String message) {
                            Toast.makeText(activity,message,Toast.LENGTH_SHORT).show();
                            activity.startActivity(new Intent(activity, MainActivity.class));

                        }
                    });
            }
        });
        builder.show();
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }

    public static Bitmap getQRCodeImage(String text) {
        Hashtable<EncodeHintType, String> hints = new Hashtable<>();
        hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
        QRCodeWriter writer = new QRCodeWriter();
        try {
            BitMatrix bitMatrix = writer.encode(text, BarcodeFormat.QR_CODE, 512, 512,hints);
            int w = bitMatrix.getWidth();
            int h = bitMatrix.getHeight();
            Bitmap bmp = Bitmap.createBitmap(w, h, Bitmap.Config.RGB_565);
            for (int x = 0; x < w; x++) {
                for (int y = 0; y < h; y++) {
                    bmp.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.WHITE);
                }
            }
            return bmp;
        } catch (WriterException e) {
            e.printStackTrace();
            Log.e(e.getClass().getName(), e.getMessage() + "");
        }
        return null;
    }

    public static void setClipboard(Context context, String text) {
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("copied", text);
        clipboard.setPrimaryClip(clip);
    }

    public static void logDebug(String logTitle, String message) {
        if (BuildConfig.DEBUG) {//logDebug only in debug mode
            Log.d(logTitle, message);
        }
    }

    public static String encodeByPublicKey(String fields) {
        logDebug("encodeString","  :"+fields);
        String pubKeyBase64 = null;
        try {
            String publicKey = BuildConfig.ORANGE_PUBLIC_KEY;
            byte[] rsaData = fields.getBytes(StandardCharsets.UTF_8);
            byte[] encodeData = RsaHelper.encryptByPublicKey(rsaData, publicKey);
            pubKeyBase64 = RsaHelper.encryptBASE64(encodeData);
            logDebug("TAGasa", "encodePubKey: " + pubKeyBase64 );
        } catch (Exception e) {
            e.printStackTrace();
        }
        // remove \n from pubKeyBase64
        if (pubKeyBase64 != null) pubKeyBase64 = pubKeyBase64.replace(System.getProperty("line.separator"), "");
        return pubKeyBase64;
    }

    public static String changeWalletType (Activity activity,String walletType){
        String walletName = "";
        if (walletType.equalsIgnoreCase("DEFAULT_WALLET")){
            walletName = activity.getString(R.string.main_wallet);
        } else if (walletType.equalsIgnoreCase("CREDIT_WALLET")) {
            walletName = activity.getString(R.string.credit_wallet);
        } else if (walletType.equalsIgnoreCase("CASH_BACK_WALLET")) {
            walletName = activity.getString(R.string.allowance_wallet);
        }
        return walletName;
    }

    public static void popupMessageAndFinish(String title, String message, Activity activity) {
        if (!activity.isFinishing()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(activity, R.style.DialogTheme));
            if (title != null) builder.setTitle(title);
            builder.setMessage(message);
            builder.setPositiveButton(String.format(Locale.US, "%s", activity.getString(R.string.close)), (dialog, which) -> activity.finish());
            builder.setCancelable(false);
            builder.show();
        }
    }

    public static void popUpcheckPermissonSetting(String title, final String message, Activity activity ) {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(activity,R.style.DialogTheme);
        if (title != null) builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(activity.getString(R.string.ok ), (dialog, which) -> {
            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            Uri uri = Uri.fromParts("package", activity.getPackageName(), null);
            intent.setData(uri);
            activity.startActivity(intent);
        });
        builder.setNegativeButton(activity.getString(R.string.cancel),null);
        builder.show();
    }

    public static String translateMessageFromSever(String message , JSONObject jsonObject) {
        Map<String, Object> hashMap = new Gson().fromJson(jsonObject.toString(),Map.class);
        for (Map.Entry<String, Object> entry : hashMap.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue().toString();
            if (message != null && message.contains(key)) {
                message = message.replaceAll(key,value);
            }
        }
        return message;
    }

    @SuppressLint("SetTextI18n")
    public static void checkUpdatePopUp(Activity mActivity) {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(mActivity, R.style.DialogTheme);
        View view = LayoutInflater.from(mActivity).inflate(R.layout.layout_updat_version, null);
        builder.setView(view);
        builder.setCancelable(false);
        TextView title = view.findViewById(R.id.titles);
        TextView text = view.findViewById(R.id.text);
        Button btnUpdateTo = view.findViewById(R.id.btn_update);
        Button btnNoThank = view.findViewById(R.id.no_thank);

        title.setText(mActivity.getString(R.string.app_name) + " " + mActivity.getString(R.string.update) + "?");
        text.setText(mActivity.getString(R.string.app_name) + " " + mActivity.getString(R.string.recommends_that_you_update));

        final Dialog dialog = builder.create();
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        btnNoThank.setOnClickListener(v -> dialog.cancel());
        btnUpdateTo.setOnClickListener(v -> {
            final String appPackageName = mActivity.getPackageName();
            try {
                mActivity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            } catch (android.content.ActivityNotFoundException anfe) {
                mActivity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
            }
        });
        dialog.show();
    }

    public static void saveStringBody(Context context , HashMap<String,String> list, String fileName){
        try {
            Gson gson = new Gson();
            String json = gson.toJson(list);

            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput(fileName+".txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(json);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    public static HashMap<String,String> getStringBody(Context context,String fileName){
        String ret = "";
        try {
            InputStream inputStream = context.openFileInput(fileName+".txt");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append("\n").append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }
        Gson gson = new Gson();
        Type type = new TypeToken<HashMap<String,String>>() {}.getType();
        return gson.fromJson(ret, type);
    }
}