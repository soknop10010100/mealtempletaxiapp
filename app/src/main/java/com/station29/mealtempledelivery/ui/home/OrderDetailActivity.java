package com.station29.mealtempledelivery.ui.home;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.api.Constant;
import com.station29.mealtempledelivery.api.request.OrderWs;
import com.station29.mealtempledelivery.data.model.DeliveryDetails;
import com.station29.mealtempledelivery.data.model.OrderItem;
import com.station29.mealtempledelivery.data.model.Tax;
import com.station29.mealtempledelivery.ui.base.BaseActivity;
import com.station29.mealtempledelivery.ui.MainActivity;
import com.station29.mealtempledelivery.ui.history.FeedBackBottomSheetFragment;
import com.station29.mealtempledelivery.utils.Util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class OrderDetailActivity extends BaseActivity implements View.OnClickListener , FeedBackBottomSheetFragment.OnClickFeedBack {

    private TextView subTv ,statusTv;
    private TextView tipTv;
    private TextView deliFeeTv;
    private TextView totalTv;
    private TextView shopName;
    private TextView shopContact;
    private TextView orderNumber, cooperateOderTv;
    private TextView orderTime, total,ratedTv;
    private TextView commentTv ,taxTv ,taxValueTv;
    private TextView deliveryTime, deliveryType, paymentType, discountTv, grandTotalTv, deliveryCommTv, deliveryFeeAfCom;
    private String service, status,back ,feedbackAction;
    private final HashMap<String, Object> reviewRatingData = new HashMap<>();
    private int deliveryId;
    private DeliveryDetails deliveryDetails;
    private View progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);

        subTv = findViewById(R.id.sub);
        tipTv = findViewById(R.id.tip);
        deliFeeTv = findViewById(R.id.deliFee);
        totalTv = findViewById(R.id.totalOrder);
        shopName = findViewById(R.id.shopName);
        shopContact = findViewById(R.id.storeContact);
        orderNumber = findViewById(R.id.order_num);
        orderTime = findViewById(R.id.orderTime);
        deliveryTime = findViewById(R.id.deliveryTime);
        deliveryType = findViewById(R.id.deliveryType);
        paymentType = findViewById(R.id.paymentType);
        total = findViewById(R.id.textTotal);
        commentTv = findViewById(R.id.order_comment);
        taxTv = findViewById(R.id.tax);
        taxValueTv = findViewById(R.id.taxValue);
        discountTv = findViewById(R.id.discount);
        grandTotalTv = findViewById(R.id.grand_to);
        deliveryCommTv = findViewById(R.id.delivery_comm);
        deliveryFeeAfCom = findViewById(R.id.delivery_com_after);
        cooperateOderTv = findViewById(R.id.order_num_cooperation);
        statusTv = findViewById(R.id.status);
        ratedTv = findViewById(R.id.text_rated);
        MaterialButton btnReview = findViewById(R.id.btnReview);
        LinearLayout linearLayoutRate = findViewById(R.id.linear_rate);
        progress = findViewById(R.id.progress);

        cooperateOderTv.setOnClickListener(this);

        if (getIntent().hasExtra("service_type")) {
            service = getIntent().getStringExtra("service_type");
        } else {
            service = getString(R.string.delivery);
        }

        if(getIntent().hasExtra("back")){
           back = getIntent().getStringExtra("back");
        }

        final RecyclerView recyclerView = findViewById(R.id.productListRecycleView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        deliveryId = (int) getIntent().getSerializableExtra("id");

        if (getIntent().hasExtra("id") && deliveryId > 0) {
            if (Constant.getBaseUrl() != null) {
                progress.setVisibility(View.VISIBLE);
                new OrderWs().viewMenu(deliveryId, this, new OrderWs.ViewMenu() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onLoadProductSuccess(List<OrderItem> product, DeliveryDetails model) {
                        progress.setVisibility(View.GONE);
                        deliveryDetails = model;
                        if (product.size() == 0) return;
                        recyclerView.setAdapter(new OrderAdapter(product, model));
                        findViewById(R.id.pb).setVisibility(View.GONE);
                        findViewById(R.id.linear_detail).setVisibility(View.VISIBLE);
                        findViewById(R.id.subBoarder).setVisibility(View.VISIBLE);
                        findViewById(R.id.deliFeeBoarder).setVisibility(View.VISIBLE);
                        findViewById(R.id.totalBoarder).setVisibility(View.VISIBLE);
                        findViewById(R.id.productListRecycleView).setVisibility(View.VISIBLE);
                        findViewById(R.id.summary).setVisibility(View.VISIBLE);
                        findViewById(R.id.sDetail).setVisibility(View.VISIBLE);
                        findViewById(R.id.linear_rate).setVisibility(View.GONE);
                        status = model.getStatus();
                        discountTv.setText(String.format(Locale.US, "%s %s", model.getCurrency(), Util.stringFormatPrice(model.getPriceDiscount())));
                        grandTotalTv.setText(String.format(Locale.US, "%s %s", model.getCurrency(), Util.stringFormatPrice(model.getProductPriceAfterDiscountVat())));
                        deliveryCommTv.setText(String.format(Locale.US, "%s %s", model.getCurrency(), Util.stringFormatPrice(model.getDeliveryCommissionPrice())));
                        deliveryFeeAfCom.setText(String.format(Locale.US, "%s %s", model.getCurrency(), Util.stringFormatPrice(model.getDeliveryFeeAfterCommissionRate())));
                        if (status.equalsIgnoreCase("DELIVERED")) {
                            linearLayoutRate.setVisibility(View.VISIBLE);
                            ratedTv.setVisibility(View.VISIBLE);
                            statusTv.setText(getString(R.string.leave_a_rate_and_review));
                        }
                        if (model.getTaxList().size() == 0) {
                            taxTv.setVisibility(View.GONE);
                            taxValueTv.setVisibility(View.GONE);
                        } else {
                            String tax = "";
                            String taxtotal = "";
                            Tax taxs = null;
                            for (int i = 0; i < model.getTaxList().size(); i++) {
                                taxs = model.getTaxList().get(i);
                                tax = tax + String.format("%s( %s %s%s ) %s", getString(R.string.tax), taxs.getNameTax(), taxs.getAmount(), "%", "\n");
                                taxtotal = taxtotal + String.format("%s %s%s", model.getCurrency(), Util.formatPrice(taxs.getTaxAmount()), "\n");
                            }
                            taxTv.setText(String.format(Locale.US, "%s", tax.trim()));
                            taxValueTv.setText(String.format(Locale.US, "%s", taxtotal.trim()));
                        }
                        shopName.setText(model.getStoreName());
                        shopContact.setText(model.getTel());
                        orderNumber.setText(model.getOrderNumber());
                        orderNumber.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                DisplayMetrics displayMetrics = new DisplayMetrics();
                                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                                int width = displayMetrics.widthPixels;
                                Toast toast = Toast.makeText(OrderDetailActivity.this, R.string.copied, Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.TOP, width / 3, 300);
                                toast.show();
                                Util.setClipboard(OrderDetailActivity.this, orderNumber.getText().toString());
                            }
                        });
                        orderTime.setText(Util.formatTimeZoneLocal(model.getOrderTime()));
                        deliveryTime.setText(Util.formatTimeZoneLocal(model.getDeliveryTime()));
                        subTv.setText(String.format(Locale.US, " %s %s", model.getCurrency(), Util.stringFormatPrice(model.getTotalProductPrice())));
                        deliveryType.setText(service);
                        String paymentMethod = "";
                        if (model.getPaymentType().equals("KESS_PAY")) {
                            paymentMethod = "Debit Card";
                        } else if (model.getPaymentType().equals("cash")) {
                            paymentMethod = getString(R.string.pay_by_cas);
                        } else if (model.getPaymentType().equals("wallet")) {
                            paymentMethod = getString(R.string.wallets);
                        } else {
                            paymentMethod = model.getPaymentType();
                        }
                        paymentType.setText(paymentMethod);
                        if (!model.getRiderTip().equals("0.00")) {
                            findViewById(R.id.tipBoarder).setVisibility(View.VISIBLE);
                            tipTv.setVisibility(View.VISIBLE);
                            tipTv.setText(String.format(Locale.US, " %s %s", model.getCurrency(), Util.stringFormatPrice(model.getRiderTip())));
                        }
                        if (!model.getDeliveryFee().equals("0.00")) {
                            deliFeeTv.setText(String.format(Locale.US, " %s %s", model.getCurrency(), Util.stringFormatPrice(model.getDeliveryFee())));
                        } else
                            deliFeeTv.setText(String.format(Locale.US, "%s %s %s", model.getCurrency(), Util.stringFormatPrice(model.getFreeDeliveryFee()), getString(R.string.pay_by_kiwigo)));
                        totalTv.setText(String.format(Locale.US, " %s %s", model.getCurrency(), Util.stringFormatPrice(model.getTotal())));
                        total.setText(String.format(Locale.US, " %s %s", model.getCurrency(), Util.stringFormatPrice(model.getTotal())));
                        if (model.getCooperationOrderId() != null) {
                            findViewById(R.id.linCooperation).setVisibility(View.VISIBLE);
                            cooperateOderTv.setText(model.getCooperationOrderId());
                        } else findViewById(R.id.linCooperation).setVisibility(View.GONE);

                        if (model.getCooperation() == 1 && product.size() > 0 && product.get(0).getComment() != null) {
                            findViewById(R.id.comment).setVisibility(View.VISIBLE);
                            commentTv.setText(product.get(0).getComment());
                        }

                        if (getIntent().hasExtra("open_rate")) {
                            if (status.equalsIgnoreCase("DELIVERED")) {
                                FeedBackBottomSheetFragment feedBackBottomSheetFragment = new FeedBackBottomSheetFragment().newInstance("customer");
                                feedBackBottomSheetFragment.show(getSupportFragmentManager(), "FeedBackBottomSheetFragment");
                            }
                        }

                        if (model.getRate().size() >= 2) {
                            btnReview.setVisibility(View.GONE);
                            linearLayoutRate.setVisibility(View.VISIBLE);
                            ratedTv.setText(getString(R.string.this_order_has_been_rated));
                            ratedTv.setVisibility(View.VISIBLE);
                            statusTv.setVisibility(View.GONE);
                        }

                        if (model.getRate().size() == 1) {
                            feedbackAction = "shop";
                        } else {
                            feedbackAction = "customer";
                        }

                    }

                    @Override
                    public void onError(String errorMs) {
                        progress.setVisibility(View.GONE);
                        Toast.makeText(getApplicationContext(), errorMs, Toast.LENGTH_LONG).show();
                    }
                });
            }
        }

        btnReview.setOnClickListener(view -> {
            if (status.equalsIgnoreCase("DELIVERED")){
                linearLayoutRate.setVisibility(View.VISIBLE);
                statusTv.setText(R.string.leave_a_rate_and_review);
                FeedBackBottomSheetFragment feedBackBottomSheetFragment = new FeedBackBottomSheetFragment().newInstance(feedbackAction);
                feedBackBottomSheetFragment.show(getSupportFragmentManager(),"FeedBackBottomSheetFragment");
            } else {
                Util.popupMessage("",getString(R.string.waiting_for_complete),OrderDetailActivity.this);
            }
        });

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(R.string.view_menus);
        }
    }

    private void reviewRating(final HashMap<String, Object> reviewData, String action) {
        new OrderWs().writeReview(reviewData, OrderDetailActivity.this, new OrderWs.OnCallBackRateReview() {
            @Override
            public void onLoadSuccess(String message) {
                if (action.equals("customer")) {
                    feedbackAction = "shop";
                    FeedBackBottomSheetFragment feedBackBottomSheetFragment = new FeedBackBottomSheetFragment().newInstance("shop");
                    feedBackBottomSheetFragment.show(getSupportFragmentManager(),"FeedBackBottomSheetFragment");
                }
                if (action.equalsIgnoreCase("shop")){
                    findViewById(R.id.btnReview).setVisibility(View.GONE);
                    findViewById(R.id.linear_rate).setVisibility(View.VISIBLE);
                    ratedTv.setText(getString(R.string.this_order_has_been_rated));
                    statusTv.setVisibility(View.GONE);
                }
                Toast.makeText(OrderDetailActivity.this, message, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLoadFail(String message) {
                Toast toast = Toast.makeText(OrderDetailActivity.this, message, Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
                toast.show();
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            if (status != null && status.equalsIgnoreCase(Constant.DELIVERED) && back.equals("back")){
                refresh();
            } else {
                finish();
            }

        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if (status != null && status.equalsIgnoreCase(Constant.DELIVERED) && back.equals("back")){
                refresh();
            }else {
                this.finish();
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void refresh() {
        Intent intent = new Intent(OrderDetailActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.order_num_cooperation) {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            int width = displayMetrics.widthPixels;
            Toast toast = Toast.makeText(this, R.string.copied, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.TOP, width / 3, 300);
            toast.show();

            Util.setClipboard(this, cooperateOderTv.getText().toString());
        }
    }

    @Override
    public void onClickSubmit(@Nullable String txtChoose, float position, @NotNull String action) {
        feedbackAction = action;
        reviewRatingData.put("order_id", deliveryDetails.getOrderId());
        reviewRatingData.put("rate",position);
        reviewRatingData.put("review",txtChoose);
        if (action.equals("customer")){
            reviewRatingData.put("status", "driver_rate_customer");
        } else if (action.equals("shop")){
            reviewRatingData.put("status", "driver_rate_merchant");
        }
        reviewRating(reviewRatingData,action);
    }
}
