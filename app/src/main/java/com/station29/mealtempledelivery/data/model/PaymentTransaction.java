package com.station29.mealtempledelivery.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PaymentTransaction implements Serializable {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("ref_number")
    @Expose
    private String refNumber;
    @SerializedName("payment_id")
    @Expose
    private Object paymentId;
    @SerializedName("user_id")
    @Expose
    private int userId;
    @SerializedName("wallet_id")
    @Expose
    private int walletId;
    @SerializedName("ref_user_id")
    @Expose
    private int refUserId;
    @SerializedName("ref_wallet_id")
    @Expose
    private int refWalletId;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("txn_id")
    @Expose
    private String txnId;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("currency_code")
    @Expose
    private String currencyCode;
    @SerializedName("country_code")
    @Expose
    private Object countryCode;
    @SerializedName("fee")
    @Expose
    private String fee;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("created_by")
    @Expose
    private int createdBy;
    @SerializedName("updated_by")
    @Expose
    private int updatedBy;
    @SerializedName("deleted_by")
    @Expose
    private Object deletedBy;
    @SerializedName("user_name")
    @Expose
    private String userName;
    @SerializedName("user_account_number")
    @Expose
    private String userAccountNumber;
    @SerializedName("order_number")
    @Expose
    private String orderNumber;
    @SerializedName("order_ref_number")
    @Expose
    private String orderRefNumber;
    @SerializedName("psp_name")
    @Expose
    private String pspName;
    @SerializedName("description_translate")
    @Expose
    private String descriptionTranslate;
    @SerializedName("ref_user_name")
    @Expose
    private String refUserName;
    @SerializedName("ref_uer_account_number")
    @Expose
    private String refUerAccountNumber;
    @SerializedName("reward_amount")
    @Expose
    private String rewardAmount;

    public String getRewardAmount() {
        return rewardAmount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRefNumber() {
        return refNumber;
    }

    public void setRefNumber(String refNumber) {
        this.refNumber = refNumber;
    }

    public Object getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(Object paymentId) {
        this.paymentId = paymentId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getWalletId() {
        return walletId;
    }

    public void setWalletId(int walletId) {
        this.walletId = walletId;
    }

    public int getRefUserId() {
        return refUserId;
    }

    public void setRefUserId(int refUserId) {
        this.refUserId = refUserId;
    }

    public int getRefWalletId() {
        return refWalletId;
    }

    public void setRefWalletId(int refWalletId) {
        this.refWalletId = refWalletId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTxnId() {
        return txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public Object getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(Object countryCode) {
        this.countryCode = countryCode;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    public int getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(int updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Object getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(Object deletedBy) {
        this.deletedBy = deletedBy;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserAccountNumber() {
        return userAccountNumber;
    }

    public void setUserAccountNumber(String userAccountNumber) {
        this.userAccountNumber = userAccountNumber;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getOrderRefNumber() {
        return orderRefNumber;
    }

    public void setOrderRefNumber(String orderRefNumber) {
        this.orderRefNumber = orderRefNumber;
    }

    public String getPspName() {
        return pspName;
    }

    public void setPspName(String pspName) {
        this.pspName = pspName;
    }

    public String getDescriptionTranslate() {
        return descriptionTranslate;
    }

    public void setDescriptionTranslate(String descriptionTranslate) {
        this.descriptionTranslate = descriptionTranslate;
    }

    public String getRefUserName() {
        return refUserName;
    }

    public void setRefUserName(String refUserName) {
        this.refUserName = refUserName;
    }

    public String getRefUerAccountNumber() {
        return refUerAccountNumber;
    }

    public void setRefUerAccountNumber(String refUerAccountNumber) {
        this.refUerAccountNumber = refUerAccountNumber;
    }
}


