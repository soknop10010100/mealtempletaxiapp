package com.station29.mealtempledelivery.api.request;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.station29.mealtempledelivery.api.CustomCallback;
import com.station29.mealtempledelivery.api.CustomResponseListener;
import com.station29.mealtempledelivery.api.RetrofitGenerator;
import com.station29.mealtempledelivery.api.ServiceAPI;
import com.station29.mealtempledelivery.data.model.User;
import com.station29.mealtempledelivery.utils.Util;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;

import static com.station29.mealtempledelivery.utils.Util.logDebug;

public class SignUpWs {

    public void accountSignUp(Context context,HashMap<String, Object> signUpParam, final RequestServer requestServer) {
        MultipartBody.Builder requestBody = new MultipartBody.Builder();
        requestBody.setType(MultipartBody.FORM);
        for (String key : signUpParam.keySet()){
            if (!"driver_photo".equals(key) && !"licence_photo_front".equals(key)&&!"licence_photo_back".equals(key)&&!"vehicle_photo".equals(key)) {
                requestBody.addFormDataPart(key, String.valueOf(signUpParam.get(key)));
            } else{
                String path = String.valueOf(signUpParam.get(key));
                // Compress file before upload
                File fNew = null;
                try {
                    if(path != null && !path.equals(""))
                        fNew = new Compressor(context).compressToFile(new File(path));
                    logDebug("filesize",fNew.length()+" "+fNew.getTotalSpace()+"s");
                } catch (IOException e) {
                    e.printStackTrace();
                    logDebug(e.getClass().getName(), e.getMessage()+"");
                }

                if (fNew == null) fNew = new File(path);
                RequestBody bodyBye = RequestBody.create(MediaType.parse("multipart/form-data"), Util.readBytesFromPath(fNew.getPath()));
                requestBody.addFormDataPart(key, Util.getFileName(fNew.getPath()), bodyBye );
            }
        }
        String map = new Gson().toJson(signUpParam);
        logDebug("maqwep", map);

        ServiceAPI service = RetrofitGenerator.createService();
        final Call<JsonElement> res = service.signUp(requestBody.build());
        res.enqueue(new CustomCallback(context,new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                requestServer.onRequestSuccess();
            }

            @Override
            public void onError(String error , int code) {
                requestServer.onRequestFailed(error);
            }
        }));

    }

    public void verifyAccountCode(Context context,HashMap<String, Object> account, final RequestServer requestServer) {
        ServiceAPI serviceAPI = RetrofitGenerator.createService();
        final Call<JsonElement> res = serviceAPI.verifyAccount(account);
        res.enqueue(new CustomCallback(context,new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                    if (resObj.getAsJsonObject().has("data")) {
                        String token= "";
                        int id = 0;
                        JsonObject dataObj = resObj.getAsJsonObject().getAsJsonObject("data");
                        if (dataObj.has("access_token")) {
                            token = dataObj.get("access_token").getAsString();
                        }
                        if (dataObj.has("user")) {
                            JsonObject userObj = dataObj.getAsJsonObject("user");
                            if (userObj.has("id")) {
                                id = userObj.get("id").getAsInt();
                            }
                            User user = new Gson().fromJson(userObj, User.class);
                            requestServer.onRequestToken(token,id,user);
                        }
                    }
            }

            @Override
            public void onError(String error, int code) {
                requestServer.onRequestFailed(error);
            }
        }));


    }

    public void forgetAccountPassword(Context context, HashMap<String, Object> account, final RequestServer requestServer) {
        ServiceAPI serviceAPI = RetrofitGenerator.createService();
        final Call<JsonElement> res = serviceAPI.forgetPassword(account);
        res.enqueue(new CustomCallback(context,new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                requestServer.onRequestSuccess();
            }

            @Override
            public void onError(String error , int code) {
                requestServer.onRequestFailed(error);
            }
        }));
//
    }

    public interface RequestServer {
        void onRequestSuccess();
        void onRequestToken(String token, int userId, User user);
        void onRequestFailed(String message);
    }
}
