package com.station29.mealtempledelivery.ui.wallet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonElement;
import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.api.Constant;
import com.station29.mealtempledelivery.api.request.PaymentWs;
import com.station29.mealtempledelivery.api.request.ProfileWs;
import com.station29.mealtempledelivery.data.model.PaymentTransaction;
import com.station29.mealtempledelivery.data.model.User;
import com.station29.mealtempledelivery.data.model.Wallets;
import com.station29.mealtempledelivery.ui.base.BaseActivity;
import com.station29.mealtempledelivery.ui.wallet.adapter.WalletAdapter;
import com.station29.mealtempledelivery.ui.wallet.adapter.WalletHistoryAdapter;
import com.station29.mealtempledelivery.utils.Util;
import com.yarolegovich.discretescrollview.DSVOrientation;
import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.InfiniteScrollAdapter;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;

public class WalletActivity extends BaseActivity implements DiscreteScrollView.OnItemChangedListener<WalletAdapter.ViewHolder>, ExportBottomSheetFragment.OnClickButton,CustomDateBottomSheetFragment.GetDate{

    private RecyclerView recyclerView;
    public WalletHistoryAdapter transactionAdapter;
    private List<PaymentTransaction> paymentTransactions = new ArrayList<>();
    private int currentItem, total , scrollDown, currentPage = 1 ,size = 10;
    private ProgressBar progressBar;
    private SwipeRefreshLayout refreshLayout;
    private LinearLayoutManager linearLayoutManager;
    private boolean isScrolling;
    private TextView txtBalance, toolBarTitle ,txt_wallet_type;
    private ImageView backButton,txtNoRecord,imgExport;
    private DiscreteScrollView scrollView;
    private InfiniteScrollAdapter<?> infiniteAdapter;
    private int walletId , walletShowPosition = 0;
    private User user;
    private Call<JsonElement> cancelService; // for cancel service when move to see other wallet that the old service not done yet.
    private ExportBottomSheetFragment exportTransactionsDialogFragment = new ExportBottomSheetFragment().newInstance();
    private String dateFrom="",dateTo="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);

        initView();
        initAction();
    }

    private void initView(){
        recyclerView = findViewById(R.id.list_payment_transaction);
        progressBar = findViewById(R.id.loadMoreProgressBar);
        refreshLayout = findViewById(R.id.swipe_layout);
        txtNoRecord = findViewById(R.id.noRecord);
        txtBalance = findViewById(R.id.acc_balance);
        toolBarTitle = findViewById(R.id.toolBarTitle);
        backButton = findViewById(R.id.backButton);
        txt_wallet_type = findViewById(R.id.txtWallet_type);
        scrollView = findViewById(R.id.picker);
        imgExport = findViewById(R.id.imgExport);
    }

    private void initAction(){

        toolBarTitle.setText(getString(R.string.wallet));

        linearLayoutManager = new LinearLayoutManager(WalletActivity.this);
        recyclerView.setLayoutManager(linearLayoutManager);
        getUserWallet();


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL){
                    isScrolling = true;
                }
            }
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItem = linearLayoutManager.getChildCount();
                total = linearLayoutManager.getItemCount();
                scrollDown = linearLayoutManager.findFirstVisibleItemPosition();
                if(dy > 0) {
                    if (isScrolling && (currentItem + scrollDown == total)) {
                        if(total == size) {
                            isScrolling = false;
                            currentPage++;
                            progressBar.setVisibility(View.VISIBLE);
                            recyclerView.scrollToPosition(paymentTransactions.size() - 1);
                            loadListPaymentTransaction(recyclerView,walletId);
                            size+=10;
                        }
                    }
                }
            }
        });

        refreshLayout.setColorSchemeResources(R.color.colorPrimary);

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                currentPage = 1;
                size = 10;
                transactionAdapter.clear();
                paymentTransactions = new ArrayList<>();
                getUserWallet();
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        imgExport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(WalletActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                        && ContextCompat.checkSelfPermission(WalletActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE}, 123);
                    }
                }else {
                    imgExport.setEnabled(false);
                    imgExport.postDelayed(() -> imgExport.setEnabled(true), 300);
                    exportTransactionsDialogFragment.show(getSupportFragmentManager(),"ExportTransactionsDialogFragment");
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 123) {
            if (grantResults.length != 0){
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    exportTransactionsDialogFragment.show(getSupportFragmentManager(),"ExportTransactionsDialogFragment");
                } else {
                    Util.popUpcheckPermissonSetting("",getString(R.string.permission_denied),WalletActivity.this);
                }
            } else {
                Toast.makeText(WalletActivity.this,getString(R.string.permission_denied),Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void loadListPaymentTransaction(RecyclerView recyclerView , int walletId) {
        new PaymentWs().getUserPaymentTransaction(WalletActivity.this,walletId,currentPage,paymentTransaction);
        transactionAdapter = new WalletHistoryAdapter(paymentTransactions,itemClickListener,WalletActivity.this);
        recyclerView.setAdapter(transactionAdapter);
    }

    private final PaymentWs.onGetPaymentTransaction paymentTransaction = new PaymentWs.onGetPaymentTransaction() {
        @Override
        public void onCancelService(Call<JsonElement> res) {
            cancelService = res;
        }

        @Override
        public void onSuccess(List<PaymentTransaction> paymentTransaction) {
            refreshLayout.setEnabled(true);
            if (!paymentTransaction.isEmpty()){
                if (paymentTransaction.get(0).getWalletId() == walletId){
                    if (!paymentTransactions.isEmpty() && !paymentTransaction.isEmpty()){
                        if (paymentTransaction.get(0).getId() != paymentTransactions.get(0).getId()){
                            paymentTransactions.addAll(paymentTransaction);
                        }
                    } else {
                        paymentTransactions.addAll(paymentTransaction);
                    }
                }
            }
            if(paymentTransactions.isEmpty()) {
                txtNoRecord.setVisibility(View.VISIBLE);
                imgExport.setVisibility(View.GONE);
            }
            else {
                txtNoRecord.setVisibility(View.GONE);
                imgExport.setVisibility(View.VISIBLE);
            }
            progressBar.setVisibility(View.GONE);
            refreshLayout.setRefreshing(false);
            transactionAdapter.notifyDataSetChanged();
        }

        @Override
        public void onError(String message) {
            refreshLayout.setEnabled(true);
            Util.popupMessage(null,message,WalletActivity.this);
        }
    };

    private void getUserWallet(){
        progressBar.setVisibility(View.VISIBLE);
        new ProfileWs().viewProfiles(WalletActivity.this, new ProfileWs.ProfileViewAndUpdate() {
            @Override
            public void onLoadProfileSuccess(User user1 , String message) {
                user = user1;
                progressBar.setVisibility(View.GONE);

                scrollView.setOrientation(DSVOrientation.HORIZONTAL);
                scrollView.addOnItemChangedListener(WalletActivity.this);
                infiniteAdapter = InfiniteScrollAdapter.wrap(new WalletAdapter(WalletActivity.this,user.getWalletsList(),user));
                scrollView.setAdapter(infiniteAdapter);
                scrollView.setItemTransitionTimeMillis(150);
                scrollView.setItemTransformer(new ScaleTransformer.Builder().setMinScale(0.8f).build());
                if (user.getWalletsList().size() == 2){ // check problem pull refresh when scroll to position wrong position
                    int position = 0;
                    if (walletShowPosition == 0){
                        position = 1;
                    }
                    onItemChanged(user.getWalletsList().get(position));
                    if (refreshLayout.isRefreshing()){
                        scrollView.scrollToPosition(position);
                    }
                } else {
                    onItemChanged(user.getWalletsList().get(walletShowPosition));
                    if (refreshLayout.isRefreshing()){
                        scrollView.scrollToPosition(walletShowPosition);
                    }
                }
            }

            @Override
            public void onError(String errorMessage) {
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onUpdateProfilesSuccess(String message) {

            }

        });
    }

    @SuppressLint("SetTextI18n")
    private void onItemChanged(Wallets wallets) {
        if (cancelService != null){
            cancelService.cancel();
        }

        refreshLayout.setEnabled(false);
        txt_wallet_type.setText( getString(R.string.transaction));
        walletId = wallets.getWalletId();
        paymentTransactions = new ArrayList<>();
        currentPage = 1;
        size = 10;
        if (transactionAdapter != null){
            transactionAdapter.clear();
            progressBar.setVisibility(View.VISIBLE);
        }
        loadListPaymentTransaction(recyclerView,wallets.getWalletId());
    }

    private final WalletHistoryAdapter.ItemClickListener itemClickListener = this::popUpDetailWallet;

    private void popUpDetailWallet(PaymentTransaction paymentTransaction){
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(WalletActivity.this,R.style.DialogTheme);
        View view = LayoutInflater.from(this).inflate(R.layout.layout_dialog_detail_wallets, null);
        builder.setView(view);
        TextView tnxIdTv = view.findViewById(R.id.id_tnx);
        TextView dateTv = view.findViewById(R.id.date_tan);
        TextView amountDetailTv = view.findViewById(R.id.amount_o);
        TextView toAccountTv = view.findViewById(R.id.to_acc);
        TextView fromAccountTv = view.findViewById(R.id.from_acc);
        TextView typeTv = view.findViewById(R.id.type);
        LinearLayout shareTo = view.findViewById(R.id.share);
        TextView descriptionTv = view.findViewById(R.id.description);
        final Dialog dialog = builder.create();
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

        String orderRefNumber = paymentTransaction.getOrderRefNumber();
        String userRef = paymentTransaction.getRefUserName();
        String orderNumber = paymentTransaction.getOrderNumber();
        String amount = paymentTransaction.getAmount();
        String currency = paymentTransaction.getCurrencyCode();
        String spsName = paymentTransaction.getPspName();
        String rewardAmount = paymentTransaction.getRewardAmount();

        if(Constant.serviceLanguage.containsKey(paymentTransaction.getDescriptionTranslate())){
            String tran = Constant.serviceLanguage.get(paymentTransaction.getDescriptionTranslate());
            String text = null;
            if(tran!=null){
                if(spsName!=null){
                    if(orderRefNumber!=null && userRef!=null && amount != null && currency!=null){
                        text =  tran.replace("psp_name",spsName).replace("order_ref_number","#"+orderRefNumber).replace("ref_user_name",userRef).replace("amount_",amount).replace("currency_code",currency);
                    }else {
                        text =  tran.replace("psp_name",spsName);
                    }

                }else if(userRef != null){
                    if(orderRefNumber!=null&&orderNumber!=null&&amount!=null&&currency!=null){
                        text =  tran.replace("ref_user_name",userRef).replace("order_ref_number","#"+orderRefNumber).replace("order_number",orderNumber).replace("amount_",amount).replace("currency_code",currency);
                    }  else if(orderNumber!=null&& amount!= null && currency != null){
                        text =  tran.replace("ref_user_name",userRef).replace("order_number",orderNumber).replace("amount_",amount).replace("currency_code",currency);
                    }
                    else if(amount!=null&&currency!=null){
                        text =  tran.replace("ref_user_name",userRef).replace("amount_",amount).replace("currency_code",currency);
                        if (rewardAmount != null && text.contains("reward_amount")){
                            text = tran.replace("reward_amount", rewardAmount).replace("ref_user_name", userRef).replace("amount_", amount).replace("currency_code", currency);
                        }
                    } else  {
                        text =  tran.replace("ref_user_name",userRef);
                    }
                }else if(orderRefNumber != null){
                    if(orderNumber!=null&&amount!=null&&currency!=null){
                        text =  tran.replace("order_ref_number","#"+orderRefNumber).replace("order_number",orderNumber).replace("amount_",amount).replace("currency_code",currency);
                    }else if(amount!=null&&currency!=null){
                        text =  tran.replace("order_ref_number","#"+orderRefNumber).replace("amount_",amount).replace("currency_code",currency);
                    }else {
                        text =  tran.replace("order_ref_number","#"+orderRefNumber);
                    }
                }
                else {
                    text = tran;
                }
            }
            descriptionTv.setText(String.format(Locale.US,"%s %s"," : ",text));
        }else {
            descriptionTv.setText(String.format(Locale.US,"%s %s"," : ",paymentTransaction.getDescription()));
        }
        String type = "";
        if(paymentTransaction.getType() != null && paymentTransaction.getType().equals("TOP_UP") ||paymentTransaction.getType() != null && paymentTransaction.getType().equals("IN")) {
            type = "+";
            amountDetailTv.setTextColor(Color.parseColor("#00c853"));
        }else if (paymentTransaction.getType() != null && paymentTransaction.getType().equals("OUT") || paymentTransaction.getType().equals("CASH_OUT")) {
            type = "-";
            amountDetailTv.setTextColor(Color.parseColor("#af1022"));
        }

        if(paymentTransaction.getAmount() != null){
            try {
                amountDetailTv.setText(String.format("%s %s %s",type,paymentTransaction.getCurrencyCode(), paymentTransaction.getAmount()));
            } catch (NumberFormatException ignored) {}
        }
        else amountDetailTv.setText("");

        tnxIdTv.setText(paymentTransaction.getRefNumber());
        dateTv.setText(Util.formatDateUptoCurrentRegion(paymentTransaction.getCreatedAt()));
        if("IN".equals(paymentTransaction.getType())){
            toAccountTv.setText(paymentTransaction.getUserName());
            fromAccountTv.setText(paymentTransaction.getRefUserName());
        } else {
            toAccountTv.setText(paymentTransaction.getRefUserName());
            fromAccountTv.setText(paymentTransaction.getUserName());
        }

        typeTv.setText(paymentTransaction.getType());

        shareTo.setOnClickListener(v -> {
            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra(Intent.EXTRA_TEXT, "This is the text that will be shared.");
            startActivity(Intent.createChooser(sharingIntent,"Share using"));
        });

        dialog.show();
    }

    @Override
    public void onCurrentItemChanged(@Nullable WalletAdapter.ViewHolder viewHolder, int adapterPosition) {
        walletShowPosition = infiniteAdapter.getRealPosition(adapterPosition);
        int positionInDataSet = infiniteAdapter.getRealPosition(adapterPosition);
        onItemChanged(user.getWalletsList().get(positionInDataSet));
    }

    @Override
    public void onClickExport(String startDate, String endDate, String action) {
        progressBar.setVisibility(View.VISIBLE);
        if(action.equals("custom")){
            exportTransaction(dateFrom,dateTo);
        }
        else {
            exportTransaction(startDate,endDate);
        }
    }

    @Override
    public void onClickCustom(TextView customDate , Button btnExport) {

        btnExport.setEnabled(false);
        btnExport.postDelayed(() -> btnExport.setEnabled(true), 200);

        customDate.setEnabled(false);
        customDate.postDelayed(() -> customDate.setEnabled(true), 100);

        CustomDateBottomSheetFragment customDateBottomSheetFragment = CustomDateBottomSheetFragment.newInstance();
        customDateBottomSheetFragment.show(getSupportFragmentManager(),"Open_custom_date");
    }

    @Override
    public void getDateFromAndTo(String from, String to) {
        if(from.equals("") && to.equals("")){
            exportTransactionsDialogFragment.dismiss();
            exportTransactionsDialogFragment.show(getSupportFragmentManager(),"ExportTransactionsDialogFragment");
        } else {
            dateFrom = from;
            dateTo = to;
        }
    }

    public void exportTransaction(String startDate,String endDate){
        progressBar.setVisibility(View.VISIBLE);
        imgExport.setEnabled(false);
        new PaymentWs().exportTransaction(WalletActivity.this, walletId, startDate, endDate, new PaymentWs.OnCallBackExport() {
            @Override
            public void onSuccess(byte[] file, File filePdf) {
                progressBar.setVisibility(View.GONE);
                imgExport.setEnabled(true);
                if (filePdf.exists()) {
                    Uri uri = FileProvider.getUriForFile(WalletActivity.this, WalletActivity.this.getPackageName()+".provider",filePdf);
                    String stringUri = uri.toString();
                    Intent target = new Intent(Intent.ACTION_VIEW);
                    target.setDataAndType(Uri.parse(stringUri),"application/pdf");
                    target.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                    try {
                        startActivity(target);
                    } catch (ActivityNotFoundException e) {
                        Toast.makeText(WalletActivity.this,  "Something went wrong", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(WalletActivity.this, "The file not exists! ", Toast.LENGTH_SHORT).show();
                    imgExport.setEnabled(true);
                }

            }

            @Override
            public void onFailed(String message) {
                progressBar.setVisibility(View.GONE);
                imgExport.setEnabled(true);
                Util.popupMessage("",message, WalletActivity.this);
            }
        });
    }
}