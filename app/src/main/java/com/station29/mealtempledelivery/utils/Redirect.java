package com.station29.mealtempledelivery.utils;

import android.app.Activity;
import android.widget.Toast;


import androidx.fragment.app.Fragment;

import com.station29.mealtempledelivery.api.Constant;
import com.station29.mealtempledelivery.data.model.PaymentService;
import com.station29.mealtempledelivery.data.model.Wallets;
import com.station29.mealtempledelivery.ui.setting.payment.OrangePayAlertDialog;
import com.station29.mealtempledelivery.ui.setting.payment.VisaMasterCardDialogFragment;

import java.util.HashMap;
import java.util.List;

public class Redirect {

    public static void openPaymentServiceScreen(Fragment fragment,Activity activity , PaymentService paymentService , List<Wallets> walletsList , String screenType, OnCallback onCallback){

        if (paymentService.getType() != null){
            if(paymentService.getType().equalsIgnoreCase("MOBILE_WALLET")
                    || paymentService.getType().equalsIgnoreCase("ORANGE_PAY")
                    || paymentService.getType().equalsIgnoreCase("AIRTEL")) {
                OrangePayAlertDialog orangePayAlertDialog = OrangePayAlertDialog.newInstance(activity, activity, paymentService, walletsList);
                orangePayAlertDialog.show(fragment.getChildFragmentManager(), "OrangePayAlertDialog");
            }  else if (paymentService.getType().equalsIgnoreCase("CARD") && screenType.equalsIgnoreCase(Constant.DEPOSIT) ) {
                VisaMasterCardDialogFragment visaMasterCardDialogFragment = new VisaMasterCardDialogFragment().newInstance("top_up",paymentService,walletsList.get(0).getWalletId());
                visaMasterCardDialogFragment.show(fragment.getChildFragmentManager(),"visaMasterCardDialogFragment");
            } else {
                Toast.makeText(activity,paymentService.getPspName() + " is coming soon!",Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(activity,paymentService.getPspName() + " is coming soon!",Toast.LENGTH_SHORT).show();
        }
    }

    public interface OnCallback {
        void onStart(HashMap<String , Object> param);
        void onFinish();
    }
}
