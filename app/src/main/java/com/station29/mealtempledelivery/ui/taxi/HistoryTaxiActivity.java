package com.station29.mealtempledelivery.ui.taxi;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.material.button.MaterialButton;
import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.api.Constant;
import com.station29.mealtempledelivery.api.request.DeliveryWs;
import com.station29.mealtempledelivery.api.request.OrderWs;
import com.station29.mealtempledelivery.data.model.DeliveryDetails;
import com.station29.mealtempledelivery.data.model.TaxiDeliveryDetail;
import com.station29.mealtempledelivery.data.model.Waypoint;
import com.station29.mealtempledelivery.ui.base.BaseActivity;
import com.station29.mealtempledelivery.ui.MainActivity;
import com.station29.mealtempledelivery.ui.history.FeedBackBottomSheetFragment;
import com.station29.mealtempledelivery.utils.Util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.Locale;

import static com.station29.mealtempledelivery.utils.Util.isConnectedToInternet;
import static com.station29.mealtempledelivery.utils.Util.popupMessage;

public class HistoryTaxiActivity extends BaseActivity implements FeedBackBottomSheetFragment.OnClickFeedBack {

    private TaxiDeliveryDetail deliveryDetails;
    private int deliveryId;
    private Waypoint waypoint;
    private String status,back ,typeOrder;
    private final HashMap<String, Object> reviewRatingData = new HashMap<>();
    private TextView statusTv,textRated;
    private LinearLayout linearLayoutRated;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_taxi);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(R.string.view_menu);
        }

        final TextView addPickup = findViewById(R.id.customerAddBs);
        final TextView addDropOff = findViewById(R.id.addDropOff);
        final TextView totalTv = findViewById(R.id.totalPriceTa);
        final TextView durationTv = findViewById(R.id.priceDu);
        final TextView distanceTv = findViewById(R.id.priceD);
        final TextView priceTv = findViewById(R.id.priceP);
        final TextView phone = findViewById(R.id.phoneCos);
        final TextView name = findViewById(R.id.customerNam);
        final TextView date = findViewById(R.id.dateTime);
        final TextView priceAfterDisTv = findViewById(R.id.price_after_dis);
        final RelativeLayout cate = findViewById(R.id.cat);
        final RelativeLayout weight = findViewById(R.id.weig);
        final TextView weightTv = findViewById(R.id.weightTv);
        final TextView  cateTv = findViewById(R.id.cateTv);
        final TextView senderNameTv = findViewById(R.id.senderName);
        final TextView senderPhoneTv = findViewById(R.id.senderPhone);
        final RelativeLayout senderPhoneTxt = findViewById(R.id.senderPhoneTxt);
        final RelativeLayout senderNameTxt = findViewById(R.id.senderNameTxt);
        final LinearLayout rule = findViewById(R.id.ruler);
        final TextView subTotal = findViewById(R.id.subTotal);
        final TextView comRateTxt = findViewById(R.id.comRate);
        final TextView comRate = findViewById(R.id.comRatePrice);
        final TextView serviceType = findViewById(R.id.serviceType);
        final TextView paymentType = findViewById(R.id.paymentType);
        final TextView priceAfterCommissionRat = findViewById(R.id.toPriceAfterCoupon);
        final TextView couponTypeTv = findViewById(R.id.dis_Coupon);
        final  RelativeLayout relativeDes = findViewById(R.id.description_re);
        final TextView descriptionTv = findViewById(R.id.descriptionTv);
        final TextView orderNumberTv = findViewById(R.id.order_number);
        final TextView paymentTypeTv = findViewById(R.id.paymentTypeTxt);
        statusTv = findViewById(R.id.status);
        MaterialButton btnReview = findViewById(R.id.btnReview);
        linearLayoutRated = findViewById(R.id.linear_rated);
        textRated = findViewById(R.id.rated_text);

        final TextView dis = findViewById(R.id.dis);
        if (getIntent() != null && getIntent().hasExtra("delivery")) {
            deliveryId = getIntent().getIntExtra("delivery", -1);
            if (getIntent().hasExtra("typeOrder")) {
                typeOrder = (String) getIntent().getSerializableExtra("typeOrder");
            }
            if (getIntent() != null && getIntent().hasExtra("waypoint")) {
                waypoint = (Waypoint) getIntent().getSerializableExtra("waypoint");
            }
            if(getIntent().hasExtra("back")){
                back = getIntent().getStringExtra("back");
            }
            if (Constant.getBaseUrl() != null)
                new DeliveryWs().readDelivery(deliveryId, typeOrder, this, new DeliveryWs.ReadDeliveryListener() {
                    @Override
                    public void onLoadDeliverySuccess(DeliveryDetails details) {

                    }

                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onLoadTaxiDetailSuccess(TaxiDeliveryDetail deliveryDetail) {
                        findViewById(R.id.li).setVisibility(View.VISIBLE);
                        findViewById(R.id.pb).setVisibility(View.GONE);
                        deliveryDetails = deliveryDetail;
                        waypoint = deliveryDetail.getWaypoints();
                        status = deliveryDetails.getStatus();
                        statusTv.setText(deliveryDetail.getStatus());
                        if (typeOrder.equals("send")) {
                            rule.setVisibility(View.VISIBLE);
                            cate.setVisibility(View.VISIBLE);
                            weight.setVisibility(View.VISIBLE);
                            senderNameTxt.setVisibility(View.VISIBLE);
                            senderPhoneTxt.setVisibility(View.VISIBLE);
                            weightTv.setText(String.format(Locale.US, "%s", deliveryDetails.getSendPackage().getWeight() + " kg"));
                            cateTv.setText(String.format(Locale.US, "%s", deliveryDetails.getSendPackage().getTypePackage()));
                            senderNameTv.setText(String.format(Locale.US, "%s", deliveryDetails.getSendPackage().getNameSender()));
                            senderPhoneTv.setText(String.format(Locale.US, "%s", deliveryDetails.getSendPackage().getPhoneSender()));
                            if (deliveryDetail.getSendPackage().getPaymentType() != null) {
                                paymentTypeTv.setText(String.format("%s", getString(R.string.payment_methods)));
                                paymentType.setText(String.format("%s", deliveryDetail.getSendPackage().getPaymentType()));
                            } else {
                                paymentType.setText(R.string.pay_by_cash);
                            }

                            serviceType.setText(String.format("%s", getString(R.string.send_box)));

                            if (deliveryDetails.getSendPackage().getAddressDetailSender() != null && !deliveryDetails.getSendPackage().getAddressDetailSender().isEmpty()) {
                                addPickup.setText(deliveryDetails.getSendPackage().getAddressDetailSender() + ", " + Util.getLoc(deliveryDetails.getPickUpLat(), deliveryDetails.getPickUpLong(), HistoryTaxiActivity.this));
                            } else {
                                addPickup.setText(Util.getLoc(deliveryDetails.getPickUpLat(), deliveryDetails.getPickUpLong(), HistoryTaxiActivity.this));
                            }
                            if (deliveryDetails.getSendPackage().getAddressDetailReceiver() != null && !deliveryDetails.getSendPackage().getAddressDetailReceiver().isEmpty()) {
                                addDropOff.setText(deliveryDetails.getSendPackage().getAddressDetailReceiver() + ", " + deliveryDetails.getSendPackage().getDropoffAddress());
                            } else {
                                addDropOff.setText(deliveryDetails.getSendPackage().getDropoffAddress());
                            }
                            if (deliveryDetails.getSendPackage().getPackageDetail() != null) {
                                relativeDes.setVisibility(View.VISIBLE);
                                descriptionTv.setText(deliveryDetails.getSendPackage().getPackageDetail());
                            } else {
                                descriptionTv.setText("");
                            }

                        } else {

                            if (deliveryDetail.getPaymentType() != null) {
                                paymentType.setText(String.format("%s", deliveryDetail.getPaymentType()));
                            } else {
                                paymentType.setText(R.string.pay_by_cash);
                            }
                            serviceType.setText(String.format("%s", getString(R.string.taxi)));
                            addPickup.setText(Util.getLoc(deliveryDetails.getPickUpLat(), deliveryDetails.getPickUpLong(), HistoryTaxiActivity.this));

                            if (deliveryDetail.getShippingLat() != 0.0 && deliveryDetail.getShippingLong() != 0.0) {
                                addDropOff.setText(Util.getLoc(deliveryDetails.getShippingLat(), deliveryDetails.getShippingLong(), HistoryTaxiActivity.this));

                            } else {
                                addDropOff.setText(R.string.no_destination);
                            }

                        }
                        if (waypoint != null) {
                            if (waypoint.getDuration() / 60 >= 60) {
                                durationTv.setText(String.format(" %s", (Util.formatDuration((int) (deliveryDetails.getWaypoints().getDuration() / 60) / 60)) + Util.formatTime(((int) Math.ceil(deliveryDetails.getWaypoints().getDuration() / 60) % 60))));
                            } else {
                                double duration = waypoint.getDuration() / 60;
                                double durations = Math.ceil(duration);
                                durationTv.setText(Util.formatTime((int) durations));
                            }
                        } else {
                            addPickup.setText(String.format(Locale.US, "%s", deliveryDetails.getPickupWayPoint().getStartAddress()));
                            durationTv.setText(String.format(Locale.US, "%s ", deliveryDetails.getDuration() + " min"));
                        }
                        orderNumberTv.setText(String.format(Locale.US, "%s", deliveryDetails.getOrderNumber()));
                        phone.setText(String.format(Locale.US, "%s", deliveryDetails.getPhoneNumber()));
                        name.setText(String.format(Locale.US, "%s", deliveryDetails.getCustomerName()));
                        distanceTv.setText(String.format(Locale.US, " %s km", deliveryDetails.getTotalDistance()));

                        date.setText(String.format(Locale.US, "%s", Util.formatTimeZoneLocal(deliveryDetails.getDatePickUp())));
                        priceAfterDisTv.setText(String.format(Locale.US, " %s %s",deliveryDetail.getCurrency(), Util.stringFormatPrice(deliveryDetails.getPriceAfterCouponDiscount())));

                        priceAfterCommissionRat.setText(String.format(Locale.US, "%s %s", deliveryDetails.getCurrency(), Util.stringFormatPrice(deliveryDetails.getPriceAfterCommission())));
                        couponTypeTv.setText(String.format(Locale.US, "%s %s", deliveryDetails.getCurrency(), Util.stringFormatPrice(deliveryDetails.getCouponDiscountPrice())));
                        dis.setText(String.format(Locale.US, "%s %s", getString(R.string.discount), "(" + deliveryDetails.getCoupon_discount_formatted() + " )"));


                        priceTv.setText(String.format(Locale.US, " %s %s", deliveryDetails.getCurrency(), Util.stringFormatPrice(deliveryDetails.getCostPerDistance())));
                        subTotal.setText(String.format(Locale.US, " %s %s", deliveryDetails.getCurrency(), Util.stringFormatPrice(deliveryDetails.getTotalPrice())));
                        comRate.setText(String.format(Locale.US, " %s %s", deliveryDetails.getCurrency(), Util.stringFormatPrice(deliveryDetails.getServiceCommissionPrice())));
                        comRateTxt.setText(String.format(Locale.US, "%s (%s)", getString(R.string.commission_rate), deliveryDetails.getServiceCommissionRate() + "%"));
                        totalTv.setText(String.format(Locale.US, "%s %s", deliveryDetails.getCurrency(), Util.stringFormatPrice(deliveryDetails.getPriceAfterCouponDiscount())));
                        if(status.equalsIgnoreCase("DELIVERED")){
                            linearLayoutRated.setVisibility(View.VISIBLE);
                            statusTv.setText(getString(R.string.leave_a_rate_and_review));
                        }
                        if (getIntent().hasExtra("open_rate")){
                            if (status.equalsIgnoreCase("DELIVERED")){
                                FeedBackBottomSheetFragment feedBackBottomSheetFragment = new FeedBackBottomSheetFragment().newInstance("customer");
                                feedBackBottomSheetFragment.show(getSupportFragmentManager(),"FeedBackBottomSheetFragment");
                            }
                        }

                        if (deliveryDetail.getRate().size() >= 1){
                            btnReview.setVisibility(View.GONE);
                            textRated.setText(getString(R.string.this_order_has_been_rated));
                            linearLayoutRated.setVisibility(View.VISIBLE);
                            statusTv.setVisibility(View.GONE);
                        } else {
                            linearLayoutRated.setVisibility(View.VISIBLE);
                            btnReview.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onLoadFail(String message) {
                       Util.popupMessage(null,message,HistoryTaxiActivity.this);
                        if (!isConnectedToInternet(HistoryTaxiActivity.this)) {
                            popupMessage(null, getString(R.string.check_yor), HistoryTaxiActivity.this);
                        } else {
                            popupMessage(null, getString(R.string.can_not_reach), HistoryTaxiActivity.this);
                        }
                    }
                });

        }

        btnReview.setOnClickListener(view -> {
            if (status.equalsIgnoreCase("DELIVERED")){
                FeedBackBottomSheetFragment feedBackBottomSheetFragment = new FeedBackBottomSheetFragment().newInstance("customer");
                feedBackBottomSheetFragment.show(getSupportFragmentManager(),"FeedBackBottomSheetFragment");
            } else {
                Util.popupMessage("",getString(R.string.waiting_for_complete), HistoryTaxiActivity.this);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if(status.equalsIgnoreCase(Constant.DELIVERED) && back.equals("back")){
                refresh();
            }else {
                setResult(RESULT_OK);
                finish();
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) ){
            if(status != null && status.equalsIgnoreCase(Constant.DELIVERED) && back.equals("back")){
                refresh();
            }else {
                //setResult(RESULT_OK);
                finish();
            }

        }
        return super.onKeyDown(keyCode, event);
    }

    private void refresh() {
        Intent intent = new Intent(HistoryTaxiActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    private void reviewRating(final HashMap<String, Object> reviewData, String action) {
        new OrderWs().writeReview(reviewData, HistoryTaxiActivity.this, new OrderWs.OnCallBackRateReview() {
            @Override
            public void onLoadSuccess(String message) {
                findViewById(R.id.btnReview).setVisibility(View.GONE);
                statusTv.setVisibility(View.GONE);
                textRated.setText(getString(R.string.this_order_has_been_rated));
                Toast.makeText(HistoryTaxiActivity.this, message, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLoadFail(String message) {
                Toast toast = Toast.makeText(HistoryTaxiActivity.this, message, Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
                toast.show();
            }
        });
    }

    @Override
    public void onClickSubmit(@Nullable String txtChoose, float position, @NotNull String action) {
        reviewRatingData.put("order_id",deliveryDetails.getOrderId());
        reviewRatingData.put("rate",position);
        reviewRatingData.put("review",txtChoose);
        if (action.equals("customer")){
            reviewRatingData.put("status", "driver_rate_customer");
        }
        reviewRating(reviewRatingData,action);
    }
}