package com.station29.mealtempledelivery.ui.setting.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.station29.mealtempledelivery.R
import com.station29.mealtempledelivery.api.Constant
import com.station29.mealtempledelivery.data.model.UserRoles

import java.util.*
import kotlin.collections.ArrayList


class ItemRewardsAdapter(private val userRoles:ArrayList<UserRoles>,private val id: Int, private var context:Context): RecyclerView.Adapter<ItemRewardsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.layout_adpater_rewards, parent, false)
        return ViewHolder(v)
    }

    @SuppressLint("SetTextI18n", "CheckResult")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val user = userRoles[position]

        holder.itemTv.text = " X " + user.credit_amount

        if (Constant.serviceLanguage.containsKey(user.activity)) {
            holder.nameTv.text = Constant.serviceLanguage[user.activity]?.replace("activity_credit",user.activity_credit)
        } else {
            holder.nameTv.text = user.activity
        }

        if (user.credit_amount == "0" || user.credit_amount == "1"){
            holder.creditTv.text = context.getString(R.string.credit)
        } else {
            holder.creditTv.text = context.getString(R.string.credits)
        }

        if (position == userRoles.size - 1) {
            holder.viewLine.visibility = View.GONE
        } else {
            holder.viewLine.visibility = View.VISIBLE
        }
    }

    override fun getItemCount() : Int {
        return userRoles.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var nameTv: TextView = view.findViewById(R.id.text_reward)
        var itemTv: TextView = view.findViewById(R.id.item_reword)
        var creditTv: TextView = view.findViewById(R.id.text_credit)
        var imageView: ImageView = view.findViewById(R.id.image_reward)
        var viewLine : View = view.findViewById(R.id.viewLine)

    }
}


