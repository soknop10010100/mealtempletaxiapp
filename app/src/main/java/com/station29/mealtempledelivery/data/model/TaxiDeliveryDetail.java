package com.station29.mealtempledelivery.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class TaxiDeliveryDetail implements Serializable {
    @SerializedName("order_id")
    @Expose
    private int orderId;
    @SerializedName("driver_id")
    @Expose
    private int driverId;
    @SerializedName("type_order")
    @Expose
    private String typeOrder;
    @SerializedName("order_number")
    @Expose
    private String orderNumber;
    @SerializedName("shipping_lat")
    @Expose
    private double shippingLat;
    @SerializedName("shipping_long")
    @Expose
    private double shippingLong;
    @SerializedName("phone_number")
    @Expose
    private String phoneNumber;
    @SerializedName("customer_name")
    @Expose
    private String customerName;
    @SerializedName("pick_up_lat")
    @Expose
    private double pickUpLat;
    @SerializedName("pick_up_long")
    @Expose
    private double pickUpLong;
    @SerializedName("date_pick_up")
    @Expose
    private String datePickUp;
    @SerializedName("service_driver")
    @Expose
    private String serviceDriver;
    @SerializedName("cost_per_distance")
    @Expose
    private String costPerDistance;
    @SerializedName("total_distance")
    @Expose
    private String totalDistance;
    @SerializedName("total_price")
    @Expose
    private String totalPrice;
    @SerializedName("start_address")
    @Expose
    private String startAddress;
    @SerializedName("waypoints")
    @Expose
    private Waypoint waypoints;
    @SerializedName("pick_up_waypoints")
    @Expose
    private Waypoint pickupWayPoint;
    @SerializedName("end_address")
    @Expose
    private String endAddress;
    @SerializedName("cost_per_distance_formatted")
    @Expose
    private String costPerDistanceFormatted;
    @SerializedName("total_price_formatted")
    @Expose
    private String totalPriceFormatted;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("currency_code")
    @Expose
    private String currencyCode;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("duration")
    @Expose
    private String duration;

    @SerializedName("service_flag_down_distance")
    @Expose
    private float serviceFlagDownDistance;

    @SerializedName("service_flag_down_cost")
    @Expose
    private float serviceFlagDownCost;

    @SerializedName("service_cost_per_distance")
    @Expose
    private float serviceCostPerDistance;

    @SerializedName("service_cost_per_min")
    @Expose
    private float serviceCostPerMin;

    @SerializedName("service_commission_rate")
    @Expose
    private float serviceCommissionRate;

    @SerializedName("send_package")
    @Expose
    private SendPackage sendPackage;

    @SerializedName("service_commission_price")
    @Expose
    private String serviceCommissionPrice;

    @SerializedName("price_after_commission")
    @Expose
    private String priceAfterCommission;

    @SerializedName("payment_type")
    @Expose
    private String paymentType;
    @SerializedName("coupon_discount")
    @Expose
    private String couponDiscount;
    @SerializedName("coupon_discount_price")
    @Expose
    private String couponDiscountPrice;
    @SerializedName("coupon_discount_type")
    @Expose
    private String couponDiscountType;
    @SerializedName("price_after_coupon_discount")
    @Expose
    private String  priceAfterCouponDiscount;

    public String getPercentageSign() {
        return percentageSign;
    }

    @SerializedName("percentage_sign")
    @Expose
    private String  percentageSign;
    @SerializedName("coupon_discount_formatted")
    @Expose
    private String  coupon_discount_formatted;
    @SerializedName("rate")
    @Expose
    private List<String> rate;

    public List<String> getRate() {
        return rate;
    }

    public String getCoupon_discount_formatted() {
        return coupon_discount_formatted;
    }

    public String getCouponDiscount() {
        return couponDiscount;
    }

    public void setCouponDiscount(String couponDiscount) {
        this.couponDiscount = couponDiscount;
    }

    public String getCouponDiscountPrice() {
        return couponDiscountPrice;
    }

    public void setCouponDiscountPrice(String couponDiscountPrice) {
        this.couponDiscountPrice = couponDiscountPrice;
    }

    public String getCouponDiscountType() {
        return couponDiscountType;
    }

    public void setCouponDiscountType(String couponDiscountType) {
        this.couponDiscountType = couponDiscountType;
    }

    public String getPriceAfterCouponDiscount() {
        return priceAfterCouponDiscount;
    }

    public void setPriceAfterCouponDiscount(String priceAfterCouponDiscount) {
        this.priceAfterCouponDiscount = priceAfterCouponDiscount;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getServiceCommissionPrice() {
        return serviceCommissionPrice;
    }

    public String getPriceAfterCommission() {
        return priceAfterCommission;
    }

    public SendPackage getSendPackage() {
        return sendPackage;
    }

    public void setSendPackage(SendPackage sendPackage) {
        this.sendPackage = sendPackage;
    }


    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getDriverId() {
        return driverId;
    }

    public void setDriverId(int driverId) {
        this.driverId = driverId;
    }

    public String getTypeOrder() {
        return typeOrder;
    }

    public void setTypeOrder(String typeOrder) {
        this.typeOrder = typeOrder;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public double getShippingLat() {
        return shippingLat;
    }

    public void setShippingLat(double shippingLat) {
        this.shippingLat = shippingLat;
    }

    public double getShippingLong() {
        return shippingLong;
    }

    public void setShippingLong(double shippingLong) {
        this.shippingLong = shippingLong;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public double getPickUpLat() {
        return pickUpLat;
    }

    public void setPickUpLat(double pickUpLat) {
        this.pickUpLat = pickUpLat;
    }

    public double getPickUpLong() {
        return pickUpLong;
    }

    public void setPickUpLong(double pickUpLong) {
        this.pickUpLong = pickUpLong;
    }

    public String getDatePickUp() {
        return datePickUp;
    }

    public void setDatePickUp(String datePickUp) {
        this.datePickUp = datePickUp;
    }

    public String getServiceDriver() {
        return serviceDriver;
    }

    public void setServiceDriver(String serviceDriver) {
        this.serviceDriver = serviceDriver;
    }

    public String getCostPerDistance() {
        return costPerDistance;
    }

    public void setCostPerDistance(String costPerDistance) {
        this.costPerDistance = costPerDistance;
    }

    public String getTotalDistance() {
        return totalDistance;
    }

    public void setTotalDistance(String totalDistance) {
        this.totalDistance = totalDistance;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getStartAddress() {
        return startAddress;
    }

    public void setStartAddress(String startAddress) {
        this.startAddress = startAddress;
    }

    public Waypoint getWaypoints() {
        return waypoints;
    }

    public void setWaypoint(Waypoint waypoints) {
        this.waypoints = waypoints;
    }

    public String getEndAddress() {
        return endAddress;
    }

    public void setEndAddress(String endAddress) {
        this.endAddress = endAddress;
    }

    public String getCostPerDistanceFormatted() {
        return costPerDistanceFormatted;
    }

    public void setCostPerDistanceFormatted(String costPerDistanceFormatted) {
        this.costPerDistanceFormatted = costPerDistanceFormatted;
    }

    public String getTotalPriceFormatted() {
        return totalPriceFormatted;
    }

    public void setTotalPriceFormatted(String totalPriceFormatted) {
        this.totalPriceFormatted = totalPriceFormatted;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public float getServiceFlagDownDistance() {
        return serviceFlagDownDistance;
    }

    public float getServiceFlagDownCost() {
        return serviceFlagDownCost;
    }

    public float getServiceCostPerDistance() {
        return serviceCostPerDistance;
    }

    public float getServiceCostPerMin() {
        return serviceCostPerMin;
    }

    public float getServiceCommissionRate() {
        return serviceCommissionRate;
    }

    public Waypoint getPickupWayPoint() {
        return pickupWayPoint;
    }
}
