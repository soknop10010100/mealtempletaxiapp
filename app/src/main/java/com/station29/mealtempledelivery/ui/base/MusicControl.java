package com.station29.mealtempledelivery.ui.base;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;

import com.station29.mealtempledelivery.R;

public class MusicControl {

    public static String STOP_ACTION = "cancel";
    private static MusicControl sInstance;
    public static MediaPlayer mMediaPlayer;
    private final Context mContext;
    private boolean   mIsPlaying;
    public static int countNotification = 0;
    public MusicControl(Context context) {
        mContext = context;
    }

    public static MusicControl getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new MusicControl(context);
        }
        return sInstance;
    }

    public void playMusic() {
        if(mMediaPlayer==null){
            mMediaPlayer = MediaPlayer.create(mContext, R.raw.android_notification);
            mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mMediaPlayer.setLooping(true);
            mMediaPlayer.start();
        }
        //clearSound();
    }

    public void stopMusic() {

            if( mMediaPlayer !=null && mMediaPlayer.isPlaying() && countNotification==0) {
                mMediaPlayer.reset();// It requires again setDataSource for player object.
                mMediaPlayer.stop();
                mMediaPlayer.setLooping(false);
                mMediaPlayer.release();// Release it
                mMediaPlayer = null;
                countNotification = 0;
                // Stop it
                // Initialize it to null so it can be used later
            }
//
       }





}
