package com.station29.mealtempledelivery.ui.wallet.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.data.model.PaymentTransaction;
import com.station29.mealtempledelivery.utils.Util;

import java.util.List;

public class WalletHistoryAdapter extends RecyclerView.Adapter<WalletHistoryAdapter.ViewHolder> {

    private final ItemClickListener onClickListenerTransactions;
    private final List<PaymentTransaction> paymentTransactions;
    private final Context context;

    public WalletHistoryAdapter(List<PaymentTransaction> paymentTransactions, ItemClickListener onClickListenerTransactions, Context context) {
        this.paymentTransactions = paymentTransactions;
        this.onClickListenerTransactions = onClickListenerTransactions;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_wallet_history, parent, false);
        return new ViewHolder(row);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final PaymentTransaction paymentTransaction = paymentTransactions.get(position);
        if (paymentTransaction != null) {

            String status= "";
            if (paymentTransaction.getStatus() != null){
                switch (paymentTransaction.getStatus()) {
                    case "SUCCESSFULL":
                        status = context.getString(R.string.success_full);
                        holder.txtStatus.setTextColor(Color.parseColor("#00c853"));
                        break;
                    case "PENDING":
                        status = context.getString(R.string.pending_s);
                        holder.txtStatus.setTextColor(Color.parseColor("#6d6d6d"));
                        break;
                    case "BLOCKED":
                        status = context.getString(R.string.blocked);
                        holder.txtStatus.setTextColor(Color.parseColor("#af1022"));
                        break;
                    case "FAILED":
                        status = context.getString(R.string.failed);
                        holder.txtStatus.setTextColor(Color.parseColor("#af1022"));
                        break;
                    default:
                        status = paymentTransaction.getStatus();
                        holder.txtStatus.setTextColor(Color.parseColor("#af1022"));
                        break;
                }
            }
            holder.txtStatus.setText(status);

            String type = "";
            if (paymentTransaction.getType() != null)
                if (paymentTransaction.getType().equals("TOP_UP") ) {
                    holder.txtTitle.setText(context.getString(R.string.deposit));
                    type = "+";
                    holder.txtPrice.setTextColor(Color.parseColor("#00c853"));
                } else if (paymentTransaction.getType().equals("IN")){
                    holder.txtTitle.setText(paymentTransaction.getRefUserName() +" "+ context.getString(R.string.paid_you));
                    type = "+";
                    holder.txtPrice.setTextColor(Color.parseColor("#00c853"));
                } else {
                    type = "-";
                    holder.txtTitle.setText(context.getString(R.string.you_paid) + " " + paymentTransaction.getRefUserName());
                    holder.txtPrice.setTextColor(Color.parseColor("#af1022"));
                }
            else holder.txtPrice.setTextColor(Color.parseColor("#9e9e9e"));

            holder.txtDate.setText(Util.formatDateUptoCurrentRegion(paymentTransaction.getCreatedAt()));
            if (paymentTransaction.getAmount() != null && paymentTransaction.getAmount() != null){
                holder.txtPrice.setText(String.format("%s %s %s",type, paymentTransaction.getCurrencyCode(), Util.stringFormatPrice( paymentTransaction.getAmount())));
            } else holder.txtPrice.setText("");

            holder.view.setOnClickListener(v -> onClickListenerTransactions.onClickListener(paymentTransaction));

            if (paymentTransactions.size() - 1 == position){
                holder.itemView.setBackgroundResource(R.drawable.card_view_white_border_bottom);
                holder.viewLine.setVisibility(View.GONE);
            } else if (0 == position) {
                holder.itemView.setBackgroundResource(R.drawable.card_view_whtie_border_top);
            } else  {
                holder.itemView.setBackgroundResource(R.color.white);
                holder.viewLine.setVisibility(View.VISIBLE);
            }
        }
    }

    public interface ItemClickListener {
        void onClickListener(PaymentTransaction paymentTransaction);
    }


    @Override
    public int getItemCount() {
        return paymentTransactions.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView txtTitle, txtDate, txtStatus, txtPrice;
        private final View view ,viewLine;

        private ViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
            txtDate = itemView.findViewById(R.id.date);
            txtPrice = itemView.findViewById(R.id.price);
            txtStatus = itemView.findViewById(R.id.status);
            txtTitle = itemView.findViewById(R.id.title);
            viewLine = itemView.findViewById(R.id.viewLine);
        }
    }

    public void clear() {
        int size = paymentTransactions.size();
        if (size > 0) {
            paymentTransactions.subList(0, size).clear();
            notifyItemRangeRemoved(0, size);
        }
    }
}
