package com.station29.mealtempledelivery.api.request;

import android.app.Activity;
import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.station29.mealtempledelivery.api.Constant;
import com.station29.mealtempledelivery.api.CustomCallback;
import com.station29.mealtempledelivery.api.CustomResponseListener;
import com.station29.mealtempledelivery.api.RetrofitGenerator;
import com.station29.mealtempledelivery.api.ServiceAPI;
import com.station29.mealtempledelivery.data.model.DeliveryDetails;
import com.station29.mealtempledelivery.data.model.OrderItem;
import com.station29.mealtempledelivery.utils.Util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;

public class OrderWs {

    public void viewMenu(final int orderId, final Activity activity, final ViewMenu viewMenu) {
        ServiceAPI serviceAPI = RetrofitGenerator.createServiceWithAuth(activity);
        final retrofit2.Call<JsonElement> res = serviceAPI.loadOrder(orderId);
        res.enqueue(new CustomCallback(activity,new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (resObj.has("success") && resObj.get("success").getAsBoolean()) {
                        JsonArray data = resObj.getAsJsonObject("data").getAsJsonArray("items");
                        JsonObject data1 = resObj.getAsJsonObject("data");
                        DeliveryDetails model = new Gson().fromJson(data1, DeliveryDetails.class);
                        List<OrderItem> productList = new ArrayList<>();
                        for (int i = 0; i < data.size(); i++) {
                            OrderItem item = new Gson().fromJson(data.get(i), OrderItem.class);
                            item.setCooperation(model.getCooperation());
                            productList.add(item);
                        }
                        viewMenu.onLoadProductSuccess(productList, model);

                }
            }

            @Override
            public void onError(String error , int code) {
                viewMenu.onError(error);
            }
        }));
    }

    public void writeReview(final HashMap<String, Object> reviewData, Context activity, final OnCallBackRateReview reviewSuccess){
        ServiceAPI serviceAPI = RetrofitGenerator.createServiceWithAuth(activity);
        final Call<JsonElement> elementApi = serviceAPI.rateReview(reviewData);
        elementApi.enqueue(new CustomCallback(activity, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject object) {
                if (object.has("success") && object.get("success").getAsBoolean()) {
                    if (Constant.serviceLanguage.containsKey(object.get("message").getAsString()))
                        reviewSuccess.onLoadSuccess(Constant.serviceLanguage.get(object.get("message").getAsString()));
                    else reviewSuccess.onLoadSuccess(object.get("message").getAsString());
                }
            }

            @Override
            public void onError(String error, int code) {
                reviewSuccess.onLoadFail(error);
            }
        }));
    }

    public interface OnCallBackRateReview{
        void onLoadSuccess(String message);
        void onLoadFail(String message);
    }

    public interface ViewMenu {
        void onLoadProductSuccess(List<OrderItem> product, DeliveryDetails orderItem);
        void onError(String errorMs);
    }
}
