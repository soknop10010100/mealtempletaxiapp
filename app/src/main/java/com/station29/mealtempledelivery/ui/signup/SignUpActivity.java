package com.station29.mealtempledelivery.ui.signup;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.rilixtech.widget.countrycodepicker.CountryCodePicker;
import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.api.Constant;
import com.station29.mealtempledelivery.data.model.ServiceTypeDriver;
import com.station29.mealtempledelivery.ui.base.BaseActivity;
import com.station29.mealtempledelivery.ui.SplashScreenActivity;

import java.util.ArrayList;
import java.util.List;


public class SignUpActivity extends BaseActivity {

    private Spinner serviceTypeSpinner, service_spinner_taxi;
    private TextInputEditText firstNameTv;
    private TextInputEditText lastNameTv;
    private TextInputEditText phoneNumberTv;
    private CountryCodePicker countryCodePicker;
    private TextInputEditText passwordTv;
    private final String userType = "USER_ANDROID";
    private int serviceId, serviceIdTaxi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        serviceTypeSpinner = findViewById(R.id.service_spinner);
        service_spinner_taxi = findViewById(R.id.service_spinner_taxi);
        firstNameTv = findViewById(R.id.txt_first_name_sign_up);
        lastNameTv = findViewById(R.id.txt_last_name_sign_up);
        phoneNumberTv = findViewById(R.id.txt_phone_number_sign_up);
        countryCodePicker = findViewById(R.id.ccp);
        passwordTv = findViewById(R.id.txt_password_sign_up);

        firstNameTv.setHintTextColor(Color.BLACK);
        passwordTv.setHintTextColor(Color.BLACK);
        lastNameTv.setHintTextColor(Color.BLACK);

        findViewById(R.id.btnSignupback).setOnClickListener(v -> finish());

        if (BaseActivity.countryCode != null && !BaseActivity.countryCode.isEmpty())
            countryCodePicker.setCountryForNameCode(BaseActivity.countryCode);
        getServiceTypeDriver();
        getServiceTaxi();

        findViewById(R.id.btn_sign_up).setOnClickListener(v -> {
            v.setEnabled(false);
            v.postDelayed(() -> v.setEnabled(true), 50);
            if (firstNameTv.getText().toString().equals("") || lastNameTv.getText().toString().equals("") || phoneNumberTv.getText().toString().equals("") || passwordTv.getText().toString().equals("")) {
                Toast toast = Toast.makeText(getApplicationContext(), getString(R.string.in_correct), Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            } else if(serviceId == 0 && serviceIdTaxi == 0){
                Toast toast = Toast.makeText(getApplicationContext(), getString(R.string.pleae_choose_one_service), Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            } else if (passwordTv.getText().toString().trim().length() < 6) {
                Toast toast = Toast.makeText(getApplicationContext(), R.string.the_password_must_be_at_lest_characters, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            } else {
                if (Constant.getBaseUrl() != null) {
                    Intent intent = new Intent(SignUpActivity.this, SignUp2Activity.class);
                    intent.putExtra("code", BaseActivity.countryCode);
                    intent.putExtra("password", passwordTv.getText().toString().trim());
                    intent.putExtra("phone", phoneNumberTv.getText().toString().trim());
                    intent.putExtra("phone_code", countryCodePicker.getFullNumber());
                    intent.putExtra("service_id", serviceId);
                    intent.putExtra("service_idTaxi", serviceIdTaxi);
                    if (serviceTypeSpinner != null && serviceTypeSpinner.getSelectedItem() != null && !serviceTypeSpinner.getSelectedItem().equals(getString(R.string.chose_food_delevery)))
                        intent.putExtra("service", serviceTypeSpinner.getSelectedItem().toString().trim());
                    intent.putExtra("firstname", firstNameTv.getText().toString().trim());
                    intent.putExtra("lastname", lastNameTv.getText().toString().trim());
                    intent.putExtra("function", "Driver");
                    intent.putExtra("user_type", userType);
                    startActivity(intent);
                }

            }
        });
    }

    private void getServiceTypeDriver() {

        final List<ServiceTypeDriver> list = new ArrayList<>();
        final List<ServiceTypeDriver> listDelivery = new ArrayList<>();
        list.add(new ServiceTypeDriver(0, getString(R.string.chose_food_delevery), 1));
        list.addAll(SplashScreenActivity.splashConfig.getServiceTypeDriver());
        List<String> type = new ArrayList<>();
        for (ServiceTypeDriver typeDelivery : list) {
            if (typeDelivery.getIsDefault() == 1) {
                type.add(typeDelivery.getService());
                listDelivery.add(typeDelivery);
            }
        }
        ArrayAdapter<String> serviceAdapter = new ArrayAdapter<String>(SignUpActivity.this, android.R.layout.simple_spinner_item, type);
        serviceAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        serviceTypeSpinner.setAdapter(serviceAdapter);

        if (list.size() > 0) {
            serviceId = list.get(0).getServiceId();

        }
        serviceTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ServiceTypeDriver serviceTypeDriver1 = listDelivery.get(position);
                if (serviceTypeDriver1 != null && serviceTypeDriver1.getIsDefault()==1) {
                    serviceId = serviceTypeDriver1.getServiceId();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    private void getServiceTaxi() {
        final List<ServiceTypeDriver> listTaxi = new ArrayList<>();
        final List<ServiceTypeDriver> listTaxiType = new ArrayList<>();
        listTaxi.add(new ServiceTypeDriver(0, getString(R.string.chose_food_delevery), 0));
        listTaxi.addAll(SplashScreenActivity.splashConfig.getServiceTypeDriver());
        List<String> typeTaxi = new ArrayList<>();
        for (ServiceTypeDriver typeDriver : listTaxi) {
            if (typeDriver.getIsDefault() == 0) {
                listTaxiType.add(typeDriver);
                typeTaxi.add(typeDriver.getService());
            }
        }
        ArrayAdapter<String> serviceAdapterTaxi = new ArrayAdapter<String>(SignUpActivity.this, android.R.layout.simple_spinner_item, typeTaxi);
        serviceAdapterTaxi.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        service_spinner_taxi.setAdapter(serviceAdapterTaxi);
        if (listTaxi.size() > 0 ) {
            serviceIdTaxi = listTaxi.get(0).getServiceId();
        }
        service_spinner_taxi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ServiceTypeDriver serviceTypeDriver1 = listTaxiType.get(position);
                if (serviceTypeDriver1 != null && serviceTypeDriver1.getIsDefault()==0) {
                    serviceIdTaxi = serviceTypeDriver1.getServiceId();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
}
