package com.station29.mealtempledelivery.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Delivery implements Serializable {

    @SerializedName("account_id")
    @Expose
    private Integer accountId;
    @SerializedName("account_name")
    @Expose
    private String accountName;
    @SerializedName("delivery_id")
    @Expose
    private Integer deliveryId;
    @SerializedName("delivery_status")
    @Expose
    private String deliveryStatus;
    @SerializedName("total_price")
    @Expose
    private String totalPrice;
    @SerializedName("total_price_formatted")
    @Expose
    private String totalPriceFormatted;
    @SerializedName("total_price_formatted_2")
    @Expose
    private String totalPriceFormatted2;
    @SerializedName("delivery_time")
    @Expose
    private String deliveryTime;

    @SerializedName("dropoff_lat")
    @Expose
    private String latDropOff;
    @SerializedName("dropoff_lng")
    @Expose
    private String lngDropOff;
    @SerializedName("pick_up_lat")
    @Expose
    private String latPickUp;
    @SerializedName("pick_up_long")
    @Expose
    private String lngPickUp;
    @SerializedName("type_order")
    @Expose
    private String typeOrder;
    @SerializedName("duration")
    @Expose
    private String duration;
    @SerializedName("waypoints")
    @Expose
    private Waypoint waypoints;

    public String getLatDropOff() {
        return latDropOff;
    }

    public String getLngDropOff() {
        return lngDropOff;
    }

    public String getLatPickUp() {
        return latPickUp;
    }

    public String getLngPickUp() {
        return lngPickUp;
    }

    public String getTypeOrder() {
        return typeOrder;
    }

    public String getDuration() {
        return duration;
    }

    public String getDistance() {
        return distance;
    }

    @SerializedName("distance")
    @Expose
    private String distance;

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public Integer getDeliveryId() {
        return deliveryId;
    }

    public void setDeliveryId(Integer deliveryId) {
        this.deliveryId = deliveryId;
    }

    public String getDeliveryStatus() {
        return deliveryStatus;
    }

    public void setDeliveryStatus(String deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getTotalPriceFormatted() {
        return totalPriceFormatted;
    }

    public void setTotalPriceFormatted(String totalPriceFormatted) {
        this.totalPriceFormatted = totalPriceFormatted;
    }

    public String getTotalPriceFormatted2() {
        return totalPriceFormatted2;
    }

    public void setTotalPriceFormatted2(String totalPriceFormatted2) {
        this.totalPriceFormatted2 = totalPriceFormatted2;
    }

    public String getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(String deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public Waypoint getWaypoints() {
        return waypoints;
    }

    public void setWaypoints(Waypoint waypoints) {
        this.waypoints = waypoints;
    }
}
