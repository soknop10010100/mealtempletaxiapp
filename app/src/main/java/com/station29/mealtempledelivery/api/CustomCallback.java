package com.station29.mealtempledelivery.api;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.ui.base.MusicControl;
import com.station29.mealtempledelivery.ui.login.LoginActivity;
import com.station29.mealtempledelivery.utils.Util;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.station29.mealtempledelivery.utils.Util.logDebug;

public class CustomCallback implements Callback<JsonElement> {

    private final CustomResponseListener customResponseListener;
    private final FirebaseAnalytics mFirebaseAnalytics;
    private final Context mContext;

    public CustomCallback(Context context, CustomResponseListener listener) {
        customResponseListener = listener;
        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
        mContext = context;
    }

    @Override
    public void onResponse(@NotNull Call<JsonElement> call, Response<JsonElement> response) {
        if (response.isSuccessful()) {
            JsonElement jsonElement = response.body();
            if (jsonElement != null && !jsonElement.isJsonNull() && jsonElement instanceof JsonObject) {
                logDebug("logResponse", "onResponse: " + jsonElement.getAsJsonObject());
                customResponseListener.onSuccess(jsonElement.getAsJsonObject());
            } else {
                customResponseListener.onError("Wrong Json Type!",response.code());
            }
        } else {
            if (response.errorBody() != null) {
                String message = "";
                ResponseBody errorString = response.errorBody();
                try {
                    String json = errorString.string();
                    logDebug("error", json);
                    JSONObject jsonOb = new JSONObject(json);

                    if (response.code() == 401) {
                        if (jsonOb.has("error") && jsonOb.getString("error").equals("Unauthenticated.")) {
                            message = mContext.getString(R.string.session_expired);
                            popupSessionExpired(mContext, message);
                        } else {
                            if (jsonOb.has("message")) {
                                message = jsonOb.getString("message");
                            } else if (jsonOb.has("error")) {
                                message = jsonOb.getString("error");
                            } else {
                                message = mContext.getString(R.string.unexpected_problem);
                            }
                            if (Constant.serviceLanguage.containsKey(message)) {
                                customResponseListener.onError(Constant.serviceLanguage.get(message),response.code());
                            } else customResponseListener.onError(message,response.code());
                        }
                    } else if (jsonOb.has("message")) {
                        message = jsonOb.getString("message");
                        if (Constant.serviceLanguage.containsKey(message))
                            customResponseListener.onError(Constant.serviceLanguage.get(message),response.code());
                        else customResponseListener.onError(message,response.code());
                    } else if (jsonOb.has("error")) {
                        message = jsonOb.getString("error");
                        if (Constant.serviceLanguage.containsKey(message))
                            customResponseListener.onError(Constant.serviceLanguage.get(message),response.code());
                        else customResponseListener.onError(message,response.code());
                    }
                    // =============================================================
                    Bundle bundle = new Bundle();
                    bundle.putString(Constant.USER_ID, Util.getString("user_id", mContext));
                    bundle.putString(Constant.PHONE_MODEL, android.os.Build.MODEL);
                    bundle.putString(Constant.ERROR_MESSAGE, Constant.serviceLanguage.get(message));
                    mFirebaseAnalytics.logEvent(this.getClass().getSimpleName(), bundle);
                    // ===============================================================
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(e.getClass().getName(), Objects.requireNonNull(e.getMessage()));
                    message = mContext.getString(R.string.unexpected_problem);
                    logDebug("cus",e.getMessage()+"");
                    customResponseListener.onError(message,response.code());
                } catch (IOException e2) {
                    Log.e(e2.getClass().getName(), Objects.requireNonNull(e2.getMessage()));
                    message = mContext.getString(R.string.unexpected_problem);
                    customResponseListener.onError(message,response.code());
                }
            }
        }
    }

    @Override
    public void onFailure(@NotNull Call<JsonElement> call, Throwable t) {
        if (Util.isConnectedToInternet((Activity) mContext))
            customResponseListener.onError(mContext.getString(R.string.check_yor),500);
        // =============================================================
        Bundle bundle = new Bundle();
        bundle.putString(Constant.USER_ID, Util.getString("user_id", mContext));
        bundle.putString(Constant.PHONE_MODEL, android.os.Build.MODEL);
        bundle.putString(Constant.ERROR_MESSAGE, t.getLocalizedMessage() + "");
        mFirebaseAnalytics.logEvent(this.getClass().getSimpleName(), bundle);
        // ===============================================================
    }

    private void popupSessionExpired(final Context context, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message);
        builder.setPositiveButton(context.getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                MusicControl.countNotification =0;
                MusicControl.getInstance(context).stopMusic();
                Util.saveString("token", "", context);
                Intent intent = new Intent(context, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(intent);
            }
        });
        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
    }

}
