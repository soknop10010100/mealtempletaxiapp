package com.station29.mealtempledelivery.ui.history;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.api.Constant;
import com.station29.mealtempledelivery.api.request.DeliveryWs;
import com.station29.mealtempledelivery.data.model.Delivery;
import com.station29.mealtempledelivery.data.model.Waypoint;
import com.station29.mealtempledelivery.ui.base.BaseFragment;
import com.station29.mealtempledelivery.ui.home.OrderDetailActivity;
import com.station29.mealtempledelivery.ui.taxi.HistoryTaxiActivity;
import com.station29.mealtempledelivery.utils.Util;

import java.util.ArrayList;
import java.util.List;

import static com.station29.mealtempledelivery.utils.Util.logDebug;

public class EarningFragment extends BaseFragment {

    private final ArrayList<String> status = new ArrayList<>();
    private final ArrayList<Delivery> list = new ArrayList<>();
    private LinearLayout empty;
    private DeliveryItemAdapter deliveryItemAdapter;
    private int currentItem;
    private int total;
    private int scrollDown;
    private int currentPage = 1;
    private int size = 10;
    private final int currentSize = 10;
    private SwipeRefreshLayout refreshLayout;
    private boolean loading;
    private boolean isScrolling;
    private ProgressBar progress;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        status.clear();
        status.add(Constant.DELIVERED);

        View root = inflater.inflate(R.layout.fragment_dashboard, container, false);

        RecyclerView recyclerView = root.findViewById(R.id.list_item);
        empty = root.findViewById(R.id.empty);
        progress = root.findViewById(R.id.progressBar);
        progress.setVisibility(View.VISIBLE);
        refreshLayout = root.findViewById(R.id.swipe_layout);

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mActivity);
        recyclerView.setLayoutManager(linearLayoutManager);
        deliveryItemAdapter = new DeliveryItemAdapter(list, 1, itemClickListener,mActivity);
        recyclerView.setAdapter(deliveryItemAdapter);

        final String driverId = Util.getString("user_id", container.getContext());
        if (!driverId.isEmpty()) {
            deliveryItemAdapter.clear();
            loadDelivery(driverId, status, currentPage);
        } else {
            empty.setVisibility(View.VISIBLE);
        }

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL){
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItem = linearLayoutManager.getChildCount();
                total = linearLayoutManager.getItemCount();
                scrollDown = linearLayoutManager.findFirstVisibleItemPosition();
                if(dy > 0) {
                    if (isScrolling && (currentItem + scrollDown == total)) {
                        if(total == size) {
                            isScrolling = false;
                            currentPage++;
                            progress.setVisibility(View.VISIBLE);
                            recyclerView.scrollToPosition(list.size() - 1);
                            loadDelivery(driverId, status, currentPage);
                            size+=10;
                        }
                    }
                }

            }
        });

        refreshLayout.setColorSchemeResources(R.color.colorPrimary);

        refreshLayout.setOnRefreshListener(() -> {
            currentPage = 1;
            size = 10;
            deliveryItemAdapter.clear();
            loadDelivery(driverId, status, currentPage);
        });

        return root;
    }

    private void loadDelivery(final String driverId, ArrayList<String> status, int page) {
        if (Constant.getBaseUrl() != null) {
            new DeliveryWs().getDelivery(mActivity, driverId, status, currentSize, page, new DeliveryWs.OnAcceptDeliveryListener() {
                @SuppressLint("NotifyDataSetChanged")
                @Override
                public void onLoadListSuccess(List<Delivery> deliveryList) {
                    progress.setVisibility(View.GONE);
                    list.addAll(deliveryList);
                    deliveryItemAdapter.notifyDataSetChanged();
                    if (list.isEmpty()) {
                        empty.setVisibility(View.VISIBLE);
                    }

                    refreshLayout.setRefreshing(false);
                    logDebug("listSize", list.size() + "");
                }

                @Override
                public void onAcceptDeliverySuccess(Waypoint waypoint) {

                }

                @Override
                public void onAcceptSuccessTaxi(Waypoint waypoint) {

                }


                @Override
                public void onAcceptFailed(String message) {
                    loading = false;
                    progress.setVisibility(View.GONE);
                    empty.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    private final DeliveryItemAdapter.DeliveryItemClickListener itemClickListener = deliveryDetail -> {
        Intent intent;
        if (deliveryDetail.getTypeOrder().equalsIgnoreCase("service")){
            intent = new Intent(mActivity, HistoryTaxiActivity.class);
            intent.putExtra("typeOrder","service");

        }else if(deliveryDetail.getTypeOrder().equalsIgnoreCase("send")){
            intent = new Intent(mActivity, HistoryTaxiActivity.class);
            intent.putExtra("typeOrder","send");
        }
        else {
            intent = new Intent(mActivity, OrderDetailActivity.class);
            intent.putExtra("typeOrder","delivery");
            intent.putExtra("id",deliveryDetail.getDeliveryId());
        }
        intent.putExtra("back","notBack");
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra("delivery", deliveryDetail.getDeliveryId());
        startActivity(intent);
    };

}