package com.station29.mealtempledelivery.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.station29.mealtempledelivery.utils.Util;

public class Translate {
    @SerializedName("user_name")
    @Expose
    private String userName;
    @SerializedName("amount_")
    @Expose
    private String amount;
    @SerializedName("currency_code")
    @Expose
    private String currencyCode;
    @SerializedName("notification_translate")
    @Expose
    private String notificationTranslate;
    @SerializedName("title_translate")
    @Expose
    private String title_translate;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAmount() {
        return Util.stringFormatPrice(amount);
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getNotificationTranslate() {
        return notificationTranslate;
    }

    public void setNotificationTranslate(String notificationTranslate) {
        this.notificationTranslate = notificationTranslate;
    }

    public String getTitle_translate() {
        return title_translate;
    }

    public void setTitle_translate(String title_translate) {
        this.title_translate = title_translate;
    }
}
