package com.station29.mealtempledelivery.ui;

import static com.station29.mealtempledelivery.fcm.MyFirebaseMessagingService.NOTIFICATION;
import static com.station29.mealtempledelivery.fcm.MyFirebaseMessagingService.n;
import static com.station29.mealtempledelivery.utils.Util.isConnectedToInternet;
import static com.station29.mealtempledelivery.utils.Util.popupMessage;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.bumptech.glide.Glide;
import com.ebanx.swipebtn.SwipeButton;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.button.MaterialButton;
import com.google.gson.Gson;
import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.api.Constant;
import com.station29.mealtempledelivery.api.request.DeliveryWs;
import com.station29.mealtempledelivery.broadcastreceiver.ExitScreenReceiver;
import com.station29.mealtempledelivery.data.model.Delivery;
import com.station29.mealtempledelivery.data.model.ServiceModel;
import com.station29.mealtempledelivery.data.model.Waypoint;
import com.station29.mealtempledelivery.fcm.MyFirebaseMessagingService;
import com.station29.mealtempledelivery.service.LocationUpdatesService;
import com.station29.mealtempledelivery.ui.base.BaseActivity;
import com.station29.mealtempledelivery.ui.base.MusicControl;
import com.station29.mealtempledelivery.ui.home.OrderDetailActivity;
import com.station29.mealtempledelivery.ui.home.WaitingDialog;
import com.station29.mealtempledelivery.utils.Util;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class ServiceArrivalActivity extends BaseActivity {

    private ServiceModel serviceModel;
    private SwipeButton acceptBtn;
    private MaterialButton cancelBtn , btnView;
    private int i = 0;

    private ExitScreenReceiver exitScreenReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_service_arrival);
        turnOnScreenn();
        unlockKeyguard();

        TextView storeName,storeAddress,customerAddress,totalPrice,paymentType,deliveryFee,deliveryTime,productPrice,riderTip,serviceType,cooperationTxt;

        if (getIntent() != null && getIntent().hasExtra("serviceModel")) {
            serviceModel = (ServiceModel) getIntent().getSerializableExtra("serviceModel");
        }

        if(getIntent().hasExtra(NOTIFICATION)){
            i = getIntent().getIntExtra(NOTIFICATION,-1);
           if (i==0){
               MusicControl.countNotification = n;
           }else {
               MusicControl.countNotification = i;
           }
        }

        if (serviceModel == null) return;

        registerReceiver(serviceModel.getOrderId(), this);
        final ShimmerFrameLayout cont = findViewById(R.id.shimmer);
        cont.startShimmer();

        cancelBtn = findViewById(R.id.cancel);
        storeName = findViewById(R.id.storeName);
        storeAddress = findViewById(R.id.storeAddBs);
        customerAddress = findViewById(R.id.customerAdd);
        totalPrice = findViewById(R.id.totalPriceBs);
        paymentType = findViewById(R.id.paymentBs);
        deliveryFee = findViewById(R.id.deliveryFeeBs);
        deliveryTime = findViewById(R.id.deliveryTimeBs);
        productPrice = findViewById(R.id.productPriceBs);
        riderTip = findViewById(R.id.riderTip);
        serviceType = findViewById(R.id.serviceType);
        acceptBtn = findViewById(R.id.accept);
        btnView = findViewById(R.id.view);
        cooperationTxt = findViewById(R.id.cooperationTxt);
        final TextView cooperateOrderId = findViewById(R.id.order_num_cooperation);
        final TextView orderNumber = findViewById(R.id.order_numbers);
        ImageView cooperationImg = findViewById(R.id.cooperationImg);

        acceptBtn.setOnStateChangeListener(active -> {
            acceptBtn.setEnabled(false);
            cont.hideShimmer();
            cancelBtn.setEnabled(false);
            cancelBtn.setTextColor(R.color.colorGrey);

            if (serviceModel.getCooperation() != null && !isFinishing()) {
                acceptDelivery(serviceModel.getDeliveryId(), serviceModel.getCooperation());
            }
            removeNotification();//case for when notification is still coming before accept order
            MainActivity.serviceModels.clear();
            MusicControl.countNotification = 0;
            MusicControl.getInstance(ServiceArrivalActivity.this).stopMusic();

        });

        cancelBtn.setOnClickListener(view -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(ServiceArrivalActivity.this, R.style.DialogTheme);
            builder.setTitle(getString(R.string.decline_order));
            builder.setMessage(getString(R.string.are_you_sure_to_decline_this_order));
            builder.setPositiveButton(String.format(Locale.US, "%s", getString(R.string.yes)), (dialogInterface, a) -> {
                MyFirebaseMessagingService.orderId = 0;
                if(MusicControl.countNotification > 0 ) {
                    MusicControl.countNotification -=1;
                    MusicControl.getInstance(ServiceArrivalActivity.this).stopMusic();
                    Intent intent = new Intent();
                    if (!MainActivity.serviceModels.isEmpty()) {
                        for (int i = 0; i < MainActivity.serviceModels.size(); i++) {
                            if (serviceModel.getOrderId() == MainActivity.serviceModels.get(i).getOrderId()) {
                                MainActivity.serviceModels.remove(MainActivity.serviceModels.get(i));
                                if (MainActivity.serviceModels!=null && !MainActivity.serviceModels.isEmpty()) {
                                    intent.putExtra("serviceModel", MainActivity.serviceModels.get(0));
                                    intent.putExtra(NOTIFICATION,MusicControl.countNotification);
                                    MainActivity.serviceModels.clear();
                                    break;
                                }else {
                                    MainActivity.serviceModels.clear();
                                    MusicControl.countNotification = 0;
                                    MusicControl.getInstance(ServiceArrivalActivity.this).stopMusic();
                                    finish();
                                }

                            }
                        }
                    }
                    setResult(RESULT_OK,intent);
                }else {
                    MusicControl.countNotification = 0;
                    MusicControl.getInstance(ServiceArrivalActivity.this).stopMusic();
                }
                finish();
            });
            builder.setNegativeButton(getString(R.string.no),null);
            builder.show();
        });

        btnView.setOnClickListener(v -> {
            Intent intent = new Intent(ServiceArrivalActivity.this, OrderDetailActivity.class);
            intent.putExtra("id", serviceModel.getDeliveryId());
            intent.putExtra("service_type",serviceModel.getTypeOrder());
            intent.putExtra("back","notBack");
            startActivity(intent);
        });

        if(serviceModel.getCooperation() == true){
            cooperateOrderId.setText(serviceModel.getCooperationOrderId());
            cooperateOrderId.setOnClickListener(v -> {
                DisplayMetrics displayMetrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                int width = displayMetrics.widthPixels;
                Toast toast = Toast.makeText(ServiceArrivalActivity.this, R.string.copied, Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP, width / 3, 300);
                toast.show();

                Util.setClipboard(ServiceArrivalActivity.this, cooperateOrderId.getText().toString());
            });
        }else {
            findViewById(R.id.linCooperations).setVisibility(View.GONE);
        }

        orderNumber.setText(serviceModel.getOrderNumber());

        orderNumber.setOnClickListener(v -> {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            int width = displayMetrics.widthPixels;
            Toast toast = Toast.makeText(ServiceArrivalActivity.this, R.string.copied, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.TOP, width / 3, 300);
            toast.show();

            Util.setClipboard(ServiceArrivalActivity.this, orderNumber.getText().toString());
        });

        storeName.setText(String.format("%s : %s" ,getString(R.string.from), serviceModel.getAccountName()));
        storeAddress.setText(Util.getLoc(serviceModel.getAccountLat(), serviceModel.getAccountLng(), this));
        customerAddress.setText(serviceModel.getDropOffAddress());
        totalPrice.setText(String.format("%s %s %s",getString(R.string.total), serviceModel.getCurrency(), Util.formatPrice(serviceModel.getTotal())));
        if(serviceModel.getPaymentType().equals("KESS_PAY")){
            paymentType.setText(": Debit Card");
        } else {
            paymentType.setText(String.format(" : %s", serviceModel.getPaymentType()));
        }

        if(serviceModel.getDeliveryFee()>0) deliveryFee.setText(String.format("%s %s", serviceModel.getCurrency(), Util.formatPrice(serviceModel.getDeliveryFee())));
        else deliveryFee.setText(String.format(Locale.US,"%s %s %s",serviceModel.getCurrency(),serviceModel.getFree_delivery(),getString(R.string.pay_by_kiwigo)));
        deliveryTime.setText(String.format(Locale.US," %s", Util.formatTimeZoneLocal(serviceModel.getDeliveryTime())));
//        float taxTotal = subTax(), surTotal = subSurCharge();
        productPrice.setText(String.format(" %s %s", serviceModel.getCurrency(), Util.formatPrice(serviceModel.getProductPriceAfterDiscountVat())));
        riderTip.setText(String.format(Locale.US, "%s %s",serviceModel.getCurrency(),Util.formatPrice(serviceModel.getRiderTip())));
        serviceType.setText(String.format(Locale.US,"%s",serviceModel.getTypeOrder()));
        cooperationTxt.setText(serviceModel.getCooperationName());
        Glide.with(this)
                .load(serviceModel.getCooperationLogo())
                .into(cooperationImg);
        if(serviceModel.getRiderTip()==0){
            riderTip.setVisibility(View.GONE);
            findViewById(R.id.tip).setVisibility(View.GONE);
        }
    }

    private final WaitingDialog.OnCloseButtonClickListener onCloseButtonClickListener = new WaitingDialog.OnCloseButtonClickListener() {
        @Override
        public void onClose(WaitingDialog waitingDialog) {}

        @Override
        public void onOk(WaitingDialog waitingDialog) {}

        @Override
        public void onClickRate(WaitingDialog waitingDialog) {

        }
    };

    //Accept Notification
    private void acceptDelivery(int deliveryId, boolean cooperation) {
        final WaitingDialog dialog = new WaitingDialog(this, onCloseButtonClickListener);
        dialog.show();
        HashMap<String, Object> params = new HashMap<>();
        params.put("delivery_id", deliveryId);
        params.put("status", Constant.ACCEPTED);
        params.put("cooperation", cooperation);
        params.put("start_latitute", LocationUpdatesService.mLocation.getLatitude());
        params.put("start_longitute", LocationUpdatesService.mLocation.getLongitude());
        params.put("end_latitute", serviceModel.getAccountLat());
        params.put("end_longitute", serviceModel.getAccountLng());

        new DeliveryWs().acceptDelivery(this,"delivery", params, new DeliveryWs.OnAcceptDeliveryListener() {
            @Override
            public void onLoadListSuccess(List<Delivery> deliveryList) {
            }

            @Override
            public void onAcceptDeliverySuccess(Waypoint waypoint) {
                Intent data = new Intent();
                data.putExtra("success", true);
                data.putExtra("serviceModel", serviceModel);
                data.putExtra("waypoint",waypoint);
                String waypointSave = new Gson().toJson(waypoint);
                Util.saveString("waypointSave", waypointSave, ServiceArrivalActivity.this);
                setResult(RESULT_OK, data);
                dialog.onDismiss();
               finish();

            }

            @Override
            public void onAcceptSuccessTaxi(Waypoint waypoint) {

            }

            @Override
            public void onAcceptFailed(String message) {
                if (!isConnectedToInternet (ServiceArrivalActivity.this)){
                    popupMessage ( null, getString(R.string.check_yor) , ServiceArrivalActivity.this);
                    dialog.onDismiss();
                } else {
                    Toast.makeText(ServiceArrivalActivity.this,message,Toast.LENGTH_LONG).show();
                }
                acceptBtn.toggleState();
                dialog.onDismiss();
                finish();
            }
        });
    }

    private void removeNotification() {
        NotificationManager manager = (NotificationManager) ServiceArrivalActivity.this.getSystemService(Context.NOTIFICATION_SERVICE);
        if (manager != null) {
            manager.cancelAll();
        }
    }

    @Override
    public void onBackPressed() {
        MusicControl.countNotification -= 1;
        MusicControl.getInstance(ServiceArrivalActivity.this).stopMusic();
        MyFirebaseMessagingService.orderId = 0;
        if(MusicControl.countNotification != 0 ) {
            Intent intent = new Intent();
            if (!MainActivity.serviceModels.isEmpty()) {
                for (int i = 0; i < MainActivity.serviceModels.size(); i++) {
                    if (serviceModel.getOrderId() == MainActivity.serviceModels.get(i).getOrderId()) {
                        MainActivity.serviceModels.remove(i);
                        if (!MainActivity.serviceModels.isEmpty()) {
                            intent.putExtra("serviceModel", MainActivity.serviceModels.get(0));
                            break;
                        }
                    }
                }
            }
            setResult(RESULT_OK,intent);
        }
        finish();
    }

    private void registerReceiver(int orderId, Activity activity){
        exitScreenReceiver = new ExitScreenReceiver(orderId, activity);
        LocalBroadcastManager.getInstance(this).registerReceiver(exitScreenReceiver, new IntentFilter(MyFirebaseMessagingService.ACTION_CANCEL));
    }

    private void unRegisterReceiver(){
        LocalBroadcastManager.getInstance(this).unregisterReceiver(exitScreenReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unRegisterReceiver();
    }
}
