package com.station29.mealtempledelivery.ui.boarding;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.station29.mealtempledelivery.data.model.OnBoardingScreen;

import java.util.ArrayList;
import java.util.List;

public class BoardingAdapter extends FragmentPagerAdapter {

    private List<OnBoardingScreen> lists = new ArrayList<>();
    private final String lang;

    public BoardingAdapter(@NonNull FragmentManager fm, List<OnBoardingScreen> lists,String lang) {
        super(fm, lists.size());
        this.lists = lists;
        this.lang = lang;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        OnBoardingScreen item = lists.get(position);
        if(item != null){
            return BoardingFragment.newInstance(item.getImage(),item.getTitle(),item.getText(),position,lang);
        }else {
            return null;
        }
    }

    @Override
    public int getCount() {
        return lists.size();
    }
}
