package com.station29.mealtempledelivery.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Wallets implements Serializable {

    @SerializedName("id")
    @Expose
    private final int walletId;
    @SerializedName("balance")
    @Expose
    private String balance;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("type")
    @Expose
    private String defaultType;
    @SerializedName("default")
    @Expose
    private int defaultPayment;

    public Wallets(int walletId, String balance, String currency) {
        this.balance = balance;
        this.currency = currency;
        this.walletId = walletId;
    }

    public Wallets(int walletId , int defaultPayment) {
        this.walletId = walletId;
        this.defaultPayment = defaultPayment;
    }

    public String getDefaultType() {
        return defaultType;
    }

    public int getDefaultPayment() {
        return defaultPayment;
    }

    public int getWalletId() {
        return walletId;
    }

    public String getBalance() {
        return balance;
    }

    public String getCurrency() {
        return currency;
    }
}
