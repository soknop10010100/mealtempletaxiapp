package com.station29.mealtempledelivery.ui.setting.adapter;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.utils.Util;

import java.util.List;

public class ListAdapter extends ArrayAdapter<String> {

    private final List<String> langs;
    private final int padding;
    private final int checkPos;

    public ListAdapter(@NonNull Context context, int resource, @NonNull List<String> objects, int checkPosition) {
        super(context, resource, objects);
        langs = objects;
        padding = Util.dpToPx(context, 16);
        checkPos = checkPosition;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        TextView textView = new TextView(parent.getContext());
        textView.setTextSize(16);
        textView.setGravity(Gravity.CENTER_VERTICAL);
        textView.setPadding(padding * 3 / 2, padding, padding * 3 / 2, padding);
        if (checkPos == position) {
            textView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_baseline_check_18, 0);
        }
        textView.setText(langs.get(position));

        return textView;
    }
}
