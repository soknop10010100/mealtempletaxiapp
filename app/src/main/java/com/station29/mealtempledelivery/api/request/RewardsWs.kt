package com.station29.mealtempledelivery.api.request

import android.app.Activity
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.station29.mealtempledelivery.api.Constant

import com.station29.mealtempledelivery.api.CustomCallback
import com.station29.mealtempledelivery.api.CustomResponseListener
import com.station29.mealtempledelivery.api.RetrofitGenerator
import com.station29.mealtempledelivery.data.model.RedeemExchange
import com.station29.mealtempledelivery.data.model.Rewards
import com.station29.mealtempledelivery.data.model.Season

class RewardsWs {

    fun getListRewards(activity: Activity, callBack: RewardSCallBack){
        val serviceAPI = RetrofitGenerator.createServiceWithAuth(activity)
        val res = serviceAPI.rewards
        res.enqueue(CustomCallback(activity, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject?) {
                if (resObj != null) {
                    if (resObj.has("data")) {
                        val listRewards = ArrayList<Rewards>()
                        var total  = 0
                        if(resObj.has("data")){
                            total = resObj.get("data").asJsonObject.get("total_reward").asInt
                            val jsonArray = resObj["data"].asJsonObject.get("data").asJsonArray
                            for (i in 0 until jsonArray.size()) {
                                val ele = jsonArray[i]
                                val rewards = Gson().fromJson(ele, Rewards::class.java)
                                listRewards.add(rewards)
                            }
                            if (!resObj["data"].asJsonObject.get("season").isJsonNull){
                                val season = Gson().fromJson(resObj["data"].asJsonObject.get("season").asJsonObject, Season::class.java)
                                callBack.onLoadingSuccessFully(season,listRewards,total)
                            } else {
                                callBack.onLoadingSuccessFully(season = null,listRewards,total)
                            }
                        }
                    }
                }
            }

            override fun onError(error: String?, code : Int) {
                callBack.onLoadingFail(error)
            }
        }))
    }

    fun getListRedeems(activity: Activity, callBack: RedeemsCallBack){
        val serviceAPI = RetrofitGenerator.createServiceWithAuth(activity)
        val res = serviceAPI.redeems
        res.enqueue(CustomCallback(activity, object : CustomResponseListener{
            override fun onSuccess(resObj: JsonObject?) {
                if (resObj != null) {
                    if (resObj.has("data")) {
                        val listRedeem = ArrayList<RedeemExchange>()
                        var total  = 0
                        if(resObj.has("data")){
                            val jsonArray = resObj["data"].asJsonObject.get("data").asJsonArray
                            for (i in 0 until jsonArray.size()) {
                                val ele = jsonArray[i]
                                val redeem = Gson().fromJson(ele, RedeemExchange::class.java)
                                listRedeem.add(redeem)
                            }
                        }
                        callBack.onLoadingSuccessFully(listRedeem)
                    }
                }
            }

            override fun onError(error: String?, code : Int) {
                callBack.onLoadingFail(error)
            }
        }))
    }

    fun ecChangeRewards(activity: Activity,hashMap: HashMap<String,Any?>, callBack: RedeemsExchangeCallBack){
        val serviceAPI = RetrofitGenerator.createServiceWithAuth(activity)
        val res = serviceAPI.exChangeRewards(hashMap)
        res.enqueue(CustomCallback(activity, object : CustomResponseListener{
            override fun onSuccess(resObj: JsonObject?) {
                if (resObj != null) {
                    if (resObj.has("data")) {
                        val ob = resObj.get("data").asJsonObject
                        if(ob.has("message")){
                            val message = ob.get("message").asString
                            var text = ""
                            text = if(Constant.serviceLanguage.containsKey(message)){
                                Constant.serviceLanguage[message].toString()
                            }else{
                                message
                            }
                            callBack.onLoadingSuccessFully(text)
                        }
                    }
                }
            }

            override fun onError(error: String? , code : Int) {
                callBack.onLoadingFail(error)
            }
        }))
    }


    interface RedeemsCallBack {
        fun onLoadingSuccessFully( listRedeem: ArrayList<RedeemExchange>)
        fun onLoadingFail(message: String?)
    }

    interface RewardSCallBack {
        fun onLoadingSuccessFully(season : Season? ,listRewards: ArrayList<Rewards>,total: Int)
        fun onLoadingFail(message: String?)
    }

    interface RedeemsExchangeCallBack {
        fun onLoadingSuccessFully(message: String?)
        fun onLoadingFail(message: String?)
    }
}