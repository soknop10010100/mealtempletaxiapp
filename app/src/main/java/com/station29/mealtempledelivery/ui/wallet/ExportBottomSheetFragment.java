package com.station29.mealtempledelivery.ui.wallet;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.utils.DateUtil;


import java.util.ArrayList;
import java.util.List;

public class ExportBottomSheetFragment extends BottomSheetDialogFragment{

    private View view;
    private ImageView img_cancel;
    private Button btnExport;
    private String action="";
    private List<Integer> list = new ArrayList<>();
    private TextView[] t = new TextView[7];
    private OnClickButton onClickButton;

    public static ExportBottomSheetFragment newInstance(){
        ExportBottomSheetFragment exportBottomSheetFragment = new ExportBottomSheetFragment();
        exportBottomSheetFragment.setArguments(new Bundle());
        return exportBottomSheetFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.export_transaction_dialog_fragment,container,false);

        initView();
        initAction();

        return view;
    }

    public void initView(){

        img_cancel = view.findViewById(R.id.closeButton);
        btnExport = view.findViewById(R.id.btn_export);

        t[0] = view.findViewById(R.id.txtToday);
        t[1] = view.findViewById(R.id.txtYtd);
        t[2] = view.findViewById(R.id.txtThisWeek);
        t[3] = view.findViewById(R.id.txtLastWeek);
        t[4] = view.findViewById(R.id.txtThisMonth);
        t[5] = view.findViewById(R.id.txtLastMonth);
        t[6] = view.findViewById(R.id.txtCustom);

        list = new ArrayList<>();

        for(int j=0;j<7;j++){
            list.add(t[j].getId());
        }
    }

    public void initAction(){

        img_cancel.setOnClickListener(view -> dismiss());

        t[0].setOnClickListener(view -> {
            changeBackgroundAndTextColor(t[0].getId());
            action = "today";
        });

        t[1].setOnClickListener(view -> {
            changeBackgroundAndTextColor(t[1].getId());
            action = "yesterday";
        });

        t[2].setOnClickListener(view -> {
            changeBackgroundAndTextColor(t[2].getId());
            action = "thisWeek";
        });

        t[3].setOnClickListener(view -> {
            changeBackgroundAndTextColor(t[3].getId());
            action = "lastWeek";
        });

        t[4].setOnClickListener(view -> {
            changeBackgroundAndTextColor(t[4].getId());
            action = "thisMonth";
        });

        t[5].setOnClickListener(view -> {
            changeBackgroundAndTextColor(t[5].getId());
            action = "lastMonth";
        });

        t[6].setOnClickListener(view -> {
            changeBackgroundAndTextColor(t[6].getId());
            action = "custom";
            onClickButton.onClickCustom(t[6],btnExport);
        });

        btnExport.setOnClickListener(view -> {
            switch (action) {
                case "today":
                    onClickButton.onClickExport(DateUtil.getTodayDate(), DateUtil.getTodayDate(), action);
                    break;
                case "yesterday":
                    onClickButton.onClickExport(DateUtil.getYesterdayDate(), DateUtil.getYesterdayDate(), action);
                    break;
                case "thisWeek":
                    onClickButton.onClickExport(DateUtil.getStartWeekDate(), DateUtil.getEndWeekDate(), action);
                    break;
                case "lastWeek":
                    onClickButton.onClickExport(DateUtil.getStartLastWeekDate(), DateUtil.getEndLastWeekDate(), action);
                    break;
                case "thisMonth":
                    onClickButton.onClickExport(DateUtil.getStartMonthDate(), DateUtil.getEndMonthDate(), action);
                    break;
                case "lastMonth":
                    onClickButton.onClickExport(DateUtil.getStartLastMonthDate(), DateUtil.getEndLastMonthDate(), action);
                    break;
                case "custom":
                    onClickButton.onClickExport("", "", action);
                    break;
            }
            dismiss();
        });
    }

    public void changeBackgroundAndTextColor(int id){
        for(int i=0;i<list.size();i++){
            if(id == list.get(i)){
                t[i].setBackgroundColor(getResources().getColor(R.color.grayLight));
                t[i].setTextColor(Color.WHITE);
            }else{
                t[i].setTextColor(getResources().getColor(R.color.colorPrimary));
                t[i].setBackgroundColor(0);
            }
        }
    }

    interface OnClickButton{
        void onClickExport(String startDate, String endDate, String action);
        void onClickCustom(TextView customDate ,Button btnExport);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Fragment fragment = getParentFragment();
        if(fragment!=null){
            onClickButton = (OnClickButton) fragment;
        }else{
            onClickButton = (OnClickButton)  context;
        }
    }
}
