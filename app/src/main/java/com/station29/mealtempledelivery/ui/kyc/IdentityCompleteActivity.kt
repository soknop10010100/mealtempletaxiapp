package com.station29.mealtempledelivery.ui.kyc

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.activity.result.ActivityResult
import com.station29.mealtempledelivery.R
import com.station29.mealtempledelivery.api.request.ProfileWs
import com.station29.mealtempledelivery.data.model.User

import com.station29.mealtempledelivery.ui.base.BaseActivity

class IdentityCompleteActivity : BaseActivity() {

    private lateinit var progressBar : View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_identity_complete)

        progressBar = findViewById(R.id.progress_bar)

        findViewById<Button>(R.id.btnFinish).setOnClickListener {
            val intent = Intent(this@IdentityCompleteActivity,WaitingVerifyKycActivity::class.java)
            activityLauncher.launch(intent) { result: ActivityResult ->
                if (result.resultCode == RESULT_OK) {
                    setResult(RESULT_OK)
                    finish()
                }
            }
        }
    }

    private fun viewProfile () {
        progressBar.visibility = View.VISIBLE
        ProfileWs().viewProfiles(this@IdentityCompleteActivity , object : ProfileWs.ProfileViewAndUpdate {
            override fun onLoadProfileSuccess(user: User?, message: String?) {
                progressBar.visibility = View.GONE
                if (user!!.kyc == null || user.kyc != null && user.kyc.status == "REJECT") {
                    val intent = Intent(this@IdentityCompleteActivity, AskToCompleteKycActivity::class.java)
                    startActivity(intent)
                } else if (user.kyc != null && user.kyc.status == "REVIEW") {
                    val intent = Intent(this@IdentityCompleteActivity,WaitingVerifyKycActivity::class.java)
                    activityLauncher.launch(intent) { result: ActivityResult ->
                        if (result.resultCode == RESULT_OK) {
                            setResult(RESULT_OK)
                            finish()
                        }
                    }
                }
            }

            override fun onError(errorMessage: String?) {
                progressBar.visibility = View.GONE
            }

            override fun onUpdateProfilesSuccess(message: String?) {
                progressBar.visibility = View.GONE
            }
        })
    }

    override fun onBackPressed() {
        viewProfile()
    }

}