package com.station29.mealtempledelivery.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ServiceModel implements Serializable {

    @SerializedName("order_id")
    @Expose
    private int orderId;

    @SerializedName("delivery_id")
    @Expose
    private int deliveryId;

    @SerializedName("delivery_time")
    @Expose
    private String deliveryTime;
    @SerializedName("account_name")
    @Expose
    private String accountName;
    @SerializedName("account_phone")
    @Expose
    private String accountContact;
    @SerializedName("account_lat")
    @Expose
    private double accountLat;
    @SerializedName("account_lng")
    @Expose
    private double accountLng;
    @SerializedName("client_phone")
    @Expose
    private String clientPhone;
    @SerializedName("client_lat")
    @Expose
    private double clientLat;
    @SerializedName("client_lng")
    @Expose
    private double clientLng;
    @SerializedName("dropoff_address")
    @Expose
    private String dropOffAddress;
    @SerializedName("product_price_after_discount_vat")
    @Expose
    private double productPriceAfterDiscountVat;
    @SerializedName("rider_tip")
    @Expose
    private float riderTip;
    @SerializedName("delivery_fee")
    @Expose
    private float deliveryFee;
    @SerializedName("total")
    @Expose
    private double total;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("payment_type")
    @Expose
    private String paymentType;
    @SerializedName("display")
    @Expose
    private String display;
    @SerializedName("type_order")
    @Expose
    private String typeOrder;
    @SerializedName("cooperation")
    @Expose
    private Boolean cooperation;

    @SerializedName("cooperation_logo")
    @Expose
    private String cooperationLogo;
    @SerializedName("cooperation_name")
    @Expose
    private String cooperationName;

    @SerializedName("cooperation_order_id")
    @Expose
    private String cooperationOrderId;

    public String getOrderNumber() {
        return orderNumber;
    }

    @SerializedName("order_number")
    @Expose
    private String orderNumber;

    public String getFree_delivery() {
        return free_delivery;
    }

    @SerializedName("free_delivery_fee")
    @Expose
    private String free_delivery;

    public String getDropOffAddress() {

        return dropOffAddress;
    }

    public String getCooperationOrderId() {
        return cooperationOrderId;
    }

    public String getCooperationLogo() {
        return cooperationLogo;
    }

    public void setCooperationLogo(String cooperationLogo) {
        this.cooperationLogo = cooperationLogo;
    }

    public String getCooperationName() {
        return cooperationName;
    }

    public void setCooperationName(String cooperationName) {
        this.cooperationName = cooperationName;
    }

//    @SerializedName("taxs")
//    @Expose
//    private List<Tax> taxList;
//
//    @SerializedName("surcharges")
//    @Expose
//    private List<SurCharge> surChargeList;


    public String getTypeOrder() {
        return typeOrder;
    }

    public int getOrderId() {
        return orderId;
    }

    public Boolean getCooperation() {
        return cooperation;
    }
    public void setCooperation(Boolean cooperation) {
        this.cooperation = cooperation;
    }
    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public String getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(String deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getAccountContact() {
        return accountContact;
    }

    public void setAccountContact(String accountContact) {
        this.accountContact = accountContact;
    }

    public double getAccountLat() {
        return accountLat;
    }

    public void setAccountLat(double accountLat) {
        this.accountLat = accountLat;
    }

    public double getAccountLng() {
        return accountLng;
    }

    public void setAccountLng(double accountLng) {
        this.accountLng = accountLng;
    }

    public String getClientPhone() {
        return clientPhone;
    }

    public void setClientPhone(String clientPhone) {
        this.clientPhone = clientPhone;
    }

    public double getClientLat() {
        return clientLat;
    }

    public void setClientLat(double clientLat) {
        this.clientLat = clientLat;
    }

    public double getClientLng() {
        return clientLng;
    }


    public void setClientLng(double clientLng) {
        this.clientLng = clientLng;
    }

    public double getProductPriceAfterDiscountVat() {
        return productPriceAfterDiscountVat;
    }

    public void setProductPriceAfterDiscountVat(float productPriceAfterDiscountVat) {
        this.productPriceAfterDiscountVat = productPriceAfterDiscountVat;
    }

    public float getRiderTip() {
        return riderTip;
    }

    public void setRiderTip(float riderTip) {
        this.riderTip = riderTip;
    }

    public float getDeliveryFee() {
        return deliveryFee;
    }

    public void setDeliveryFee(float deliveryFee) {
        this.deliveryFee = deliveryFee;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public int getDeliveryId() {
        return deliveryId;
    }

    public void setDeliveryId(int deliveryId) {
        this.deliveryId = deliveryId;
    }


    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

//    public List<Tax> getTaxList() {
//        return taxList;
//    }
//
//    public List<SurCharge> getSurChargeList() {
//        return surChargeList;
//    }
}

