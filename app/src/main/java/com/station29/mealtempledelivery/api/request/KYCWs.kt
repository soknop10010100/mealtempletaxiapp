package com.station29.mealtempledelivery.api.request

import android.app.Activity
import android.util.Log
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.station29.mealtempledelivery.api.CustomCallback
import com.station29.mealtempledelivery.api.CustomResponseListener
import com.station29.mealtempledelivery.api.RetrofitGenerator
import com.station29.mealtempledelivery.utils.Util
import id.zelory.compressor.Compressor
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File
import java.io.IOException
import kotlin.collections.HashMap

class KYCWs {

    fun uploadKYC(context: Activity?, action : String ,signUpParam : HashMap<String,Any> , onCallBack : OnCallBackUploadKYC ) {

        val requestBody = MultipartBody.Builder()
        requestBody.setType(MultipartBody.FORM)
        for (key in signUpParam.keys) {
            if ("identity_type" == key) {
                requestBody.addFormDataPart(key, signUpParam[key].toString())
            } else {
                val path: String = signUpParam[key].toString()
                // Compress file before upload
                var fNew: File? = null
                try {
                    if (path != "") fNew = Compressor(context).compressToFile(File(path))
                    Log.d("filesize", fNew!!.length().toString() + " " + fNew.totalSpace + "s")
                } catch (e: IOException) {
                    e.printStackTrace()
                    Log.d(e.javaClass.name, e.message + "")
                }
                if (fNew == null)
                    fNew = File(path)
                val file = File(path)
                val bodyBye = file.asRequestBody("multipart/form-data".toMediaType())
                requestBody.addFormDataPart(key, Util.getFileName(fNew.path), bodyBye)
            }
        }
        val map = Gson().toJson(signUpParam)
        Util.logDebug("maqwep", map)

        val serviceAPI = RetrofitGenerator.createServiceWithAuth(context)
        var res = serviceAPI.uploadKYC(requestBody.build())
        if (action == "update") {
            res = serviceAPI.updateKYC(requestBody.build())
        }
        res.enqueue(CustomCallback(context, object : CustomResponseListener {
            override fun onSuccess(resObj: JsonObject) {
                if (resObj.has("message")) {
                    onCallBack.onSuccess("Success")
                }
            }
            override fun onError(error: String, resCode: Int) {
                onCallBack.onFailed(error)
            }
        }))
    }

    interface OnCallBackUploadKYC {
        fun onSuccess(message: String)
        fun onFailed(message : String)
    }

}