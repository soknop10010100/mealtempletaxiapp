package com.station29.mealtempledelivery.ui.kyc

import android.app.NotificationManager
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import androidx.activity.result.ActivityResult
import androidx.appcompat.app.AlertDialog
import com.google.firebase.auth.FirebaseAuth
import com.station29.mealtempledelivery.R
import com.station29.mealtempledelivery.api.Constant
import com.station29.mealtempledelivery.api.request.LogoutWs
import com.station29.mealtempledelivery.api.request.LogoutWs.LogoutCallback
import com.station29.mealtempledelivery.api.request.MockupData
import com.station29.mealtempledelivery.ui.base.BaseActivity
import com.station29.mealtempledelivery.ui.base.MusicControl
import com.station29.mealtempledelivery.ui.login.LoginActivity
import com.station29.mealtempledelivery.utils.Util
import java.util.*

class AskToCompleteKycActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ask_to_complete_kyc)

        findViewById<ImageView>(R.id.btnBack).setOnClickListener {
            val builder = AlertDialog.Builder(this@AskToCompleteKycActivity, R.style.DialogTheme)
            builder.setTitle(R.string.logout)
            builder.setMessage(R.string.logout_inform)
            builder.setPositiveButton(R.string.logout) { dialog, which ->
                logout()
            }
            builder.setNegativeButton(getString(R.string.cancel), null).show()
        }

        findViewById<Button>(R.id.btnVerify).setOnClickListener {
            val intent = Intent(this@AskToCompleteKycActivity, IdentityVerificationActivity::class.java)
            activityLauncher.launch(intent) { result: ActivityResult ->
                if (result.resultCode == RESULT_OK) {
                    setResult(RESULT_OK)
                    finish()
                }
            }
        }

    }

    override fun onBackPressed() {

    }

    private fun logout() {
        findViewById<View>(R.id.progress).visibility = View.VISIBLE
        val deviceTypes = HashMap<String, String>()
        deviceTypes["device_type"] = "ANDROID"
        val topic = Util.getString("topic", this@AskToCompleteKycActivity)
        val deviceToken = Util.getString("firebase_token", this@AskToCompleteKycActivity)
        LogoutWs().logOut(this@AskToCompleteKycActivity, deviceTypes, deviceToken, topic, object : LogoutCallback {
            override fun onLoadSuccess(message: String) {
                findViewById<View>(R.id.progress).visibility = View.GONE
                MusicControl.countNotification -= 1
                MusicControl.getInstance(this@AskToCompleteKycActivity).stopMusic()
                removeNotification()
                FirebaseAuth.getInstance().signOut()
                val bundle = Bundle()
                bundle.putString(
                    Constant.USER_ID,
                    Util.getString("user_id", this@AskToCompleteKycActivity) + "was Logout at " + Calendar.getInstance()
                )
                mFirebaseAnalytics.logEvent(this.javaClass.simpleName, bundle)
                val intent = Intent(this@AskToCompleteKycActivity, LoginActivity::class.java)
                Util.saveString("token", "", this@AskToCompleteKycActivity)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
            }

            override fun onLoadFailed(errorMessage: String) {
                findViewById<View>(R.id.progress).visibility = View.GONE
                Util.popupMessage(null, errorMessage, this@AskToCompleteKycActivity)
            }
        })
    }

    private fun removeNotification() {
        val manager = this@AskToCompleteKycActivity.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        manager.cancelAll()
    }

}