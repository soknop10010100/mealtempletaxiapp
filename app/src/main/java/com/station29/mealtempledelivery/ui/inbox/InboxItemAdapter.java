package com.station29.mealtempledelivery.ui.inbox;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.api.Constant;
import com.station29.mealtempledelivery.data.model.Inbox;

import java.util.List;
import java.util.Locale;

public class InboxItemAdapter extends RecyclerView.Adapter<InboxItemAdapter.ViewHolder> {

    public List<Inbox> listInbox;
    public OnClickItemInbox itemInbox;
    private int i =-1;

    public InboxItemAdapter(List<Inbox> listInbox,OnClickItemInbox itemInbox,int i) {
        this.listInbox = listInbox;
        this.itemInbox = itemInbox;
        this.i = i;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_inbox_item_adapter, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("WrongConstant")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Inbox list = listInbox.get(position);
        if(list!=null){
            if(Constant.serviceLanguage.containsKey(list.getNotificationTranslate())){
                holder.titleTV.setText(String.format(Locale.US,"%s",Constant.serviceLanguage.get(list.getNotificationTranslate())));
            }else {
                holder.titleTV.setText(String.format(Locale.US,"%s",list.getTitle()));
            }

            holder.bodyTv.setText(String.format(Locale.US,"%s",list.getDescription()));

            holder.timeTv.setText(String.format(Locale.US,"%s",list.getSendTime()));

            holder.itemView.setOnClickListener(view -> itemInbox.onLoadItemClick(list,i));

            if (listInbox.size() - 1 == position){
                holder.viewLine.setVisibility(View.GONE);
            } else {
                holder.viewLine.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return listInbox.size();
    }

    public interface OnClickItemInbox{
        void onLoadItemClick(Inbox list,int i);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView titleTV,bodyTv,timeTv ;
        View viewLine;

        public ViewHolder(View view) {
            super(view);
            timeTv = view.findViewById(R.id.time_text);
            titleTV  = view.findViewById(R.id.title_text);
            bodyTv = view.findViewById(R.id.body_text);
            viewLine = view.findViewById(R.id.viewLine);

        }
    }

    public void clear() {
        int size = listInbox.size ();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                listInbox.remove ( 0 );
            }
            notifyItemRangeRemoved ( 0, size );
        }
    }
}