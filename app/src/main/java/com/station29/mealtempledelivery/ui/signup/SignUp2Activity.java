package com.station29.mealtempledelivery.ui.signup;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.bumptech.glide.Glide;
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog;
import com.google.android.material.textfield.TextInputEditText;
import com.squareup.picasso.Picasso;
import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.api.request.MockupData;
import com.station29.mealtempledelivery.api.request.SignUpWs;
import com.station29.mealtempledelivery.data.model.User;
import com.station29.mealtempledelivery.ui.SplashScreenActivity;
import com.station29.mealtempledelivery.ui.base.BaseActivity;
import com.station29.mealtempledelivery.ui.base.BaseCameraActivity;
import com.station29.mealtempledelivery.ui.home.WaitingDialog;
import com.station29.mealtempledelivery.ui.login.VerifyCodeActivity;
import com.station29.mealtempledelivery.utils.FileUtil;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

public class SignUp2Activity extends BaseActivity {

    private Spinner vehicleColorListSpinner;
    private TextView birthDayTv;
    private TextInputEditText platNumberEt;
    private ImageView userProleImg, frontImg, backImg, vehicleImg;
    private Button submitBtn;
    private WaitingDialog mDialog;

    private final String driverPhotoAction = "driver-photo";
    private final String driverLicenceFront = "driver-licence-front";
    private final String driverLicenceBack = "driver-licence-back";
    private final String driverVehiclePhoto = "vehicle-photo";
    private String color;
    private String imgProfilePath;
    private String imgLicenceFrontPath;
    private String imgLicenceBackPath;
    private String imgVehiclePhotoPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rigister);

        mDialog = new WaitingDialog(this, null);
        birthDayTv = findViewById(R.id.service_date);
        vehicleColorListSpinner = findViewById(R.id.service_color);
        RelativeLayout driverPhoto = findViewById(R.id.driver_photo);
        RelativeLayout licencePhotoFront = findViewById(R.id.driving_licence_front);
        RelativeLayout licencePhotoBack = findViewById(R.id.driving_licence_back);
        RelativeLayout vehiclePhoto = findViewById(R.id.vehicle_photo);
        userProleImg = findViewById(R.id.user_profile_im);
        frontImg = findViewById(R.id.image_front);
        backImg = findViewById(R.id.image_back);
        vehicleImg = findViewById(R.id.image_ve);
        ImageView back = findViewById(R.id.btnupback);
        platNumberEt = findViewById(R.id.txt_plate);
        submitBtn = findViewById(R.id.submitSigUp);

        submitBtn.setOnClickListener(view -> {
            view.setEnabled(false);
            view.postDelayed(() -> view.setEnabled(true),1000);
            doSigUp();
        });

        back.setOnClickListener(v -> {
            MockupData.setAccount(getSignUpParam());
            finish();
        });

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        String dat = df.format(Calendar.getInstance().getTime());
        birthDayTv.setText(dat);

        birthDayTv.setOnClickListener(v -> showDateTimePicker());

        driverPhoto.setOnClickListener(v -> {
            if (BaseActivity.countryCode != null) {
                showPictureDialog(driverPhotoAction);
            }
        });

        licencePhotoFront.setOnClickListener(v -> {
            if (BaseActivity.countryCode != null)
                showPictureDialog(driverLicenceFront);
        });

        licencePhotoBack.setOnClickListener(v -> {
            if (BaseActivity.countryCode != null)
                showPictureDialog(driverLicenceBack);
        });

        vehiclePhoto.setOnClickListener(v -> {
            if (BaseActivity.countryCode != null)
                showPictureDialog(driverVehiclePhoto);
        });

        getColors();

        if (MockupData.getAccount() != null) {
            initOldData();
        }
    }

    private void getColors() {
        ArrayAdapter<String> serviceAdapter = new ArrayAdapter<>(SignUp2Activity.this, android.R.layout.simple_spinner_item, SplashScreenActivity.splashConfig.getColors());
        vehicleColorListSpinner.setAdapter(serviceAdapter);
        for (int i = 0; i < SplashScreenActivity.splashConfig.getColors().size(); i++) {
            if (SplashScreenActivity.splashConfig.getColors().get(i).equals(color)) {
                vehicleColorListSpinner.setSelection(i);
                break;
            }
        }

    }

    public void showDateTimePicker() {
        new SingleDateAndTimePickerDialog.Builder(this)
                .bottomSheet()
                .curved()
                .displayMinutes(false)
                .displayHours(false)
                .displayDays(false)
                .displayMonth(true)
                .displayYears(true)
                .displayDaysOfMonth(true)
                .listener(date -> {
                    DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                    String dat = df.format(date);
                    birthDayTv.setText(dat);
                })
                .display();
    }

    private void showPictureDialog(String action) {
        Intent intent = new Intent(SignUp2Activity.this, BaseCameraActivity.class);
        intent.putExtra("action",action);
        activityLauncher.launch(intent, result -> {
            if (result.getResultCode() == Activity.RESULT_OK) {
                if (result.getData() != null){
                    if (result.getData().hasExtra("image_path")) {
                        String imagePath = result.getData().getStringExtra("image_path");
                        Uri uri = Uri.parse(imagePath);
                        File actualFile = new File(uri.getPath());

                        switch (action) {
                            case driverLicenceBack:
                                imgLicenceBackPath = actualFile.getAbsolutePath();
                                Glide.with(SignUp2Activity.this).load(imgLicenceBackPath).into(backImg);
                                break;
                            case driverPhotoAction:
                                imgProfilePath = actualFile.getAbsolutePath();
                                Glide.with(SignUp2Activity.this).load(imgProfilePath).into(userProleImg);
                                break;
                            case driverLicenceFront:
                                imgLicenceFrontPath = actualFile.getAbsolutePath();
                                Glide.with(SignUp2Activity.this).load(imgLicenceFrontPath).into(frontImg);
                                break;
                            case driverVehiclePhoto:
                                imgVehiclePhotoPath = actualFile.getAbsolutePath();
                                Glide.with(SignUp2Activity.this).load(imgVehiclePhotoPath).into(vehicleImg);
                                break;
                        }

                    }

                    if (result.getData().hasExtra("select_image")) {
                        String imagePath = result.getData().getStringExtra("select_image");
                        Uri uri = Uri.parse(imagePath);
                        File actualFile = FileUtil.from(this, uri);

                        switch (action) {
                            case driverLicenceBack:
                                imgLicenceBackPath = actualFile.getAbsolutePath();
                                Picasso.get().load(actualFile).into(backImg);
                                break;
                            case driverPhotoAction:
                                imgProfilePath = actualFile.getAbsolutePath();
                                Picasso.get().load(actualFile).into(userProleImg);
                                break;
                            case driverLicenceFront:
                                imgLicenceFrontPath = actualFile.getAbsolutePath();
                                Picasso.get().load(actualFile).into(frontImg);
                                break;
                            case driverVehiclePhoto:
                                imgVehiclePhotoPath = actualFile.getAbsolutePath();
                                Picasso.get().load(actualFile).into(vehicleImg);
                                break;
                        }
                    }
                }
            }
        });

    }

    private HashMap<String, Object> getSignUpParam() {
        // Get data from 1st screen
        Intent data = getIntent();
        String firstName = data.getStringExtra("firstname");
        String lastName = data.getStringExtra("lastname");
        String phone = data.getStringExtra("phone");
        String code = data.getStringExtra("code");
        String phoneCode = data.getStringExtra("phone_code");
        int serviceId = data.getIntExtra("service_id", -1);
        int serviceIdTaxi = data.getIntExtra("service_idTaxi", -1);
        String serviceName = data.getStringExtra("service");
        String password = data.getStringExtra("password");
        String function = data.getStringExtra("function");
        String userType = data.getStringExtra("user_type");
        String serviceIds="";
        if(serviceId == 0){
            serviceIds = String.valueOf(serviceIdTaxi);
        }else if(serviceIdTaxi == 0){
            serviceIds = String.valueOf(serviceId);
        }else {
            serviceIds = serviceId + ","+ serviceIdTaxi;
        }
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("code", code);
        hashMap.put("password", password);
        hashMap.put("phone", phone);
        hashMap.put("phone_code", phoneCode);
        //hashMap.put("service", serviceName);
        hashMap.put("service_ids",serviceIds);
       // hashMap.put("service_id", String.valueOf(serviceId));
        hashMap.put("firstname", firstName);
        hashMap.put("lastname", lastName);
        hashMap.put("function", function);
        hashMap.put("user_type", userType);

        if (vehicleColorListSpinner != null && vehicleColorListSpinner.getSelectedItem() != null)
            hashMap.put("color", vehicleColorListSpinner.getSelectedItem().toString());
        if (platNumberEt.getText() != null)
            hashMap.put("plate_number", platNumberEt.getText().toString());
        hashMap.put("date_of_birth", birthDayTv.getText().toString());
        if (imgProfilePath != null)
            hashMap.put("driver_photo", imgProfilePath);
        if (imgLicenceFrontPath != null)
            hashMap.put("licence_photo_front", imgLicenceFrontPath);
        if (imgLicenceBackPath != null)
            hashMap.put("licence_photo_back", imgLicenceBackPath);
        if (imgVehiclePhotoPath != null)
            hashMap.put("vehicle_photo", imgVehiclePhotoPath);
        MockupData.setAccount(hashMap);
        return hashMap;
    }

    private void doSigUp() {
        mDialog.show();
        new SignUpWs().accountSignUp(SignUp2Activity.this, getSignUpParam(), registerCallback);
    }

    private final SignUpWs.RequestServer registerCallback = new SignUpWs.RequestServer() {
        @Override
        public void onRequestSuccess() {
            mDialog.dismiss();
            submitBtn.setEnabled(true);
            Intent intent = new Intent(SignUp2Activity.this, VerifyCodeActivity.class);
            startActivity(intent);
        }

        @Override
        public void onRequestToken(String token, int userId, User user) {
        }

        @Override
        public void onRequestFailed(String message) {
            mDialog.dismiss();
            submitBtn.setEnabled(true);
            messAlert(message);

        }
    };

    private void messAlert(String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(SignUp2Activity.this,R.style.DialogTheme);
        builder.setMessage(message);
        builder.setNegativeButton(SignUp2Activity.this.getString(R.string.ok), null);
        builder.setPositiveButton(null,null);
        builder.show();
    }

    private void initOldData() {
        HashMap<String, Object> data = MockupData.getAccount();
        if (data == null) return;
        if (data.containsKey("plate_number")) {
            String plate = String.valueOf(data.get("plate_number"));
            platNumberEt.setText(plate);
        }
        if (data.containsKey("color")) {
            color = String.valueOf(data.get("color"));
        }
        if (data.containsKey("date_of_birth")) {
            String birthDay = String.valueOf(data.get("date_of_birth"));
            birthDayTv.setText(birthDay);
        }
        if (data.containsKey("driver_photo")) {
            imgProfilePath = String.valueOf(data.get("driver_photo"));
            Glide.with(SignUp2Activity.this).load(imgProfilePath).into(userProleImg);
        }
        if (data.containsKey("licence_photo_front")) {
            imgLicenceFrontPath = String.valueOf(data.get("licence_photo_front"));
            Glide.with(SignUp2Activity.this).load(imgLicenceFrontPath).into(frontImg);
        }
        if (data.containsKey("licence_photo_back")) {
            imgLicenceBackPath = String.valueOf(data.get("licence_photo_back"));
            Glide.with(SignUp2Activity.this).load(imgLicenceBackPath).into(backImg);
        }
        if (data.containsKey("vehicle_photo")) {
            imgVehiclePhotoPath = String.valueOf(data.get("vehicle_photo"));
            Glide.with(SignUp2Activity.this).load(imgVehiclePhotoPath).into(vehicleImg);
        }
    }

}