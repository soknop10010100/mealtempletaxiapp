package com.station29.mealtempledelivery.ui.boarding;

import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.tabs.TabLayout;
import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.api.Constant;
import com.station29.mealtempledelivery.api.request.MockupData;
import com.station29.mealtempledelivery.ui.base.BaseActivity;
import com.station29.mealtempledelivery.ui.login.LoginActivity;
import com.station29.mealtempledelivery.utils.Util;


public class BoardingActivity extends BaseActivity {

    private ViewPager viewPager;
    private MaterialButton btnContinue;
    private String lang;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_boarding);

        if(getIntent().hasExtra("lang")){
            lang = getIntent().getStringExtra("lang");
        }

        Util.changeLanguage(BoardingActivity.this,lang);
        viewPager = findViewById(R.id.viewpager);
        btnContinue = findViewById(R.id.btnContinue);
        final TabLayout tabLayout = findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager, true);
        btnContinue.setText(R.string.continues);

        BoardingAdapter boardingViewAdapter = new BoardingAdapter(getSupportFragmentManager(), MockupData.getOnBoardingScreen(BoardingActivity.this), lang);
        viewPager.setAdapter(boardingViewAdapter);

        btnContinue.setOnClickListener(v -> nextCurrentItem(viewPager.getCurrentItem(),true));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if(tab.getPosition()==3){
                    btnContinue.setText(R.string.i_got_it);
                    btnContinue.setOnClickListener(v -> {
                        Intent intent = new Intent(BoardingActivity.this,LoginActivity.class);
                        intent.putExtra("lang",lang);
                        startActivity(intent);
                        Constant.setStart("stop");
                        Util.start(BoardingActivity.this,"stop");
                        Util.changeLanguage(BoardingActivity.this,lang);
                    });
                } else {
                    btnContinue.setText(R.string.continus);
                    btnContinue.setOnClickListener(v -> nextCurrentItem(viewPager.getCurrentItem(),true));
                }
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    public void nextCurrentItem (int item,boolean t) {
        viewPager.setCurrentItem(item + 1 , t);
        if(item + 1 == 3){
            btnContinue.setText(R.string.i_got_it);
            btnContinue.setOnClickListener(v -> {
                Intent intent = new Intent(BoardingActivity.this,LoginActivity.class);
                intent.putExtra("lang",lang);
                startActivity(intent);
                Util.changeLanguage(BoardingActivity.this,lang);
            });
        }

    }

    @Override
    public void onBackPressed() {
        finishAffinity();
    }

}