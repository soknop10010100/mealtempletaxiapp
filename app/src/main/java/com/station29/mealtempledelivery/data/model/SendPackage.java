package com.station29.mealtempledelivery.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SendPackage implements Serializable {

    @SerializedName("unit")
    @Expose
    private String unit;
    @SerializedName("weight")
    @Expose
    private String weight;
    @SerializedName("rider_tip")
    @Expose
    private String riderTip;
    @SerializedName("cost_per_km")
    @Expose
    private String costPerKm;
    @SerializedName("name_sender")
    @Expose
    private String nameSender;
    @SerializedName("total_price")
    @Expose
    private int totalPrice;
    @SerializedName("delivery_fee")
    @Expose
    private int deliveryFee;
    @SerializedName("payment_type")
    @Expose
    private String paymentType;
    @SerializedName("phone_sender")
    @Expose
    private String phoneSender;
    @SerializedName("type_package")
    @Expose
    private String typePackage;
    @SerializedName("currency_code")
    @Expose
    private String currencyCode;
    @SerializedName("currency_sign")
    @Expose
    private String currencySign;
    @SerializedName("name_receiver")
    @Expose
    private String nameReceiver;
    @SerializedName("package_detail")
    @Expose
    private String packageDetail;
    @SerializedName("phone_receiver")
    @Expose
    private String phoneReceiver;
    @SerializedName("pickup_address")
    @Expose
    private String pickupAddress;
    @SerializedName("total_distance")
    @Expose
    private double totalDistance;
    @SerializedName("dropoff_address")
    @Expose
    private String dropoffAddress;
    @SerializedName("pickup_latitute")
    @Expose
    private double pickupLatitute;
    @SerializedName("dropoff_latitute")
    @Expose
    private double dropoffLatitute;
    @SerializedName("pickup_longitute")
    @Expose
    private double pickupLongitute;
    @SerializedName("dropoff_longitute")
    @Expose
    private double dropoffLongitute;
    @SerializedName("address_detail_sender")
    @Expose
    private String addressDetailSender;
    @SerializedName("address_detail_receiver")
    @Expose
    private String addressDetailReceiver;

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getRiderTip() {
        return riderTip;
    }

    public void setRiderTip(String riderTip) {
        this.riderTip = riderTip;
    }

    public String getCostPerKm() {
        return costPerKm;
    }

    public void setCostPerKm(String costPerKm) {
        this.costPerKm = costPerKm;
    }

    public String getNameSender() {
        return nameSender;
    }

    public void setNameSender(String nameSender) {
        this.nameSender = nameSender;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getDeliveryFee() {
        return deliveryFee;
    }

    public void setDeliveryFee(int deliveryFee) {
        this.deliveryFee = deliveryFee;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getPhoneSender() {
        return phoneSender;
    }

    public void setPhoneSender(String phoneSender) {
        this.phoneSender = phoneSender;
    }

    public String getTypePackage() {
        return typePackage;
    }

    public void setTypePackage(String typePackage) {
        this.typePackage = typePackage;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getCurrencySign() {
        return currencySign;
    }

    public void setCurrencySign(String currencySign) {
        this.currencySign = currencySign;
    }

    public String getNameReceiver() {
        return nameReceiver;
    }

    public void setNameReceiver(String nameReceiver) {
        this.nameReceiver = nameReceiver;
    }

    public String getPackageDetail() {
        return packageDetail;
    }

    public void setPackageDetail(String packageDetail) {
        this.packageDetail = packageDetail;
    }

    public String getPhoneReceiver() {
        return phoneReceiver;
    }

    public void setPhoneReceiver(String phoneReceiver) {
        this.phoneReceiver = phoneReceiver;
    }

    public String getPickupAddress() {
        return pickupAddress;
    }

    public void setPickupAddress(String pickupAddress) {
        this.pickupAddress = pickupAddress;
    }

    public double getTotalDistance() {
        return totalDistance;
    }

    public void setTotalDistance(double totalDistance) {
        this.totalDistance = totalDistance;
    }

    public String getDropoffAddress() {
        return dropoffAddress;
    }

    public void setDropoffAddress(String dropoffAddress) {
        this.dropoffAddress = dropoffAddress;
    }

    public double getPickupLatitute() {
        return pickupLatitute;
    }

    public void setPickupLatitute(double pickupLatitute) {
        this.pickupLatitute = pickupLatitute;
    }

    public double getDropoffLatitute() {
        return dropoffLatitute;
    }

    public void setDropoffLatitute(double dropoffLatitute) {
        this.dropoffLatitute = dropoffLatitute;
    }

    public double getPickupLongitute() {
        return pickupLongitute;
    }

    public void setPickupLongitute(double pickupLongitute) {
        this.pickupLongitute = pickupLongitute;
    }

    public double getDropoffLongitute() {
        return dropoffLongitute;
    }

    public void setDropoffLongitute(double dropoffLongitute) {
        this.dropoffLongitute = dropoffLongitute;
    }

    public String getAddressDetailSender() {
        return addressDetailSender;
    }

    public void setAddressDetailSender(String addressDetailSender) {
        this.addressDetailSender = addressDetailSender;
    }

    public String getAddressDetailReceiver() {
        return addressDetailReceiver;
    }

    public void setAddressDetailReceiver(String addressDetailReceiver) {
        this.addressDetailReceiver = addressDetailReceiver;
    }

}