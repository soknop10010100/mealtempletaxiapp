package com.station29.mealtempledelivery.ui.setting.payment

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.fragment.app.DialogFragment
import com.google.android.material.bottomsheet.BottomSheetBehavior.STATE_EXPANDED
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputEditText
import com.google.gson.Gson
import com.station29.mealtempledelivery.R
import com.station29.mealtempledelivery.data.model.PaymentService
import com.station29.mealtempledelivery.utils.Util
import com.szagurskii.patternedtextwatcher.PatternedTextWatcher

class VisaMasterCardDialogFragment :  BottomSheetDialogFragment() {

    private lateinit var cardNumber : TextInputEditText
    private lateinit var cardName : TextInputEditText
    private lateinit var cardDate : TextInputEditText
    private lateinit var cardCVC : TextInputEditText
    private lateinit var amount : TextInputEditText
    private var cardType : String = "001"
    private var action : String? = null
    private var onClickButton: OnClickButton? = null
    private var pspId = 0
    private var walletId = 0

    fun newInstance(action : String) : VisaMasterCardDialogFragment {
        val args = Bundle()
        args.putString("action",action)
        val fragment = VisaMasterCardDialogFragment()
        fragment.arguments = args
        return fragment
    }

    fun newInstance(action : String, paymentService: PaymentService, walletId : Int) : VisaMasterCardDialogFragment {
        val args = Bundle()
        args.putString("action",action)
        args.putSerializable("paymentService",paymentService)
        args.putInt("walletId",walletId)
        val fragment = VisaMasterCardDialogFragment()
        fragment.arguments = args
        return fragment
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v: View = inflater.inflate(R.layout.visa_master_card_dialog_fragment, container, false)
        dialog!!.window!!.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        return v
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.BottomSheetDialogThemeNoFloating)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        if (dialog is BottomSheetDialog) {
            dialog.behavior.skipCollapsed = true
            dialog.behavior.state = STATE_EXPANDED
        }
        return dialog
    }

    @SuppressLint("ResourceType")
    override fun onViewCreated(root: View, savedInstanceState: Bundle?) {
        super.onViewCreated(root, savedInstanceState)

        val buttonDone = root.findViewById<MaterialButton>(R.id.btnDone)
        root.findViewById<ImageView>(R.id.btnBack).setOnClickListener { dismiss() }
        cardNumber = root.findViewById(R.id.cardNumber)
        cardName = root.findViewById(R.id.holderName)
        cardDate = root.findViewById(R.id.cardDate)
        cardCVC = root.findViewById(R.id.cardCVC)
        amount = root.findViewById(R.id.amount)

        if (arguments != null && requireArguments().containsKey("action")) {
            action = requireArguments().getString("action")
            if (action == "top_up") {
                val paymentService = requireArguments().getSerializable("paymentService") as PaymentService
                pspId = paymentService.id
                walletId = requireArguments().getInt("walletId")
                root.findViewById<LinearLayout>(R.id.viewTopUp).visibility = View.VISIBLE
            }
        }

        cardDate.addTextChangedListener(PatternedTextWatcher("##/##"))
        cardNumber.addTextChangedListener(PatternedTextWatcher("####-####-####-####"))

        val body = HashMap<String, Any>()
        val bodySourceOfFunds = HashMap<String, Any>()
        val bodyCardDetail = HashMap<String, Any>()
        val bodyExpiry = HashMap<String, Any>()

        buttonDone.setOnClickListener {
            when {
                action == "top_up" && amount.text.toString().isEmpty() -> {
                    amount.requestFocus()
                    amount.error = getString(R.string.please_input_all_requied_fields)
                }
                cardName.text.toString().isEmpty() -> {
                    cardName.requestFocus()
                    cardName.error = getString(R.string.please_input_all_requied_fields)
                }
                cardNumber.text.toString().isEmpty() || cardNumber.text.toString().length < 19 -> {
                    cardNumber.requestFocus()
                    cardNumber.error = getString(R.string.please_input_all_requied_fields)
                }
                !cardNumber.text.toString().contains("-",ignoreCase = true) ->{
                    cardNumber.requestFocus()
                    cardNumber.error = getString(R.string.please_input_all_requied_fields)
                }
                cardDate.text.toString().isEmpty() || cardDate.text.toString().length < 5 -> {
                    cardDate.requestFocus()
                    cardDate.error = getString(R.string.please_input_all_requied_fields)
                }
                !cardDate.text.toString().contains("/", ignoreCase = true) ->{
                    cardDate.requestFocus()
                    cardDate.error = getString(R.string.please_input_all_requied_fields)
                }
                cardCVC.text.toString().isEmpty() || cardCVC.text.toString().length < 3 -> {
                    cardCVC.requestFocus()
                    cardCVC.error = getString(R.string.please_input_all_requied_fields)
                }
                else -> {
                    val expiryArr: Array<String> = cardDate.text.toString().split("/".toRegex()).toTypedArray()

                    bodyExpiry["month"] = expiryArr[0]
                    bodyExpiry["year"] = "20"+expiryArr[1]

                    bodyCardDetail["number"] = cardNumber.text.toString().replace("-".toRegex(), "")
                    bodyCardDetail["securityCode"] = cardCVC.text.toString()
                    bodyCardDetail["expiry"] = bodyExpiry

                    bodySourceOfFunds["type"] = cardType
                    bodySourceOfFunds["provided"] = bodyCardDetail

                    body["service"] = "tokenize_visa_master"
                    body["holder_name"] = Util.encodeByPublicKey(cardName.text.toString())
                    body["sourceOfFunds"] = Util.encodeByPublicKey(Gson().toJson(bodySourceOfFunds))

                    if (action == "top_up") { // top up with visa-master
                        body.remove("service")
                        if (amount.text.toString().isEmpty()){
                            amount.requestFocus()
                            amount.error = getString(R.string.please_enter_amount)
                        } else {
                            body["amount"] = amount.text.toString()
                            body["is_save"] = 0
                            body["psp_id"] = pspId
                            body["wallet_id"] = walletId
                            onClickButton?.onClickDone(body)
                            dismiss()
                        }
                    } else if (action == "add_card"){ // save visa-master
                        onClickButton?.onClickDone(body)
                        dismiss()
                    } else if (action == "order") { // order with visa-master
                        body.remove("service")
                        onClickButton?.onClickDone(body)
                        dismiss()
                    }

                }
            }

        }

        cardNumber.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable) {
                if (s.toString().length == 1 && s.toString() == "4") {
                    cardType = "001" // 4 for visa card
                } else if (s.toString().length == 1 && s.toString() == "5") {
                    cardType = "002" // 5 for master card
                } else if (s.toString().length == 1 && s.toString() == "3") {
                    cardType = "003" // 3 for AMEX
                }
            }
        })
    }

    interface OnClickButton{
        fun onClickDone(hashMap: HashMap<String , Any>)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val parent = parentFragment
        onClickButton = if (parent != null) {
            parent as OnClickButton
        } else {
            context as OnClickButton
        }
    }

}