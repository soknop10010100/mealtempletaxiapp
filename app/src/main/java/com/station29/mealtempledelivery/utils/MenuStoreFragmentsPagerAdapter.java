package com.station29.mealtempledelivery.utils;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.station29.mealtempledelivery.ui.base.BaseFragment;

import java.util.ArrayList;
import java.util.List;

/*
    Use to decorate menu tab bars on Tos Eat Home Screen.
    Set pager action to each tabs.
 */

public class MenuStoreFragmentsPagerAdapter extends FragmentStatePagerAdapter {

    private final List<BaseFragment> fragments = new ArrayList<>();
    private final List<String> titles = new ArrayList<>();

    public MenuStoreFragmentsPagerAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
    }

    public MenuStoreFragmentsPagerAdapter(@NonNull FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getCount() {
        return titles.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles.get(position);
    }

    public void addFragment(BaseFragment fragment, String title){
        fragments.add(fragment);
        titles.add(title);
    }


    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }
}
