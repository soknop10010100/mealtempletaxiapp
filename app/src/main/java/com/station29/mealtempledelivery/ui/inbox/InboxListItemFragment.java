package com.station29.mealtempledelivery.ui.inbox;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.api.request.InboxWs;
import com.station29.mealtempledelivery.data.model.Inbox;
import com.station29.mealtempledelivery.ui.base.BaseFragment;
import com.station29.mealtempledelivery.utils.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * A fragment representing a list of Items.
 */
public class InboxListItemFragment extends BaseFragment {

    public InboxItemAdapter inboxItemViewAdapter;
    private final List<Inbox> listInbox = new ArrayList<>();
    private RecyclerView recyclerView;
    private LinearLayout emptyTv;
    private SwipeRefreshLayout refreshLayout;
    private LinearLayoutManager linearLayoutManager;
    private int currentPage = 1 ,size = 10,state=-1;
    private String topic;
    private ProgressBar progressBar;
    private int currentItem;
    private int total;
    private int scrollDown;
    private final int sate=-1;
    private boolean isScrolling;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_inbox_list_item, container, false);

        recyclerView = view.findViewById(R.id.list_inbox);
        refreshLayout = view.findViewById(R.id.swipe_layouts);
        emptyTv = view.findViewById(R.id.emptyTexts);
        progressBar = view.findViewById(R.id.progressBar);

        linearLayoutManager = new LinearLayoutManager(mActivity);
        recyclerView.setLayoutManager(linearLayoutManager);

        String userId = Util.getString("user_id", mActivity);
        topic = Util.getString("topic",mActivity);
        if(!userId.isEmpty()){
            progressBar.setVisibility(View.VISIBLE);
            loadListInbox(recyclerView);
        }else {
            emptyTv.setVisibility(View.VISIBLE);
        }

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL){
                    isScrolling = true;
                }
            }
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItem = linearLayoutManager.getChildCount();
                total = linearLayoutManager.getItemCount();
                scrollDown = linearLayoutManager.findFirstVisibleItemPosition();
                if(dy > 0) {
                    if (isScrolling && (currentItem + scrollDown == total)) {
                        if(total == size) {
                            isScrolling = false;
                            currentPage++;
                            progressBar.setVisibility(View.GONE);
                            recyclerView.scrollToPosition(listInbox.size() - 1);
                            loadListInbox(recyclerView);
                            size+=10;
                        }else {
                            progressBar.setVisibility(View.GONE);
                        }
                    }
                }

            }
        });
        refreshLayout.setColorSchemeResources(R.color.colorPrimary);

        refreshLayout.setOnRefreshListener(() -> {
            currentPage = 1;
            size = 10;
            inboxItemViewAdapter.clear();
            loadListInbox(recyclerView);
        });

        return view;
    }

    private final InboxItemAdapter.OnClickItemInbox itemInbox = (list, i) -> {
        state = i;
        Intent intent = new Intent(mActivity,InboxDetailActivity.class);
        intent.putExtra("inbox",list);
        startActivity(intent);
    };

    private void loadListInbox(RecyclerView recyclerView){
        new InboxWs().getInbox(mActivity,currentPage,size,topic,"DRIVER",inboxCallBack);
        inboxItemViewAdapter = new InboxItemAdapter(listInbox,itemInbox,sate);
        recyclerView.setAdapter(inboxItemViewAdapter);
    }

    private final InboxWs.InboxCallBack inboxCallBack = new InboxWs.InboxCallBack() {
        @SuppressLint("NotifyDataSetChanged")
        @Override
        public void onLoadingSuccessFully(List<Inbox>generalNotifications) {
            progressBar.setVisibility(View.GONE);
            listInbox.addAll(generalNotifications);
            refreshLayout.setRefreshing(false);
            inboxItemViewAdapter.notifyDataSetChanged();
            if(listInbox.isEmpty()) emptyTv.setVisibility(View.VISIBLE);
            else emptyTv.setVisibility(View.GONE);
        }

        @Override
        public void onLoadingFail(String message) {
            progressBar.setVisibility(View.GONE);
            Util.popupMessage(null,message,mActivity);
        }
    };


}