package com.station29.mealtempledelivery.ui.wallet;


import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.gson.JsonObject;
import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.api.request.ScanMeWs;
import com.station29.mealtempledelivery.data.model.MyQRCode;
import com.station29.mealtempledelivery.utils.Util;

public class ScanMeFragment extends DialogFragment {

    private ImageView myCodeImg , btnChangeCode;
    private ProgressBar progressBar;
    private TextView txtUserName;
    private int numberRotation;

    public static ScanMeFragment newInstance() {
        Bundle args = new Bundle();
        ScanMeFragment fragment = new ScanMeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_scan_me, container, false);

        myCodeImg = view.findViewById(R.id.my_code);
        progressBar = view.findViewById(R.id.progress);
        btnChangeCode = view.findViewById(R.id.btnChangeCode);
        txtUserName = view.findViewById(R.id.txtUserName);

        getCode();

        if (getDialog() != null) {
            getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(this.getResources().getColor(R.color.zs_color_transparent)));
        }

        btnChangeCode.setOnClickListener(view1 -> {
            myCodeImg.setImageResource(R.color.white);
            numberRotation += 50;
            progressBar.setVisibility(View.VISIBLE);
            btnChangeCode.setRotation(numberRotation);
            getCode();
        });

        return view;
    }

    private void getCode(){
        new ScanMeWs().getMyQRCode(getContext(), new ScanMeWs.ScanMeCallback() {
            @Override
            public void onSuccess(MyQRCode qrCode, JsonObject dataObj){
                Bitmap qrCodeImageBitmap = Util.getQRCodeImage(dataObj.toString());
                myCodeImg.setImageBitmap(qrCodeImageBitmap);
                txtUserName.setText(qrCode.getFullName());
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError(String error) {
                Toast.makeText(getContext(), error, Toast.LENGTH_LONG).show();
            }
        });
    }
}

