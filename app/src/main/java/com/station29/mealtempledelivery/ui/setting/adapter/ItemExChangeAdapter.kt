package com.station29.mealtempledelivery.ui.setting.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.station29.mealtempledelivery.R
import com.station29.mealtempledelivery.data.model.Redeem
import com.station29.mealtempledelivery.utils.Util

import java.util.*
import kotlin.collections.ArrayList


class ItemExChangeAdapter(private val listRedeem: ArrayList<Redeem>, private var type:String, private var context:Context, private var itemClickChangeListener: OnItemClickChangeListener): RecyclerView.Adapter<ItemExChangeAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.layout_adpater_list_redeem, parent, false)
        return ViewHolder(v)
    }

    @SuppressLint("SetTextI18n", "CheckResult")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val redeem = listRedeem[position]

        if (redeem.point == "0" || redeem.point == "1"){
            holder.nameTv.text = redeem.point + " " + holder.btnExchange.context.getString(R.string.credit) + " " +context.getString(R.string.will_get)+ " " + Util.stringFormatPrice(redeem.redeem_amount) + " " +redeem.currency_sign
        } else {
            holder.nameTv.text = redeem.point + " " + holder.btnExchange.context.getString(R.string.credits) + " " +context.getString(R.string.will_get)+ " " + Util.stringFormatPrice(redeem.redeem_amount) + " " +redeem.currency_sign
        }
        holder.btnExchange.setOnClickListener {
            itemClickChangeListener.onItemClick(redeem,type)
        }

        if (position == listRedeem.size - 1) {
            holder.viewLine.visibility = View.GONE
        } else {
            holder.viewLine.visibility = View.VISIBLE
        }

    }

    override fun getItemCount(): Int {
        return listRedeem.size
    }

    interface OnItemClickChangeListener {
        fun onItemClick(redeem: Redeem,type: String)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var nameTv : TextView = view.findViewById(R.id.body_text)
        var btnExchange: TextView = view.findViewById(R.id.title_text)
        var viewLine : View = view.findViewById(R.id.viewLine)
    }

    fun clear() {
        val size: Int = listRedeem.size
        if (size > 0) {
            for (i in 0 until size) {
                listRedeem.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }

}