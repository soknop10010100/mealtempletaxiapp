package com.station29.mealtempledelivery.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by sambath on 5/16/17.
 */

public class DirectionResults {
    @SerializedName("geocoded_waypoints")
    private List<GeoCodedWayPoint> geocoded_waypoints;
    @SerializedName("routes")
    private List<Route> routes;
    @SerializedName("status")
    private String status;
    @SerializedName("error_message")
    private String error_message;

    public String getError_message() {
        return error_message;
    }

    public void setError_message(String error_message) {
        this.error_message = error_message;
    }

    public List<GeoCodedWayPoint> getGeocoded_waypoints() {
        return geocoded_waypoints;
    }

    public void setGeocoded_waypoints(List<GeoCodedWayPoint> geocoded_waypoints) {
        this.geocoded_waypoints = geocoded_waypoints;
    }

    public List<Route> getRoutes() {
        return routes;
    }

    public void setRoutes(List<Route> routes) {
        this.routes = routes;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public class Route {

        @SerializedName("bounds")
        private Bound bounds;
        @SerializedName("copyrights")
        private String copyrights;
        @SerializedName("legs")
        private List<Legs> legs;
        @SerializedName("overview_polyline")
        private OverviewPolyLine overviewPolyLine;
        @SerializedName("summary")
        private String summary;

        public Bound getBounds() {
            return bounds;
        }

        public void setBounds(Bound bounds) {
            this.bounds = bounds;
        }

        public String getCopyrights() {
            return copyrights;
        }

        public void setCopyrights(String copyrights) {
            this.copyrights = copyrights;
        }

        public List<Legs> getLegs() {
            return legs;
        }

        public void setLegs(List<Legs> legs) {
            this.legs = legs;
        }

        public OverviewPolyLine getOverviewPolyLine() {
            return overviewPolyLine;
        }

        public void setOverviewPolyLine(OverviewPolyLine overviewPolyLine) {
            this.overviewPolyLine = overviewPolyLine;
        }

        public String getSummary() {
            return summary;
        }

        public void setSummary(String summary) {
            this.summary = summary;
        }
    }

    public class Legs {

        @SerializedName("distance")
        private TextValueResult distance;
        @SerializedName("duration")
        private TextValueResult duration;
        @SerializedName("end_address")
        private String end_address;
        @SerializedName("end_location")
        private Location end_location;
        @SerializedName("start_address")
        private String start_address;
        @SerializedName("start_location")
        private Location start_location;
        @SerializedName("steps")
        private List<Steps> steps;

        public TextValueResult getDistance() {
            return distance;
        }

        public void setDistance(TextValueResult distance) {
            this.distance = distance;
        }

        public TextValueResult getDuration() {
            return duration;
        }

        public void setDuration(TextValueResult duration) {
            this.duration = duration;
        }

        public String getEnd_address() {
            return end_address;
        }

        public void setEnd_address(String end_address) {
            this.end_address = end_address;
        }

        public Location getEnd_location() {
            return end_location;
        }

        public void setEnd_location(Location end_location) {
            this.end_location = end_location;
        }

        public String getStart_address() {
            return start_address;
        }

        public void setStart_address(String start_address) {
            this.start_address = start_address;
        }

        public Location getStart_location() {
            return start_location;
        }

        public void setStart_location(Location start_location) {
            this.start_location = start_location;
        }

        public List<Steps> getSteps() {
            return steps;
        }

        public void setSteps(List<Steps> steps) {
            this.steps = steps;
        }
    }

    public class Steps {
        @SerializedName("distance")
        private TextValueResult distance;
        @SerializedName("duration")
        private TextValueResult duration;
        @SerializedName("end_location")
        private Location end_location;
        @SerializedName("html_instructions")
        private String html_instructions;
        @SerializedName("maneuver")
        private String maneuver;
        @SerializedName("polyline")
        private OverviewPolyLine polyline;
        @SerializedName("start_location")
        private Location start_location;
        @SerializedName("travel_mode")
        private String travel_mode;

        public TextValueResult getDistance() {
            return distance;
        }

        public void setDistance(TextValueResult distance) {
            this.distance = distance;
        }

        public TextValueResult getDuration() {
            return duration;
        }

        public void setDuration(TextValueResult duration) {
            this.duration = duration;
        }

        public Location getEnd_location() {
            return end_location;
        }

        public void setEnd_location(Location end_location) {
            this.end_location = end_location;
        }

        public String getHtml_instructions() {
            return html_instructions;
        }

        public void setHtml_instructions(String html_instructions) {
            this.html_instructions = html_instructions;
        }

        public String getManeuver() {
            return maneuver;
        }

        public void setManeuver(String maneuver) {
            this.maneuver = maneuver;
        }

        public OverviewPolyLine getPolyline() {
            return polyline;
        }

        public void setPolyline(OverviewPolyLine polyline) {
            this.polyline = polyline;
        }

        public Location getStart_location() {
            return start_location;
        }

        public void setStart_location(Location start_location) {
            this.start_location = start_location;
        }

        public String getTravel_mode() {
            return travel_mode;
        }

        public void setTravel_mode(String travel_mode) {
            this.travel_mode = travel_mode;
        }
    }

    public class OverviewPolyLine {

        @SerializedName("points")
        public String points;

        public String getPoints() {
            return points;
        }

        public void setPoints(String points) {
            this.points = points;
        }
    }

    public class Location {
        @SerializedName("lat")
        private double lat;
        @SerializedName("lng")
        private double lng;

        public double getLat() {
            return lat;
        }

        public void setLat(double lat) {
            this.lat = lat;
        }

        public double getLng() {
            return lng;
        }

        public void setLng(double lng) {
            this.lng = lng;
        }
    }

    public class GeoCodedWayPoint{
        @SerializedName("geocoder_status")
        private String geocoder_status;
        @SerializedName("place_id")
        private String place_id;
        @SerializedName("types")
        private List<String> types;

        public String getGeocoder_status() {
            return geocoder_status;
        }

        public void setGeocoder_status(String geocoder_status) {
            this.geocoder_status = geocoder_status;
        }

        public String getPlace_id() {
            return place_id;
        }

        public void setPlace_id(String place_id) {
            this.place_id = place_id;
        }

        public List<String> getTypes() {
            return types;
        }

        public void setTypes(List<String> types) {
            this.types = types;
        }
    }

    public class Bound{
        @SerializedName("northeast")
        private Location northeast;
        @SerializedName("southwest")
        private Location southwest;

        public Location getNortheast() {
            return northeast;
        }

        public void setNortheast(Location northeast) {
            this.northeast = northeast;
        }

        public Location getSouthwest() {
            return southwest;
        }

        public void setSouthwest(Location southwest) {
            this.southwest = southwest;
        }
    }

    public class TextValueResult{
        @SerializedName("text")
        String text;
        @SerializedName("value")
        Long value;

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public Long getValue() {
            return value;
        }

        public void setValue(Long value) {
            this.value = value;
        }
    }

}


