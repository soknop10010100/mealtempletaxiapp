package com.station29.mealtempledelivery.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MapDirectionRequestModel {
    @SerializedName("distance")
    @Expose
    private long distance;
    @SerializedName("arr_json_lat_lng")
    @Expose
    private List<LatLngCustom> arrJsonLatLng = null;

    public long getDistance() {
        return distance;
    }

    public void setDistance(long distance) {
        this.distance = distance;
    }

    public List<LatLngCustom> getArrJsonLatLng() {
        return arrJsonLatLng;
    }

    public void setArrJsonLatLng(List<LatLngCustom> arrJsonLatLng) {
        this.arrJsonLatLng = arrJsonLatLng;
    }
}
