package com.station29.mealtempledelivery.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PaymentHistory implements Serializable {

    @SerializedName("pivot")
    @Expose
    private Pivot pivot;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("payment_id")
    @Expose
    private Integer paymentId;
    @SerializedName("psp_name")
    @Expose
    private String pspName;
    @SerializedName("logo")
    @Expose
    private String logo;
    @SerializedName("country_code")
    @Expose
    private String countryCode;
    @SerializedName("payment_token")
    @Expose
    private String paymentToken;
    @SerializedName("psp_type")
    @Expose
    private String pspType;
    @SerializedName("description")
    @Expose
    private String description;

    public Integer getId() {
        return id;
    }

    public Integer getPaymentId() {
        return paymentId;
    }

    public Pivot getPivot() {
        return pivot;
    }

    public String getPspName() {
        return pspName;
    }

    public String getLogo() {
        return logo;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public String getPaymentToken() {
        return paymentToken;
    }

    public String getPspType() {
        return pspType;
    }

    public String getDescription() {
        return description;
    }

    public static class Pivot implements Serializable {
        @SerializedName("user_id")
        @Expose
        private Integer userId;
        @SerializedName("psp_id")
        @Expose
        private Integer pspId;

        public Integer getUserId() {
            return userId;
        }

        public Integer getPspId() {
            return pspId;
        }
    }
}
