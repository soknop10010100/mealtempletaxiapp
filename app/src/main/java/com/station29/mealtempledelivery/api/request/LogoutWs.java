package com.station29.mealtempledelivery.api.request;

import android.content.Context;
import android.util.Log;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.station29.mealtempledelivery.api.Constant;
import com.station29.mealtempledelivery.api.CustomCallback;
import com.station29.mealtempledelivery.api.CustomResponseListener;
import com.station29.mealtempledelivery.api.RetrofitGenerator;
import com.station29.mealtempledelivery.api.ServiceAPI;

import java.util.HashMap;

import retrofit2.Call;

import static com.station29.mealtempledelivery.utils.Util.logDebug;

public class LogoutWs {

    public void logOut(Context context, HashMap<String, String> deviceType, String deviceToken ,String topic, final LogoutCallback logoutCallback) {
        ServiceAPI serviceAPI = RetrofitGenerator.createServiceWithAuth(context);
        final Call<JsonElement> res = serviceAPI.logout(deviceType,deviceToken,topic);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                logDebug("onSuccessLogin", resObj +"");
                if (resObj.has("data") && resObj.getAsJsonObject("data").has("message")) {
                    String message = resObj.getAsJsonObject("data").get("message").getAsString();
                    if(Constant.serviceLanguage.containsKey(message))
                    logoutCallback.onLoadSuccess(Constant.serviceLanguage.get(message));
                    else logoutCallback.onLoadSuccess(message);
                }
            }

            @Override
            public void onError(String error , int code) {
                logoutCallback.onLoadFailed(error);
            }
        }));
    }

    public interface LogoutCallback {
        void onLoadSuccess(String message);

        void onLoadFailed(String errorMessage);
    }
}
