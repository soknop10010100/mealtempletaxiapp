package com.station29.mealtempledelivery.ui.setting;

import androidx.annotation.RequiresApi;
import androidx.core.content.res.ResourcesCompat;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.api.Constant;
import com.station29.mealtempledelivery.api.request.LogoutWs;
import com.station29.mealtempledelivery.api.request.MockupData;
import com.station29.mealtempledelivery.api.request.ProfileWs;
import com.station29.mealtempledelivery.ui.base.AlertPasswordActivity;
import com.station29.mealtempledelivery.ui.base.BaseActivity;
import com.station29.mealtempledelivery.ui.base.MusicControl;
import com.station29.mealtempledelivery.ui.login.LoginActivity;
import com.station29.mealtempledelivery.utils.Util;

import java.util.Calendar;
import java.util.HashMap;

public class DeleteAccountActivity extends BaseActivity {

    private Button btnDelete;
    private EditText editText_other;
    private int saveViewLinearLayoutCheck = 0;
    private int saveViewRadioCheck = 0;
    private String reason;
    private LinearLayout removeLinearLayout;
    private RadioButton removeRadio;
    private View progressBar;
    private ImageView imgBack;
    private TextView toolBarTitle;


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_account);

        initView();
        initAction();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void initView() {

        toolBarTitle = findViewById(R.id.toolBarTitle);
        imgBack = findViewById(R.id.backButton);
        progressBar = findViewById(R.id.progressBar);
        btnDelete = findViewById(R.id.btn_delete);
        editText_other = findViewById(R.id.other_reason);

        LinearLayout listString = findViewById(R.id.listString);

        for (int i = 0; i < MockupData.getStringReason(this).size(); i++) {
            LinearLayout linearLayout = new LinearLayout(this);
            TextView textView = new TextView(this);
            RadioButton radioButton = new RadioButton(this);
            View view = new View(this);

            // set id to view , set random id for view to check tick
            linearLayout.setId(9876 + i);
            radioButton.setId(5678 + i);
            textView.setId(6789 + i);

            //params for textview
            LinearLayout.LayoutParams paramsTextView = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1f);
            //params for radio button
            LinearLayout.LayoutParams paramsRadioButton = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            //params for view
            LinearLayout.LayoutParams paramsView = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            //create textview
            Typeface typeface = ResourcesCompat.getFont(DeleteAccountActivity.this, R.font.lexend_deca_regular_title);
            textView.setTypeface(typeface);
            textView.setLayoutParams(paramsTextView);
            textView.setTextSize(14);
            textView.setText(MockupData.getStringReason(this).get(i));
            textView.setSingleLine(false);
            textView.setPadding(Util.dpToPx(this, 15), Util.dpToPx(this, 10), Util.dpToPx(this, 0), Util.dpToPx(this, 10));

            //create radio button
            paramsRadioButton.setMargins(0, 0, Util.dpToPx(this, 15), 0);
            radioButton.setLayoutParams(paramsRadioButton);
            radioButton.setGravity(Gravity.CENTER_VERTICAL);

            //create view
            paramsView.setMargins(Util.dpToPx(this, 10), 0, Util.dpToPx(this, 10), 0);
            view.setLayoutParams(paramsView);
            view.setBackgroundColor(getResources().getColor(R.color.white));
            view.setMinimumHeight(Util.dpToPx(this, 2));

            //add textview and radio button to linear
            linearLayout.setOrientation(LinearLayout.HORIZONTAL);
            linearLayout.setGravity(Gravity.CENTER_VERTICAL);
            linearLayout.addView(textView);
            linearLayout.addView(radioButton);

            listString.addView(linearLayout);
            listString.addView(view);

            linearLayout.setOnClickListener(view1 -> {
                btnDelete.setEnabled(true);
                if (editText_other.getText().toString().equals("")) {
                    if (saveViewRadioCheck != 0) {
                        removeLinearLayout = findViewById(saveViewLinearLayoutCheck);
                        removeRadio = removeLinearLayout.findViewById(saveViewRadioCheck);
                        removeRadio.setChecked(false);

                    }
                    saveViewLinearLayoutCheck = linearLayout.getId();
                    saveViewRadioCheck = radioButton.getId();
                    reason = textView.getText().toString();
                    radioButton.setChecked(true);
                }
            });

            radioButton.setOnClickListener(view1 -> {
                btnDelete.setEnabled(true);
                if (editText_other.getText().toString().equals("")) {
                    if (saveViewRadioCheck != 0) {
                        removeLinearLayout = findViewById(saveViewLinearLayoutCheck);
                        removeRadio = removeLinearLayout.findViewById(saveViewRadioCheck);
                        removeRadio.setChecked(false);
                    }
                    saveViewLinearLayoutCheck = linearLayout.getId();
                    saveViewRadioCheck = radioButton.getId();
                    radioButton.setChecked(true);
                    reason = textView.getText().toString();
                }
            });

            editText_other.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    String str = editable.toString();
                    if (saveViewRadioCheck != 0) {
                        removeLinearLayout = findViewById(saveViewLinearLayoutCheck);
                        removeRadio = removeLinearLayout.findViewById(saveViewRadioCheck);
                        removeRadio.setChecked(false);

                    }
                    if (!str.equals("")) {
                        reason = str;
                        btnDelete.setEnabled(true);
                        radioButton.setEnabled(false);
                    } else {
                        btnDelete.setEnabled(false);
                        radioButton.setEnabled(true);
                    }
                }
            });
        }
    }

    public void initAction(){

        Util.hideSoftKeyboard(editText_other);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        toolBarTitle.setText(getString(R.string.delete_account));

        imgBack.setOnClickListener(view -> {
            finish();
        });

        btnDelete.setOnClickListener(view -> {
            if (!reason.equals("")) {
                Intent intent = new Intent(DeleteAccountActivity.this, AlertPasswordActivity.class);
                intent.putExtra("action","delete_account");
                activityLauncher.launch(intent, result -> {
                    if (result.getResultCode() == RESULT_OK) {
                        if (result.getData() != null && result.getData().hasExtra("pin")) {
                            Util.hideSoftKeyboard(view);
                            progressBar.setVisibility(View.VISIBLE);
                            String pin = result.getData().getStringExtra("pin");
                            HashMap<String, Object> objectHashMap = new HashMap<>();
                            objectHashMap.put("is_deactivated", 1);
                            objectHashMap.put("reason", reason);
                            objectHashMap.put("password",Util.encodeByPublicKey(pin));
                            objectHashMap.put("action_type","deactivate");
                            new ProfileWs().deleteAccountUser(DeleteAccountActivity.this, objectHashMap, new ProfileWs.ReadOnOffLine() {
                                @Override
                                public void onLoadSuccess(String message) {
                                    Toast.makeText(DeleteAccountActivity.this,Constant.serviceLanguage.get(message),Toast.LENGTH_SHORT).show();
                                    logout();
                                }
                                @Override
                                public void onLoadFail(String message) {
                                    progressBar.setVisibility(View.GONE);
                                    Util.popupMessage(getString(R.string.error), message, DeleteAccountActivity.this);
                                }
                            });
                        }
                    }
                });
            }

        });
    }

    private void logout() {
        HashMap<String, String> deviceTypes = new HashMap<>();
        deviceTypes.put("device_type", "ANDROID");
        String topic = Util.getString("topic", DeleteAccountActivity.this);
        String deviceToken = Util.getString("firebase_token", DeleteAccountActivity.this);
        new LogoutWs().logOut(DeleteAccountActivity.this, deviceTypes, deviceToken, topic, new LogoutWs.LogoutCallback() {
            @Override
            public void onLoadSuccess(String message) {
                progressBar.setVisibility(View.GONE);
                MusicControl.countNotification -= 1;
                MusicControl.getInstance(DeleteAccountActivity.this).stopMusic();
                NotificationManager manager = (NotificationManager) DeleteAccountActivity.this.getSystemService(Context.NOTIFICATION_SERVICE);
                if (manager != null) {
                    manager.cancelAll();
                }

                FirebaseAuth.getInstance().signOut();

                Bundle bundle = new Bundle();
                bundle.putString(Constant.USER_ID, Util.getString("user_id", DeleteAccountActivity.this) + "was Logout at " + Calendar.getInstance());
                mFirebaseAnalytics.logEvent(this.getClass().getSimpleName(), bundle);

                Intent intent = new Intent(DeleteAccountActivity.this, LoginActivity.class);
                Util.saveString("token", "", DeleteAccountActivity.this);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }

            @Override
            public void onLoadFailed(String errorMessage) {
                progressBar.setVisibility(View.GONE);
                Util.popupMessage(null, errorMessage, DeleteAccountActivity.this);
            }
        });
    }
}