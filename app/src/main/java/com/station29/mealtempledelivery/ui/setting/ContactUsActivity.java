package com.station29.mealtempledelivery.ui.setting;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;

import com.google.android.material.button.MaterialButton;
import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.data.model.User;
import com.station29.mealtempledelivery.ui.base.BaseActivity;

import zendesk.core.AnonymousIdentity;
import zendesk.core.Identity;
import zendesk.core.Zendesk;
import zendesk.support.Guide;
import zendesk.support.Support;
import zendesk.support.guide.HelpCenterActivity;
import zendesk.support.requestlist.RequestListActivity;

public class ContactUsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(R.string.zg_general_contact_us_button_label_accessibility);
        }

        MaterialButton btnHelp = findViewById(R.id.btnHelp);
        MaterialButton btnMyTicket = findViewById(R.id.btnMyTicket);

        Zendesk.INSTANCE.init(ContactUsActivity.this, "https://kiwigosupport.zendesk.com/",
                "aa31c00c257adecf7e592345db384edafe2f7ad7b073b407",
                "mobile_sdk_client_d38fe24b665541ca2d0f");

        Identity identity;
        if (getIntent().hasExtra("user") && getIntent().getSerializableExtra("user") != null){
            User user = (User) getIntent().getSerializableExtra("user");
            identity = new AnonymousIdentity.Builder()
                    .withNameIdentifier(user.getFirstName()+ " "+user.getLastName())
                    .build();
        } else {
            identity = new AnonymousIdentity();
        }

        Zendesk.INSTANCE.setIdentity(identity);
        Support.INSTANCE.init(Zendesk.INSTANCE);
        Guide.INSTANCE.init(Zendesk.INSTANCE);

        btnHelp.setOnClickListener(v -> HelpCenterActivity.builder().show(ContactUsActivity.this));

        btnMyTicket.setOnClickListener(v -> RequestListActivity.builder().show(ContactUsActivity.this));
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
