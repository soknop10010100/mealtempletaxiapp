package com.station29.mealtempledelivery.api.request;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.station29.mealtempledelivery.api.Constant;
import com.station29.mealtempledelivery.api.CustomCallback;
import com.station29.mealtempledelivery.api.CustomResponseListener;
import com.station29.mealtempledelivery.api.RetrofitGenerator;
import com.station29.mealtempledelivery.api.ServiceAPI;
import com.station29.mealtempledelivery.data.model.Config;
import com.station29.mealtempledelivery.data.model.ServiceTypeDriver;
import com.station29.mealtempledelivery.utils.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import retrofit2.Call;

public class ConfigWs {

    public void readConfig(final Context context, final String code, String appType , final String configType , final ReadConfigCallback readConfigCallback) {
        ServiceAPI service = RetrofitGenerator.createService();
        Call<JsonElement> res;
        if(configType.equals("getConfig")){
            res = service.readConfig(code);
        } else res = service.readConfigLanguage(code, appType);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                List<ServiceTypeDriver> typeDriverList = new ArrayList<>();
                if (resObj.has("data") && !resObj.get("data").isJsonNull() && configType.equals("getConfig")) {
                    JsonObject dataObj = resObj.getAsJsonObject("data");
                    Config config = new Gson().fromJson(dataObj, Config.class);
                    if(dataObj.has("service_type_driver")){
                        JsonArray objects = dataObj.getAsJsonObject().getAsJsonArray("service_type_driver");
                        for (int i=0 ; i < objects.size() ; i++){
                            typeDriverList.add(new Gson().fromJson(objects.get(i), ServiceTypeDriver.class));
                        }
                        readConfigCallback.onSuccess(config,typeDriverList);
                    }
                } else {
                    JsonObject dataObj = resObj.getAsJsonObject("data");
                    for (Map.Entry<String,JsonElement> entry : dataObj.entrySet()) {
                        if (entry.getKey() == null || entry.getKey().equals("") || entry.getValue().isJsonNull() ) continue;
                        Constant.serviceLanguage.put(entry.getKey(),entry.getValue().getAsString());
                    }
                    Util.saveStringBody(context,Constant.serviceLanguage,"list_language");
                    readConfigCallback.onSuccess(new Config(),typeDriverList);
                }
            }

            @Override
            public void onError(String error,int code) {
                readConfigCallback.onError(error);
            }
        }));

    }

    public interface ReadConfigCallback {
        void onSuccess(Config config,List<ServiceTypeDriver> serviceTypeDriver);
        void onError(String message);
    }
}
