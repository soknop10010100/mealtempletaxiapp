package com.station29.mealtempledelivery.ui.base

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.station29.mealtempledelivery.R

class AlertPasswordActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_update_profile)

        var action = ""

        if (intent != null && intent.hasExtra("action")) {
            action = intent.getStringExtra("action").toString()

            if (action == "delete_account") {
                title = getString(R.string.delete_account)
                findViewById<TextView>(R.id.txtMakeSure).text = getString(R.string.are_you_sure_to_delete_your_account)
            } else if (action == "account_deactive") {
                title = getString(R.string.account_deactive)
                findViewById<TextView>(R.id.txtMakeSure).text = getString(R.string.do_you_want_to_activate_your_account)
            }
        }

        findViewById<EditText>(R.id.etPassword).addTextChangedListener(object  : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun afterTextChanged(p0: Editable?) {
                findViewById<Button>(R.id.btnYes).isEnabled = p0.toString() != ""
            }
        })

        findViewById<Button>(R.id.btnYes).setOnClickListener {
            val intent = Intent()
            intent.putExtra("pin",findViewById<EditText>(R.id.etPassword).text.toString())

            setResult(RESULT_OK, intent)
            finish()
        }

        findViewById<Button>(R.id.btnNo).setOnClickListener {
            if (action == "account_deactive") {
                setResult(RESULT_OK)
            }
            finish()
        }

    }
}