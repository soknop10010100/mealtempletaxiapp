package com.station29.mealtempledelivery.ui.setting.payment;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import com.station29.mealtempledelivery.R;
import com.station29.mealtempledelivery.api.Constant;
import com.station29.mealtempledelivery.api.request.MockupData;
import com.station29.mealtempledelivery.data.model.PaymentSuccess;
import com.station29.mealtempledelivery.ui.base.BaseActivity;
import com.station29.mealtempledelivery.utils.Util;

public class PaymentSuccessActivity extends BaseActivity {

    private PaymentSuccess paymentSuccess;
    private String action;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_success);

        if (getIntent() != null && getIntent().hasExtra("paymentSuccess")){
            paymentSuccess = (PaymentSuccess) getIntent().getSerializableExtra("paymentSuccess");
        }

        if (getIntent() != null && getIntent().hasExtra("action")) {
            action = getIntent().getStringExtra("action");
        }

        MaterialButton backButton = findViewById(R.id.backbtn);
        TextView txtAmount = findViewById(R.id.txtAmount);
        TextView txtRefId = findViewById(R.id.txtRefId);
        TextView txtPaymentType = findViewById(R.id.txtPaymentType);
        TextView txtTransactionDate = findViewById(R.id.txtTransactionDate);
        TextView txtToAccount = findViewById(R.id.txtToAccount);
        TextView txtFee = findViewById(R.id.txtFee);
        TextView txtFromAccount = findViewById(R.id.txtFromAccount);
        LinearLayout viewFromAccount = findViewById(R.id.viewFromAccount);

        if (action.equals("top_up")) {
            viewFromAccount.setVisibility(View.GONE);
        }

        if (action.equals("top_up") ) {
            txtToAccount.setText(MockupData.getUser().getFirstName() + " " + MockupData.getUser().getLastName());
            txtAmount.setTextColor(Color.parseColor("#00c853"));
            txtAmount.setText( "+ "+ Constant.getConfig().getCurrencySign() + " " + Util.formatPrice(paymentSuccess.getAmount()));
        } else {
            txtToAccount.setText(paymentSuccess.getToAccount());
            txtFromAccount.setText(paymentSuccess.getFromAccount());
            txtAmount.setTextColor(Color.parseColor("#af1022"));
            txtAmount.setText( "- " + Constant.getConfig().getCurrencySign() + " " + Util.formatPrice(paymentSuccess.getAmount()));
        }

        if (paymentSuccess.getFee() != null) {
            txtFee.setText(Constant.getConfig().getCurrencySign() + " " + paymentSuccess.getFee());
        }
        if (paymentSuccess.getTxnId() != null){
            txtRefId.setText(paymentSuccess.getTxnId());
        }
        if (paymentSuccess.getType() != null) {
            txtPaymentType.setText(paymentSuccess.getType().replaceAll("_"," "));
        }
        if (paymentSuccess.getTransactionDate() != null){
            txtTransactionDate.setText(Util.formatDateUptoCurrentRegion(paymentSuccess.getTransactionDate()));
        }

        backButton.setOnClickListener( v-> {
            setResult(RESULT_OK);
            finish();
        });
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_OK);
        finish();
    }
}
